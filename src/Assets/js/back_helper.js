export function pluck(obj_or_array, key) {
    if (!obj_or_array)
        return null
    else if (obj_or_array.length == 0)
        return [];
    else {
        if (Number.isInteger(obj_or_array.length)) // array
            return obj_or_array && obj_or_array.length > 0 ? obj_or_array.map(o => o[key]) : [];
        else// object
            return obj_or_array && obj_or_array.hasOwnProperty(key) ? obj_or_array[key] : null;
    }
}


export function addTranslationsCopyFromEnglish(translations)
{
    return translations.map(trans => {
        trans.copy_from_english = false;
        return trans;
    });
}


export function checkTranslationsEnglishClones(translations, fields = [])
{
    return translations.map(trans => {
        if (trans.copy_from_english) {
            let en_trans = translations.find(trans => trans.locale === 'en');

            fields.forEach(field => {
                trans[field] = en_trans[field];
            })
        }
        return trans;
    });
}