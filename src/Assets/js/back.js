import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import Dropzone from 'vue2-dropzone';
import 'vue2-dropzone/dist/vue2Dropzone.min.css';
import Quill from 'quill';
import VueQuillEditor from 'vue-quill-editor';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import draggable from 'vuedraggable';
import Switches from 'vue-switches';
import swal from 'sweetalert';
import Multiselect from 'vue-multiselect';
import VuePaginate from 'vue-paginate'
import ImageRepo from './components/Images'
import ImageRepoSeo from './components/ImagesWithSeo'
import DocsRepo from './components/Documents'
import Seo from './components/Seo'
import SeoIndexable from './components/SeoIndexable'
import SeoImage from './components/SeoImage'
import MenuTable from './components/MenuTable'
import {pluck, addTranslationsCopyFromEnglish, checkTranslationsEnglishClones} from './back_helper.js'

// loads the Icon plugin
UIkit.use(Icons);

window.UIkit = UIkit;

/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */


window._ = require('lodash');
window.slugify = function(text, options) {
    var slugify = require('slugify')
    var res = slugify(text, options)
    res = res.replace('·', '')
    res = res.replace(/["']/g, "")
    return res
};


/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.axios.interceptors.request.use(function (config) {
    $('.loader').fadeTo(250, 0.6);
    return config;
}, function (error) {
    $('.loader').fadeTo(250, 0.6);
    return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    $('.loader').fadeOut(250);
    return response;
}, function (error) {
    $('.loader').fadeOut(250);
    return Promise.reject(error);
});

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });

require('velocity-animate');

require('./menu.js');

window.Vue = require('vue');

// Quill (vue-quill-editor v3 required)
// If you add custom fonts in the Quill, be sure to check the _quill.scss file!
var Font = Quill.import('formats/font');
Font.whitelist = []; // Array of custom fonts
Quill.register(Font, true);
var toolbarOptions = [
	['bold', 'italic', 'underline', 'strike'],
	['blockquote', 'code-block'],

	[{ 'header': 1 }, { 'header': 2 }],
	[{ 'list': 'ordered'}, { 'list': 'bullet' }],
	[{ 'script': 'sub'}, { 'script': 'super' }],
	[{ 'indent': '-1'}, { 'indent': '+1' }],
	[{ 'direction': 'rtl' }],

	[{ 'size': ['small', 'normal', 'large', 'huge'] }],
	[{ 'header': [1, 2, 3, 4, 5, 6, false] }],

	[{ 'color': [] }, { 'background': [] }], // Custom colors
	// [{ 'font': ['Comfortaa-Regular', 'Comfortaa-Bold'] }], // Array of custom fonts
	[{ 'align': [] }],

	['clean']
];
Vue.use(VueQuillEditor, {
	modules: {
		toolbar: toolbarOptions
	},
	theme: 'snow'
});

Vue.component('dropzone', Dropzone);
Vue.component('seo', Seo);
Vue.component('seo-indexable', SeoIndexable);
Vue.component('seo-image', SeoImage);
Vue.component('draggable', draggable);
Vue.component('menu-table', MenuTable);
Vue.component('switches', Switches);
Vue.component('multiselect', Multiselect);
Vue.use(VuePaginate);
Vue.component('image-repo', ImageRepo);
Vue.component('image-repo-seo', ImageRepoSeo);
Vue.component('docs-repo', DocsRepo);
Vue.mixin({
    methods: {
        pluck: pluck,
        addTranslationsCopyFromEnglish: addTranslationsCopyFromEnglish,
        checkTranslationsEnglishClones: checkTranslationsEnglishClones,
    }
})

window.moment = require('moment');