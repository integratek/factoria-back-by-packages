$(function () {
	function menuAction() {
		let sidebar = $('.sidebar');
		let padder = $('.sidebar-padder');
		let fadder = $('.fadder');
		let timer =  300;
		if (sidebar.css('visibility') == 'hidden') {
			sidebar.css({visibility: 'visible'});
			sidebar.velocity({translateX: -300}, 1);
			sidebar.velocity({translateX: 0}, timer);
			if ($(document).width() < 960) {
				// On small device
				fadder.css({display: 'block'});
				fadder.velocity({opacity: 0.5}, timer);
			} else {
				// On big device
				padder.velocity({paddingLeft: 300}, timer);
			}
		} else {
			sidebar.css({x: 0});
			sidebar.velocity({translateX: -300}, timer, function () {
				sidebar.css({visibility: 'hidden'});
			});
			if ($(document).width() < 960) {
				// On small device
				fadder.velocity({opacity: 0}, timer, function () {
					fadder.css({display: 'none'});
				});
			} else {
				// On big device
				padder.velocity({paddingLeft: 0}, timer);
			}
		}
	}
	$('.fadder').click(menuAction);
	$('#menu-toggle').click(menuAction);
	$(window).resize(function () {
		let sidebar = $('.sidebar');
		let padder = $('.sidebar-padder');
		let fadder = $('.fadder');
		if ($(document).width() > 960) {
			sidebar.css({visibility: 'visible'});
			sidebar.velocity({translateX: 0}, 1);
		} else {
			sidebar.velocity({translateX: -300}, 1);
			sidebar.css({visibility: 'hidden'});
		}
	});
});
