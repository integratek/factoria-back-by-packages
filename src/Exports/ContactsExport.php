<?php

namespace Lafactoria\Backend\Exports;

use Maatwebsite\Excel\Concerns\FromView;

class ContactsExport implements FromView
{

    public function __construct($contacts)
    {
        $this->contacts = $contacts;
    }


    public function view(): \Illuminate\Contracts\View\View
    {
        return view('backend::admin.exports.contacts', [
            'contacts' => $this->contacts
        ]);
    }
}