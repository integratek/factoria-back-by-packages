<?php

namespace Lafactoria\Backend\Controllers\Admin\Config;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\Config;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Requests\Admin\Config\SaveConfig;

class APIController extends Controller
{
    /**
     * Show the current configuration settings.
     *
     * @return array
     */
    public function show()
    {
        return Config::with('translations')->first();
    }

    /**
     * Saves the configuration settings.
     *
     * @param  SaveConfig $request
     * @return array
     */
    public function save(SaveConfig $request)
    {
        $config = Config::first();
        $config->update($request->only([
            'name', 'phone', 'email', 'adress', 'city', 'country',
            'scripts', 'styles'
        ]));
        $this->saveTranslations($config, $request);

        return response($config);
    }


    /**
     * Saves the translations to a given general class.
     *
     * @param  Config $config
     * @param  Request $request
     * @return void
     */
    private function saveTranslations(Config $config, Request $request)
    {
        foreach ($request->translations as $translation) {
            if (in_array($translation['locale'], config('translatable.locales'))) {
                $trans = $config->translateOrNew($translation['locale']);
                $trans->config_id = $config->id;
                $trans->tags = array_key_exists('tags', $translation) ? $translation['tags'] : "";
                $trans->description = array_key_exists('description', $translation) ? $translation['description'] : "";
                $trans->save();
            }
        }
    }
}
