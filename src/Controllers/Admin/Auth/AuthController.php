<?php

namespace Lafactoria\Backend\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use Lafactoria\Backend\Requests\Admin\LoginRequest;
use Lafactoria\Backend\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Lafactoria\Backend\Models\AdminPasswordReset;
use Lafactoria\Backend\Models\Admin;
use Illuminate\Support\Carbon;

class AuthController extends Controller
{
    /**
     * Logins the administrator the administration panel.
     *
     * @param  LoginRequest $request
     * @return Response
     */
    public function login(LoginRequest $request)
    {
        if (auth()->guard('admin')->attempt($request->only(['email', 'password']), $request->has('remember'))) {
            return redirect()
                ->intended(route('admin.dashboard'))
                ->with('success', __('backend::admin-auth.successful_login'));
        }

        return redirect()
            ->route('admin.login')
            ->with('error', __('backend::admin-auth.invalid_credentials'))
            ->withInput();
    }

    /**
     * Logouts the administrator.
     *
     * @param  Request $request
     * @return Response
     */
    public function logout(Request $request)
    {
        auth()->guard('admin')->logout();

        return redirect()
            ->route('admin.login')
            ->with('success', __('backend::admin-auth.logout_complete'));
    }

    /**
     * Updates the user password
     *
     * @param  Request $request
     * @return Response
     */
    public function updatePassword(Request $request)
    {
        // Validate
        $attributes = $request->validate([
            'password' => 'required|confirmed|string|'.config('backend.password_validation_rule').'|nullable',
        ]);
        
        // Get user
        $user = auth()->guard('admin')->user();
        
        // Check if password is already used
        $attributes['password'] = $user->setPassword($attributes['password']);
        if($attributes['password'] == null){
            $error = ValidationException::withMessages(['password' => __('validation.password_already_used')]);
            throw $error;
        }

        // Set & save
        $user->password = $attributes['password'];
        $user->save();

        return 'ok';
    }

    /**
     * Send a reset password to the email provided
     *
     * @param  Request $request
     * @return Response
     */
    public function sendResetPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|exists:admins'
        ]);

        // If already exists a register for this user, delete it
        AdminPasswordReset::where('email', $request->email)->delete();

        $token = Hash::make($request->email);

        $passwordReset = AdminPasswordReset::create(['email' => $request->email, 'token' => $token]);
        $passwordReset->created_at = Carbon::today()->toDateTimeString();
        $passwordReset->save();

        $passwordReset->sendEmail();

        return 'ok';
    }

    /**
     * Get the view to reset password
     *
     * @param  Admin $admin
     * @return Response
     */
    public function resetPasswordView(Admin $admin)
    {
        // Check token caducity
        $passwordReset = AdminPasswordReset::where('email', $admin->email)->first();
        if($passwordReset == null){
            return redirect()
                ->route('admin.login')
                ->with('error', __('backend::admin-auth.reset_token_expired'));
        }
        $created_at = Carbon::createFromFormat('Y-m-d H:i:s', $passwordReset->created_at);
        $diff = $created_at->diffInDays(Carbon::now());
        if($diff > config('backend.password_reset_caducity')){
            return redirect()
                ->route('admin.login')
                ->with('error', __('backend::admin-auth.reset_token_expired'));
        }
        return view('backend::admin.auth.login', compact('admin'));
    }

    /**
     * Reset the password
     *
     * @param  Request $request
     * @return Response
     */
    public function resetPassword(Request $request)
    {
        // Validate
        $attributes = $request->validate([
            'password' => 'required|confirmed|string|'.config('backend.password_validation_rule').'|nullable',
        ]);
        
        // Get user
        $user = Admin::where('email', $request->email)->first();
        
        // Check if password is already used
        $attributes['password'] = $user->setPassword($attributes['password']);
        if($attributes['password'] == null){
            $error = ValidationException::withMessages(['password' => __('validation.password_already_used')]);
            throw $error;
        }

        // Set & save
        $user->password = $attributes['password'];
        $user->save();

        // Delete reset password reg
        AdminPasswordReset::where('email', $request->email)->delete();

        return 'ok';
    }
}
