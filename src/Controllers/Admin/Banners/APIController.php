<?php

namespace Lafactoria\Backend\Controllers\Admin\Banners;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\Banner;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Models\HeaderImageModel;
use Lafactoria\Backend\Controllers\Admin\LaFactoriaBaseController;


class APIController extends LaFactoriaBaseController
{

    public function __construct(){
        $this->model = new Banner();
    }

    /**
     * Returns to the index view with all the Banners
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        $banners = null;
        if($request->filter == ''){
            $banners = Banner::paginate(25);
        } else{
            $banners = $this->filter($request->filter);
        }
        return view('backend::admin.banners.index', ['filter' => $request->filter] ,compact('banners'));
    }

    /**
     * Returns all the banners.
     *
     * @return Collection
     */
    public function banners()
    {
        return Banner::all();
    }

    /**
     * Returns the banner information.
     *
     * @param  Banner  $banner
     * @return Lafactoria\Backend\Models\Banner
     */
    public function show(Banner $banner)
    {
        return $banner;
    }

    /**
     * Updates the banner information.
     *
     * @param  UpdateBanner $request
     * @return Lafactoria\Backend\Models\Banner
     */
    public function save(Request $request)
    {
        $request->validate([
            'active' => 'required|boolean',
            'name'  => 'required|string',
            'description' => 'required|string'
        ]);

        $banner = Banner::findOrFail($request->banner);
        $banner->update($request->only(['active', 'name', 'description', 'url']));

        // update header image
        if ($request->headerImageModel != null) {
            HeaderImagemodel::updateOrCreate([
                'model_id' => $banner->id,
                'model' => get_class($banner)
            ],[
                'image_id' => $request->headerImageModel
            ]);
        } else {
            if ($banner->headerImageModel)
                $banner->headerImageModel->delete();
        }

        return $banner;
    }

    /**
     * Create a new banner.
     *
     * @param  CreateBanner $request
     * @return Lafactoria\Backend\Models\Banner
     */
    public function create(Request $request)
    {
        $request->validate([
            'active' => 'required|boolean',
            'name'  => 'required|string',
            'description' => 'required|string'
        ]);

        $banner = Banner::create($request->only([
            'active', 'name', 'description', 'url'
        ]));

        // save header image
        if ($request->headerImageModel != null) {
            HeaderImageModel::create([
                'image_id' => $request->headerImageModel,
                'model_id' => $banner->id,
                'model' => get_class($banner)
            ]);
        }

        return $banner;
    }

    /**
     * Deletes an existign banner.
     *
     * @param  Request $request
     * @return array
     */
    public function delete(Request $request)
    {
        $banner = Banner::findOrFail($request->banner);

        // delete header image
        if ($banner->headerImageModel != null) {
            $banner->headerImageModel->delete();
        }

        return ['status' => $banner->delete()];
    }
}
