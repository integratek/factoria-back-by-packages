<?php

namespace Lafactoria\Backend\Controllers\Admin\Menus;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\HeaderImageModel;
use Lafactoria\Backend\Models\ImageModel;
use Lafactoria\Backend\Models\Seo;
use Lafactoria\Backend\Models\Menu;
use Lafactoria\Backend\Traits\Helper;
use Spatie\Sitemap\SitemapGenerator;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Requests\Admin\Menus\SaveMenu;
use Lafactoria\Backend\Requests\Admin\Menus\CreateMenu;

class APIController extends Controller
{
    /**
     * Returns all the menu items ordered by display order.
     *
     * @return Collection
     */
    public function menus()
    {
        return Menu::parents()->map(function ($parent) {
            return $parent->childs->prepend($parent);
        })->flatten();
    }

    /**
     * Returns the menu information.
     *
     * @param  Lafactoria\Backend\Models\Menu   $menu
     * @return Lafactoria\Backend\Models\Menu
     */
    public function info(Menu $menu)
    {
        return $menu;
    }

    /**
     * Toggles the active menu.
     *
     * @param  Request $request
     * @return Lafactoria\Backend\Models\Menu
     */
    public function toggleActives(Request $request)
    {
        $request->validate(['menus' => 'array']);

        foreach ($request->menus as $menu) {
            $menu2 = tap(Menu::findOrFail($menu['id']))->update(['active' => $menu['active']]);

            foreach ($menu['childs'] as $child) {
                Menu::findOrFail($child['id'])->update(['active' => $child['active']]);
            }
        }

        return ['status' => true];
    }

    /**
     * Creates a new menu item.
     *
     * @param  CreateMenu $request
     * @return Lafactoria\Backend\Models\Menu
     */
    public function create(CreateMenu $request)
    {
        $order = Menu::where('parent_id', $request->menu['parent_id'])->orderBy('order', 'desc')->first();
        $order = $order ? $order->order : 0;

        if ($request->menu['parent_id'] && Menu::findOrFail($request->menu['parent_id'])->type != Menu::SUBMENU_TYPE) {
            return response()->json([
                'message' => 'The given data was invalid',
                'errors' => ['parent_id' => ['Invalid parent ID, please try a diferent parent']]
            ], 403);
        }

        if ($request->menu['parent_id'] && $request->menu['type'] == Menu::SUBMENU_TYPE) {
            return response()->json([
                'message' => 'The given data was invalid',
                'errors' => ['type' => ['Invalid type, please try a diferent type']]
            ], 403);
        }

        $menu = Menu::create([
            'active' => $request->menu['active'],
            'order' => $order + 1,
            'type' => $request->menu['type'],
            'parent_id' => $request->menu['parent_id'] ? $request->menu['parent_id'] : null,
            'on_nav' => $request->menu['on_nav'],
            'on_footer' => $request->menu['on_footer']
        ]);

        $this->saveTranslations($menu, $request);
        Seo::create(['class' => get_class($menu), 'reference' => $menu->id]);

        // save header image
        if ($request->menu['headerImageModel'] != null) {
            HeaderImageModel::create([
                'image_id' => $request->menu['headerImageModel'],
                'model_id' => $menu->id,
                'model' => get_class($menu)
            ]);
        }

        // save gallery images
        if (count($request->menu["imageModel"]) != 0) {
            foreach ($request->menu["imageModel"] as $image_id)
                ImageModel::create([
                    'image_id' => $image_id,
                    'model_id' => $menu->id,
                    'model' => get_class($menu)
                ]);
        }

        /* SitemapGenerator::create(url('/'))
            ->writeToFile(public_path('sitemap.xml')); */

        return $menu;
    }

    /**
     * Saves the translations to a given menu class.
     *
     * @param  Menu    $menu
     * @param  Request $request
     * @return void
     */
    private function saveTranslations(Menu $menu, Request $request)
    {
        foreach ($request->translations as $translation) {
            if (in_array($translation['locale'], config('translatable.locales'))) {
                $trans = $menu->translateOrNew($translation['locale']);
                $trans->menu_id= $menu->id;
                $trans->title = $translation['title'];
                $trans->content = array_key_exists('content', $translation) ? $translation['content'] : "";
                $trans->highlighted = array_key_exists('highlighted', $translation) ? $translation['highlighted'] : "";
                $trans->url = $translation['url'];
                $trans->copy_from_english = array_key_exists('copy_from_english', $translation) ? $translation['copy_from_english'] : false;
                $trans->save();

                if (array_key_exists('files', $translation)) {
                    $current = $menu->files()->wherePivot('locale', $translation['locale'])->get();
                    $new_files = collect($translation['files']);
                    $diff = $new_files->diff($current->pluck('id')->toArray());

                    $added = $diff->each(function ($file_id) use ($menu, $translation){
                        $menu->files()->attach($file_id, ['locale' => $translation['locale']]);
                    });

                    $deleted = $menu->files()
                        ->wherePivot('locale', $translation['locale'])
                        ->wherePivotNotIn('document_id', $new_files->toArray())
                        ->detach();
                } else {
                    $menu->files()->wherePivot('locale', $translation['locale'])->detach();
                }
            }
        }
    }

    /**
     * Save order of the menus.
     *
     * @param  Request $requet
     * @return array
     */
    public function order(Request $request)
    {
        $request->validate(['menus' => 'required|array']);

        foreach ($request->menus as $menu) {
            Menu::findOrFail($menu['id'])->update(['order' => $menu['order']]);
        }

        return ['status' => true];
    }

    /**
     * Saves the edited menu.
     *
     * @param  Request $request
     * @return Lafactoria\Backend\Models\Menu
     */
    public function edit(SaveMenu $request)
    {
        $menu = Menu::findOrFail($request->menu['id']);

        $menu->update([
            'active' => $request->menu['active'],
            'on_nav' => $request->menu['on_nav'],
            'on_footer' => $request->menu['on_footer']
        ]);

        $this->saveTranslations($menu, $request);

        // update header image
        if ($request->menu['headerImageModel'] != null) {
            HeaderImagemodel::updateOrCreate([
                'model_id' => $menu->id,
                'model' => get_class($menu)
            ],[
                'image_id' => $request->menu['headerImageModel']
            ]);
        } else {
            if ($menu->headerImageModel)
                $menu->headerImageModel->delete();
        }

        // update gallery images
        if (count($request->menu["imageModel"]) != 0) {
            if (count($menu->imageModel) > 0) {
                $images = ImageModel::where([
                    'model_id' => $menu->id,
                    'model' => get_class($menu)
                ])->get();

                $images->whereNotIn('image_id', $request->menu["imageModel"])->each->delete();
            }

            foreach ($request->menu["imageModel"] as $image_id) {
                ImageModel::updateOrCreate([
                    'model_id' => $menu->id,
                    'model' => get_class($menu),
                    'image_id' => $image_id
                ],[
                    'image_id' => $image_id
                ]);
            }
        } else {
            if (count($menu->imageModel) > 0) {
                $menu->imageModel->each->delete();
            }
        }

        /* SitemapGenerator::create(url('/'))
            ->writeToFile(public_path('sitemap.xml')); */

        return $menu;
    }

    /**
     * Deletes a menu item.
     *
     * @param  Request $request
     * @return array
     */
    public function delete(Request $request)
    {
        $request->validate(['menu' => 'required|integer|exists:menus,id']);

        $menu = Menu::findOrFail($request->menu);

        foreach ($menu->childs as $child) {
            Seo::where(['class' => get_class($child), 'reference' => $child->id])->delete();
            $child->translations()->delete();
            if ($child->headerImageModel != null) {
                $child->headerImageModel->delete();
            }
            // save gallery images
            if (count($child->imageModel) != 0) {
                $child->imageModel->each->delete();
            }
            $child->delete();
        }

        $seo = Seo::where(['class' => get_class($menu), 'reference' => $menu->id]);

        $menu->translations()->delete();

        // delete header image
        if ($menu->headerImageModel != null) {
            $menu->headerImageModel->delete();
        }

        // save gallery images
        if (count($menu->imageModel) != 0) {
            $menu->imageModel->each->delete();
        }

        /* SitemapGenerator::create(url('/'))
            ->writeToFile(public_path('sitemap.xml')); */

        return ['status' => $menu->delete() && $seo->delete()];
    }

    /**
     * Returns the menu translations.
     *
     * @param  Request $request
     * @return array
     */
    public function translations(Request $request)
    {
        $locale = $request->locale ? $request->locale : config('app.locale');
        $file = __DIR__ . "../../../Translations/{$locale}/admin-menus.php";

        if (!file_exists($file)) {
            return [];
        }

        return include $file;
    }

    /**
     * Returns the available translations of the requested menu.
     *
     * @param  Request $request
     * @return Collection
     */
    public function seotranslations(Request $request)
    {
        $menu = $request->menu ? Menu::findOrFail($request->menu) : null;
        $trans = Helper::getModelLanguagesMerged($menu);

        $files = isset($menu) ? $menu->files->groupBy('pivot.locale') : [];

        $trans = $trans->map(function ($tr) use ($files){
            $tr['files'] = isset($files[$tr['locale']]) ? collect($files[$tr['locale']])->pluck('id') : [];
            return $tr;
        });

        return $trans;
    }
}
