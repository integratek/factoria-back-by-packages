<?php

namespace Lafactoria\Backend\Controllers\Admin\Admins;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Lafactoria\Backend\Models\Admin;
use Lafactoria\Backend\Models\Config;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Requests\Admin\Users\EditUser;
use Lafactoria\Backend\Requests\Admin\Users\CreateUser;
use Lafactoria\Backend\Requests\Admin\Users\DeleteUser;
use Illuminate\Validation\ValidationException;

class APIController extends Controller
{
    /**
     * Returns all the admin users.
     *
     * @return Collection
     */
    public function users()
    {
        return Admin::all();
    }
    /**
     * Return the admin information.
     *
     * @param  Admin $admin
     * @return Lafactoria\Backend\Models\Admin
     */
    public function user(Admin $admin)
    {
        return $admin;
    }

    /**
     * Updates a admin with the given data.
     *
     * @param Request $request
     * @return Lafactoria\Backend\Models\Admin
     */
    public function saveUser(Request $request)
    {
        $attributes = $request->validate([
            'user' => 'required|exists:admins,id',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'string|nullable|confirmed|'.config('backend.password_validation_rule').'|max:255',
            'role_id' => 'required|integer',
            'languages' => 'required_if:role_id,'.Admin::$ROLE_EDITOR
        ]);

        $admin = Admin::findOrFail($attributes['user']);

        unset($attributes['user']);

        if ($admin->email != $attributes['email'] && Admin::where('email', $attributes['email'])->first()) {
            return response([
                'message' => 'The email is alreay in use',
                'errors' => [
                    'email' => ['The email is already in use'],
                ],
            ], 422);
        }

        if (config('backend.password_caducity') > 0) {
            // Check if the password is being updated
            if (isset($attributes['password']) == null) {
                $attributes['password'] = $admin->password;
            } else {
                // Try to set the password (check if already used it)
                $attributes['password'] = $admin->setPassword($attributes['password']);
                if ($attributes['password'] == null) {
                    $error = ValidationException::withMessages(['new_password' => __('validation.password_already_used')]);
                    throw $error;
                }
            }
            // Update admin
            $admin->update($attributes);
        } else {
            // Update admin
            $admin->update(collect($attributes)->only(['name', 'email', 'role_id', 'languages'])->toArray());
            if ($request->password) {
                // Update password
                $admin->update(collect($attributes)->only('password', 'role_id', 'languages')->toArray());
            }
        }

        return $admin;
    }

    /**
     * Creates a new admin.
     *
     * @param  CreateUser $request
     * @return Lafactoria\Backend\Models\Admin
     */
    public function createUser(CreateUser $request)
    {
        // Validate
        $attributes = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'string|nullable|confirmed|'.config('backend.password_validation_rule').'|max:255',
            'role_id' => 'required|integer',
            'languages' => 'required_if:role_id,'.Admin::$ROLE_EDITOR
        ]);

        // Create
        $admin = Admin::create(collect($attributes)->only(['name', 'email', 'password', 'role_id', 'languages'])->toArray());

        // Set password
        $password = $admin->setPassword($attributes['password']);
        $admin->password = $password;
        $admin->save();

        return $admin;
    }

    /**
     * Deletes a admin from the database.
     *
     * @param  Request $request
     * @return array
     */
    public function deleteUser(Request $request)
    {
        $request->validate([
            'user' => 'required|exists:admins,id',
        ]);

        return ['status' => Admin::findOrFail($request->user)->delete()];
    }
}
