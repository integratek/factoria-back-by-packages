<?php

namespace Lafactoria\Backend\Controllers\Admin\Contact;

use Illuminate\Http\Request;
use Lafactoria\Backend\Exports\ContactsExport;
use Lafactoria\Backend\Models\Contact;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Controllers\Admin\LaFactoriaBaseController;
use Maatwebsite\Excel\Facades\Excel;

class APIController extends LaFactoriaBaseController
{

    public function __construct(){
        $this->model = new Contact();
    }

    /**
     * Returns to the index view with all the contacts
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        $contacts = null;
        if($request->filter == ''){
            $contacts = Contact::paginate(25);
        } else{
            $contacts = $this->filter($request->filter);
        }
        return view('backend::admin.contact.index', ['filter' => $request->filter] ,compact('contacts'));
    }

    /**
     * Returns all the contact requests.
     *
     * @return Collection
     */
    public function contacts()
    {
        return Contact::all();
    }

    /**
     * Return the contact information
     *
     * @param  Contact $contact
     * @return Collection
     */
    public function info(Contact $contact)
    {
        return $contact;
    }

    /**
     * Deletes the contact
     *
     * @param  Contact $contact
     * @return Collection
     */
    public function delete(Request $request)
    {
        $contact = Contact::findOrFail($request->contact);
        return ['status' => $contact->delete()];
    }


    public function export()
    {
        $contacts = Contact::get();
        return Excel::download(new ContactsExport($contacts), 'contacts.xlsx');
    }
}
