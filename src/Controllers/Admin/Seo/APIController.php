<?php

namespace Lafactoria\Backend\Controllers\Admin\Seo;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Lafactoria\Backend\Traits\Helper;
use Lafactoria\Backend\Models\Seo;
use Lafactoria\Backend\Controllers\Controller;

class APIController extends Controller
{
    /**
     * Returns the translations of a SEO model.
     *
     * @param  Request $request
     * @return Collection
     */
    public function getSeo(Request $request)
    {
        $request->validate(['class' => 'required', 'reference' => 'required']);

        $seo =  Seo::where($request->only(['class', 'reference']))->first();

        if (!$seo) {
            $seo = collect([
                'follow' => true,
                'indexable' => true,
                'translations' => []
            ]);
        }

        if ($seo) {
            $current_translations = collect($seo['translations']);
            $new_translations = collect([]);

            collect(config('translatable.locales'))->each(function ($locale) use (&$new_translations, $current_translations, $seo){
                $exist = $current_translations->firstWhere('locale', $locale);

                if (!$exist) {
                    $new_translations->push([
                        'title' => '',
                        'description' => '',
                        'canonical' => '',
                        'tags' => '',
                        'locale' => $locale,
                    ]);
                } else {
                    $new_translations->push($exist);
                }

            });
            $seo = $seo->toArray();
            $seo['translations'] = $new_translations;
        }
        return $seo;
    }

    /**
     * Translates the model information and returns the seo model.
     *
     * @param  SeoRequest $request
     * @return Lafactoria\Backend\Models\Seo
     */
    public function seoSave(Request $request)
    {
        $request->validate([
            'translations' => 'sometimes|nullable|array',
            'translations.*.locale' => 'sometimes|string',
            'translations.*.title' => 'sometimes|string',
            'translations.*.tags' => 'sometimes|string',
            'translations.*.description' => 'sometimes|string',
            'translations.*.canonical' => 'sometimes|nullable|string',
            'class' => 'required|string',
            'reference' => 'required|numeric',
            'indexable' => 'required|boolean',
            'follow' => 'required|boolean',
        ]);

        $seo = Seo::updateOrCreate(
            $request->only(['class', 'reference']),
            $request->only(['indexable', 'follow'])
        );

        if ($request->translations && count($request->translations) > 0) {
            foreach ($request->translations as $translation) {
                if (in_array($translation['locale'], config('translatable.locales'))) {
                    $trans = $seo->translateOrNew($translation['locale']);
                    $trans->seo_id = $seo->id;
                    $trans->title = $translation['title'];
                    $trans->description = $translation['description'];
                    $trans->canonical = $translation['canonical'];
                    $trans->tags = $translation['tags'];
                    $trans->locale = $translation['locale'];
                    $trans->save();
                }
            }
        }
        return $seo->toArray();
    }


    /**
     * Update seo indexable by model
     *
     * @param Request $request
     * @return mixed
     */
    public function saveIndexable(Request $request)
    {
        $model_id = $request->model_id;
        $seo_indexable = $request->seo_indexable;
        $model_class = $request->model_class;

        $model = $model_class::findOrFail($model_id);
        return tap($model)->update(['seo_indexable' => $seo_indexable]);
    }


    /**
     * Update seo Image
     *
     * @param Request $request
     * @return mixed
     */
    public function saveImage(Request $request)
    {
        $request->validate([
            'translations' => 'required|array',
            'translations.*.seo_alt' => 'required|string'
        ]);

        $model_class = $request->model_class;
        $model_id = $request->model_id;
        $key_id = Str::contains($model_class,'HeaderImageModel') ? 'header_image_model_id' : 'image_model_id';
        $model = $model_class::findOrFail($model_id);

        foreach ($request->translations as $translation) {
            if (in_array($translation['locale'], config('translatable.locales'))) {
                $trans = $model->translateOrNew($translation['locale']);
                $trans[$key_id] = $model->id;
                $trans->seo_alt = $translation['seo_alt'];
                $trans->save();
            }
        }
        return $model;
    }


    /**
     * Returns the translations needed for the seo component to work.
     *
     * @return array
     */
    public function trans(Request $request)
    {
        $locale = $request->locale ? $request->locale : config('app.locale');
        $file = __DIR__ . "/../../../Translations/{$locale}/admin-seo.php";

        if (!file_exists($file)) {
            return [];
        }

        return include $file;
    }

    /**
     * Returns all the available world languages in a given locale.
     *
     * @param  Request $request
     * @return Collection
     */
    public function languages(Request $request)
    {
        return Helper::languages($request->locale ? $request->locale : config('app.locale'));
    }
}
