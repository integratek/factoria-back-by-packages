<?php

namespace Lafactoria\Backend\Controllers\Admin\Images;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\Image;
use Lafactoria\Backend\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ImagesController extends Controller
{
    /**
     * Downloads the image as a file.
     *
     * @param  Image  $image
     * @return Response
     */
    public function download(Image $image)
    {
        $path = "app/images/{$image->file}";
        if (config('backend.s3')) {
            return redirect(Storage::disk('s3')->temporaryUrl(
                $path,
                now()->addHour(),
                ['ResponseContentDisposition' => 'attachment']
            ));
        } else {
            return response()
                ->download(storage_path($path));
        }
    }
}
