<?php

namespace Lafactoria\Backend\Controllers\Admin\Images;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\HeaderImageModel;
use Lafactoria\Backend\Models\Image;
use Lafactoria\Backend\Models\ImageModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Requests\Admin\Images\SaveImage;
use Lafactoria\Backend\Requests\Admin\Images\UploadImage;
use Lafactoria\Backend\Controllers\Admin\LaFactoriaBaseController;

class APIController extends LaFactoriaBaseController
{

    public function __construct(){
        $this->model = new Image();
    }

    /**
     * Returns to the index view with all the news
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        $images = null;
        if($request->filter == ''){
            $images = Image::paginate(25);
        } else{
            $images = $this->filter($request->filter);
        }
        return view('backend::admin.images.index', ['filter' => $request->filter] ,compact('images'));
    }

    /**
     * Returns the translations of the image.
     *
     * @param  Request $request
     * @return array
     */
    public function trans(Request $request)
    {
        $locale = $request->locale ? $request->locale : config('app.locale');
        $file = __DIR__ . "/../../../Translations/{$locale}/admin-images.php";

        if (!file_exists($file)) {
            return [];
        }

        return include $file;
    }

    /**
     * Returns images by IDs
     *
     * @return Collection
     */
    public function getImages(Request $request)
    {
         return Image::whereIn('id', $request->ids)->get();
    }

    /**
     * Returns image by ID
     *
     * @return Collection
     */
    public function image(Request $request)
    {
         return Image::select('*', 'id as image_id')->find($request->id);
    }

    /**
     * Returns all the images.
     *
     * @return Collection
     */
    public function images(Request $request)
    {
        $images = null;

        if($request->filter != null){
            $images = Image::where('name', 'like', '%' . $request->filter . '%')->select('*', 'id as image_id')->paginate(25);
        } else{
            $images = Image::select('*', 'id as image_id')->paginate(25);
        }

        return $images;
    }

    /**
     * Returns the image information.
     *
     * @param  Image  $image
     * @return Lafactoria\Backend\Models\Image
     */
    public function show(Image $image)
    {
        return $image;
    }

    /**
     * Saves the image information.
     *
     * @param  SaveImage $request
     * @return Lafactoria\Backend\Models\Image
     */
    public function save(SaveImage $request)
    {
        return tap(Image::findOrFail($request->image))->update($request->only('name'));
    }

    /**
     * Uploads a new image to the repository.
     *
     * @param  UploadImage $request
     * @return Lafactoria\Backend\Models\Image
     */
    public function uploadInRepository(UploadImage $request)
    {
        $name = collect(explode('.', $request->file->getClientOriginalName()));
        $extension = $name->last();
        $name->pop();
        $name = $name->implode('.');
        $name = $request->name ? $request->name : $name;
        $fname = Str::snake($name);
        $file_name = "{$fname}.{$extension}";

        while ($this->getFileExists($file_name)) {
            $random = Str::random(5);
            $file_name = "{$fname}__{$random}.{$extension}";
        }

        if (config('backend.s3')) {
            Storage::disk('s3')->put('app/images/' . $file_name, file_get_contents($request->file));
        } else {
            $request->file->move(storage_path('app/images'), $file_name);
        }

        return Image::create(['name' => $name, 'file' => $file_name]);
    }

    private function getFileExists($file_name)
    {
        $exists = false;
        $path = "app/images/{$file_name}";
        if (config('backend.s3')) {
            $exists = Storage::disk('s3')->exists($path);
        } else {
            $exists = file_exists(storage_path($path));
        }
        return $exists;
    }


    /**
     * Deletes the given image from the server.
     *
     * @param  DeleteImage $request
     * @return array
     */
    public function delete(Request $request)
    {
        $image = Image::findOrFail($request->image);

        if ($this->getFileExists($image->file)) {
            if (config('backend.s3')) {
                Storage::disk('s3')->delete("app/images/{$image->file}");
            } else {
                Storage::delete("images/{$image->file}");
            }
        }

        HeaderImageModel::where('image_id', $image->id)->delete();
        ImageModel::where('image_id', $image->id)->delete();

        return ['status' => $image->delete()];
    }
}
