<?php

namespace Lafactoria\Backend\Controllers\Admin\Dashboard;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\User;
use Lafactoria\Backend\Models\News;
use Lafactoria\Backend\Models\Contact;
use Lafactoria\Backend\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Dashboard index page.
     *
     * @return Response
     */
    public function index()
    {
        return view('backend::admin.dashboard.index');
    }
}
