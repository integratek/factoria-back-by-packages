<?php

namespace Lafactoria\Backend\Controllers\Admin\Slides;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Lafactoria\Backend\Models\HeaderImageModel;
use Lafactoria\Backend\Models\News;
use Lafactoria\Backend\Models\Slide;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Requests\Admin\Slides\VideosSlide;
use Lafactoria\Backend\Requests\Admin\Slides\CreateSlide;
use Lafactoria\Backend\Requests\Admin\Slides\UpdateSlide;
use Lafactoria\Backend\Controllers\Admin\LaFactoriaBaseController;
use Lafactoria\Backend\Traits\Helper;

class APIController extends LaFactoriaBaseController
{

    public function __construct(){
        $this->model = new Slide();
    }

    /**
     * Returns to the index view with all the slides
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        $slides = null;
        if($request->filter == '')
        {
            $slides = Slide::paginate(25);
        } else {
            $slides = $this->filter($request->filter);
        }

        return view('backend::admin.slides.index', ['filter' => $request->filter] ,compact('slides'));
    }

    /**
     * Returns all the slides.
     *
     * @return Collection
     */
    public function slides()
    {
        return Slide::all();
    }

    /**
     * Returns the slide information.
     *
     * @param  Slide  $slide
     * @return Lafactoria\Backend\Models\Slide
     */
    public function show(Slide $slide)
    {
        return $slide;
    }

    /**
     * Updates the slide information.
     *
     * @param  UpdateSlide $request
     * @return Lafactoria\Backend\Models\Slide
     */
    public function save(UpdateSlide $request)
    {
        $slide = Slide::findOrFail($request->slide);
        $slide->update([
            'active' => (int) $request->active, 'new_tab' => (int) $request->new_tab
        ]);
        $this->saveTranslations($slide, $request);

        // update header image
        if ($request->headerImageModel != null) {
            HeaderImagemodel::updateOrCreate([
                'model_id' => $slide->id,
                'model' => get_class($slide)
            ],[
                'image_id' => $request->headerImageModel
            ]);
        } else {
            if ($slide->headerImageModel)
                $slide->headerImageModel->delete();
        }

        return $slide;
    }

    /**
     * Create a new slide.
     *
     * @param  CreateSlide $request
     * @return Lafactoria\Backend\Models\Slide
     */
    public function create(CreateSlide $request)
    {
        $slideForSave = $request->only([
            'active', 'new_tab'
        ]);


        $slide = Slide::create($slideForSave);
        $this->saveTranslations($slide, $request);

        // save header image
        if ($request->headerImageModel != null) {
            HeaderImageModel::create([
                'image_id' => $request->headerImageModel,
                'model_id' => $slide->id,
                'model' => get_class($slide)
            ]);
        }

        return $slide;
    }

    /**
     * Deletes an existign slide.
     *
     * @param  Request $request
     * @return array
     */
    public function delete(Request $request)
    {
        $slide = Slide::findOrFail($request->slide);

        // delete header image
        if ($slide->headerImageModel != null) {
            $slide->headerImageModel->delete();
        }

        return ['status' => $slide->delete()];
    }

    /**
     * Returns the available tralsnations of the requested news.
     *
     * @param  Request $request
     * @return Collection
     */
    public function seotranslations(Request $request)
    {
        $slide = $request->slide ? Slide::findOrFail($request->slide) : null;
        return Helper::getModelLanguagesMerged($slide);
    }

    /**
     * Saves the translations to a given news class.
     *
     * @param  Slide $slide
     * @param  Request $request
     * @return void
     */
    private function saveTranslations(Slide $slide, Request $request)
    {
        foreach ($request->translations as $translation) {
            if (in_array($translation['locale'], config('translatable.locales'))) {
                $trans = $slide->translateOrNew($translation['locale']);
                $trans->slide_id= $slide->id;
                $trans->name = $translation['name'];
                $trans->description = array_key_exists('description', $translation) ? $translation['description'] : "";
                $trans->url = array_key_exists('url', $translation) ? $translation['url'] : null;
                $trans->save();
            }
        }
    }

    /**
     * @param  VideosSlide  $request
     * @param  Slide  $slide
     */
    public function uploadVideos (VideosSlide $request, Slide $slide)
    {
        $toDelete = collect(json_decode($request->to_delete));
        $toUpload = collect(json_decode($request->uploading_files));
        foreach (json_decode($request->translations) as $translation ) {
            if (in_array($translation, config('translatable.locales'))) {
                $trans = $slide->translateOrNew($translation);
                $video_name = 'video_name_'.$translation;
                $video_webm = 'video_webm_'.$translation;
                $video_mp4 = 'video_mp4_'.$translation;
                $webmToDelete = $toDelete->contains($video_webm) ? 'delete' : '';
                $webM = $toUpload->contains($video_webm) ? $this->uploadVideo($request, 'video_webm_'.$translation) : $webmToDelete;
                $mp4ToDelete = $toDelete->contains($video_mp4) ? 'delete' : '';
                $mp4 = $toUpload->contains($video_mp4) ? $this->uploadVideo($request, $video_mp4) : $mp4ToDelete;
                $name = $request->$video_name;
                $trans->video_name = $name !== 'null' ? $name : null;
                if($webM) {
                    $trans->video_webm = $webM !== 'delete' ? $webM  : null;
                }
                if($mp4) {
                    $trans->video_mp4 = $mp4 !== 'delete' ? $mp4  : null;
                }
                $trans->save();
            }
        }
    }
    /**
     * @param  Request  $request
     * @param  string  $field_name
     * @param  array  $slideForSave
     * @return bool
     */
    private function uploadVideo (Request $request, string $field_name) :string
    {
        if ($request->$field_name !== '' && !is_null($request->file($field_name))) {
           $files = $request->file($field_name);
           $fname = Str::snake(str_replace($files->getClientOriginalExtension(),'', $files->getClientOriginalName()));
           $file_name = "{$fname}.{$files->getClientOriginalExtension()}";

           if (config('backend.s3')) {
                Storage::disk('s3')->put('app/videos/' . $file_name, file_get_contents($files));
            } else {
                $files->move(storage_path('app/videos/'), $file_name);
            }
           
           return $file_name;
       }
        return false;
    }
}
