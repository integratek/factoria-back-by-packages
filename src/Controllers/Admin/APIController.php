<?php

namespace Lafactoria\Backend\Controllers\Admin;

use Illuminate\Http\Request;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Lafactoria\Backend\Controllers\Controller;
use Illuminate\Support\Str;

class APIController extends Controller
{
    /**
     * Flashes a new message to the user.
     *
     * @param  Request $request
     * @return void
     */
    public function flash(Request $request)
    {
        $request->validate(['msg' => 'required', 'type' => 'required']);

        $msg = Str::start($request->msg, 'backend::');

        $request->session()->flash($request->type, __($msg) == $msg ? $msg : __($msg));
    }

    /**
     * Returns a translation from the server.
     *
     * @param  Request $request
     * @return array
     */
    public function trans(Request $request)
    {
        $request->validate(['msg' => 'required']);

        $msg = Str::start($request->msg, 'backend::');

        return ['msg' => __($msg) == $msg ? $msg : __($msg)];
    }

    /**
     * Craw the site and generate the sitemap.
     *
     */
    public function crawlsite() :void
    {
        ini_set('memory_limit', '-1');
        set_time_limit(10000);

        foreach (config('translatable.locales') as $locale) {
            $sitemap =  Sitemap::create();
            foreach (config('backend.sitemap.models') as $model) {
                foreach ($model::sitemap($locale) as $model_row) {
                    $result = $model::urlFormat($model_row, $locale);
                    if (is_array($result)) {
                        foreach ($result as $result_item) {
                            $sitemap->add($result_item);
                        }
                    } else {
                        if ($result)
                            $sitemap->add($result);
                    }
                }
            }

            if (isset(config('backend.sitemap.urls')[$locale])) {
                foreach (config('backend.sitemap.urls')[$locale] as $url) {
                    $sitemap->add($url);
                }
            }

            // cookies
            \Lafactoria\Backend\Models\Sitemap::updateOrCreate(
                ['model' => 'cookies', 'model_id' => 1, 'locale' => $locale],
                ['url' => url('/').'/'.$locale.'/cookies']
            );

            // legal
            \Lafactoria\Backend\Models\Sitemap::updateOrCreate(
                ['model' => 'legal', 'model_id' => 1, 'locale' => $locale],
                ['url' => url('/').'/'.$locale.'/legal']
            );

            // privacy
            \Lafactoria\Backend\Models\Sitemap::updateOrCreate(
                ['model' => 'privacy', 'model_id' => 1, 'locale' => $locale],
                ['url' => url('/').'/'.$locale.'/privacy']
            );

            // contact
            $contact_url = url('/').'/'.$locale.'/contact';
            \Lafactoria\Backend\Models\Sitemap::updateOrCreate(
                ['model' => 'contact', 'model_id' => 1, 'locale' => $locale],
                ['url' => $contact_url]
            );
            $sitemap->add($contact_url);

            // write
            $sitemap->writeToDisk('public', "sitemap_{$locale}.xml");
        }
    }
}
