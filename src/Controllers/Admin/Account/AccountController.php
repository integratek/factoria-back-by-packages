<?php

namespace Lafactoria\Backend\Controllers\Admin\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Requests\Admin\Account\EditAccount;

class AccountController extends Controller
{
    /**
     * Updates the user profile
     *
     * @param  EditAccount $request
     * @return Response
     */
    public function update(EditAccount $request)
    {
        $user = auth()->guard('admin')->user();

        if (!Hash::check($request->current_password, $user->password)) {
            return redirect()
                ->route('admin.account')
                ->with('error', __('backend::admin-account.wrong_current_password'));
        }

        $user->update($request->only(['name']));

        if ($request->new_password) {
            $user->update(['password' => $request->new_password]);
        }

        return redirect()
            ->route('admin.account')
            ->with('success', __('backend::admin-account.account_updated'));
    }
}
