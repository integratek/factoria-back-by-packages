<?php

namespace Lafactoria\Backend\Controllers\Admin\Account;

use Illuminate\Http\Request;
use Lafactoria\Backend\Controllers\Controller;

class APIController extends Controller
{
    /**
     * Returns the user avatar.
     *
     * @return array
     */
    public function avatar()
    {
        return ['url' => auth()->guard('admin')->user()->avatar()];
    }
}
