<?php

namespace Lafactoria\Backend\Controllers\Admin\General;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\General;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Models\Seo;
use Lafactoria\Backend\Requests\Admin\General\SaveGeneral;
use Lafactoria\Backend\Traits\Helper;

class APIController extends Controller
{

    /**
     * Show all the general rows
     *
     * @return Collection
     */
    public function index(){
        return General::all();
    }

    /**
     * Edit view
     *
     * @return Collection
     */
    public function editView($id){
        $general = General::where('id', '=', $id)->first();
        return view('backend::admin.general.edit', compact('general'));
    }

    /**
     * Create new General
     *
     * @return Collection
     */
    public function create(Request $request){
        $general = General::create();
        $this->saveTranslations($general, $request);
        return view('backend::admin.general.index');
    }

    /**
     * Update General
     *
     * @return Collection
     */
    public function update(Request $request, $id){
        $general = General::where('id', '=', $id)->first();
        $this->saveTranslations($general, $request);
        return view('backend::admin.general.index');
    }

    /**
     * Delete General
     *
     * @return Collection
     */
    public function delete(Request $request){
        $id = $request->id;
        $general = General::where('id', '=', $id)->first();
        $general->deleteTranslations();
        $general->delete();
    }

    /**
     * Show the general information
     *
     * @return Collection
     */
    public function show()
    {
        return General::first();
    }

    /**
     * Save the general information.
     *
     * @param  SaveGeneral $request
     * @return Collection
     */
    public function save(SaveGeneral $request)
    {
        $general = General::first();

        $this->saveTranslations($general, $request);
        Seo::create(['class' => get_class($general), 'reference' => $general->id]);

        return $general;
    }


    /**
     * Saves the translations to a given general class.
     *
     * @param  General $general
     * @param  Request $request
     * @return void
     */
    private function saveTranslations(General $general, Request $request)
    {
        foreach ($request->translations as $translation) {
            if (in_array($translation['locale'], config('translatable.locales'))) {
                $trans = $general->translateOrNew($translation['locale']);
                $trans->general_id= $general->id;
                $trans->title = array_key_exists('title', $translation) ? $translation['title'] : "";
                $trans->text = array_key_exists('text', $translation) ? $translation['text'] : "";
                //$trans->text = strip_tags($trans->text);
                $trans->save();
            }
        }
    }


    /**
     * Returns the available translations of the requested general.
     *
     * @param  Request $request
     * @return Collection
     */
    public function seotranslations(Request $request)
    {
        $general = General::first();
        return Helper::getModelLanguagesMerged($general);
    }
}
