<?php

namespace Lafactoria\Backend\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class LaFactoriaBaseController extends BaseController
{
    public $model;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function filter($txt, $pagination = 25){
        $model = $this->model;
        $started = false;

        foreach ($this->model->filterAttributes as $key) {
            $trans_attrs = $this->model->translatedAttributes;

            if($trans_attrs && in_array($key, $trans_attrs)){
                $model = $model->orWhereTranslationLike($key, '%'.$txt.'%');
            } else{
                if(!$started){
                    $model = $model->where($this->model->getTable().'.'.$key, 'like', '%'.$txt.'%');
                    $started = true;
                } else{
                    $model = $model->orWhere($this->model->getTable().'.'.$key, 'like', '%'.$txt.'%');
                }
            }
        }

        $model = $this->model->filterPersonalized($model, $txt);
        
        // Paginated ?
        if($pagination != -1){
            $model = $model->paginate($pagination);
        }

        return $model;
    }
}
