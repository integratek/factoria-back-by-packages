<?php

namespace Lafactoria\Backend\Controllers\Admin\Documents;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\Document;
use Lafactoria\Backend\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DocumentsController extends Controller
{
    /**
     * Downloads the document as a file.
     *
     * @param  Document $document
     * @return Response
     */
    public function download(Document $document)
    {
        $path = "app/documents/{$document->file}";
        if (config('backend.s3')) {
            return redirect(Storage::disk('s3')->temporaryUrl(
                $path,
                now()->addHour(),
                ['ResponseContentDisposition' => 'attachment']
            ));
        } else {
            return response()
                ->download(storage_path($path));
        }
    }
}
