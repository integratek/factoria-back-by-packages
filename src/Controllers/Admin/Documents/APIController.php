<?php

namespace Lafactoria\Backend\Controllers\Admin\Documents;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Lafactoria\Backend\Models\Document;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Requests\Admin\Documents\SaveDocument;
use Lafactoria\Backend\Requests\Admin\Documents\UploadDocument;
use Lafactoria\Backend\Controllers\Admin\LaFactoriaBaseController;

class APIController extends LaFactoriaBaseController
{

    public function __construct(){
        $this->model = new Document();
    }

    /**
     * Returns to the index view with all the documents
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        $documents = null;
        if($request->filter == ''){
            $documents = Document::paginate(25);
        } else{
            $documents = $this->filter($request->filter);
        }
        return view('backend::admin.documents.index', ['filter' => $request->filter] ,compact('documents'));
    }


    /**
     * Returns the translations of the document.
     *
     * @param  Request $request
     * @return array
     */
    public function trans(Request $request)
    {
        $locale = $request->locale ? $request->locale : config('app.locale');
        $file = __DIR__ . "/../../../Translations/{$locale}/admin-documents.php";

        if (!file_exists($file)) {
            return [];
        }

        return include $file;
    }


    /**
     * Returns Documents by IDs
     *
     * @return Collection
     */
    public function getDocuments(Request $request)
    {
        return Document::whereIn('id', $request->ids)->get();
    }


    /**
     * Returns document by ID
     *
     * @return Collection
     */
    public function document(Request $request)
    {
        return Document::find($request->id);
    }


    /**
     * Returns all the documents.
     *
     * @return Collection
     */
    public function documents(Request $request)
    {
        $documents = null;
        if($request->filter == ''){
            $documents = Document::paginate(25);
        } else{
            $documents = $this->filter($request->filter);
        }
        return $documents;
    }

    /**
     * Return the specified document.
     *
     * @param  Document $document
     * @return Lafactoria\Backend\Models\Document
     */
    public function show(Document $document)
    {
        return $document;
    }

    /**
     * Saves a document to the database.
     *
     * @param  SaveDocument $request
     * @return Lafactoria\Backend\Models\Document
     */
    public function save(SaveDocument $request)
    {
        return tap(Document::findOrFail($request->document))->update($request->only(['name', 'seo_indexable']));
    }

    /**
     * Uploads a new document to the database.
     *
     * @param  UploadDocument $request
     * @return Lafactoria\Backend\Models\Document
     */
    public function upload(UploadDocument $request)
    {
        $file_name = $request->file->getClientOriginalName();
        $doc_name = Str::slug(collect(explode('.', $file_name))->first());
        $extension = collect(explode('.', $file_name))->last();
        $file_name = "{$doc_name}.{$extension}";

        while ($this->getFileExists($file_name)) {
            $random = Str::random(5);
            $file_name = "{$doc_name}__{$random}.{$extension}";
        }

        if (config('backend.s3')) {
            Storage::disk('s3')->put('app/documents/' . $file_name, file_get_contents($request->file));
        } else {
            $request->file->move(storage_path('app/documents'), $file_name);
        }

        return Document::create(['name' => $request->name, 'file' => $file_name]);
    }

    private function getFileExists($file_name)
    {
        $exists = false;
        $path = "app/documents/{$file_name}";
        if (config('backend.s3')) {
            $exists = Storage::disk('s3')->exists($path);
        } else {
            $exists = file_exists(storage_path($path));
        }
        return $exists;
    }

    /**
     * Deletes a document from the database.
     *
     * @param  Request $request
     * @return array
     */
    public function delete(Request $request)
    {
        $document = Document::findOrFail($request->document);

        if ($this->getFileExists($document->file)) {
            if (config('backend.s3')) {
                Storage::disk('s3')->delete("app/documents/{$document->file}");
            } else {
                Storage::delete("documents/{$document->file}");
            }
        }

        return ['status' => $document->delete()];
    }
}
