<?php

namespace Lafactoria\Backend\Controllers\Admin\Users;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\User;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Requests\Admin\Users\EditUser;
use Lafactoria\Backend\Requests\Admin\Users\CreateUser;
use Lafactoria\Backend\Requests\Admin\Users\DeleteUser;

class APIController extends Controller
{
    /**
     * Returns all the users.
     *
     * @return Collection
     */
    public function users()
    {
        return User::all();
    }
    /**
     * Return the user information.
     *
     * @param  User   $user
     * @return Lafactoria\Backend\Models\User
     */
    public function user(User $user)
    {
        return $user;
    }

    /**
     * Updates a user with the given data.
     *
     * @param  EditUser $request
     * @param  User     $user
     * @return Lafactoria\Backend\Models\User
     */
    public function saveUser(EditUser $request)
    {
        $user = User::findOrFail($request->user);

        if ($user->email != $request->email && User::where('email', $request->email)->first()) {
            return response([
                'message' => 'The email is alreay in use',
                'errors' => [
                    'email' => ['The email is already in use'],
                ],
            ], 422);
        }

        $user->update($request->only(['name', 'email']));

        if ($request->password) {
            $user->update($request->only('password'));
        }

        return $user;
    }

    /**
     * Creates a new user.
     *
     * @param  CreateUser $request
     * @return Lafactoria\Backend\Models\User
     */
    public function createUser(CreateUser $request)
    {
        return User::create($request->only(['name', 'email', 'password']));
    }

    /**
     * Deletes a user from the databse.
     *
     * @param  DeleteUser $request
     * @return array
     */
    public function deleteUser(DeleteUser $request)
    {
        return ['status' => User::findOrFail($request->user)->delete()];
    }
}
