<?php

namespace Lafactoria\Backend\Controllers\Admin\News;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\HeaderImageModel;
use Lafactoria\Backend\Models\ImageModel;
use Lafactoria\Backend\Models\Seo;
use Lafactoria\Backend\Models\News;
use Lafactoria\Backend\Traits\Helper;
use Spatie\Sitemap\SitemapGenerator;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Requests\Admin\News\EditNews;
use Lafactoria\Backend\Requests\Admin\News\CreateNews;
use Lafactoria\Backend\Requests\Admin\News\DeleteNews;
use Lafactoria\Backend\Controllers\Admin\LaFactoriaBaseController;

class APIController extends LaFactoriaBaseController
{

    public function __construct(){
        $this->model = new News();
    }

    /**
     * Returns to the index view with all the news
     *
     * @return Collection
     */
    public function index(Request $request)
    {
        $news = null;
        if($request->filter == ''){
            $news = News::with('admin')->orderBy('id','desc')->paginate(25);
        } else{
            $news = $this->filter($request->filter);
        }
        return view('backend::admin.news.index', ['filter' => $request->filter] ,compact('news'));
    }

    /**
     * Returns all the news from the database.
     *
     * @return Collection
     */
    public function news()
    {
        return News::with('admin')->get();
    }

    /**
     * Returns the information of a new.
     *
     * @param  News   $news
     * @return Lafactoria\Backend\Models\News
     */
    public function info(News $news)
    {
        return $news;
    }

    /**
     * Creates a new new.
     *
     * @param  CreateNews $request
     * @return Lafactoria\Backend\Models\News
     */
    public function createNews(CreateNews $request)
    {
        $news = auth()->guard('admin')->user()->news()->create([
            'active' => $request->news['active'],
            'slug' => $request->news['slug'],
            'display_after' => $request->news['display_after'],
        ]);

        $this->saveTranslations($news, $request);
        Seo::create(['class' => get_class($news), 'reference' => $news->id]);

        // save header image
        if ($request->news['headerImageModel'] != null) {
            HeaderImageModel::create([
                'image_id' => $request->news['headerImageModel'],
                'model_id' => $news->id,
                'model' => get_class($news)
            ]);
        }

        // save gallery images
        if (count($request->news["imageModel"]) != 0) {
            foreach ($request->news["imageModel"] as $image_id)
                ImageModel::create([
                    'image_id' => $image_id,
                    'model_id' => $news->id,
                    'model' => get_class($news)
                ]);
        }

        /* SitemapGenerator::create(url('/'))
            ->writeToFile(public_path('sitemap.xml')); */

        return $news;
    }

    /**
     * Saves the translations to a given news class.
     *
     * @param  News    $news
     * @param  Request $request
     * @return void
     */
    private function saveTranslations(News $news, Request $request)
    {
        foreach ($request->translations as $translation) {
            if (in_array($translation['locale'], config('translatable.locales'))) {
                $trans = $news->translateOrNew($translation['locale']);
                $trans->news_id= $news->id;
                $trans->title = $translation['title'];
                $trans->description = array_key_exists('description', $translation) ? $translation['description'] : "";
                $trans->content = array_key_exists('content', $translation) ? $translation['content'] : "";
                $trans->save();
                if (array_key_exists('files', $translation)) {
                    $current = $news->files()->wherePivot('locale', $translation['locale'])->get();
                    $new_files = collect($translation['files']);
                    $diff = $new_files->diff($current->pluck('id')->toArray());

                    $added = $diff->each(function ($file_id) use ($news, $translation){
                        $news->files()->attach($file_id, ['locale' => $translation['locale']]);
                    });

                    $deleted = $news->files()
                        ->wherePivot('locale', $translation['locale'])
                        ->wherePivotNotIn('document_id', $new_files->toArray())
                        ->detach();
                } else {
                    $news->files()->wherePivot('locale', $translation['locale'])->detach();
                }
            }
        }
    }

    /**
     * Deletes a news from the database.
     *
     * @param  DeleteNews $request
     * @return array
     */
    public function deleteNews(DeleteNews $request)
    {
        $news = News::findOrFail($request->news);

        $seo = Seo::where(['class' => get_class($news), 'reference' => $news->id]);

        /* SitemapGenerator::create(url('/'))
            ->writeToFile(public_path('sitemap.xml')); */

        $news->deleteTranslations();

        // delete header image
        if ($news->headerImageModel != null) {
            $news->headerImageModel->delete();
        }

        // save gallery images
        if (count($news->imageModel) != 0) {
            $news->imageModel->each->delete();
        }

        return ['status' => $news->delete() && $seo->delete()];
    }

    /**
     * Edits a news with the given data.
     *
     * @param  EditNews $request
     * @return Lafactoria\Backend\Models\News
     */
    public function saveNews(EditNews $request)
    {
        $news = News::findOrFail($request->news['id']);

        $news->update([
            'active' => $request->news['active'],
            'slug' => $request->news['slug'],
            'display_after' => $request->news['display_after'],
        ]);

        $this->saveTranslations($news, $request);

        // update header image
        if ($request->news['headerImageModel'] != null) {
            HeaderImagemodel::updateOrCreate([
                'model_id' => $news->id,
                'model' => get_class($news)
            ],[
                'image_id' => $request->news['headerImageModel']
            ]);
        } else {
            if ($news->headerImageModel)
                $news->headerImageModel->delete();
        }

        // update gallery images
        if (count($request->news["imageModel"]) != 0) {
            if (count($news->imageModel) > 0) {
                $images = ImageModel::where([
                    'model_id' => $news->id,
                    'model' => get_class($news)
                ])->get();

                $images->whereNotIn('image_id', $request->news["imageModel"])->each->delete();
            }

            foreach ($request->news["imageModel"] as $image_id) {
                ImageModel::updateOrCreate([
                    'model_id' => $news->id,
                    'model' => get_class($news),
                    'image_id' => $image_id
                ],[
                    'image_id' => $image_id
                ]);
            }
        } else {
            if (count($news->imageModel) > 0) {
                $news->imageModel->each->delete();
            }
        }

        /* SitemapGenerator::create(url('/'))
            ->writeToFile(public_path('sitemap.xml')); */

        return $news;
    }

    /**
     * Returns the available tralsnations of the requested news.
     *
     * @param Request $request
     * @return Helper
     *
     */
    public function seotranslations(Request $request)
    {
        $news = $request->news ? News::findOrFail($request->news) : null;
        $trans = Helper::getModelLanguagesMerged($news);

        $files = isset($news) ? $news->files->groupBy('pivot.locale') : [];

        $trans = $trans->map(function ($tr) use ($files){
            $tr['files'] = isset($files[$tr['locale']]) ? collect($files[$tr['locale']])->pluck('id') : [];
            return $tr;
        });

        return $trans;
    }
}
