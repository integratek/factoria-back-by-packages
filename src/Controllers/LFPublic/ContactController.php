<?php

namespace Lafactoria\Backend\Controllers\LFPublic;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\Contact;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Models\Seo;
use Lafactoria\Backend\Requests\LFPublic\SendContact;

class ContactController extends Controller
{
    /**
     * Returns the contact view.
     *
     * @return Response
     */
    public function contact()
    {
        Seo::hreflang('contact', 1);
        return view(config('backend.public_views.contact'));
    }

    /**
     * Send a new contact email and stores it to the database.
     *
     * @return Response
     */
    public function send(SendContact $request)
    {
        Contact::create($request->only(['name', 'surname', 'email', 'phone', 'message']))->sendEmail();

        return redirect()
            ->route('contact')
            ->with('success', __('backend::public.contact_sent'));
    }
}
