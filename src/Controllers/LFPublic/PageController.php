<?php

namespace Lafactoria\Backend\Controllers\LFPublic;

use Illuminate\Http\Request;
use Lafactoria\Backend\Models\Menu;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Traits\LocalePath;
use Lafactoria\Backend\Models\Seo;

class PageController extends Controller
{
	use LocalePath;
    /**
     * Returns the public user-defined pages.
     *
     * @param  string $page
     * @return Response
     */
    public function page()
    {
        $actualPathWithoutLocale = $this->actualPathWithoutLocale();

    	$page = Menu::where('type', Menu::LAYOUT_TYPE)
            ->whereTranslation('url', '/'.$actualPathWithoutLocale)
            ->orWhereTranslation('url', $actualPathWithoutLocale)
            ->firstOrFail();

		Seo::translation($page);
        Seo::hreflang($page, $page->id);

        return view(config('backend.public_views.page'), compact('page'));
    }
}
