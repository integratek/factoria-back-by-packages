<?php

namespace Lafactoria\Backend\Controllers\LFPublic;

use Illuminate\Http\Request;
use Lafactoria\Backend\Controllers\Controller;
use Lafactoria\Backend\Models\Seo;

class ViewController extends Controller
{
    /**
     * Returns the contact view.
     *
     * @return Response
     */
    public function cookies()
    {
        Seo::hreflang('cookies', 1);
        return view(config('backend.public_views.cookies'));
    }

    /**
     * Returns the contact view.
     *
     * @return Response
     */
    public function legal()
    {
        Seo::hreflang('legal', 1);
        return view(config('backend.public_views.legal'));
    }

    /**
     * Returns the contact view.
     *
     * @return Response
     */
    public function privacy()
    {
        Seo::hreflang('privacy', 1);
        return view(config('backend.public_views.privacy'));
    }
}
