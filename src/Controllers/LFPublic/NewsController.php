<?php

namespace Lafactoria\Backend\Controllers\LFPublic;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Lafactoria\Backend\Models\Seo;
use Lafactoria\Backend\Models\News;
use Lafactoria\Backend\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Show all the news
     *
     * @return Response
     */
    public function all()
    {
        $news = News::where('active', true)
            ->whereDate('display_after', '<', Carbon::now())
            ->orderBy('id', 'desc')
            ->paginate(config('backend.pagination.frontend-news'));

        return view(config('backend.public_views.all_news'), compact('news'));
    }

    /**
     * Returns the news page.
     *
     * @return Response
     */
    public function news($locale, $slug)
    {
        if (!$slug)
            return redirect('404');

        $news = News::where('slug', $slug)->firstOrFail();

        $translation = Seo::translation($news);
        Seo::hreflang($news, $news->id);

        $errors = collect();
        return view(config('backend.public_views.news'), compact('news', 'translation', 'errors'));
    }
}
