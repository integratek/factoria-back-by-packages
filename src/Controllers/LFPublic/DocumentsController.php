<?php

namespace Lafactoria\Backend\Controllers\LFPublic;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Lafactoria\Backend\Models\Document;
use Lafactoria\Backend\Controllers\Controller;

class DocumentsController extends Controller
{
    /**
     * Downloads or preview the document as a file.
     *
     * @param  Document $document
     * @return Response
     */
    public function download(Document $document)
    {
        if (config('backend.s3')) {
            $file = Storage::disk('s3')->get("app/documents/{$document->file}");
            $type = Storage::disk('s3')->mimeType("app/documents/{$document->file}");

            return response()->file($file, [
                'Content-Type' => $type
            ]);
        } else {
            $type = Storage::mimeType("documents/{$document->file}");
    
            return response()->file(storage_path("app/documents/{$document->file}"), [
                'Content-Type' => $type
            ]);
        }

        /*return response()
            ->download(storage_path("app/documents/{$document->file}"));*/
    }
}
