<?php

return [
    'banners' => 'Banners',
    'banners_sub' => 'Consultad y gestionad los banners de la aplicación',
    'create_banner' => 'Crear banner',
    'create_banner_sub' => 'Crea un nuevo banner',
    'banner_created' => 'Banner creado',
    'banner_deleted' => 'Banner eliminado',
    'edit_banner' => 'Edita banner',
    'edit_banner_sub' => 'Editar la información del banner',
    'banner_updated' => 'Banner actualizado',
    'save' => 'Guardar',
    'create' => 'Crear',
    'name' => 'Nombre',
    'description' => 'Descripción',
    'active' => 'Activo',
    'created_at' => 'Fecha de creación',
    'actions' => 'Acciones',
    'no_image' => 'Sin imagen',
    'image' => 'Imagen',
    'url' => 'URL',
    'yes' => 'Si',
    'no' => 'No',
    'confirm_delete' => '¿Estás seguro?',
    'delete_yes' => 'OK',
    'delete_no' => 'CANCELAR'
];
