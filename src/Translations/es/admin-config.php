<?php

return [
    'configuration' => 'Configuración',
    'configuration_sub' => 'Editad las preferencias de vuestro sitio web',
    'name' => 'Nombre',
    'phone' => 'Teléfono',
    'email' => 'E-mail',
    'adress' => 'Dirección',
    'city' => 'Ciudad',
    'country' => 'País',
    'tags' => 'Etiquetas',
    'description' => 'Descripción',
    'scripts' => 'Scripts adicionales',
    'styles' => 'Estilos adicionales',
    'config_updated' => 'Se ha actualizado la configuración',
    'edit_configuration' => 'Edita la configuración',
    'save' => 'Guarda la configuración',
    'mail_configured' => 'La información del e-mail está configurada correctamente, enviando com a: <b>:name</b>',
    'mail_not_configured' => 'La información del e-mail <b>no está</b> configurada, poneos en contacto con el administrador',
    'mail_information' => 'Información del e-mail',
];
