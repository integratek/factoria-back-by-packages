<?php

return [
    'contact' => 'Contactar',
    'contact_requests' => 'Solicitud de contacto',
    'contact_requests_sub' => 'Revisad todas las solicitudes de contacto de vuestra aplicación',
    'name' => 'Nombre',
    'email' => 'E-mail',
    'phone' => 'Teléfono',
    'surname' => 'Apellido',
    'message' => 'Mensaje',
    'created_at' => 'Fecha de creación',
    'actions' => 'Acciones',
    'contact_request_info' => 'Información de solicitud de contacto',
    'contact_request_info_sub' => 'Revisad la información de la solicitud de contacto',
    'captcha_required' => 'Verificad que no sois un robot.',
    'captcha_error' => "¡Error Captcha! Volverlo a intentar más tarde o poneos en contacto con el administrador de la web.",
    'contact_deleted' => 'Contacto eliminado'
];
