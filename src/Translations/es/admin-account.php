<?php

return [
    'account_settings' => 'Configuración de la cuenta',
    'account_settings_sub' => 'Consultad y editad toda la información de vuestra cuenta',
    'wrong_current_password' => 'La contraseña actual no es correcta',
    'account_updated' => 'Se debe actualizar la información de la cuenta',
    'name' => 'Nombre',
    'email' => 'E-mail',
    'leave_blank' => 'Dejadlo en blanco para ignorar',
    'new_password' => 'Nueva contraseña',
    'new_password_r' => 'Repite nueva contraseña',
    'current_password' => 'Contraseña actual',
    'save' => 'Guardar',
];
