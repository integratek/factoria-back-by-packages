<?php

return [
    'dashboard' => 'Panel de control',
    'dashboard_sub' => 'Bienvenido de nuevo, :user',
    'users' => 'Usuarios',
    'news' => 'Notícias',
    'contacts' => 'Contactos',
    'last_users' => 'Usuarios recientes de la última semana',
    'last_news' => 'Notícias recientes de la última semana',
    'last_contacts' => 'Solicitudes recientes de contacto de la última semana',
    'menus' => 'Menús',
    'images' => 'Imágenes',
    'documents' => 'Documentos',
    'slides' => 'Slides',
    'banners' => 'Banners',
];
