<?php

return [
    'sitemap_updated' => 'Sitemap actualizado',
    'login' => 'Iniciar sesión',
    'login_sub' => 'Introducid vuestras credenciales',
    'email' => 'E-mail',
    'password' => 'Contraseña',
    'remember_me' => 'Recuérdame',
    'successful_login' => 'Inicio de sesión con éxito',
    'invalid_credentials' => 'Credenciales inválidas',
    'logout_complete' => 'Sesión cerrada correctamente',
    'requires_authentication' => "No estáis autorizados, primero debéis iniciar sesión",
    'actions' => 'Acciones',
    'logout' => 'Cerrar sesión',
    'account_settings' => 'Configuración de la cuenta',
    'password_update_required' => 'Actualización de la contraseña requerida',
    'new_password' => 'Nueva contraseña',
    'repeat_password' => 'Repite la contraseña',
    'update_password' => 'Actualiza la contraseña',
    'reset_password' => 'Reestablece la contraseña',
    'reset_password_message' => 'Haz click en el siguiente botón para reestablecer tu contraseña.',
    'reset_token_expired' => 'El correo ha caducado, vuélvelo a intentar',
    'sended_correctly' => 'Enviado correctamente',
    'updated_correctly' => 'Actualizado correctamente'
];
