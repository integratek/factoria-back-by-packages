<?php

return [
    'login' => 'Iniciar sesión',
    'register' => 'Registrar',
    'logout' => 'Cerrar sesión',
    'contact' => 'Contactar',
    'send' => 'Enviar',
    'name' => 'Nombre',
    'surname' => 'Apellido',
    'email' => 'E-mail',
    'phone' => 'Teléfono',
    'message' => 'Mensaje',
    'captcha' => 'Captcha',
    'contact_sent' => 'Se ha mandado el mensaje de contacto',
    'contact_request' => 'Solicitud de contacto',
    'contact_request_info' => ":name (:email) ha enviado una nueva solicitud de contacto, podéis revisarla en la aplicación haciendo clic en el siguiente botón:",
    'review_message' => 'Revisa el mensaje',
    'thanks' => 'Gracias',
    'yeah' => '¡Correcto!',
    'whops' => '¡Whops!',
    'accept' => 'Aceptar',
    'more_info' => 'Más info',
    'error_title' => '¡Ops! Error',
    'error_subtitle' => 'La página que buscas no existe',
    'error_return' => 'Vuelve a la página de inicio',
    'date' => 'Fecha',
    'export' => 'Exportar'
];
