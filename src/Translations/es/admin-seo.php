<?php

return [
    'alt' => 'Texto alternativo',
    'SEO' => 'SEO',
    'title' => 'Título',
    'canonical' => 'Canonical',
    'description' => 'Descripción',
    'indexable' => 'Indexable / sitemap',
    'indexable_no' => 'No indexable / sitemap',
    'follow' => 'Follow',
    'follow_no' => 'No follow',
    'tags' => 'Etiquetas',
    'locale' => 'Idioma',
    'close' => 'Cerrar',
    'save' => 'Guardar',
    'saved' => '¡Guardado!',
    'sitemap_enabled' => 'Sitemap habilitado',
    'sitemap_disabled' => 'Sitemap deshabilitado'
];
