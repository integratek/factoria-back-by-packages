<?php

return [
    'news' => 'Noticias',
    'news_sub' => 'Cread o modificad las novedades existentes de vuestra web',
    'no_news' => 'No hay ninguna noticia',
    'start_by_creatin' => 'Empezad creando algunas noticias...',
    'start_by_creatin_desc' => "El mejor modo de empezar consiste en introducir algunas noticias. Podéis hacer clic al botón siguiente para crear vuestra primera noticia. Una vez creada vuestra primera entrada, podréis verla aquí.",
    'create_new' => 'Crea tu primera noticia',
    'create_news' => 'Crea noticias',
    'create_news_sub' => 'Cread noticias para vuestra web',
    'create' => 'Crear',
    'title' => 'Título',
    'author' => 'Autor',
    'created_at' => 'Fecha de creación',
    'actions' => 'Acciones',
    'description' => 'Descripción',
    'content' => 'Contenido',
    'image' => 'Imagen principal',
    'more_images' => 'Más imágenes',
    'slug' => 'Slug',
    'active' => 'Activo',
    'display_after' => 'Muestra después de la fecha',
    'autoslug' => 'Autoslug  (Crea URL automáticamente del título)',
    'read_more' => 'Leer más',
    'save' => 'Guardar',
    'delete' => 'Eliminar',
    'news_created' => 'Habéis creado una entrada nueva',
    'news_deleted' => 'Se han eliminado las noticias seleccionadas',
    'edit_news' => 'Editar noticias',
    'edit_news_sub' => 'Editad la información de las noticias',
    'news_edited' => 'Noticias guardadas',
    'new_edited' => 'Noticia guardada',
    'published_by' => 'Publicado por<b>:name</b> :time',
    'yes' => 'Si',
    'no' => 'No',
    'show-html' => 'Mostrar HTML',
    'confirm_delete' => '¿Estás seguro?',
    'delete_yes' => 'OK',
    'delete_no' => 'CANCELAR'
];
