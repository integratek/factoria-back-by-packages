<?php

return [
    'general_updated' => 'Se ha actualizado la configuración general',
    'cookies' => 'Cookies',
    'legal' => 'Legal',
    'privacy' => 'Política de privacidad',
    'cookies_description' => 'Descripción de las cookies',
    'save' => 'Guardar',
    'general' => 'General',
    'general_sub' => 'Consultad y editad la configuración general',
    'title' => 'Título',
    'text' => 'Contenido',
    'edit_general' => 'Editar elemento general',
    'create_general' => 'Crear elemento general',
    'general_deleted' => 'Elemento general eliminado',
    'cookies_head_script' => 'Script de la cabecera de Cookiebot',
    'cookies_declaration_script' => 'Script de la Declaración de Cookies',
    'cookies_script' => 'Script'
];
