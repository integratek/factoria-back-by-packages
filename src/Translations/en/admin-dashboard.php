<?php

return [
    'dashboard' => 'Dashboard',
    'dashboard_sub' => 'Welcome back, :user',
    'users' => 'Users',
    'news' => 'News',
    'contacts' => 'Contacts',
    'last_users' => 'Latest users in the last week',
    'last_news' => 'Latest news in the last week',
    'last_contacts' => 'Latest contacts in the last week',
    'menus' => 'Menus',
    'images' => 'Images',
    'documents' => 'Documents',
    'slides' => 'Slides',
    'banners' => 'Banners',
];
