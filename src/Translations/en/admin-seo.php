<?php

return [
    'alt' => 'Alternate text',
    'SEO' => 'Search engine optimization',
    'title' => 'Title',
    'canonical' => 'Canonical',
    'description' => 'Description',
    'indexable' => 'Indexable / sitemap',
    'indexable_no' => 'No indexable / sitemap',
    'follow' => 'Follow',
    'follow_no' => 'No follow',
    'tags' => 'Tags',
    'locale' => 'Locale',
    'close' => 'Close',
    'save' => 'Save',
    'saved' => 'Saved!',
    'sitemap_enabled' => 'Sitemap enabled',
    'sitemap_disabled' => 'Sitemap disabled'
];
