<?php

return [
    'configuration' => 'Configuration',
    'configuration_sub' => 'Edit your website preferences',
    'name' => 'Application Name',
    'phone' => 'Phone',
    'email' => 'Email',
    'adress' => 'Adress',
    'city' => 'City',
    'country' => 'Country',
    'tags' => 'Tags',
    'description' => 'Description',
    'scripts' => 'Additional scripts',
    'styles' => 'Additional styles',
    'config_updated' => 'Configuration updated',
    'edit_configuration' => 'Edit configuration',
    'save' => 'Save configuration',
    'mail_configured' => 'The email information is configured correctly, sending as: <b>:name</b>',
    'mail_not_configured' => 'The email information is <b>not</b> configured, please contact the administrator',
    'mail_information' => 'Email information',
];
