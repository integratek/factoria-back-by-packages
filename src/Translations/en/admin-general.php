<?php

return [
    'general_updated' => 'The general settings have been updated',
    'cookies' => 'Cookies',
    'legal' => 'Legal',
    'privacy' => 'Privacy policy',
    'cookies_description' => 'Cookies description',
    'save' => 'Save',
    'general' => 'General',
    'general_sub' => 'View and edit the general configuration',
	'title' => 'Title',
	'text' => 'Content',
    'edit_general' => 'Edit general element',
    'create_general' => 'Create general element',
    'general_deleted' => 'General element deleted',
    'cookies_head_script' => 'Header script from Cookiebot',
    'cookies_declaration_script' => 'Cookies Declaration script from Cookiebot',
    'cookies_script' => 'Script'
];
