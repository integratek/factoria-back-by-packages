<?php

return [
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'contact' => 'Contact',
    'send' => 'Send',
    'name' => 'Name',
    'surname' => 'Surname',
    'email' => 'Email',
    'phone' => 'Phone',
    'message' => 'Message',
    'captcha' => 'Captcha',
    'contact_sent' => 'The contact message has been sent',
    'contact_request' => 'Contact Request',
    'contact_request_info' => ":name (:email) sent a new contact request, you may review it in the application pressing the button below:",
    'review_message' => 'Review message',
    'thanks' => 'Thanks',
    'yeah' => 'Yeah!',
    'whops' => 'Whops!',
    'accept' => 'Accept',
    'more_info' => 'More info',
    'date' => 'Date',
    'export' => 'Export'
];
