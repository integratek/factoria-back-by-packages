<?php

return [
    'account_settings' => 'Account settings',
    'account_settings_sub' => 'View and edit all your account information',
    'wrong_current_password' => 'The current password is not correct',
    'account_updated' => 'The account information has been updated',
    'name' => 'Name',
    'email' => 'Email',
    'leave_blank' => 'Leave blank to ignore',
    'new_password' => 'New password',
    'new_password_r' => 'Repeat new password',
    'current_password' => 'Current password',
    'save' => 'Save',
];
