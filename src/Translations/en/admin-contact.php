<?php

return [
    'contact' => 'Contact',
    'contact_requests' => 'Contact Requests',
    'contact_requests_sub' => 'Review all the contact requests in your application',
    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'surname' => 'Surname',
    'message' => 'Message',
    'created_at' => 'Creation Date',
    'actions' => 'Actions',
    'contact_request_info' => 'Contact request information',
    'contact_request_info_sub' => 'Review the information of the contact request',
    'captcha_required' => 'Please verify that you are not a robot.',
    'captcha_error' => 'Captcha error! try again later or contact site admin.',
    'contact_deleted' => 'Contact deleted'
];
