<?php

return [
    'sitemap_updated' => 'Sitemap updated',
    'login' => 'Login',
    'login_sub' => 'Please enter your credentials',
    'email' => 'Email',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'successful_login' => 'Successfull login',
    'invalid_credentials' => 'Invalid credentials',
    'logout_complete' => 'Logout successfully',
    'requires_authentication' => "You're not authenticated, please login first",
    'actions' => 'Actions',
    'logout' => 'Logout',
    'account_settings' => 'Account settings',
    'password_update_required' => 'Password update required',
    'new_password' => 'New password',
    'repeat_password' => 'Repeat password',
    'update_password' => 'Update password',
    'reset_password' => 'Resest password',
    'reset_password_message' => 'Click on the button to reset your password.',
    'reset_token_expired' => 'The email expired, please try again',
    'sended_correctly' => 'Sended correctly',
    'updated_correctly' => 'Updated correctly'
];
