<?php

return [
    'slides' => 'Slides',
    'slides_sub' => 'Consulteu i gestioneu els slides de l\'aplicació',
    'create_slide' => 'Crear slide',
    'create_slide_sub' => 'Crear un nou slide',
    'slide_created' => 'Slide creat',
    'slide_deleted' => 'Slide eliminat',
    'edit_slide' => 'Editar slide',
    'edit_slide_sub' => 'Editeu la informació del slide',
    'slide_updated' => 'Slide actualitzat',
    'save' => 'Guardar',
    'create' => 'Crear',
    'name' => 'Nom',
    'description' => 'Descripció',
    'active' => 'Actiu',
    'created_at' => 'Data de creació',
    'actions' => 'Accions',
    'select_image' => 'Sel·lecciona una imatge',
    'image' => 'Imatge',
    'url' => 'URL',
    'new_tab' => 'Obre en una pestanya nova',
    'yes' => 'Si',
    'no' => 'No',
    'confirm_delete' => 'Estàs segur?',
    'delete_yes' => 'OK',
    'delete_no' => 'CANCEL·LAR',
    'mp4' => 'mp4',
    'webm' => 'webm',
    'video_name' => 'Nom del vídeo',
    'select_video' => 'Selecciona vídeo'
];
