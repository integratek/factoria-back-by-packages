<?php

return [
    'configuration' => 'Configuració',
    'configuration_sub' => 'Editeu les preferències del vostre lloc web',
    'name' => 'Nom',
    'phone' => 'Telèfon',
    'email' => 'E-mail',
    'adress' => 'Adreça',
    'city' => 'Ciutat',
    'country' => 'País',
    'tags' => 'Etiquetes',
    'description' => 'Descripció',
    'scripts' => 'Scripts addicionals',
    'styles' => 'Estils addicionals',
    'config_updated' => 'S\'ha actualitzat la configuració',
    'edit_configuration' => 'Edita la configuració',
    'save' => 'Desa la configuració',
    'mail_configured' => 'La informació de l\'e-mail està configurada correctament, enviant com a: <b>:name</b>',
    'mail_not_configured' => 'La informació de l\'e-mail <b>no està</b> configurada, poseu-vos en contacte amb l\'administrador',
    'mail_information' => 'Informació de l\'e-mail',
];
