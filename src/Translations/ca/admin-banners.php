<?php

return [
    'banners' => 'Banners',
    'banners_sub' => 'Consulteu i gestioneu els banner de l\'aplicació',
    'create_banner' => 'Crear banner',
    'create_banner_sub' => 'Crea un nou banner',
    'banner_created' => 'Banner creat',
    'banner_deleted' => 'Banner eliminat',
    'edit_banner' => 'Edita banner',
    'edit_banner_sub' => 'Editeu la informació del banner',
    'banner_updated' => 'Banner actualitzat',
    'save' => 'Guardar',
    'create' => 'Crear',
    'name' => 'Nom',
    'description' => 'Descripció',
    'active' => 'Actiu',
    'created_at' => 'Data de creació',
    'actions' => 'Accions',
    'no_image' => 'Sense imatge',
    'image' => 'Imatge',
    'url' => 'URL',
    'yes' => 'Si',
    'no' => 'No',
    'confirm_delete' => 'Estàs segur?',
    'delete_yes' => 'OK',
    'delete_no' => 'CANCEL·LAR'
];
