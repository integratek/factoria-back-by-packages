<?php

return [
    'sitemap_updated' => 'Sitemap actualitzat',
    'login' => 'Iniciar sessió',
    'login_sub' => 'Introduïu les vostres credencials',
    'email' => 'E-mail',
    'password' => 'Contrasenya',
    'remember_me' => 'Recorda\'m',
    'successful_login' => 'Inici de sessió amb èxit',
    'invalid_credentials' => 'Credencials invàlides',
    'logout_complete' => 'Sessió tancada correctament',
    'requires_authentication' => "No esteu autoritzats, primer heu d'iniciar sessió",
    'actions' => 'Accions',
    'logout' => 'Tancar sessió',
    'account_settings' => 'Configuració del compte',
    'password_update_required' => 'Actualització de la contrasenya requerida',
    'new_password' => 'Nova contrasenya',
    'repeat_password' => 'Repeteix la contrasenya',
    'update_password' => 'Actualitza la contrasenya',
    'reset_password' => 'Reestableix la contrasenya',
    'reset_password_message' => 'Fes click en el següent botó per a reestablir la teva contrasenya.',
    'reset_token_expired' => 'El correu ha caducat, torneu-ho a intenar',
    'sended_correctly' => 'Enviat correctament',
    'updated_correctly' => 'Actualitzat correctament'
];
