<?php

return [
    'login' => 'Iniciar sessió',
    'register' => 'Registrar',
    'logout' => 'Tancar sessió',
    'contact' => 'Contactar',
    'send' => 'Enviar',
    'name' => 'Nom',
    'surname' => 'Cognom',
    'email' => 'E-mail',
    'phone' => 'Telèfon',
    'message' => 'Missatge',
    'captcha' => 'Captcha',
    'contact_sent' => 'S\'ha enviat el missatge de contacte',
    'contact_request' => 'Sol·licitud de contacte',
    'contact_request_info' => ":name (:email) ha enviat una nova sol·licitud de contacte, podeu revisar-la a l'aplicació prement el botó de sota:",
    'review_message' => 'Repassa el missatge',
    'thanks' => 'Gràcies',
    'yeah' => 'Correcte!',
    'whops' => 'Whops!',
    'accept' => 'Acceptar',
    'more_info' => 'Més info',
    'error_title' => 'Ops! Error ',
    'error_subtitle' => 'La pàgina que busques no existeix',
    'error_return' => 'Torna a la pàgina d\'inici',
    'date' => 'Data',
    'export' => 'Exportar'
];
