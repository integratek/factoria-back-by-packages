<?php

return [
    'general_updated' => 'S\'ha actualitzat la configuració general',
    'cookies' => 'Cookies',
    'legal' => 'Legal',
    'privacy' => 'Política de privacitat',
    'cookies_description' => 'Descripció de les cookies',
    'save' => 'Guardar',
    'general' => 'General',
    'general_sub' => 'Consulteu i editeu la configuració general',
    'title' => 'Títol',
    'text' => 'Contingut',
    'edit_general' => 'Editar element general',
    'create_general' => 'Crear element general',
    'general_deleted' => 'Element general eliminat',
    'cookies_head_script' => 'Script de la capçalera de Cookiebot',
    'cookies_declaration_script' => 'Script de la Declaració de Cookies',
    'cookies_script' => 'Script'
];
