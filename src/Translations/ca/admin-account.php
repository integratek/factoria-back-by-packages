<?php

return [
    'account_settings' => 'Configuració del compte',
    'account_settings_sub' => 'Consulteu i editeu tota la informació del vostre compte',
    'wrong_current_password' => 'La contrasenya actual no és correcta',
    'account_updated' => 'S\'ha actualitzat la informació del compte',
    'name' => 'Nom',
    'email' => 'E-mail',
    'leave_blank' => 'Deixeu-ho en blanc per ignorar',
    'new_password' => 'Nova contrasenya',
    'new_password_r' => 'Repeteix nova contrasenya',
    'current_password' => 'Contrasenya actual',
    'save' => 'Guardar',
];
