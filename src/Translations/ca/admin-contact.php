<?php

return [
    'contact' => 'Contactar',
    'contact_requests' => 'Sol·licitud de contacte',
    'contact_requests_sub' => 'Reviseu totes les sol·licituds de contacte de la vostra aplicació',
    'name' => 'Nom',
    'email' => 'E-mail',
    'phone' => 'Telèfon',
    'surname' => 'Cognom',
    'message' => 'Missatge',
    'created_at' => 'Data de creació',
    'actions' => 'Accions',
    'contact_request_info' => 'Informació de sol·licitud de contacte',
    'contact_request_info_sub' => 'Reviseu la informació de la sol·licitud de contacte',
    'captcha_required' => 'Verifiqueu que no sou un robot.',
    'captcha_error' => "Error Captcha! Torna-ho a provar més tard o poseu-vos en contacte amb l'administrador del web.",
    'contact_deleted' => 'Contacte eliminat'
];
