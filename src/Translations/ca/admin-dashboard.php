<?php

return [
    'dashboard' => 'Panell de control',
    'dashboard_sub' => 'Benvingut de nou, :user',
    'users' => 'Usuaris',
    'news' => 'Notícies',
    'contacts' => 'Contactes',
    'last_users' => 'Últims usuaris de la darrera setmana',
    'last_news' => 'Últimes notícies de la darrera setmana',
    'last_contacts' => 'Últimes sol·licituds de contacte de la darrera setmana',
    'menus' => 'Menús',
    'images' => 'Imatges',
    'documents' => 'Documents',
    'slides' => 'Slides',
    'banners' => 'Banners',
];
