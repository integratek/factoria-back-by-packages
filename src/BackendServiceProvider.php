<?php

namespace Lafactoria\Backend;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Lafactoria\Backend\Traits\Helper;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        config(['auth.guards' => array_merge(config('auth.guards'), [
            'admin' => [
                'driver' => 'session',
                'provider' => 'admins',
            ],
        ])]);
        config(['auth.providers' => array_merge(config('auth.providers'), [
            'admins' => [
                'driver' => 'eloquent',
                'model' => \Lafactoria\Backend\Models\Admin::class,
            ],
        ])]);

        $router->aliasMiddleware('auth.admin', \Lafactoria\Backend\Middleware\AuthAdmin::class);
        $router->aliasMiddleware('admin.features', \Lafactoria\Backend\Middleware\AdminFeatures::class);
        $router->aliasMiddleware('locale', \Lafactoria\Backend\Middleware\Locale::class);

        $this->publishes([
            __DIR__.'/Config/backend.php' => config_path('backend.php'),
        ], 'backend_config');

        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');

        $this->loadMigrationsFrom(__DIR__ . '/Migrations');

        $this->loadTranslationsFrom(__DIR__ . '/Translations', 'backend');

        $this->publishes([
            __DIR__ . '/Translations' => resource_path('lang/vendor/backend'),
        ], 'backend_translations');

        $this->loadViewsFrom(__DIR__ . '/Views', 'backend');

        $this->publishes([
            __DIR__ . '/Views' => resource_path('views/vendor/backend'),
        ], 'backend_views');

        $this->publishes([
            __DIR__ . '/Assets' => resource_path('assets'),
        ], 'backend_assets');

        $this->publishes([
            __DIR__ . '/PublicAssets' => public_path(),
        ], 'backend_public_assets');

        $this->publishes([
            __DIR__ . '/Exceptions' => app_path('Exceptions'),
        ], 'backend_exceptions');

        if ($this->app->runningInConsole()) {
            $this->commands([
                \Lafactoria\Backend\Commands\BackendCommand::class,
            ]);
        }

        View::share('locales', config('translatable.locales'));
        View::share('translations_images', Helper::getFileTranslations('admin-images'));
        View::share('translations_seo', Helper::getFileTranslations('admin-seo'));
        View::share('translations_docs', Helper::getFileTranslations('admin-documents'));
        View::share('languages', Helper::languages());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/Config/backend.php',
            'backend'
        );
    }
}
