<?php

namespace Lafactoria\Backend\Middleware;

use Closure;
use Lafactoria\Backend\Models\Config;

class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->guard('admin')->check()) {
            return redirect()
                ->route('admin.login')
                ->with('error', __('backend::admin-auth.requires_authentication'));
        } else{
            if(config('backend.password_caducity') > 0){
                // Check password caducity
                if(auth()->guard('admin')->user()->passwordUpdatedDays() >= config('backend.password_caducity')){
                    return redirect()
                        ->route('admin.login')
                        ->with('password_update', __('backend::admin-auth.password_update_required'));
                }    
            }
        }

        return $next($request);
    }
}
