<?php

namespace Lafactoria\Backend\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Lafactoria\Backend\Models\Admin;
use Lafactoria\Backend\Models\Menu;
use Lafactoria\Backend\Models\Config;
use Illuminate\Support\Facades\Route;
use Lafactoria\Backend\Models\General;

class AdminFeatures
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        collect(Config::first())->each(function ($item, $key) {
            if ($item) {
                config(['backend.config.' . $key => $item]);
            }
        });

        collect(General::all())->each(function ($item) {
            if ($item) {
                $key = $item->id;
                switch ($item->id) {
                    case 1:
                        $key = 'cookies';
                        break;
                    case 2:
                        $key = 'cookies_description';
                        break;
                    case 3:
                        $key = 'legal';
                        break;
                    case 4:
                        $key = 'privacy';
                        break;
                }
                config(['backend.general.' . $key => $item]);
            }
        });

        // CookieBot
        $cookieBotHeader = config('backend.general.cookies_description')->text;
        $pos = strpos($cookieBotHeader, 'src="');
        if ($pos != -1) {
            $cookieBotHeader = substr_replace($cookieBotHeader, 'data-culture="'.app()->getLocale().'" ', $pos, 0);
        } else {
            $cookieBotHeader = null;
        }
        config(['backend.cookie_bot_header' => $cookieBotHeader]);

        $cookieBotPage = config('backend.general.cookies')->text;
        $pos = strpos($cookieBotPage, 'src="');
        if ($pos != -1) {
            $cookieBotPage = substr_replace($cookieBotPage, 'data-culture="'.app()->getLocale().'" ', $pos, 0);
        } else {
            $cookieBotPage = null;
        }
        config(['backend.cookie_bot_page' => $cookieBotPage]);


        config(['backend.menus' => Menu::parents()]);

        if (config('backend.config.name')) {
            config(['app.name' => config('backend.config.name')]);
            config(['mail.from.name' => config('backend.config.name')]);
        }

        config([
            'backend.menu' => collect(config('backend.menu'))->map(function ($menu) {
                $menu['name'] = (__($menu['name']) == $menu['name']) ? $menu['name'] : __($menu['name']);
                if (isset($menu['url']))
                    $menu['url'] = (Route::has($menu['url'])) ? route($menu['url']) : url($menu['url']);
                return $menu;
            }),
        ]);

        // filter admin permissions by role
        $admin_user = auth('admin')->user();
        $route_name = $request->route()->getAction('as');

        if ($admin_user && $admin_user->role_id == Admin::$ROLE_EDITOR && !Str::contains($route_name, 'dashboard') && !Str::contains($route_name, 'logout')  && !Str::contains($route_name, 'seo')) {
            // filter languages
            $admin_langs = $admin_user->languages;
            config(['translatable.locales' => $admin_langs]);

            // filter index, create and delete routes
            $uri = $request->route()->uri;
            $uri_segments = Str::replaceFirst('admin/', '', $uri); // remove first item of uri segments (admin segment)
            $admin_role_editor_segments = config('backend.admin_role_editor_segments');

            $is_edit = Str::contains($route_name, 'edit');
            $is_create = Str::contains($route_name, 'create');
            $is_delete = Str::contains($route_name, 'delete');

            if ($is_create || $is_delete) {
                // create or delete
                return redirect()->to('/admin');
            } else if ($is_edit) {
                // edit
                $start_pos = strpos($uri_segments, '/{');
                $end_pos = strpos($uri_segments, '}');
                $uri_segments = substr_replace($uri_segments, '', $start_pos, $end_pos); // remove parameter string

                if (Str::contains($uri_segments, '/edit')) {
                    $txt = '/edit';
                    $start_pos = strpos($uri_segments, $txt);
                    $end_pos = strlen($txt);
                    $uri_segments = substr_replace($uri_segments, '', $start_pos, $end_pos); // remove edit string if exists
                }

                if (!in_array($uri_segments, $admin_role_editor_segments)) {
                    return redirect()->to('/admin');
                }
            }  else if (!$is_edit && !$is_create && !$is_delete && !in_array($uri_segments, $admin_role_editor_segments)) {
                // index or api
                if (Str::contains($uri_segments, 'api')) { // api case
                    // check if the module is inside available segments
                    $available = false;
                    collect($admin_role_editor_segments)->each(function ($seg) use ($uri_segments, &$available){
                        if (!$available && Str::contains($uri_segments, $seg)) {
                            $available = true;
                            return;
                        }
                    });

                    if (!$available)
                        return redirect()->to('/admin');
                } else // index case
                    return redirect()->to('/admin');
            }
        }

        return $next($request);
    }
}
