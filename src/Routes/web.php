<?php


Route::namespace('Lafactoria\Backend\Controllers')->middleware('web')->group(function (){

    $locales = 'admin.features'; // empty middleware is not working so.
    $prefix = '';

    /*
    |--------------------------------------------------------------------------
    | Dynamic route definitions
    |--------------------------------------------------------------------------
    |
    | Dynamic route definition created to register the routes defined in the
    | administration panel, on the menu module as LAYOUT type and in the news module.
    |
    */

    /*
    Route::middleware('admin.features')->namespace('LFPublic')->group(function () use ($prefix, $locales) {
        Route::middleware($locales)->prefix($prefix)->group(function () {
            try {
                if (Schema::hasTable(with(new Lafactoria\Backend\Models\Menu)->getTable())) {
                    foreach (Lafactoria\Backend\Models\Menu::where(['type' => Lafactoria\Backend\Models\Menu::LAYOUT_TYPE])->get() as $page) {
                        Route::get($page->url, 'PageController@page')->name(Lafactoria\Backend\Models\Menu::ROUTE_PREFIX . "_{$page->id}");
                    }
                };
                if (Schema::hasTable(with(new Lafactoria\Backend\Models\News)->getTable())) {
                    foreach (Lafactoria\Backend\Models\News::whereDate('display_after', '<', Carbon\Carbon::now())->get() as $news) {
                        Route::get(Lafactoria\Backend\Models\News::generateUrl($news), 'NewsController@news')->name(Lafactoria\Backend\Models\News::ROUTE_PREFIX . "_{$news->id}");
                    }
                }

            } catch (Exception $e) {
            }
        });
    });
    */

	/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	| Note: It also contains the admin.features middleware that enables
	| the administration configuration to be set up.
	|
	*/
	Route::namespace('Admin\Documents')->prefix('documents')->group(function () {
		Route::get('/download/{document?}', 'DocumentsController@download')->name('documents.public.download');
	});


	/*
	|--------------------------------------------------------------------------
	| Administration routes
	|--------------------------------------------------------------------------
	|
	| All the administration routes will be defined here, any route unreleated
	| to the administration should NOT be defined here.
	|
	*/
Route::as('admin.')->middleware('admin.features')->prefix('admin')->namespace('Admin')->group(function () {
	Route::namespace('Auth')->group(function () {
		Route::view('login', 'backend::admin.auth.login')->name('login');
		Route::post('login', 'AuthController@login');
		Route::post('updatePassword', 'AuthController@updatePassword')->name('updatePassword');
		Route::post('sendResetPassword', 'AuthController@sendResetPassword')->name('sendResetPassword');
		Route::get('resetPasswordView/{admin}', 'AuthController@resetPasswordView')->name('resetPassword.view');
		Route::post('resetPassword', 'AuthController@resetPassword')->name('resetPassword');
	});
	Route::middleware('auth.admin')->group(function () {
		Route::post('logout', 'Auth\AuthController@logout')->name('logout');
		Route::prefix('api')->post('flash', 'APIController@flash')->name('flash');
		Route::prefix('api')->post('trans', 'APIController@trans')->name('trans');
		Route::post('/sitemap_generate', 'APIController@crawlsite')->name('sitemap.generate');

		/*
		|--------------------------------------------------------------------------
		| Account module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the account module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Account')->group(function () {
			Route::view('account', 'backend::admin.account.edit')->name('account');
			Route::post('account', 'AccountController@update');
			Route::as('api.')->prefix('api')->get('account/avatar', 'APIController@avatar')->name('avatar');
		});

		/*
		|--------------------------------------------------------------------------
		| Dashboard module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the dashboard module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Dashboard')->group(function () {
			Route::get('/', 'DashboardController@index')->name('dashboard');
		});

		/*
		|--------------------------------------------------------------------------
		| Users module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the users module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		/*Route::namespace('Users')->prefix('users')->group(function () {
			Route::view('/', 'backend::admin.users.index')->name('users');
			Route::view('/create', 'backend::admin.users.create')->name('users.create');
			Route::view('/edit/{user}', 'backend::admin.users.edit')->name('users.edit');
			Route::as('api.')->prefix('api/users')->group(function () {
				Route::get('/', 'APIController@users')->name('users');
				Route::post('create', 'APIController@createUser')->name('users.create');
				Route::post('edit', 'APIController@saveUser')->name('users.edit');
				Route::post('delete', 'APIController@deleteUser')->name('users.delete');
				Route::get('{user}', 'APIController@user')->name('users.info');
			});
		});*/


		/*
		|--------------------------------------------------------------------------
		| Admins module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the admins module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Admins')->prefix('users')->group(function () {
			Route::view('/', 'backend::admin.users.index')->name('users');
			Route::view('/create', 'backend::admin.users.create')->name('users.create');
			Route::view('/edit/{user?}', 'backend::admin.users.edit')->name('users.edit');
			Route::as('api.')->prefix('api/users')->group(function () {
				Route::get('/', 'APIController@users')->name('users');
				Route::post('create', 'APIController@createUser')->name('users.create');
				Route::post('edit', 'APIController@saveUser')->name('users.edit');
				Route::post('delete', 'APIController@deleteUser')->name('users.delete');
				Route::get('{admin?}', 'APIController@user')->name('users.info');
			});
		});


		/*
		|--------------------------------------------------------------------------
		| News module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the news module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('News')->prefix('news')->group(function () {
			Route::any('/', 'APIController@index')->name('news');
			Route::view('/create', 'backend::admin.news.create')->name('news.create');
			Route::view('/edit/{news?}', 'backend::admin.news.edit')->name('news.edit');
			Route::as('api.')->prefix('api/news')->group(function () {
				Route::get('/', 'APIController@news')->name('news');
				Route::post('create', 'APIController@createNews')->name('news.create');
				Route::post('edit', 'APIController@saveNews')->name('news.edit');
				Route::post('delete', 'APIController@deleteNews')->name('news.delete');
				Route::post('/seotranslations', 'APIController@seotranslations')->name('news.seotranslations');
				Route::get('{news?}', 'APIController@info')->name('news.info');
			});
		});

		/*
		|--------------------------------------------------------------------------
		| Config module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the config module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Config')->prefix('config')->group(function () {
			Route::view('/', 'backend::admin.config.edit')->name('config');
			Route::as('api.')->prefix('api/config')->group(function () {
				Route::get('/', 'APIController@show')->name('config');
				Route::post('/', 'APIController@save')->name('config.save');
			});
		});

		/*
		|--------------------------------------------------------------------------
		| Contact module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the contact module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Contact')->prefix('contact')->group(function () {
            Route::get('/export', 'APIController@export')->name('contact.export');
			Route::any('/', 'APIController@index')->name('contact');
			Route::view('/{contact?}', 'backend::admin.contact.show')->name('contact.show');
			Route::as('api.')->prefix('api/contact')->group(function () {
				Route::get('/', 'APIController@contacts')->name('contact');
				Route::get('/{contact?}', 'APIController@info')->name('contact.info');
				Route::post('/delete', 'APIController@delete')->name('contact.delete');
			});
		});

		/*
		|--------------------------------------------------------------------------
		| General module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the general module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('General')->prefix('general')->group(function () {
			Route::view('/', 'backend::admin.general.index')->name('general');
			Route::view('/create', 'backend::admin.general.create')->name('general.create');
			Route::get('/edit/{id?}', 'APIController@editView')->name('general.edit');
			Route::post('/delete', 'APIController@delete')->name('general.delete');
			Route::as('api.')->prefix('api/general')->group(function () {
				Route::get('/', 'APIController@index')->name('general.index');
				Route::post('/create', 'APIController@create')->name('general.create');
				Route::post('/edit/{id?}', 'APIController@update')->name('general.edit');
				Route::post('/seotranslations', 'APIController@seotranslations')->name('general.seotranslations');
			});
		});

		/*
		|--------------------------------------------------------------------------
		| Images module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the images module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Images')->prefix('images')->group(function () {
			Route::any('/', 'APIController@index')->name('images');
			Route::get('/download/{image?}', 'ImagesController@download')->name('images.download');
			Route::view('/{image?}', 'backend::admin.images.edit')->name('images.edit');
			Route::as('api.')->prefix('api/images')->group(function () {
				Route::get('/', 'APIController@images')->name('images');
				Route::get('/getImage', 'APIController@image')->name('image');
				Route::post('/getImages', 'APIController@getImages')->name('getImages');
				Route::post('/', 'APIController@save')->name('images.save');
				Route::post('/upload', 'APIController@uploadInRepository')->name('images.upload');
				Route::post('/delete', 'APIController@delete')->name('images.delete');
				Route::post('/translations', 'APIController@trans')->name('images.translations');
				Route::get('/{image?}', 'APIController@show')->name('images.info');
			});
		});

		/*
		|--------------------------------------------------------------------------
		| Slides module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the slides module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Slides')->prefix('slides')->group(function () {
			Route::any('/', 'APIController@index')->name('slides');
			Route::view('/create', 'backend::admin.slides.create')->name('slides.create');
			Route::view('/edit/{slide?}', 'backend::admin.slides.edit')->name('slides.edit');
			Route::as('api.')->prefix('api/slides')->group(function () {
				Route::get('/', 'APIController@slides')->name('slides');
				Route::post('/', 'APIController@save')->name('slides.save');
				Route::post('/create', 'APIController@create')->name('slides.create');
				Route::post('/delete', 'APIController@delete')->name('slides.delete');
                Route::post('/upload-file/{slide}', 'APIController@uploadVideos')->name('slides.upload_videos');
				Route::post('/seotranslations', 'APIController@seotranslations')->name('slides.seotranslations');
				Route::get('{slide?}', 'APIController@show')->name('slides.info');
			});
		});


		/*
		|--------------------------------------------------------------------------
		| Banners module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the slides module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Banners')->prefix('banners')->group(function () {
			Route::any('/', 'APIController@index')->name('banners');
			Route::view('/create', 'backend::admin.banners.create')->name('banners.create');
			Route::view('/edit/{banner?}', 'backend::admin.banners.edit')->name('banners.edit');
			Route::as('api.')->prefix('api/banners')->group(function () {
				Route::get('/', 'APIController@banners')->name('banners');
				Route::post('/', 'APIController@save')->name('banners.save');
				Route::post('/create', 'APIController@create')->name('banners.create');
				Route::post('/delete', 'APIController@delete')->name('banners.delete');
				Route::get('{banner?}', 'APIController@show')->name('banners.info');
			});
		});


		/*
		|--------------------------------------------------------------------------
		| Documents module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the documents module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Documents')->prefix('documents')->group(function () {
			Route::any('/', 'APIController@index')->name('documents');
			Route::view('/upload', 'backend::admin.documents.upload')->name('documents.upload');
			Route::get('/download/{document?}', 'DocumentsController@download')->name('documents.download');
			Route::view('/{document?}', 'backend::admin.documents.edit')->name('documents.edit');
			Route::as('api.')->prefix('api/documents')->group(function () {
				Route::get('/', 'APIController@documents')->name('documents');
				Route::post('/', 'APIController@save')->name('documents.save');
                Route::get('/getDocument', 'APIController@document')->name('document');
                Route::post('/getDocuments', 'APIController@getDocuments')->name('getDocuments');
				Route::post('/upload', 'APIController@upload')->name('documents.upload');
				Route::post('/delete', 'APIController@delete')->name('documents.delete');
				Route::get('/{document?}', 'APIController@show')->name('documents.info');
				Route::post('/translations', 'APIController@trans')->name('documents.translations');
			});
		});

		/*
		|--------------------------------------------------------------------------
		| SEO module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the SEO module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Seo')->prefix('seo')->group(function () {
			Route::as('api.')->prefix('api/seo')->group(function () {
				Route::post('/', 'APIController@getSeo')->name('seo');
				Route::post('/save', 'APIController@seoSave')->name('seo.save');
                Route::post('/save-indexable', 'APIController@saveIndexable')->name('seo.save-indexable');
                Route::post('/save-image', 'APIController@saveImage')->name('seo.save-image');
				Route::post('/translations', 'APIController@trans')->name('seo.translations');
				Route::post('/languages', 'APIController@languages')->name('seo.languages');
			});
		});

		/*
		|--------------------------------------------------------------------------
		| Menus module
		|--------------------------------------------------------------------------
		|
		| This routes contain all the menus module routes defined by default.
		| Changing these routes should not change the app behaviour, please keep
		| in mind to use route() instead of url() for a propper app route structure.
		|
		*/
		Route::namespace('Menus')->prefix('menus')->group(function () {
			Route::view('/', 'backend::admin.menus.index')->name('menus');
			Route::view('/create/{parent?}', 'backend::admin.menus.create')->name('menus.create');
			Route::view('/edit/{menu?}', 'backend::admin.menus.edit')->name('menus.edit');
			Route::as('api.')->prefix('api/menus')->group(function () {
				Route::get('/', 'APIController@menus')->name('menus');
				Route::post('/create', 'APIController@create')->name('menus.create');
				Route::post('/edit', 'APIController@edit')->name('menus.edit');
				Route::post('/delete', 'APIController@delete')->name('menus.delete');
				Route::post('/order', 'APIController@order')->name('menus.order');
				Route::post('/toggleActives', 'APIController@toggleActives')->name('menus.toggleActives');
				Route::post('/translations', 'APIController@translations')->name('menus.translations');
				Route::get('/{menu?}', 'APIController@info')->name('info');
				Route::post('/seotranslations', 'APIController@seotranslations')->name('menus.seotranslations');
			});
		});
	});
});
});

$prefix = null;
$locales = null;
$configLocales = config('translatable.locales');
if(isset($configLocales) && count($configLocales) > 1){
    $locales = 'locale';
    $prefix = '{lang?}';
}
Route::namespace('Lafactoria\Backend\Controllers')->middleware('web')->group(function () use ($prefix, $locales) {
	 Route::middleware('admin.features')->namespace('LFPublic')->group(function () use ($prefix, $locales)  {
		if (isset($locales)) {
			// Routes with locale
			Route::middleware($locales)->prefix($prefix)->group(function ()  {
				include('publicRoutes.php');
			});
		} else {
			// Routes without locale
			include('publicRoutes.php');
		}
    });
});
