<?php

use Lafactoria\Backend\Models\News;

Route::post('contact', 'ContactController@send');
Route::get('contact', 'ContactController@contact')->name('contact');
Route::get('cookies', 'ViewController@cookies')->name('cookies');
Route::get('legal', 'ViewController@legal')->name('legal');
Route::get('privacy', 'ViewController@privacy')->name('privacy');
Route::get('news', 'NewsController@all')->name('news');             

try {
    // menus
    if (Schema::hasTable(with(new Lafactoria\Backend\Models\Menu)->getTable())) {
        foreach (Lafactoria\Backend\Models\Menu::where(['type' => Lafactoria\Backend\Models\Menu::LAYOUT_TYPE])->where('active', true)->get() as $page) {
            foreach ($page->translations as $page_translation) {
                Route::get($page_translation->url, 'PageController@page')->name(Lafactoria\Backend\Models\Menu::ROUTE_PREFIX.'_'.$page_translation->locale."_{$page->id}");
            }
        }
    };

    // news
    Route::get(News::ROUTE_PREFIX."/{slug}", 'NewsController@news')->name(Lafactoria\Backend\Models\News::ROUTE_PREFIX."_get_by_slug");
} catch (Exception $e) {
}
    