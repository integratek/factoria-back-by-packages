<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Administration Logo
    |--------------------------------------------------------------------------
    |
    | This string stores the logo URL. It will be parsed as asset($url).
    |
    */
    'logo' => 'img/logo.png',

    /*
    |--------------------------------------------------------------------------
    | Public views
    |--------------------------------------------------------------------------
    |
	| This array stores the public views loaded in the diferent sections.
	| 
    | The value can be an array with the locales, ex:
    | 'TrendyFormulations' => [
    |    'en' => '/inspiration/trendy-formulations',
    |    'es' => '/inspiracion/tendencias-formulacion
    | ]
    |
    */
    'public_routes' => [
        'Home' => '/',
        'Contact' => '/contact',
        'Cookies' => '/cookies',
        'Legal' => '/legal',
        'News' => '/news',
    ],

    'public_views' => [
        'contact' => 'backend::public.contact',
        'cookies' => 'backend::public.cookies',
        'legal' => 'backend::public.legal',
	    'privacy' => 'backend::public.privacy',
        'all_news' => 'backend::public.all_news',
        'news' => 'backend::public.news',
        'page' => 'backend::public.page'
    ],

    /*
    |--------------------------------------------------------------------------
    | Dashboard Classes
    |--------------------------------------------------------------------------
    |
    | The dashboard counters, specify the text, the icon, the eloquent class (or point
    | to a Collection instance) and optionally the custom_count that should be the name of
    | an static method of the class you previously specified that returns the desired count.
    |
    */
    'dashboard_counters' => [
      [
        'text' => 'backend::admin-dashboard.users',
        'icon' => 'mdi-account',
        'class' => Lafactoria\Backend\Models\Admin::class
      ],
      [
        'text' => 'backend::admin-dashboard.contacts',
        'icon' => 'mdi-email',
        'class' => Lafactoria\Backend\Models\Contact::class
      ],
      [
        'text' => 'backend::admin-dashboard.news',
        'icon' => 'mdi-newspaper',
        'class' => Lafactoria\Backend\Models\News::class
      ],
      [
        'text' => 'backend::admin-dashboard.menus',
        'icon' => 'mdi-menu',
        'class' => Lafactoria\Backend\Models\Menu::class
      ],
      [
        'text' => 'backend::admin-dashboard.images',
        'icon' => 'mdi-camera',
        'class' => Lafactoria\Backend\Models\Image::class
      ],
      [
        'text' => 'backend::admin-dashboard.documents',
        'icon' => 'mdi-file-document',
        'class' => Lafactoria\Backend\Models\Document::class
      ],
      [
        'text' => 'backend::admin-dashboard.slides',
        'icon' => 'mdi-camera-burst',
        'class' => Lafactoria\Backend\Models\Slide::class
      ],
      [
        'text' => 'backend::admin-dashboard.banners',
        'icon' => 'mdi-image-area',
        'class' => Lafactoria\Backend\Models\Banner::class
      ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Public Pagination
    |--------------------------------------------------------------------------
    |
    | This array stores the public pagination settings.
    |
    */
    'pagination' => [
        'backend-news' => 25,
        'backend-contacts' => 25,
        'backend-documents' => 25,
        'backend-images' => 25,
        'frontend-news' => 5
    ],

    /*
    |--------------------------------------------------------------------------
    | Administration Menu
    |--------------------------------------------------------------------------
    |
    | This array stores the aditional menu items shown in the administration
    | panel, theese items can be manually edited (NOT RECOMMENDED) or added
    | using the helper class, localed in: Lafactoria\Backend\Classes\Menu::
    | add(string $name, string $url, string $icon, string $segment = '', bool $translation = false)
    | It's best to set it in the AppServiceProvider. Pss, you can use realtime facades
    | to achive a clean way to add it!
    |
    */
   'menu' => [
       [
           'name' => 'backend::admin-dashboard.dashboard',
           'url' => 'admin.dashboard',
           'icon' => 'mdi-view-dashboard',
           'segment' => 'dashboard',
       ],
       [
           'name' => 'backend::admin-menus.menus',
           'url' => 'admin.menus',
           'icon' => 'mdi-lan',
           'segment' => 'menus',
       ],
       [
           'name' => 'backend::admin-users.users',
           'url' => 'admin.users',
           'icon' => 'mdi-account-multiple',
           'segment' => 'users',
       ],
       [
           'name' => 'backend::admin-news.news',
           'url' => 'admin.news',
           'icon' => 'mdi-newspaper',
           'segment' => 'news',
       ],
       [
           'name' => 'backend::admin-contact.contact',
           'url' => 'admin.contact',
           'icon' => 'mdi-email',
           'segment' => 'contact',
       ],
       [
           'name' => 'backend::admin-images.images',
           'url' => 'admin.images',
           'icon' => 'mdi-camera',
           'segment' => 'images',
       ],
       [
           'name' => 'backend::admin-documents.documents',
           'url' => 'admin.documents',
           'icon' => 'mdi-file-document',
           'segment' => 'documents',
       ],
       [
           'name' => 'backend::admin-slides.slides',
           'url' => 'admin.slides',
           'icon' => 'mdi-camera-burst',
           'segment' => 'slides',
       ],
       [
           'name' => 'backend::admin-banners.banners',
           'url' => 'admin.banners',
           'icon' => 'mdi-image-area',
           'segment' => 'banners',
       ],
       [
           'name' => 'backend::admin-general.general',
           'url' => 'admin.general',
           'icon' => 'mdi-lightbulb-outline',
           'segment' => 'general',
       ],
       [
           'name' => 'backend::admin-config.configuration',
           'url' => 'admin.config',
           'icon' => 'mdi-settings',
           'segment' => 'config',
       ],
   ], // Please do not edit this value here, would make me unhappy :c

	/*
    |--------------------------------------------------------------------------
    | Modules with Optional images component Configuration
    |--------------------------------------------------------------------------
    |
    | This values should be added in backend.php published in every project.
    |
    */
	'images' =>[
		'banners' => true
	],

    /*
    |--------------------------------------------------------------------------
    | Administration Roles and features
    |--------------------------------------------------------------------------
    |
    | This array stores the admin roles and some of their features
    |
    */

    'admin_roles' => [
        ['id' => 1, 'name' => 'backend::admin-users.admin'],
        ['id' => 2, 'name' => 'backend::admin-users.editor']
    ],
    'admin_role_editor_segments' => [
        'dashboard',
        'menus',
        'news',
        'contact',
        'images',
        'documents',
        'slides',
        'banners',
        'general',
        'config'
    ],

    /*
    |--------------------------------------------------------------------------
    | Administration Configuration
    |--------------------------------------------------------------------------
    |
    | This array stores the default configuration used by the application.
    | Those values are automatically changed to the database data in the
    | admin.features middleware (Lafactoria\Backend\Middleware\AdminFeatures).
    |
    */
    'config' => [
        'name' => 'La Factoria Interactiva',
        'email' => 'lafactoria@lafactoria.eu',
        'phone' => '+ 34 938206362',
        'adress' => 'Gavarresa, 10 , 08650',
        'city' => 'Cabrianes',
        'country' => 'Spain',
        'title' => '',
        'tags' => 'desarrollo material digital educativo, desarrollo contenido digital educativo, desarrollo apps educativas, desarrollo apps, desarrollo aplicaciones educativas, desarrollo contenido digital educativo, proveedores contenido digital educativo, Contenido Digital, Gamification Educativo , Mobile, Android, iOS, Android, simulación, realidad virtual, Advergaming, HTML5, Desarrollo, Diseño Digital, marketing digital, Interactiva, La Factoria, diseño multimedia, juegos pedagogía, juegos, juegos educativos, juegos interactivos, la factoría, lafactoria, juego Interactivo Publicitario, juego inteactius educativos, juegos didáctica, juegos educativos, advergaming, diseño de Interactivos',
        'description' => 'La Factoria ha construido una reputación de ofrecer innovaciones creativas y técnicas de vanguardia a sus clientes: desde la era pre-teléfono inteligente de los videojuegos y el diseño web en contenido digital interactivo y que ahora proporcionan una gran cantidad de diseño multimedia y las actividades de marketing digital como aplicaciones móvil , HTML5 y programación nativa del sistema operativo(IOS, Android...), contenidos digitales educativos, Gamification, simulación y Realidad Virtual y Advergaming.',
        'canonical' => '',
        'indexable' => '',
        'scripts' => '',
        'styles' => '',
    ],

    /*
    |--------------------------------------------------------------------------
    | Password caducity
    |--------------------------------------------------------------------------
    |
    | The password caducity in days
    | 0 means has no caducity
    |
    */
    'password_caducity' => 0,

    /*
    |--------------------------------------------------------------------------
    | Password validation rule
    |--------------------------------------------------------------------------
    |
    | The password validation rule
    |
    | Ex: min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*"-]).{8,}$/
    |   -> Min 8, Min 1 upper, Min 1 lower, Min 1 Number, Min 1 Special char
    |
    */
    'password_validation_rule' => 'min:6',

    /*
    |--------------------------------------------------------------------------
    | Password reset token caducity
    |--------------------------------------------------------------------------
    |
    | The password reset token caducity in days
    |
    */
	'password_reset_caducity' => 190,
	
	/*
    |--------------------------------------------------------------------------
    | Submenus with url
    |--------------------------------------------------------------------------
    |
    | Weather the submenus have an URL
    |
    */
    'submenus_with_urls' => true,

    /*
	|--------------------------------------------------------------------------
	| Sitemap constructor params
	|--------------------------------------------------------------------------
	|
	| Set Model and fields with values to construct function
	| Models should have sitemap scope
	*/
    'sitemap' => [
        'models' => [
            Lafactoria\Backend\Models\Menu::class,
            Lafactoria\Backend\Models\News::class,
        ],
        'urls' => [
            'en' => [
                app()->runningInConsole() ? '' : url('/'),
                app()->runningInConsole() ? '' : url('/') . '/en/news',
            ],
            'es' => [
                app()->runningInConsole() ? '' : url('/'),
                app()->runningInConsole() ? '' : url('/') . '/es/noticias',
            ]
        ],
	],
	
	/*
    |--------------------------------------------------------------------------
    | Modules configuration
    |--------------------------------------------------------------------------
    |
    | Here you can configure how the modules work in your application
    |
	*/
	'modules_configuration' => [
		'slides' => [
			'description_with_quill' => true
		]
    ],

    /*
    |--------------------------------------------------------------------------
    | AWS S3 Storage
    |--------------------------------------------------------------------------
    |
    | Weather this app must store the storage files using AWS S3
    |
	*/
    's3' => false
];
