<?php
namespace Lafactoria\Backend\Traits;


trait Text2Url
{
	function  Text2Url($value){
    // reemplaza cualquier cadena inválida por "-";
	$o=array('ó','ò','ö','Ò','Ó','Ö');
	$u=array('ú','ù','ü','Ù','Ú','Ü');
	$i=array('í','ì','ï','Ì','Í','Ï');
	$a=array('á','à','ä','a','Á','À','Ä');
	$e=array('è','é','ë','È','É','Ë');
	$n=array('ñ','Ñ');
	$c=array('ç','Ç');
	$title = str_replace($o, 'o', $value);
	$title = str_replace($e, 'e', $title);
	$title = str_replace($i, 'i', $title);
	$title = str_replace($u, 'u', $title);
	$title = str_replace($a, 'a', $title);
	$title = str_replace($c, 'c', $title);
	$title = str_replace($n, 'n', $title);
	//$title = htmlentities($title);
	$title = strtolower("$title");

	return $title;
	}
}