<?php

namespace Lafactoria\Backend\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Lafactoria\Backend\Models\Admin;

trait Helper
{
    /**
     * Returns the language list based on a locale.
     *
     * @param  string $locale
     * @return Collection
     */
    public static function languages($locale = null) : Collection
    {
        $locale = isset($locale) ? $locale : config('app.locale');

        return Collection::make(include __DIR__ . "/../../../../../vendor/umpirsky/language-list/data/{$locale}/language.php")->map(function ($lang) {
            return ucfirst($lang);
        });
    }

    /**
     * Returns the language name based on a locale.
     *
     * @param  string $lang
     * @param  string $locale
     * @return string
     */
    public static function language(string $lang = 'en', string $locale = 'en') : string
    {
        return static::languages($locale)->get($lang);
    }

    /**
     * Return the gavatar for the given email.
     * 
     * @param string $email
     * @param int|integer $size
     * @return string
     */
    public static function gavatar(string $email, int $size = 100) : string
    {
        $hash = md5(strtolower(trim($email)));

        return "https://www.gravatar.com/avatar/{$hash}.jpg?s={$size}";
    }

     /**
     * Returns if the request URL have an active segment.
     *
     * @param  string   $name
     * @param  int      $segment
     * @param  string   $class
     * @return string
     */
    public static function activeSegUrl(string $name, int $segment = 1, $class = 'active') : string
    {
        if (Str::contains($name, '/')) {
            // multiple segments
            $segs = explode('/', $name);
            $segs_actives = [];
            collect($segs)->each(function ($seg) use (&$segs_actives, &$segment){
                array_push($segs_actives, Request::segment($segment) == $seg);
                $segment+=1;
            });
            $active = in_array(false, $segs_actives) ? '' : $class;
        } else {
            $active = Request::segment($segment) == $name ? $class : '';
        }
        return $active;
    }


    public static function getLanguages($locale = null) {
        $translations = collect();
        $languages = Helper::languages(isset($locale) ? $locale : config('app.locale'));
        $locales = config('translatable.locales');

        foreach ($locales as $locale) {
            $translations->push([
                'locale' => $locale,
                'locale_name' => $languages[$locale]
            ]);
        }
        return $translations;
    }


    public static function getModelLanguagesMerged($model = null)
    {
        $translations = Helper::getLanguages();

        $user = auth('admin')->user();
        if ($user->role_id == Admin::$ROLE_EDITOR) {
            $langs = $user->languages;
            $translations = $translations->whereIn('locale', $langs);
        }

        if(!$model)
            return $translations;

        return $translations->map(function ($trans) use ($model){
            $model_locale_trans = $model->getTranslation($trans['locale']);

            if ($model_locale_trans) {
                $model_locale_trans['locale_name'] = $trans['locale_name'];
                return $model_locale_trans;
            }
            return $trans;
        });
    }

    public static function showMenuByRole($menu) {
        $user = auth('admin')->user();

        if ($user->role_id == 2) {
            $segments = config('backend.admin_role_editor_segments');

            if (in_array('*', $segments))
                return true;
            else
                if (in_array($menu['segment'], $segments))
                    return true;
			    return false;
        }
        return true;
    }


    public static function getFileTranslations($filename, $locale = null)
    {
        $locale = isset($locale) ? $locale : config('app.locale');
        $file = __DIR__ . "/../Translations/{$locale}/".$filename.".php";

        if (!file_exists($file))
            return [];
        return include $file;
    }
}