<?php
namespace Lafactoria\Backend\Traits;


trait LocalePath
{
	function  actualPathWithoutLocale(){
		$configLocales = config('translatable.locales');
	    if (isset($configLocales) && count($configLocales) > 1) {
			$path = substr(request()->path(), 3);
	    }else{
	    	$path = request()->path();
	    }
	   return $path;
	}
}