<?php

namespace Lafactoria\Backend\Models;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class HeaderImageModel extends Model
{
    use Translatable;

    protected $table = 'header_images_model';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'image_id', 'model_id', 'model'
    ];


    /**
     * Stores the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'seo_alt'
    ];


    /**
     * Relation to Image
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    /**
     * Relation to model
     *
     * @return mixed
     */
    public function model()
    {
        $model = $this->model;
        return $model::findOrFail($this->model_id);
    }
}
