<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;

class SeoTranslation extends Model
{
    /**
     * Determines if the timestamps are added.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'canonical', 'tags'
    ];
}
