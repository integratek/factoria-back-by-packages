<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends BaseList
{
    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'file', 'seo_indexable'
    ];

    /**
     * Stores the attributes that are used on filter.
     *
     * @var array
     */
    public $filterAttributes = [
        'name'
    ];
}
