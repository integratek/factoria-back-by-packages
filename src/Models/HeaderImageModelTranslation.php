<?php

namespace Lafactoria\Backend\Models;
use Illuminate\Database\Eloquent\Model;

class HeaderImageModelTranslation extends Model
{
    protected $table = 'header_image_model_translations';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    public $fillable = [
        'seo_alt', 'locale', 'header_image_model_id'
    ];


    /**
     * Relation to HeaderImageModel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function header_image_model()
    {
        return $this->belongsTo(HeaderImageModel::class);
    }
}
