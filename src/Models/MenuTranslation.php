<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Database\Eloquent\Model;
use Lafactoria\Backend\Traits\Text2Url;

class MenuTranslation extends Model
{
    use Text2Url;

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'highlighted', 'url', 'copy_from_english'
    ];


    protected $casts = ['copy_from_english' => 'boolean'];

    /**
     * Set url without some especiall characters
     */
    public function setUrlAttribute($value){
        $this->attributes["url"] = $this->Text2Url($value);
    }
}
