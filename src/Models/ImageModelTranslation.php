<?php

namespace Lafactoria\Backend\Models;
use Illuminate\Database\Eloquent\Model;

class ImageModelTranslation extends Model
{
    protected $table = 'image_model_translations';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    public $fillable = [
        'seo_alt', 'locale', 'image_model_id'
    ];


    /**
     * Relation to ImageModel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image_model()
    {
        return $this->belongsTo(ImageModel::class);
    }
}
