<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralTranslation extends Model
{
    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    public $fillable = [
        'title', 'text', 'cookies', 'cookies_description', 'legal', 'general_id'
    ];
}
