<?php

namespace Lafactoria\Backend\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use Translatable;

    /**
     * Stores the table name as the model class is the same as the table.
     *
     * @var string
     */
    protected $table = 'config';

    /**
     * Stores the mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'email', 'adress', 'city', 'country',
        'scripts', 'styles'
    ];


    /**
     * Stores the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'tags', 'description'
    ];
}
