<?php

namespace Lafactoria\Backend\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class SlideTranslation extends Model
{
    /**
     * Determines if the timestamps are added.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'video_mp4','video_webm', 'url'
    ];
}
