<?php

namespace Lafactoria\Backend\Models;

class Slide extends BaseListWithTranslation
{
	/**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'active', 'new_tab'
    ];
      /**
     * Stores the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
       'name', 'description','video_name', 'video_mp4','video_webm', 'url'
    ];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['headerImageModel'];

    /**
     * Stores the attributes that are used on filter.
     *
     * @var array
     */
    public $filterAttributes = [
        'name', 'description', 'url'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'new_tab' => 'boolean',
    ];


    /**
     * Returns the HeaderImageModel related to Slide
     *
     * @return \Lafactoria\Backend\Models\HeaderImageModel
     */
    public function getHeaderImageModelAttribute()
    {
        return $this->headerImageModelRelation()->first();
    }

    /**
     * Returns the HeaderImageModel related to Slide
     *
     * @return \Lafactoria\Backend\Models\HeaderImageModel
     */
    public function headerImageModelRelation()
    {
        return $this->hasOne(HeaderImageModel::class, 'model_id')->where('model', get_class($this));
    }

    /**
     * Returns true if the slide is active and the image exists.
     *
     * @return bool
     */
    public function canDisplay()
    {
        return ($this->active === true) && (optional($this->image)->id !== null);
    }
}
