<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Database\Eloquent\Model;

class AdminPassword extends Model
{
    /**
     * Mass assignable attributes
     *
     * @var array
     */
    protected $fillable = [
        'admin_id', 'password'
    ];
}
