<?php

namespace Lafactoria\Backend\Models;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;

class BaseListWithTranslation extends Model
{
    use Translatable;

    /**
     * Stores the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [];


    public function filterPersonalized($query, $searchKey=''){
        return $query;
    }
}
