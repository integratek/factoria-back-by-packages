<?php

namespace Lafactoria\Backend\Models;
use Illuminate\Database\Eloquent\Model;


class Banner extends BaseList
{
    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'url', 'active'
    ];


    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['headerImageModel'];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Stores the attributes that are used on filter.
     *
     * @var array
     */
    public $filterAttributes = [
        'name', 'description', 'url'
    ];


    /**
     * Set url format.
     *
     * @param $value
     */
    public function setUrlAttribute($value)
    {
        $this->attributes['url'] = url($value);
    }


    /**
     * Returns the HeaderImageModel related to Banner
     *
     * @return \Lafactoria\Backend\Models\HeaderImageModel
     */
    public function getHeaderImageModelAttribute()
    {
        return $this->hasOne(HeaderImageModel::class, 'model_id')->where('model', get_class($this))->first();
    }
}
