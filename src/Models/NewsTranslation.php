<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Database\Eloquent\Model;

class NewsTranslation extends Model
{
    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'content'
    ];
}
