<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Database\Eloquent\Model;
use Lafactoria\Backend\Mail\ResetPasswordEmail;
use Illuminate\Support\Facades\Mail;

class AdminPasswordReset extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'email';
    public $incrementing = false;

    /**
     * Mass assignable attributes
     *
     * @var array
     */
    protected $fillable = [
        'email', 'token'
    ];

    /**
     * Sends the contact email.
     *
     * @return Response
     */
    public function sendEmail()
    {
        return Mail::to($this->email)->send(new ResetPasswordEmail(Admin::where('email', $this->email)->first()));
    }
}
