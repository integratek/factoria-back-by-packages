<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigTranslation extends Model
{
    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    public $fillable = [
        'tags', 'description'
    ];
}
