<?php

namespace Lafactoria\Backend\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class General extends Model
{
    use Translatable;

    /**
     * Stores the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title', 'text', 'cookies', 'cookies_description', 'legal'
    ];
}
