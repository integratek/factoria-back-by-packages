<?php

namespace Lafactoria\Backend\Models;

use Lafactoria\Backend\Mail\LFPublic\ContactEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;

class Contact extends BaseList
{
    /**
     * The mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'phone', 'message'
    ];

    /**
     * Stores the attributes that are used on filter.
     *
     * @var array
     */
    public $filterAttributes = [
        'name', 'surname', 'email', 'phone', 'message'
	];
	
	/**
	 * Returns the created_at date correctly formatted
	 *
	 * @param mixed $date
	 * @return string
	 */
	public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::parse($date)->format('Y-m-d H:i:s');
	}

    /**
     * Sends the contact email.
     *
     * @return Response
     */
    public function sendEmail()
    {
        return Mail::to(config('backend.config.email'))->send(new ContactEmail($this));
    }
}
