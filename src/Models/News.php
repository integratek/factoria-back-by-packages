<?php

namespace Lafactoria\Backend\Models;

class News extends BaseListWithTranslation
{
    /**
     * Constant definitions
     */
    const LAYOUT_IMAGE_HEIGHT = 350; // In pixels
    const ROUTE_PREFIX = 'news';

    /**
     * Determine the table name, as 'new' is a reserved word in PHP.
     *
     * @var string
     */
    protected $name = 'news';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'display_after', 'active'
    ];

    /**
     * Stores the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title', 'description', 'content'
    ];

    /**
     * Stores the attributes that are used on filter.
     *
     * @var array
     */
    public $filterAttributes = [
        'title', 'description', 'content'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['imageModel', 'headerImageModel'];


    /**
     * Returns the personalized query
     *
     *@return \Illuminate\Database\Eloquent\Collection
     */
    public function filterPersonalized($query, $searchKey = '')
    {
        return $query->with('admin')->orderBy('id','desc');
    }


    /**
     * Returns the seo instance.
     *
     * @return App\Seo
     */
    public function seo()
    {
        return Seo::where(['class' => get_class($this), 'reference' => $this->id])->first();
    }

    /**
     * Generate the URL of the given news.
     *
     * @param  News   $news
     * @return string
     */
    /*public static function generateUrl(News $news)
    {
        return News::ROUTE_PREFIX . "/{slug}";
    }*/

    /**
     * Get the news associated with the given URL.
     *
     * @param  string $url
     * @return Lafactoria\Backend\Models\News|null
     */
    public static function getFromUrl(string $url)
    {
        $remove_url = url('/');
        $remove_url = substr($remove_url, -1) == '/' ? $remove_url : $remove_url.'/';
        return News::find(explode('/', str_replace($remove_url, '', $url))[2]);
    }

    /**
     * Returns the administrator who posted the new.
     *
     * @return Lafactoria\Backend\Models\Admin
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }


    /**
     * Returns the HeaderImageModel related to New
     *
     */
    public function headerImageModelRelation()
    {
        return $this->hasOne(HeaderImageModel::class, 'model_id')->where('model', get_class($this));
    }


    /**
     * Returns the HeaderImageModel related to New
     *
     * @return \Lafactoria\Backend\Models\HeaderImageModel
     */
    public function getHeaderImageModelAttribute()
    {
        return $this->headerImageModelRelation()->first();
    }


    /**
     * Returns the ImageModel related to New
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getImageModelAttribute()
    {
        return $this->hasMany(ImageModel::class, 'model_id')->where('model', get_class($this))->get();
    }

    /**
     * Return te news files
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function files()
    {
        return $this->belongsToMany(Document::class, 'document_news', 'news_id', 'document_id')->withPivot('locale');
    }


    /**
     * Sitemap routes
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function sitemap($locale)
    {
        return self::leftjoin('seos', function ($join) {
                $join->on('news.id', '=', 'seos.reference')
                    ->where('seos.class', '=', 'Lafactoria\Backend\Models\News');
            })
            ->leftjoin('seo_translations', function ($join) use ($locale){
                $join->on('seos.id', '=', 'seo_translations.seo_id')
                    ->where('seo_translations.locale', $locale);
            })
            ->where(function ($query) {
                return $query->whereNull('indexable')
                    ->orWhere('indexable', 1);
            })
            ->where('active', 1)
            ->select('news.*')
            ->get();
    }

    static function urlFormat($news, $locale) {
        $path = url('/');
        $result = $path . "/{$locale}" . '/' . self::ROUTE_PREFIX . "/{$news->slug}";

        Sitemap::updateOrCreate(
            ['model' => get_class($news), 'model_id' => $news->id, 'locale' => $locale],
            ['url' => $result]
        );

        return $result;
    }
}
