<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Database\Eloquent\Model;

class Sitemap extends Model
{
    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'model', 'model_id', 'url', 'locale'
    ];
}
