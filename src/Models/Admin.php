<?php

namespace Lafactoria\Backend\Models;

use Lafactoria\Backend\Traits\Helper;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

class Admin extends Authenticatable
{
    use Notifiable;

    public static $ROLE_ADMIN = 1;
    public static $ROLE_EDITOR = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'languages'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['avatar'];

    /**
     * Update password_updated_at field
     *
     * @param  string  $password
     * @return void
     */
    public function setPasswordAttribute($password)
    {
        if (config('backend.password_caducity') > 0) {
            if (strlen($password) == 60 && strpos($password, '$2y$') === 0) {
                // Already encrypted
                $this->attributes['password'] = $password;
            } else {
                // Not encrypted
                $this->attributes['password'] = bcrypt($password);
			}
			
            $this->attributes['password_updated_at'] = DB::raw('CURRENT_TIMESTAMP');
        } else {
            if (strlen($password) == 60 && strpos($password, '$2y$') === 0) {
                // Already encrypted
                $this->attributes['password'] = $password;
            } else {
                // Not encrypted
                $this->attributes['password'] = bcrypt($password);
            }
        }
    }

    public function setLanguagesAttribute($langs)
    {
        $this->attributes['languages'] = isset($langs) && count($langs) > 0 ? collect($langs)->implode('locale', ';') : null;
    }

    public function getLanguagesAttribute($langs)
    {
        if ($langs)
            return explode(';', $langs);
        return $langs;
    }


    /**
     * Check if is a valid password and register
     *
     * @param string $password
     * @return string
     */
    public function setPassword($password)
    {
        $encryptedPass = bcrypt($password);
        $allPasswords = AdminPassword::where('admin_id', $this->id)->get();
        foreach ($allPasswords as $pass) {
            if (Hash::check($password, $pass->password)) {
                return null;
            }
        }
        AdminPassword::create(['admin_id' => $this->id, 'password' => $encryptedPass]);
        return $encryptedPass;
    }

    /**
     * Get the number of days since last updated password
     *
     * @return int
     */
    public function passwordUpdatedDays(){
        $updated = new Carbon($this->password_updated_at);
        $now = Carbon::now();
        $difference = $updated->diff($now)->days;
        return $difference;
    }

    /**
     * Returns the user gavatar
     *
     * @return string
     */
    public function getAvatarAttribute()
    {
        return Helper::gavatar($this->email);
    }

    /**
     * Returns all the news from the administrator
     *
     * @return Collection
     */
    public function news()
    {
        return $this->hasMany(News::class);
    }
}
