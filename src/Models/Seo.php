<?php

namespace Lafactoria\Backend\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Seo extends Model
{
    use Translatable;

    /**
     * Stores the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title', 'description', 'canonical', 'tags'
    ];

    /**
     * The mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'class', 'reference', 'indexable', 'follow'
    ];

    public static function translation($class, $identifier = 'id', $locale = null)
    {
        $model = Str::replaceFirst('WithoutRelations', '', get_class($class));
        $seo = Seo::where(['class' => $model, 'reference' => $class->$identifier])->first();
        $locale = $locale ? $locale : config('app.locale');
        if ($seo) {
			$translation = $seo->translate($locale);
			if (!is_null($translation)) {
				config(['backend.config.title' => $translation->title]);
				config(['backend.config.description' => $translation->description]);
				config(['backend.config.tags' => $translation->tags]);
				config(['backend.config.canonical' => $translation->canonical]);
				config(['backend.config.indexable' => $seo->indexable]);
				config(['backend.config.follow' => $seo->follow]);
			}
        }

        return isset($translation) ? $translation : null;
    }


    /**
     * Get Sitemap urls
     *
     * @param $model
     * @param $model_id
     */
    public static function hreflang($model, $model_id)
    {
        $public_routes = ['cookies', 'legal', 'privacy', 'contact'];

        if (!in_array($model, $public_routes))
            $model = Str::replaceFirst('WithoutRelations', '', get_class($model));

        $hreflangs = Sitemap::where('model', $model)->where('model_id', $model_id)->get(['url', 'locale']);

        config(['backend.config.hreflangs' => $hreflangs]);
    }
}
