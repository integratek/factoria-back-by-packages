<?php

namespace Lafactoria\Backend\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Menu extends Model
{
    use Translatable;

    /**
     * Constants to define what the type is.
     */
    const LAYOUT_TYPE = 0;
    const SUBMENU_TYPE = 1;
    const LINK_TYPE = 2;
    const LAYOUT_IMAGE_HEIGHT = 350; // In pixels
    const SUBMENU_CONTENT = false;
    const ROUTE_PREFIX = 'page';
    const HEADERS = ['header1.png', 'header2.png', 'header3.png', 'header4.png', 'header5.png', 'header6.png', 'header7.png', 'header8.png'];

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    public $fillable = [
        'active', 'order', 'type', 'parent_id', 'on_nav', 'on_footer'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['childs', 'imageModel', 'headerImageModel'];

    /**
     * Stores the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title', 'content', 'highlighted', 'url', 'copy_from_english'
    ];

    /**
     * Returns the available types.
     *
     * @return Collection
     */
    public static function types()
    {
        return collect([
            'Layout' => Menu::LAYOUT_TYPE,
            'Submenu' => Menu::SUBMENU_TYPE,
            'Link' => Menu::LINK_TYPE,
        ]);
    }


    /**
     * Get the seo information.
     *
     * @return App\Seo
     */
    public function seo()
    {
        return Seo::where(['class' => get_class($this), 'reference' => $this->id])->first();
    }



    /**
     * Returns the available routes that will be shown in the create/edit dropdown.
     *
     * @return Collection
     */
    public static function routes()
    {
        $routes = collect(config('backend.public_routes'));

        if (Route::has('login') && Route::has('register')) {
            $routes->put('Login', route('login'));
            $routes->put('Register', route('register'));
        }

        return $routes;
    }

    /**
     * Generates the page URL given a menu.
     *
     * @param  Menu   $page
     * @return string
     */
    /*public static function generateUrl(Menu $page)
    {
        return str_replace(url('/'), '', $page->url);
    }*/

    /**
     * Returns the menu parents
     *
     * @return Collection
     */
    public static function parents()
    {
        return Menu::with(['translations','childRows' => function ($query){
            return $query->with('translations');
        }])->where('parent_id', null)->orderBy('order')->get();
    }

    /***
     * RELATION
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childRows (){
        return $this->hasMany(self::class, 'parent_id');
    }

    /**
     * Returns the childs of the parent.
     *
     * @return Collection
     */
    public function getChildsAttribute()
    {
        if ($this->isParent()) {
            return collect([]);
        }

        return Menu::where('parent_id', $this->id)->orderBy('order')->get();
    }

    /**
     * Returns true if the menu is a parent.
     *
     * @return boolean
     */
    public function isParent()
    {
        return $this->parent_id != null;
    }



    /**
     * Returns the HeaderImageModel related to Menu
     *
     * @return \Lafactoria\Backend\Models\HeaderImageModel
     */
    public function getHeaderImageModelAttribute()
    {
        return $this->hasOne(HeaderImageModel::class, 'model_id')->where('model', get_class($this))->first();
    }


    /**
     * Returns the ImageModel related to Menu
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getImageModelAttribute()
    {
        return $this->hasMany(ImageModel::class, 'model_id')->where('model', get_class($this))->get();
    }


    /**
     * Return te menu files
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function files()
    {
        return $this->belongsToMany(Document::class)->withPivot('locale');
    }


    /**
     * Get random image header.
     *
     * @return mixed
     */
    public static function getRandomHeader()
    {
        return asset('assets/'.collect(Menu::HEADERS)->random());
    }

    /**
     * Sitemap routes
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function sitemap($locale)
    {
        return self::leftJoin('seos', function ($join) {
            $join->on('menus.id', '=', 'seos.reference')
                ->where('seos.class','=', 'Lafactoria\Backend\Models\Menu');
        })
            ->leftjoin('seo_translations', function ($join) use ($locale){
                $join->on('seos.id', '=', 'seo_translations.seo_id')
                    ->where('seo_translations.locale', $locale);
            })
            ->where(function ($query) {
                return $query->whereNull('indexable')
                    ->orWhere('indexable', 1);
            })
            ->select('menus.*')
            ->get();
    }

    static function urlFormat($menu, $locale) {
        $path = url('/');

        if (isset($menu->translate($locale)->url)) {
            $url = $menu->translate($locale)->url;
            $result = $path . "/{$locale}". (Str::startsWith($url, '/') ? $url : '/'.$url);

            $result = Str::endsWith($result, '/') ? substr_replace($result, "", -1) : $result;

            Sitemap::updateOrCreate(
                ['model' => get_class($menu), 'model_id' => $menu->id, 'locale' => $locale],
                ['url' => $result]
            );

            return $result;
        }
        return null;
    }
}
