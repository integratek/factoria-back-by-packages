<?php

namespace Lafactoria\Backend\Models;

use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image as IImage;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Image extends BaseList
{
    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'file'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['is_used', 'base64'];

    /**
     * Stores the attributes that are used on filter.
     *
     * @var array
     */
    public $filterAttributes = [
        'name'
    ];

    /**
     * Appends the base64 of the image.
     *
     * @return string
     */
    public function getBase64Attribute()
    {
        $encoded = $this->preview();
        if($encoded != ''){
            return $encoded->encoded;
        }
    }

    /**
     * Get the administrator flag for the user.
     *
     * @return bool
     */
    public function getIsUsedAttribute()
    {
        return $this->isUsed();
    }

    /**
     * Returns if the image is used.
     *
     * @return boolean
     */
    public function isUsed()
    {
        return (bool) ($this->image_models()->count() + $this->header_image_model()->count());
    }


    /**
     * Returns all the ImageModel associated with the image.
     *
     * @return Collection
     */
    public function image_models()
    {
        return $this->hasMany(ImageModel::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function header_image_model()
    {
        return $this->hasOne(HeaderImageModel::class);
    }


    /**
     * Previews the image.
     *
     * @return string
     */
    public function preview($dimensions = null)
    {
        if ($dimensions && count($dimensions) < 2) {
            return '';
        }
        $exists = false;
        $path = 'app/images/' . $this->file;
        if (config('backend.s3')) {
            $exists = Storage::disk('s3')->exists($path);
        } else {
            $exists = file_exists(storage_path($path));
        }
        if(!$exists || $this->file == ''){
            return '';
        }

        try {
            $name = "image_{$this->id}";
            if ($dimensions) {
                $name = "{$name}_{$dimensions[0]}_{$dimensions[1]}";
            }
            $encoded = Cache::rememberForever($name, function () use ($dimensions, $path) {
                $file = null;
                if (config('backend.s3')) {
                    $file = Storage::disk('s3')->get($path);
                } else {
                    $file = storage_path($path);
                }
                $encoded = IImage::make($file);
                if ($dimensions) {
                    $encoded = $encoded->resize($dimensions[0], $dimensions[1]);
                }
                return $encoded->encode('data-url');
            });
        } catch (Exception $e) {
            $encoded = '';
        }

        return $encoded;
    }
}
