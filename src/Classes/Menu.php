<?php

namespace Lafactoria\Backend\Classes;

class Menu
{
    /**
     * Adds an item to the desired administration menu.
     *
     * @param string       $name
     * @param string       $url
     * @param string       $icon
     * @param string       $segment
     * @return  self
     *
     */
    public function add(string $name, string $url, string $icon, string $segment = '') : Menu
    {
        config(['backend.menu' => collect(config('backend.menu'))->push(compact('name', 'url', 'icon', 'segment'))]);

        return $this;
    }
}
