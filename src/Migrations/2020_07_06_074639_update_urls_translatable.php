<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateUrlsTranslatable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $menus = DB::table('menus')->get(['id', 'url']);
        Schema::table('menu_translations', function (Blueprint $table) {
            $table->text('url')->nullable();
        });

        $menus->each(function ($menu) {
            DB::table('menu_translations')
                ->where('menu_id', $menu->id)
                ->update(["url" => $menu->url]);
        });

        Schema::table('menus', function (Blueprint $table) {
            $table->dropColumn('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
