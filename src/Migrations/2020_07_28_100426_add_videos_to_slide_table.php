<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVideosToSlideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('slide_translations', function (Blueprint $table) {
            $table->string('video_name')->nullable();
            $table->string('video_mp4')->nullable();
            $table->string('video_webm')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slide_translations', function (Blueprint $table) {
            $table->dropColumn('video_name');
            $table->dropColumn('video_mp4');
            $table->dropColumn('video_webm');
        });
    }
}
