<?php

use Lafactoria\Backend\Models\General;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
         Schema::create('general_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale')->index();
            $table->integer('general_id');
            $table->string('title');
            $table->longText('text');
            $table->timestamps();
        });

        $locales = config('translatable.locales');

        $cookies_title = ['Cookies', 'Cookies', 'Cookies'];
        $cookies_text = [
            "POLÍTICA DE COOKIES


            Què són les cookies?
            
            Una cookie és un fitxer que es descarrega en el seu ordinador en accedir a determinades pàgines web. Les cookies permeten a una pàgina web, entre altres coses, emmagatzemar i recuperar informació sobre els hàbits de navegació d'un usuari o del seu equip i, depenent de la informació que continguin i de la forma en què utilitzi el seu equip, poden utilitzar-se per reconèixer a l'usuari.
            
            
            Per a què utilitzen les cookies aquesta pàgina web i quins són?
            
            Aquesta pàgina web utilitza les cookies per a una sèrie de finalitats, incloses:
            
            ·        Anàlisi: són aquelles cookies que bé, tractades per nosaltres o per tercers, ens permeten quantificar el nombre d'usuaris i així realitzar el mesurament i anàlisi estadístic de la utilització que fan els usuaris del servei. Per això s'analitza la seva navegació a la nostra pàgina web per tal de millorar l'experiència de l'usuari.
            
            ·        Tècniques: Són aquelles necessàries per a la navegació i el bon funcionament de la nostra pàgina web. Permeten per exemple, controlar el tràfic i la comunicació de dades, accedir a parts d'accés restringit, utilitzar elements de seguretat, emmagatzemar continguts per a poder difondre vídeos o compartir continguts a través de xarxes socials.
            
            ·        Sessió: Les cookies de sessió permeten al lloc que estàs visitant mantenir un seguiment del teu moviment d'una a una altra pàgina, de manera que no et demanarà la informació que ja li has donat anteriorment. Les cookies et permeten moure't per moltes pàgines d'un mateix lloc de manera ràpida i fàcil, sense haver d'autenticar-te de nou o tornar a iniciar el procés en cada zona que visites.
            
            
            
            El detall de les cookies utilitzades en aquesta pàgina web és el següent:
            
            
            Cookies: _ga _gid _gat
            
            Nom: Google Analytics
            
            Tipus: De tercers
            
            Propòsit: Recollir informació sobre la navegació dels usuaris pel lloc amb la finalitat de conèixer l'origen de les visites i altres dades similars a nivell estadístic. No obté dades dels noms o cognoms dels usuaris ni de l'adreça postal concreta des d'on es connecten.
            
            Més informació: Google Analytics Centre de privacitat de Google: http://www.google.com/intl/es/policies/privacy/
            
            
            Cookies: PHPSESSID
            
            Nom: Tècnica
            
            Tipus: Pròpia
            
            Propòsit: S'usa per controlar la sessió de l'usuari. Necessàries per a la navegació i el bon funcionament de la nostra pàgina web. S'activa automàticament.
            
            Més informació: https://www.lafactoriainteractiva.com
            
            
            Cookies: HexCookieSeed
            
            Nom: Avís polìtica cookies
            
            Tipus: Pròpia
            
            Propòsit: Detecta que ja t'hem explicat la nostra política de cookies. Una vegada que l'hagis acceptat o continuïs navegant, gràcies a aquesta cookie no et tornem a mostrar una vegada i una altra el mateix avís.
            
            Més informació: https://www.lafactoriainteractiva.com
            
            
            Cookies:_atuvc _atuvs
            
            Nom: Contingut Xarxes Socials
            
            Tipus: De tercers
            
            Propòsit: Comparteix contingut amb una àmplia gamma plataformes de xarxes i recursos compartits. Emmagatzema un comptador actualitzat de pàgines compartides.
            
            Més informació: https://www.lafactoriainteractiva.com
            
            
            Nota: Les cookies de tipus 'pròpies' són utilitzades només pel propietari d'aquesta web i les cookies 'De tercers' són utilitzades, pel prestador del servei que està detallat en el quadre anterior.
            
            
            Eliminar o bloquejar les cookies
            
            Vostè pot permetre, bloquejar o eliminar les cookies instal·lades en el seu equip mitjançant la configuració de les opcions del navegador instal·lat en el seu ordinador.
            
            Pot veure més informació sobre el seu navegador i les cookies als següents enllaços:
            
                Firefox: http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we
            
            
                Chrome: http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647
            
            
                Internet Explorer: http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9
            
            
                Safari: http://support.apple.com/kb/ph5042
            
            
                Opera: http://help.opera.com/Windows/11.50/es-ES/cookies.html
            
             
            
            Complement d'inhabilitació per a navegadors de Google Analytics
            
            Si desitja rebutjar les cookies analítiques de Google Analytics en tots els navegadors, de manera que no s'enviï informació seva a Google Analitys, pot descarregar un complement que realitza aquesta funció des d'aquest enllaç: https://tools.google.com/dlpage/gaoptout.
            
            ",
            "POLITICA DE COOKIES


            ¿Qué son las cookies?
            
            Una cookie es un fichero que se descarga descarga en su ordenador al acceder a determinadas páginas web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario.
            
            
            ¿Para qué utiliza las cookies esta página web y cuáles son?
            
            Esta página web utiliza las cookies para una serie de finalidades, incluidas:
            
            
                Análisis: son aquellas cookies que bien, tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio. Para ello se analiza su navegación en nuestra página web con el fin de mejorar la experiencia del usuario.
            
            
                Técnicas: Son aquellas necesarias para la navegación y el buen funcionamiento de nuestra página web. Permiten por ejemplo, controlar el tráfico y la comunicación de datos, acceder a partes de acceso restringido, utilizar elementos de seguridad, almacenar contenidos para poder difundir vídeos o compartir contenidos a través de redes sociales.
            
            
                Sesión: Las cookies de sesión permiten al lugar que estás visitando mantener un seguimiento de tu movimiento de una a otra página, de forma que no te pedirá la información que ya le has dado anteriormente. Las cookies te permiten moverte por muchas páginas de un mismo lugar de manera rápida y fácil, sin tener que autenticarte de nuevo o volver a iniciar el proceso en cada zona que visitas.
            
            
            El detalle de las cookies utilizadas en esta página web es el siguiente:
            
            Cookies: _ga _gid _gat
            
            Nombre: Google Analytics
            
            Tipo: De terceros
            
            Propósito: Recoger información sobre la navegación de los usuarios por el sitio con el fin de conocer el origen de las visitas y otros datos similares a nivel estadístico. No obtiene datos de los nombres o apellidos de los usuarios ni de la dirección postal concreta desde donde se conectan.
            
            Más información: Google Analytics Centro de privacidad de Google: http://www.google.com/intl/es/policies/privacy/
            
            
            Cookies: PHPSESSID
            
            Nombre: Cookie de sesión
            
            Tipo: Propia
            
            Propósito: Se usa para controlar la sesión del usuario. Necesarias para la navegación y el buen funcionamiento de nuestra página web. Se activa automáticamente.
            
            Más información: https://www.lafactoriainteractiva.com
            
            
            Cookies: HexCookieSeed
            
            Nombre: Aviso política cookies
            
            Tipo: Propia
            
            Propósito: Detecta que ya te hemos explicado nuestra política de cookies. Una vez que la hayas aceptado o continúes navegando, gracias a esta cookie no te volvemos a mostrar una y otra vez el mismo aviso.
            
            Más información: https://www.lafactoriainteractiva.com
            
            
            Cookies: _atuvc _atuvs
            
            Nombre: Contenido Redes Sociales
            
            Tipo: De terceros
            
            Propósito: Comparte contenido con una amplia gama plataformas de redes y recursos compartidos. Almacena un contador actualizado de páginas compartidas.
            
            Más información: https://www.lafactoriainteractiva.com
            
             
            
            Nota: Las cookies de tipo 'Propias' son utilizadas sólo por el propietario de esta web y las cookies 'De terceros' son utilizadas, por el prestador del servicio que está detallado en el cuadro anterior.
            
            
            Eliminar o bloquear las cookies
            
            Usted puede permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador.
            
            Pueder ver más información sobre su navegador y las cookies en los siguientes enlaces:
            
            
                Firefox: http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we
            
            
            
                Chrome: http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647
            
            
            
                Internet Explorer: http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9
            
            
            
                Safari: http://support.apple.com/kb/ph5042
            
            
            
                Opera: http://help.opera.com/Windows/11.50/es-ES/cookies.html
            
            
            Complemento de inhabilitación para navegadores de Google Analytics
            
            Si desea rechazar las cookies analíticas de Google Analytics en todos los navegadores, de forma que no se envíe información suya a Google Analitys, puede descargar un complemento que realiza esta función desde este enlace: https://tools.google.com/dlpage/gaoptout.",
            "POLITICA DE COOKIES


            ¿Qué son las cookies?
            
            Una cookie es un fichero que se descarga descarga en su ordenador al acceder a determinadas páginas web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario.
            
            
            ¿Para qué utiliza las cookies esta página web y cuáles son?
            
            Esta página web utiliza las cookies para una serie de finalidades, incluidas:
            
                Análisis: son aquellas cookies que bien, tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio. Para ello se analiza su navegación en nuestra página web con el fin de mejorar la experiencia del usuario.
            
            
                Técnicas: Son aquellas necesarias para la navegación y el buen funcionamiento de nuestra página web. Permiten por ejemplo, controlar el tráfico y la comunicación de datos, acceder a partes de acceso restringido, utilizar elementos de seguridad, almacenar contenidos para poder difundir vídeos o compartir contenidos a través de redes sociales.
            
            
                Sesión: Las cookies de sesión permiten al lugar que estás visitando mantener un seguimiento de tu movimiento de una a otra página, de forma que no te pedirá la información que ya le has dado anteriormente. Las cookies te permiten moverte por muchas páginas de un mismo lugar de manera rápida y fácil, sin tener que autenticarte de nuevo o volver a iniciar el proceso en cada zona que visitas.
            
            
            El detalle de las cookies utilizadas en esta página web es el siguiente:
            
            Cookies: _ga _gid _gat
            
            Nombre: Google Analytics
            
            Tipo: De terceros
            
            Propósito: Recoger información sobre la navegación de los usuarios por el sitio con el fin de conocer el origen de las visitas y otros datos similares a nivel estadístico. No obtiene datos de los nombres o apellidos de los usuarios ni de la dirección postal concreta desde donde se conectan.
            
            Más información: Google Analytics Centro de privacidad de Google: http://www.google.com/intl/es/policies/privacy/
            
            
            Cookies: PHPSESSID
            
            Nombre: Cookie de sesión
            
            Tipo: Propia
            
            Propósito: Se usa para controlar la sesión del usuario. Necesarias para la navegación y el buen funcionamiento de nuestra página web. Se activa automáticamente.
            
            Más información: https://www.lafactoriainteractiva.com
            
            
            Cookies: HexCookieSeed
            
            Nombre: Aviso política cookies
            
            Tipo: Propia
            
            Propósito: Detecta que ya te hemos explicado nuestra política de cookies. Una vez que la hayas aceptado o continúes navegando, gracias a esta cookie no te volvemos a mostrar una y otra vez el mismo aviso.
            
            Más información: https://www.lafactoriainteractiva.com
            
            
            Cookies: _atuvc _atuvs
            
            Nombre: Contenido Redes Sociales
            
            Tipo: De terceros
            
            Propósito: Comparte contenido con una amplia gama plataformas de redes y recursos compartidos. Almacena un contador actualizado de páginas compartidas.
            
            Más información: https://www.lafactoriainteractiva.com
            
             
            
            Nota: Las cookies de tipo 'Propias' son utilizadas sólo por el propietario de esta web y las cookies 'De terceros' son utilizadas, por el prestador del servicio que está detallado en el cuadro anterior.
            
            
            Eliminar o bloquear las cookies
            
            Usted puede permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador.
            
            Pueder ver más información sobre su navegador y las cookies en los siguientes enlaces:
            
                Firefox: http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we
            
            
                Chrome: http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647
            
            
                Internet Explorer: http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9
            
            
                Safari: http://support.apple.com/kb/ph5042
            
            
                Opera: http://help.opera.com/Windows/11.50/es-ES/cookies.html
            
            
            Complemento de inhabilitación para navegadores de Google Analytics
            
            Si desea rechazar las cookies analíticas de Google Analytics en todos los navegadores, de forma que no se envíe información suya a Google Analitys, puede descargar un complemento que realiza esta función desde este enlace: https://tools.google.com/dlpage/gaoptout."
        ];

        $cookies_desc_title = ['Cookies descripció', 'Cookies descripción', 'Cookies description'];
        $cookies_desc_text = [
            "Utilitzem cookies pròpies i de tercers per millorar la experiència de navegació. Al continuar amb la navegació entenem que accepta la nostra política de cookies.",
            "Utilizamos cookies propias y de terceros para mejorar la experiencia de navegación. Si continúa navegando, entendemos que acepta nuestra política de cookies.",
            "We use our own and third party cookies to enhance your browsing experience. By continuing to browse the site you are agreeing to accept our use of cookies."
        ];

        $legal_title = ['Avís legal', 'Aviso legal', 'Legal'];
        $legal_text = [
            "Avís Legal


            1.    OBJECTE I ACEPTACIÓ
            
            
            El present avís legal regula l’ús i utilització del lloc web www.lafactoriainteractiva.com (en endavant, LA WEB), del que és titular LA FACTORIA D’IMATGES,SERVEIS GRÀFICS,S.L. (en endavant, EL PROPIETARI DE LA WEB).
            
            La navegació pel lloc web de EL PROPIETARI DE LA WEB li atribueix la condició d’usuari del mateix i comporta la seva acceptació plena i sense reserves de totes i cadascuna de les condicions publicades en aquest avís legal, advertint que aquestes condicions podran ser modificades sense notificació prèvia per part de EL PROPIETARI DE LA WEB, en aquest cas es procedirà a la seva publicació i avís amb la màxima antelació possible.
            
            Per això és recomanable llegir atentament el seu contingut en cas de desitjar accedir i fer ús de la informació i dels serveis oferts des d’aquest lloc web.
            
            L’usuari a més, s’obliga a fer un ús correcte del lloc web de conformitat amb les lleis, la bona fe, l’ordre públic, els usos del tràfic i el present Avís Legal, i respondrà enfront a EL PROPIETARI DE LA WEB o enfront a tercers, de qualsevols danys i perjudicis que poguessin causar-se com a conseqüència de l’incompliment d’aquesta obligació.
            
            Qualsevol utilització diferent a l’autoritzada està expressament prohibida, podent EL PROPIETARI DE LA WEB denegar o retirar l’accés i el seu ús en qualsevol moment.
            
             
            
             
            
            2. IDENTIFICACIÓ I COMUNICACIÓ
            
            EL PROPIETARI DE LA WEB, en compliment de la Llei 34/2002, d’11 de juliol, de Serveis de la Societat de la Informació i de Comerç Electrònic, li informa que:
            
            ·        La seva denominació social és: LA FACTORIA D’IMATGES,SERVEIS GRÀFICS,S.L. 
            
            ·        El seu CIF és: B-63379937
            
            ·        El seu domicili social està a: C/ Gavarresa10 - 08650 – Cabrianes, Inscrita al Registre Mercantil de BARCELONA Volum 36215 , Foli 161, Full B281045, Inscripció 1.
            
            Per a comunicar-se amb nosaltres, posem a la seva disposició diferents mitjans de contacte que detallem a continuació:
            
            ·        Telèfon: 938206362
            
            ·        E-mail: info@lafactoriainteractiva.com
            
            Totes les notificacions i comunicacions entre els usuaris i PROPIETARI DE LA WEB es consideraran eficaces, a tots els efectes, quan es realitzin a través de qualsevol mitjà dels detallats anteriorment.
            
             
            
            3. CONDICIONS D’ACCÉS I UTILIZACIÓ
            
            El lloc web i els seus serveis són d’accés lliure i gratuït. No obstant, PROPIETARI DE LA WEB pot condicionar la utilització d’alguns dels serveis oferts a la seva web al previ emplenament del corresponent formulari.
            
            L’usuari garanteix l’autenticitat i actualitat de totes aquelles dades que comuniqui a PROPIETARI DE LA WEB i serà l’únic responsable de les manifestacions falses o inexactes que realitzi.
            
            L’usuari es compromet expressament a fer un ús adequat dels continguts i serveis de PROPIETARI DE LA WEB i a no emprar-los per a, entre altres:
            
            a.    Difondre continguts delictius, violents, pornogràfics, racistes, xenòfobs, ofensius, d’apologia del terrorisme o, en general, contraris a la llei o a l’ordre públic.
            
            b.    Introduir a la xarxa virus informàtics o realitzar actuacions susceptibles d’alterar, espatllar, interrompre o generar errors o danys en els documents electrònics, dades o sistemes físics i lògics de PROPIETARI DE LA WEB o de terceres persones; així com obstaculitzar l’accés d’altres usuaris al lloc web i als seus serveis mitjançant el consum massiu dels recursos informàtics a través dels quals PROPIETARI DE LA WEB presta els seus serveis.
            
            c.    Intentar accedir als comptes de correu electrònic d’altres usuaris o a àrees restringides dels sistemes informàtics de PROPIETARI DE LA WEB o de tercers i, en el seu cas, extreure informació.
            
            d.    Vulnerar els drets de propietat intel·lectual o industrial, així com violar la confidencialitat de la informació de PROPIETARI DE LA WEB o de tercers.
            
            e.    Suplantar la identitat de qualsevol altre usuari.
            
            f.     Reproduir, copiar, distribuir, posar a disposició de, o qualsevol altra forma de comunicació pública, transformar o modificar els continguts, tret que es compti amb l'autorització del titular dels corresponents drets o això resulti legalment permès.
            
            g.    Recollir dades amb finalitat publicitària i de remetre publicitat de qualsevol classe i comunicacions amb finalitats de venda o unes altres de naturalesa comercial sense que intervingui la seva prèvia sol·licitud o consentiment.
            
            Tots els continguts del lloc web, com textos, fotografies, gràfics, imatges, icones, tecnologia, programari, així com el seu disseny gràfic i codis font, constitueixen una obra la propietat de la qual pertany a PROPIETARI DE LA WEB, sense que puguin entendre's cedits a l'usuari cap dels drets d'explotació sobre els mateixos més enllà de l'estrictament necessari per al correcte ús de la web.
            
            En definitiva, els usuaris que accedeixin a aquest lloc web poden visualitzar els continguts i efectuar, si s’escau, còpies privades autoritzades sempre que els elements reproduïts no siguin cedits posteriorment a tercers, ni s'instal·lin a servidors connectats a xarxes, ni siguin objecte de cap tipus d'explotació.
            
            Així mateix, totes les marques, noms comercials o signes distintius de qualsevol classe que apareixen al lloc web són propietat de PROPIETARI DE LA WEB, sense que pugui entendre's que l'ús o accés al mateix atribueixi a l'usuari dret algun sobre els mateixos.
            
            La distribució, modificació, cessió o comunicació pública dels continguts i qualsevol altre acte que no hagi estat expressament autoritzat pel titular dels drets d'explotació queden prohibits.
            
            L'establiment d'un hipervincle no implica en cap cas l'existència de relacions entre PROPIETARI DE LA WEB i el propietari del lloc web en la qual s'estableixi, ni l'acceptació i aprovació per part de PROPIETARI DE LA WEB dels seus continguts o serveis.
            
            PROPIETARI DE LA WEB no es responsabilitza de l'ús que cada usuari li doni als materials posats a disposició en aquest lloc web ni de les actuacions que realitzi sobre la base dels mateixos.
            
             
            
            4. EXCLUSIÓ DE GARANTIES I DE RESPONSABILITAT
            
            El contingut del present lloc web és de caràcter general i té una finalitat merament informativa, sense que es garanteixi plenament l'accés a tots els continguts, ni la seva exhaustivitat, correcció, vigència o actualitat, ni la seva idoneïtat o utilitat per a un objectiu específic.
            
            PROPIETARI DE LA WEB exclou, fins a on permet l'ordenament jurídic, qualsevol responsabilitat pels danys i perjudicis de tota naturalesa derivats de:
            
            a.    La impossibilitat d'accés al lloc web o la falta de veracitat, exactitud, exhaustivitat i/o actualitat dels continguts, així com l'existència de vicis i defectes de tota classe dels continguts transmesos, difosos, emmagatzemats, posats a disposició, als quals s'hagi accedit a través del lloc web o dels serveis que s'ofereixen.
            
            b.    La presència de virus o d'altres elements en els continguts que puguin produir alteracions en els sistemes informàtics, documents electrònics o dades dels usuaris.
            
            c.    L'incompliment de les lleis, la bona fe, l'ordre públic, els usos del tràfic i el present avís legal com a conseqüència de l'ús incorrecte del lloc web. En particular, i a manera exemplificativa, PROPIETARI DE LA WEB no es fa responsable de les actuacions de tercers que vulnerin drets de propietat intel·lectual i industrial, secrets empresarials, drets a l'honor, a la intimitat personal i familiar i a la pròpia imatge, així com la normativa en matèria de competència deslleial i publicitat il·lícita..
            
            Així mateix, PROPIETARI DE LA WEB declina qualsevol responsabilitat respecte a la informació que es trobi fora d'aquesta web i no sigui gestionada directament pel nostre web màster (administrador del lloc web). La funció dels links que apareixen en aquesta web és exclusivament la d'informar a l'usuari sobre l'existència d'altres fonts susceptibles d'ampliar els continguts que ofereix aquest lloc web. PROPIETARI DE LA WEB no garanteix ni es responsabilitza del funcionament o accessibilitat dels llocs enllaçats; ni suggereix, convida o recomana la visita als mateixos, per la qual cosa tampoc serà responsable del resultat obtingut. PROPIETARI DE LA WEB no es responsabilitza de l'establiment d’hipervincles per part de tercers.
            
             
            
            5. POLÍTICA DE PRIVACITAT
            
            A continuació li indiquem els detalls referits als diferents tractaments de dades personals que realitzem a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.
            
            
             5.1. Tractament de les dades personals de potencials clients i contactes via web
            
             5.1.1. Informació bàsica sobre Protecció de dades
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.
            
            Finalitat:
            
            Prestar els serveis sol·licitats, així com l’enviament de comunicacions comercials i newsletters informatives.
            
            Legitimació:
            
            Execució d’un contracte.
            
            Consentiment de l’interessat.
            
            Destinataris:
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així como altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com.
            
            
            5.1.2. Informació addicional sobre Protecció de dades
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.– NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 – 08650 Cabrianes
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com.
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. tractem la informació que ens faciliten les persones interessades amb la finalitat d’atendre la seva sol·licitud i enviar-li comunicacions comercials, així com els nostres butlletins.
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre l’interessat no sol·liciti la seva supressió.
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Consentiment de l’interessat: Atendre la seva sol·licitud i enviar-li comunicacions comercials, així com els nostres butlletins.
            
            A quins destinataris es comunicaran les seves dades?
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa10 - 08650 – Cabrianes.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web www.agpd.es.
            
            5.2. Tractament de les dades personals de clients
            
             5.2.1. Informació bàsica sobre Protecció de dades
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L
            
            Finalitat:
            
            Gestió de clients i enviament de comunicacions comercials sobre els nostres productes i/o serveis.
            
            Legitimació:
            
            Execució d’un contracte.
            
            Consentiment de l’interessat.
            
            Destinataris:
            
            Estan previstes cessions de dades a: Assessoria fiscal i comptable, registres públics, Bancs i Caixes d'Estalvi, Administració Tributària.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així com altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com.
            
            
            5.2.2. Informació addicional sobre Protecció de dades
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 – 08650 CABRIANES
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L tractem la informació que ens faciliten les persones interessades amb la finalitat de gestionar la gestió administrativa, comptable i fiscal dels serveis sol·licitats, així com enviar comunicacions comercials sobre els nostres productes i serveis.
            
            No es van a prendre decisions automatitzades en base de dades proporcionats.
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre es mantingui la relació comercial, i si escau, durant els anys necessaris per complir amb les obligacions legals.
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Consentiment de l’interessat: Enviar-li comunicacions comercials.
            
            A quins destinataris es comunicaran les seves dades?
            
            Les dades es comunicaran als següents destinataris:
            
            ·        Assessoria fiscal i comptable, registres públics, Bancs i Caixes d'Estalvi, Administració Tributària, amb la finalitat de Complir amb les obligacions legals.
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa,10 08650 CABRIANES.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web: www.agpd.es.
            
            5.3. Tractament de les dades personals de contactes de correu electrònic
            
             5.3.1. Informació bàsica sobre Protecció de dades
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L
            
            Finalitat:
            
            Prestar els serveis sol·licitats, així com l’enviament de comunicacions comercials i newsletters informatives.
            
            Legitimació:
            
            Execució d’un contracte, Interès legítim del responsable o Consentiment de l’interessat.
            
            Destinataris:
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així como altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com
            
            
            5.3.2. Informació addicional sobre Protecció de dades
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 08650 - CABRIANES
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L tractem la informació que ens faciliten les persones interessades amb la finalitat de prestar els serveis que ens ha sol·licitat, atendre les seves sol·licituds d’informació i enviar-li comunicacions comercials.
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre l’interessat no sol·liciti la seva supressió.
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Interès legítim del responsable: Enviament de comunicacions comercials sobre els nostres serveis i/o productes.
            
            ·        Consentiment de l’interessat: Enviament de comunicacions comercials sobre els nostres serveis i/o productes en base a sol·licituds prèvies mantingudes amb el responsable del tractament.
            
            A quins destinataris es comunicaran les seves dades?
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa,10 08650 CABRIANES.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web: www.agpd.es.
            
             
            
            6. PROCEDIMENT EN CAS DE REALITZACIÓ D'ACTIVITATS DE CARÀCTER IL·LÍCIT
            
            En el cas que qualsevol usuari o un tercer consideri que existeixen fets o circumstàncies que revelin el caràcter il·lícit de la utilització de qualsevol contingut i/o de la realització de qualsevol activitat a les pàgines web incloses o accessibles a través del lloc web, haurà d'enviar una notificació a PROPIETARI DE LA WEB identificant-se degudament i especificant les suposades infraccions i declarant expressament i sota la seva responsabilitat que la informació proporcionada en la notificació és exacta.
            
             Per a tota qüestió litigiosa que incumbeixi al lloc web del PROPIETARI DE LA WEB, serà aplicable la legislació espanyola, sent competents els Jutjats i Tribunals de Barcelona (España).
            
             
            
            7. PUBLICACIONS
            
            La informació administrativa facilitada a través del lloc web no substitueix la publicitat legal de les lleis, normatives, plans, disposicions generals i actes que hagin de ser publicats formalment als diaris oficials de les administracions públiques, que constitueixen l'únic instrument que dona fe de la seva autenticitat i contingut. La informació disponible en aquest lloc web ha d'entendre's com una guia sense propòsit de validesa legal.",
            "Aviso Legal


            1. OBJETO Y ACEPTACIÓN
            
            
            El presente aviso legal regula el uso y utilización del sitio web www.lafactoriainteractiva.com (en adelante, LA WEB), del que es titular LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L. (en adelante, LA FACTORIA).
            
            La navegación por el sitio web de LA FACTORIA le atribuye la condición de usuario del mismo y conlleva su aceptación plena y sin reservas de todas y cada una de las condiciones publicadas en este aviso legal, advirtiendo de que dichas condiciones podrán ser modificadas sin notificación previa por parte de WINDAT, en cuyo caso se procederá a su publicación y aviso con la máxima antelación posible.
            
            Por ello es recomendable leer atentamente su contenido en caso de desear acceder y hacer uso de la información y de los servicios ofrecidos desde este sitio web.
            
            El usuario además, se obliga a hacer un uso correcto del sitio web de conformidad con las leyes, la buena fe, el orden público, los usos del tráfico y el presente Aviso Legal, y responderá frente a LA FACTORIA o frente a terceros, de cualesquiera daños y perjuicios que pudieran causarse como consecuencia del incumplimiento de dicha obligación.
            
            Cualquier utilización distinta a la autorizada está expresamente prohibida, pudiendo LA FACTORIA denegar o retirar el acceso y su uso en cualquier momento.
            
            
            2. IDENTIFICACIÓN Y COMUNICACIONES
            
            
            LA FACTORIA, en cumplimiento de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, le informa de que:
            
            Su denominación social es: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.
            
            Su nombre comercial es: LA FACTORIA
            
            Su CIF es: B-63379937
            
            Su domicilio social está en: C/ Gavarresa10 - 08650 – Cabrianes
            
            Inscrita en el Registro Mercantil de BARCELONA Volum 36215, Foli 161, Full B281045, Inscripció 1.
            
            Para comunicarse con nosotros, ponemos a su disposición diferentes medios de contacto que detallamos a continuación:
            
            Teléfono: 938206362
            
            Email: info@lafactoriainteractiva.com
            
            Todas las notificaciones y comunicaciones entre los usuarios y LA FACTORIA se considerarán eficaces, a todos los efectos, cuando se realicen a través de cualquier medio de los detallados anteriormente.
            
             
            
            3. CONDICIONES DE ACCESO Y UTILIZACIÓN
            
            
            El sitio web y sus servicios son de acceso libre y gratuito. No obstante, WINDAT puede condicionar la utilización de algunos de los servicios ofrecidos en su web a la previa cumplimentación del correspondiente formulario.
            
            El usuario garantiza la autenticidad y actualidad de todos aquellos datos que comunique a LA FACTORIA y será el único responsable de las manifestaciones falsas o inexactas que realice.
            
            El usuario se compromete expresamente a hacer un uso adecuado de los contenidos y servicios de LA FACTORIA y a no emplearlos para, entre otros:
            
            a.  Difundir contenidos delictivos, violentos, pornográficos, racistas, xenófobos, ofensivos, de apología del terrorismo o, en general, contrarios a la ley o al orden público.
            
            b.  Introducir en la red virus informáticos o realizar actuaciones susceptibles de alterar, estropear, interrumpir o generar errores o daños en los documentos electrónicos, datos o sistemas físicos y lógicos de LA FACTORIA o de terceras personas; así como obstaculizar el acceso de otros usuarios al sitio web y a sus servicios mediante el consumo masivo de los recursos informáticos a través de los cuales LA FACTORIA presta sus servicios.
            
            c.   Intentar acceder a las cuentas de correo electrónico de otros usuarios o a áreas restringidas de los sistemas informáticos de LA FACTORIA o de terceros y, en su caso, extraer información.
            
            d.  Vulnerar los derechos de propiedad intelectual o industrial, así como violar la confidencialidad de la información de LA FACTORIA o de terceros.
            
            e.   Suplantar la identidad de cualquier otro usuario.
            
            f.    Reproducir, copiar, distribuir, poner a disposición de, o cualquier otra forma de comunicación pública, transformar o modificar los contenidos, a menos que se cuente con la autorización del titular de los correspondientes derechos o ello resulte legalmente permitido.
            
            g.  Recabar datos con finalidad publicitaria y de remitir publicidad de cualquier clase y comunicaciones con fines de venta u otras de naturaleza comercial sin que medie su previa solicitud o consentimiento.
            
            Todos los contenidos del sitio web, como textos, fotografías, gráficos, imágenes, iconos, tecnología, software, así como su diseño gráfico y códigos fuente, constituyen una obra cuya propiedad pertenece a LA FACTORIA, sin que puedan entenderse cedidos al usuario ninguno de los derechos de explotación sobre los mismos más allá de lo estrictamente necesario para el correcto uso de la web.
            
            En definitiva, los usuarios que accedan a este sitio web pueden visualizar los contenidos y efectuar, en su caso, copias privadas autorizadas siempre que los elementos reproducidos no sean cedidos posteriormente a terceros, ni se instalen a servidores conectados a redes, ni sean objeto de ningún tipo de explotación.
            
            Asimismo, todas las marcas, nombres comerciales o signos distintivos de cualquier clase que aparecen en el sitio web son propiedad de WINDAT, sin que pueda entenderse que el uso o acceso al mismo atribuya al usuario derecho alguno sobre los mismos.
            
            La distribución, modificación, cesión o comunicación pública de los contenidos y cualquier otro acto que no haya sido expresamente autorizado por el titular de los derechos de explotación quedan prohibidos.
            
            El establecimiento de un hiperenlace no implica en ningún caso la existencia de relaciones entre LA FACTORIA y el propietario del sitio web en la que se establezca, ni la aceptación y aprobación por parte de LA FACTORIA de sus contenidos o servicios.
            
            LA FACTORIA no se responsabiliza del uso que cada usuario le dé a los materiales puestos a disposición en este sitio web ni de las actuaciones que realice en base a los mismos.
            
            
            4. EXCLUSIÓN DE GARANTÍAS Y DE RESPONSABILIDAD
            
            
            El contenido del presente sitio web es de carácter general y tiene una finalidad meramente informativa, sin que se garantice plenamente el acceso a todos los contenidos, ni su exhaustividad, corrección, vigencia o actualidad, ni su idoneidad o utilidad para un objetivo específico.
            
            LA FACTORIA excluye, hasta donde permite el ordenamiento jurídico, cualquier responsabilidad por los daños y perjuicios de toda naturaleza derivados de:
            
            La imposibilidad de acceso al sitio web o la falta de veracidad, exactitud, exhaustividad y/o actualidad de los contenidos, así como la existencia de vicios y defectos de toda clase de los contenidos transmitidos, difundidos, almacenados, puestos a disposición, a los que se haya accedido a través del sitio web o de los servicios que se ofrecen.
            
            La presencia de virus o de otros elementos en los contenidos que puedan producir alteraciones en los sistemas informáticos, documentos electrónicos o datos de los usuarios.
            
            El incumplimiento de las leyes, la buena fe, el orden público, los usos del tráfico y el presente aviso legal como consecuencia del uso incorrecto del sitio web. En particular, y a modo ejemplificativo, LA FACTORIA no se hace responsable de las actuaciones de terceros que vulneren derechos de propiedad intelectual e industrial, secretos empresariales, derechos al honor, a la intimidad personal y familiar y a la propia imagen, así como la normativa en materia de competencia desleal y publicidad ilícita.
            
            Asimismo, LA FACTORIA declina cualquier responsabilidad respecto a la información que se halle fuera de esta web y no sea gestionada directamente por nuestro webmaster. La función de los links que aparecen en esta web es exclusivamente la de informar al usuario sobre la existencia de otras fuentes susceptibles de ampliar los contenidos que ofrece este sitio web. LA FACTORIA no garantiza ni se responsabiliza del funcionamiento o accesibilidad de los sitios enlazados; ni sugiere, invita o recomienda la visita a los mismos, por lo que tampoco será responsable del resultado obtenido. LA FACTORIA no se responsabiliza del establecimiento de hipervínculos por parte de terceros.
            
            
            5. POLÍTICA DE PRIVACIDAD
            
            
            A continuación le indicamos los detalles referidos a los distintos tratamientos de datos personales que realizamos en LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L. 
            
            
            5.1. Tratamiento de los datos personales de potenciales clientes y contactos vía web
            
            5.1.1. Información básica sobre Protección de datos
            
            ﻿
            
            Responsable:          
            
            LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L.
            
            
            Finalidad:     
            
            Prestar los servicios solicitados, así como el envío de comunicaciones comerciales y newsletter informativo.
            
            
            Legitimación:         
            
            Ejecución de un contrato.
            
            Consentimiento del interesado.
            
            
            Destinatarios:         
            
            No se cederán datos a terceros, salvo obligación legal.
            
            
            Derechos:    
            
            Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, indicados en la información adicional, que puede ejercer dirigiéndose a la dirección del responsable del tratamiento en info@lafactoriainteractiva.com
            
             
            
            5.1.2. Información adicional sobre protección de datos
            
            
            ¿Quién es el responsable del tratamiento de sus datos?
            
            Entidad: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L.– NIF: B63379937
            
            Dirección postal: C/ Gavarresa,10 – 08650 Cabrianes
            
            Teléfono: 938206362
            
            Correo electrónico: info@lafactoriainteractiva.com.
            
            
            ¿Con qué finalidad tratamos sus datos personales?
            
            En LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. tratamos la información que nos facilitan las personas interesadas con el fin de atender su solicitud y enviarle comunicaciones comerciales, así como nuestros boletines.
            
            
            ¿Por cuánto tiempo conservaremos sus datos?
            
            Los datos se conservarán mientras el interesado no solicite su supresión.
            
            
            ¿Cuál es la legitimación para el tratamiento de sus datos?
            
            Le indicamos la base legal para el tratamiento de sus datos:
            
            Ejecución de un contrato: Prestación de los servicios solicitados
            
            Consentimiento del interesado: Atender su solicitud y enviarle comunicaciones comerciales, así como nuestros boletines.
            
            
            ¿A qué destinatarios se comunicarán sus datos?
            
            No se cederán datos a terceros, salvo obligación legal.
            
            Transferencias de datos a terceros países.
            
            No están previstas transferencias de datos a terceros países.
            
            
            ¿Cuáles son sus derechos cuando nos facilita sus datos?
            
            Cualquier persona tiene derecho a obtener confirmación sobre si en LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L estamos tratando datos personales que les conciernan, o no.
            
            Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos.
            
            En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones. Asimismo, en los supuestos legalmente establecidos, tendrá derecho a la portabilidad de sus datos personales.
            
            En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos. En este caso, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L dejará de tratar los datos, salvo por motivos legítimos imperiosos, o el ejercicio o la defensa de posibles reclamaciones.
            
            Podrá ejercitar materialmente sus derechos de la siguiente forma: enviando un correo electrónico a info@lafactoriainteractiva.com o una carta a C/ Gavarresa, 10 – 08650 - Cabrianes.
            
            Cuando se realice el envío de comunicaciones comerciales utilizando como base jurídica el interés legítimo del responsable, el interesado podrá oponerse al tratamiento de sus datos con ese fin.
            
            Si ha otorgado su consentimiento para alguna finalidad concreta, tiene derecho a retirar el consentimiento otorgado en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.
            
            En caso de que sienta vulnerados sus derechos en lo concerniente a la protección de sus datos personales, especialmente cuando no haya obtenido satisfacción en el ejercicio de sus derechos, puede presentar una reclamación ante la Autoridad de Control en materia de Protección de Datos competente a través de su sitio web: www.agpd.es.
            
             
            
            5.2. Tratamiento de los datos personales de clientes
            
            5.2.1. Información básica sobre Protección de datos
            
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L
            
            Finalidad:
            
            Gestión de clientes y envío de comunicaciones comerciales sobre nuestros productos y/o servicios.
            
            Legitimación:
            
            Ejecución de un contrato.
            
            Consentimiento del interesado.
            
            Destinatarios:
            
            Están previstas cesiones de datos a: Asesoría fiscal y contable, registros públicos, Bancos y Cajas de Ahorro, Administración Tributaria.
            
            Derechos:
            
            Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, indicados en la información adicional, que puede ejercer dirigiéndose a la dirección del responsable del tratamiento en info@lafactoriainteractiva.com
            
            
             5.2.2. Información adicional sobre protección de datos
            
            
            ¿Quién es el responsable del tratamiento de sus datos?
            
            Entidad: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Dirección postal: C/ Gavarresa,10 – 08650 CABRIANES
            
            Teléfono: 938206362
            
            Correo electrónico: info@lafactoriainteractiva.com
            
            
            ¿Con qué finalidad tratamos sus datos personales?
            
            En LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L tratamos la información que nos facilitan las personas interesadas con el fin de realizar la gestión administrativa, contable y fiscal de los servicios solicitados, así como enviar comunicaciones comerciales sobre nuestros productos y servicios.
            
            No se van a tomar decisiones automatizadas en base de datos proporcionados.
            
            
            ¿Por cuánto tiempo conservaremos sus datos?
            
            Los datos se conservarán mientras se mantenga la relación comercial, y en su caso, durante los años necesarios para cumplir con las obligaciones legales.
            
            
            ¿Cuál es la legitimación para el tratamiento de sus datos?
            
            Le indicamos la base legal para el tratamiento de sus datos:
            
            Ejecución de un contrato: Prestación de los servicios solicitados
            
            Consentimiento del interesado: Envío de comunicaciones comerciales
            
            
            ¿A qué destinatarios se comunicarán sus datos?
            
            Los datos se comunicarán a los siguientes destinatarios:
            
            Asesoría fiscal y contable, registros públicos, Bancos y Cajas de Ahorro, Administración Tributaria, con la finalidad de Cumplir con las obligaciones legales.
            
            
            Transferencias de datos a terceros países
            
            No están previstas transferencias de datos a terceros países.
            
            
            ¿Cuáles son sus derechos cuando nos facilita sus datos?
            
            Cualquier persona tiene derecho a obtener confirmación sobre si en LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L. estamos tratando, o no, datos personales que les conciernan.
            
            Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos. Igualmente tiene derecho a la portabilidad de sus datos.
            
            En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones.
            
            En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos. En este caso, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L. dejará de tratar los datos, salvo por motivos legítimos imperiosos, o el ejercicio o la defensa de posibles reclamaciones.
            
            Podrá ejercitar materialmente sus derechos de la siguiente forma: enviando un correo electrónico a info@lafactoriainteractiva.com o una carta a C/ Gavarresa, 10 - 08650 – Cabrianes.
            
            Cuando se realice el envío de comunicaciones comerciales utilizando como base jurídica el interés legítimo del responsable, el interesado podrá oponerse al tratamiento de sus datos con ese fin.
            
            Si ha otorgado su consentimiento para alguna finalidad concreta, tiene derecho a retirar el consentimiento otorgado en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.
            
            En caso de que sienta vulnerados sus derechos en lo concerniente a la protección de sus datos personales, especialmente cuando no haya obtenido satisfacción en el ejercicio de sus derechos, puede presentar una reclamación ante la Autoridad de Control en materia de Protección de Datos competente a través de su sitio web: www.agpd.es.
            
            
            5.3. Tratamiento de los datos personales de los contactos de correo electrónico
            
             5.3.1. Información básica sobre Protección de datos
            
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L
            
            Finalidad:
            
            Prestarle los servicios que nos ha solicitado, atender sus solicitudes de información y enviarle comunicaciones comerciales.
            
            Legitimación:
            
            Ejecución de contrato, Interés legítimo del responsable o Consentimiento del Interesado.
            
            Destinatarios:
            
            No se cederán datos a terceros, salvo obligación legal.
            
            Derechos:
            
            Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, indicados en la información adicional, que puede ejercer dirigiéndose a la dirección del responsable del tratamiento en info@lafactoriainteractiva.com
            
             
            
            5.3.2. Información adicional sobre protección de datos
            
            
            ¿Quién es el responsable del tratamiento de sus datos?
            
            Entidad: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Dirección postal: C/ Gavarresa,10 08650 - CABRIANES 
            
            Teléfono: 938206362
            
            Correo electrónico: info@lafactoriainteractiva.com
            
            
            ¿Con qué finalidad tratamos sus datos personales?
            
            En LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L tratamos la información que nos facilitan las personas interesadas con el fin de prestarle los servicios que nos ha solicitado, atender sus solicitudes de información y enviarle comunicaciones comerciales.
            
            
            ¿Por cuánto tiempo conservaremos sus datos?
            
            Los datos se conservarán mientras el interesado no solicite su supresión.
            
            
            ¿Cuál es la legitimación para el tratamiento de sus datos?
            
            Le indicamos la base legal para el tratamiento de sus datos:
            
            Ejecución de contrato: Prestación los servicios solicitados
            
            Interés legítimo del responsable: Envío de comunicaciones comerciales sobre nuestros servicios y/o productos.
            
            Consentimiento del Interesado: Envío de comunicaciones comerciales sobre nuestros servicios y/o productos en base a solicitudes previas mantenidas con el responsable del tratamiento.
            
            
            ¿A qué destinatarios se comunicarán sus datos?
            
            No se cederán datos a terceros, salvo obligación legal.
            
            
            Transferencias de datos a terceros países.
            
            No están previstas transferencias de datos a terceros países.
            
            
            ¿Cuáles son sus derechos cuando nos facilita sus datos?
            
            Cualquier persona tiene derecho a obtener confirmación sobre si en LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L estamos tratando datos personales que les conciernan, o no.
            
            Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos.
            
            En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones. Asimismo, en los supuestos legalmente establecidos, tendrá derecho a la portabilidad de sus datos personales.
            
            En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos. En este caso, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L dejará de tratar los datos, salvo por motivos legítimos imperiosos, o el ejercicio o la defensa de posibles reclamaciones.
            
            Podrá ejercitar materialmente sus derechos de la siguiente forma: enviando un correo electrónico a info@lafactoriainteractiva.com o una carta a C/ Gavarresa, 10 - 08650 – Cabrianes.
            
            Cuando se realice el envío de comunicaciones comerciales utilizando como base jurídica el interés legítimo del responsable, el interesado podrá oponerse al tratamiento de sus datos con ese fin.
            
            Si ha otorgado su consentimiento para alguna finalidad concreta, tiene derecho a retirar el consentimiento otorgado en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.
            
            En caso de que sienta vulnerados sus derechos en lo concerniente a la protección de sus datos personales, especialmente cuando no haya obtenido satisfacción en el ejercicio de sus derechos, puede presentar una reclamación ante la Autoridad de Control en materia de Protección de Datos competente a través de su sitio web: www.agpd.es.
            
            
            6. PROCEDIMIENTO EN CASO DE REALIZACIÓN DE ACTIVIDADES DE CARÁCTER ILÍCITO
            
            
            En el caso de que cualquier usuario o un tercero considere que existen hechos o circunstancias que revelen el carácter ilícito de la utilización de cualquier contenido y/o de la realización de cualquier actividad en las páginas web incluidas o accesibles a través del sitio web, deberá enviar una notificación a LA FACTORIA identificándose debidamente, especificando las supuestas infracciones y declarando expresamente y bajo su responsabilidad que la información proporcionada en la notificación es exacta.
            
            Para toda cuestión litigiosa que incumba al sitio web de LA FACTORIA, será de aplicación la legislación española, siendo competentes los Juzgados y Tribunales de Barcelona (España).
            
            
            7. PUBLICACIONES
            
            
            La información administrativa facilitada a través del sitio web no sustituye la publicidad legal de las leyes, normativas, planes, disposiciones generales y actos que tengan que ser publicados formalmente a los diarios oficiales de las administraciones públicas, que constituyen el único instrumento que da fe de su autenticidad y contenido. La información disponible en este sitio web debe entenderse como una guía sin propósito de validez legal.",
            "Avís Legal


            1.    OBJECTE I ACEPTACIÓ
            
            
            El present avís legal regula l’ús i utilització del lloc web www.lafactoriainteractiva.com (en endavant, LA WEB), del que és titular LA FACTORIA D’IMATGES,SERVEIS GRÀFICS,S.L. (en endavant, EL PROPIETARI DE LA WEB).
            
            La navegació pel lloc web de EL PROPIETARI DE LA WEB li atribueix la condició d’usuari del mateix i comporta la seva acceptació plena i sense reserves de totes i cadascuna de les condicions publicades en aquest avís legal, advertint que aquestes condicions podran ser modificades sense notificació prèvia per part de EL PROPIETARI DE LA WEB, en aquest cas es procedirà a la seva publicació i avís amb la màxima antelació possible.
            
            Per això és recomanable llegir atentament el seu contingut en cas de desitjar accedir i fer ús de la informació i dels serveis oferts des d’aquest lloc web.
            
            L’usuari a més, s’obliga a fer un ús correcte del lloc web de conformitat amb les lleis, la bona fe, l’ordre públic, els usos del tràfic i el present Avís Legal, i respondrà enfront a EL PROPIETARI DE LA WEB o enfront a tercers, de qualsevols danys i perjudicis que poguessin causar-se com a conseqüència de l’incompliment d’aquesta obligació.
            
            Qualsevol utilització diferent a l’autoritzada està expressament prohibida, podent EL PROPIETARI DE LA WEB denegar o retirar l’accés i el seu ús en qualsevol moment.
            
             
            
             
            
            2. IDENTIFICACIÓ I COMUNICACIÓ
            
            EL PROPIETARI DE LA WEB, en compliment de la Llei 34/2002, d’11 de juliol, de Serveis de la Societat de la Informació i de Comerç Electrònic, li informa que:
            
            ·        La seva denominació social és: LA FACTORIA D’IMATGES,SERVEIS GRÀFICS,S.L. 
            
            ·        El seu CIF és: B-63379937
            
            ·        El seu domicili social està a: C/ Gavarresa10 - 08650 – Cabrianes, Inscrita al Registre Mercantil de BARCELONA Volum 36215 , Foli 161, Full B281045, Inscripció 1.
            
            Per a comunicar-se amb nosaltres, posem a la seva disposició diferents mitjans de contacte que detallem a continuació:
            
            ·        Telèfon: 938206362
            
            ·        E-mail: info@lafactoriainteractiva.com
            
            Totes les notificacions i comunicacions entre els usuaris i PROPIETARI DE LA WEB es consideraran eficaces, a tots els efectes, quan es realitzin a través de qualsevol mitjà dels detallats anteriorment.
            
             
            
            3. CONDICIONS D’ACCÉS I UTILIZACIÓ
            
            El lloc web i els seus serveis són d’accés lliure i gratuït. No obstant, PROPIETARI DE LA WEB pot condicionar la utilització d’alguns dels serveis oferts a la seva web al previ emplenament del corresponent formulari.
            
            L’usuari garanteix l’autenticitat i actualitat de totes aquelles dades que comuniqui a PROPIETARI DE LA WEB i serà l’únic responsable de les manifestacions falses o inexactes que realitzi.
            
            L’usuari es compromet expressament a fer un ús adequat dels continguts i serveis de PROPIETARI DE LA WEB i a no emprar-los per a, entre altres:
            
            a.    Difondre continguts delictius, violents, pornogràfics, racistes, xenòfobs, ofensius, d’apologia del terrorisme o, en general, contraris a la llei o a l’ordre públic.
            
            b.    Introduir a la xarxa virus informàtics o realitzar actuacions susceptibles d’alterar, espatllar, interrompre o generar errors o danys en els documents electrònics, dades o sistemes físics i lògics de PROPIETARI DE LA WEB o de terceres persones; així com obstaculitzar l’accés d’altres usuaris al lloc web i als seus serveis mitjançant el consum massiu dels recursos informàtics a través dels quals PROPIETARI DE LA WEB presta els seus serveis.
            
            c.    Intentar accedir als comptes de correu electrònic d’altres usuaris o a àrees restringides dels sistemes informàtics de PROPIETARI DE LA WEB o de tercers i, en el seu cas, extreure informació.
            
            d.    Vulnerar els drets de propietat intel·lectual o industrial, així com violar la confidencialitat de la informació de PROPIETARI DE LA WEB o de tercers.
            
            e.    Suplantar la identitat de qualsevol altre usuari.
            
            f.     Reproduir, copiar, distribuir, posar a disposició de, o qualsevol altra forma de comunicació pública, transformar o modificar els continguts, tret que es compti amb l'autorització del titular dels corresponents drets o això resulti legalment permès.
            
            g.    Recollir dades amb finalitat publicitària i de remetre publicitat de qualsevol classe i comunicacions amb finalitats de venda o unes altres de naturalesa comercial sense que intervingui la seva prèvia sol·licitud o consentiment.
            
            Tots els continguts del lloc web, com textos, fotografies, gràfics, imatges, icones, tecnologia, programari, així com el seu disseny gràfic i codis font, constitueixen una obra la propietat de la qual pertany a PROPIETARI DE LA WEB, sense que puguin entendre's cedits a l'usuari cap dels drets d'explotació sobre els mateixos més enllà de l'estrictament necessari per al correcte ús de la web.
            
            En definitiva, els usuaris que accedeixin a aquest lloc web poden visualitzar els continguts i efectuar, si s’escau, còpies privades autoritzades sempre que els elements reproduïts no siguin cedits posteriorment a tercers, ni s'instal·lin a servidors connectats a xarxes, ni siguin objecte de cap tipus d'explotació.
            
            Així mateix, totes les marques, noms comercials o signes distintius de qualsevol classe que apareixen al lloc web són propietat de PROPIETARI DE LA WEB, sense que pugui entendre's que l'ús o accés al mateix atribueixi a l'usuari dret algun sobre els mateixos.
            
            La distribució, modificació, cessió o comunicació pública dels continguts i qualsevol altre acte que no hagi estat expressament autoritzat pel titular dels drets d'explotació queden prohibits.
            
            L'establiment d'un hipervincle no implica en cap cas l'existència de relacions entre PROPIETARI DE LA WEB i el propietari del lloc web en la qual s'estableixi, ni l'acceptació i aprovació per part de PROPIETARI DE LA WEB dels seus continguts o serveis.
            
            PROPIETARI DE LA WEB no es responsabilitza de l'ús que cada usuari li doni als materials posats a disposició en aquest lloc web ni de les actuacions que realitzi sobre la base dels mateixos.
            
             
            
            4. EXCLUSIÓ DE GARANTIES I DE RESPONSABILITAT
            
            El contingut del present lloc web és de caràcter general i té una finalitat merament informativa, sense que es garanteixi plenament l'accés a tots els continguts, ni la seva exhaustivitat, correcció, vigència o actualitat, ni la seva idoneïtat o utilitat per a un objectiu específic.
            
            PROPIETARI DE LA WEB exclou, fins a on permet l'ordenament jurídic, qualsevol responsabilitat pels danys i perjudicis de tota naturalesa derivats de:
            
            a.    La impossibilitat d'accés al lloc web o la falta de veracitat, exactitud, exhaustivitat i/o actualitat dels continguts, així com l'existència de vicis i defectes de tota classe dels continguts transmesos, difosos, emmagatzemats, posats a disposició, als quals s'hagi accedit a través del lloc web o dels serveis que s'ofereixen.
            
            b.    La presència de virus o d'altres elements en els continguts que puguin produir alteracions en els sistemes informàtics, documents electrònics o dades dels usuaris.
            
            c.    L'incompliment de les lleis, la bona fe, l'ordre públic, els usos del tràfic i el present avís legal com a conseqüència de l'ús incorrecte del lloc web. En particular, i a manera exemplificativa, PROPIETARI DE LA WEB no es fa responsable de les actuacions de tercers que vulnerin drets de propietat intel·lectual i industrial, secrets empresarials, drets a l'honor, a la intimitat personal i familiar i a la pròpia imatge, així com la normativa en matèria de competència deslleial i publicitat il·lícita..
            
            Així mateix, PROPIETARI DE LA WEB declina qualsevol responsabilitat respecte a la informació que es trobi fora d'aquesta web i no sigui gestionada directament pel nostre web màster (administrador del lloc web). La funció dels links que apareixen en aquesta web és exclusivament la d'informar a l'usuari sobre l'existència d'altres fonts susceptibles d'ampliar els continguts que ofereix aquest lloc web. PROPIETARI DE LA WEB no garanteix ni es responsabilitza del funcionament o accessibilitat dels llocs enllaçats; ni suggereix, convida o recomana la visita als mateixos, per la qual cosa tampoc serà responsable del resultat obtingut. PROPIETARI DE LA WEB no es responsabilitza de l'establiment d’hipervincles per part de tercers.
            
             
            
            5. POLÍTICA DE PRIVACITAT
            
            A continuació li indiquem els detalls referits als diferents tractaments de dades personals que realitzem a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.
            
            
             5.1. Tractament de les dades personals de potencials clients i contactes via web
            
             5.1.1. Informació bàsica sobre Protecció de dades
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.
            
            Finalitat:
            
            Prestar els serveis sol·licitats, així com l’enviament de comunicacions comercials i newsletters informatives.
            
            Legitimació:
            
            Execució d’un contracte.
            
            Consentiment de l’interessat.
            
            Destinataris:
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així como altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com.
            
            
            5.1.2. Informació addicional sobre Protecció de dades
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.– NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 – 08650 Cabrianes
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com.
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. tractem la informació que ens faciliten les persones interessades amb la finalitat d’atendre la seva sol·licitud i enviar-li comunicacions comercials, així com els nostres butlletins.
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre l’interessat no sol·liciti la seva supressió.
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Consentiment de l’interessat: Atendre la seva sol·licitud i enviar-li comunicacions comercials, així com els nostres butlletins.
            
            A quins destinataris es comunicaran les seves dades?
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa10 - 08650 – Cabrianes.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web www.agpd.es.
            
            5.2. Tractament de les dades personals de clients
            
             5.2.1. Informació bàsica sobre Protecció de dades
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L
            
            Finalitat:
            
            Gestió de clients i enviament de comunicacions comercials sobre els nostres productes i/o serveis.
            
            Legitimació:
            
            Execució d’un contracte.
            
            Consentiment de l’interessat.
            
            Destinataris:
            
            Estan previstes cessions de dades a: Assessoria fiscal i comptable, registres públics, Bancs i Caixes d'Estalvi, Administració Tributària.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així com altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com.
            
            
            5.2.2. Informació addicional sobre Protecció de dades
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 – 08650 CABRIANES
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L tractem la informació que ens faciliten les persones interessades amb la finalitat de gestionar la gestió administrativa, comptable i fiscal dels serveis sol·licitats, així com enviar comunicacions comercials sobre els nostres productes i serveis.
            
            No es van a prendre decisions automatitzades en base de dades proporcionats.
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre es mantingui la relació comercial, i si escau, durant els anys necessaris per complir amb les obligacions legals.
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Consentiment de l’interessat: Enviar-li comunicacions comercials.
            
            A quins destinataris es comunicaran les seves dades?
            
            Les dades es comunicaran als següents destinataris:
            
            ·        Assessoria fiscal i comptable, registres públics, Bancs i Caixes d'Estalvi, Administració Tributària, amb la finalitat de Complir amb les obligacions legals.
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa,10 08650 CABRIANES.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web: www.agpd.es.
            
            5.3. Tractament de les dades personals de contactes de correu electrònic
            
             5.3.1. Informació bàsica sobre Protecció de dades
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L
            
            Finalitat:
            
            Prestar els serveis sol·licitats, així com l’enviament de comunicacions comercials i newsletters informatives.
            
            Legitimació:
            
            Execució d’un contracte, Interès legítim del responsable o Consentiment de l’interessat.
            
            Destinataris:
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així como altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com
            
            
            5.3.2. Informació addicional sobre Protecció de dades
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 08650 - CABRIANES
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L tractem la informació que ens faciliten les persones interessades amb la finalitat de prestar els serveis que ens ha sol·licitat, atendre les seves sol·licituds d’informació i enviar-li comunicacions comercials.
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre l’interessat no sol·liciti la seva supressió.
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Interès legítim del responsable: Enviament de comunicacions comercials sobre els nostres serveis i/o productes.
            
            ·        Consentiment de l’interessat: Enviament de comunicacions comercials sobre els nostres serveis i/o productes en base a sol·licituds prèvies mantingudes amb el responsable del tractament.
            
            A quins destinataris es comunicaran les seves dades?
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa,10 08650 CABRIANES.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web: www.agpd.es.
            
             
            
            6. PROCEDIMENT EN CAS DE REALITZACIÓ D'ACTIVITATS DE CARÀCTER IL·LÍCIT
            
            En el cas que qualsevol usuari o un tercer consideri que existeixen fets o circumstàncies que revelin el caràcter il·lícit de la utilització de qualsevol contingut i/o de la realització de qualsevol activitat a les pàgines web incloses o accessibles a través del lloc web, haurà d'enviar una notificació a PROPIETARI DE LA WEB identificant-se degudament i especificant les suposades infraccions i declarant expressament i sota la seva responsabilitat que la informació proporcionada en la notificació és exacta.
            
             Per a tota qüestió litigiosa que incumbeixi al lloc web del PROPIETARI DE LA WEB, serà aplicable la legislació espanyola, sent competents els Jutjats i Tribunals de Barcelona (España).
            
             
            
            7. PUBLICACIONS
            
            La informació administrativa facilitada a través del lloc web no substitueix la publicitat legal de les lleis, normatives, plans, disposicions generals i actes que hagin de ser publicats formalment als diaris oficials de les administracions públiques, que constitueixen l'únic instrument que dona fe de la seva autenticitat i contingut. La informació disponible en aquest lloc web ha d'entendre's com una guia sense propòsit de validesa legal."
        ];

        $privacy_title = ['Política de privacitat', 'Política de privacidad', 'Privacy policy'];
        $privacy_text = [
            "POLÍTICA DE PRIVACITAT

            A continuació li indiquem els detalls referits als diferents tractaments de dades personals que realitzem a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.
            
            
            Tractament de les dades personals de potencials clients i contactes via web
            
            Informació bàsica sobre Protecció de dades
            
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.
            
            Finalitat:
            
            Prestar els serveis sol·licitats, així com l’enviament de comunicacions comercials i newsletters informatives.
            
            Legitimació:
            
            Execució d’un contracte.
            
            Consentiment de l’interessat.
            
            Destinataris:
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així com altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com.
            
            
            Informació addicional sobre Protecció de dades
            
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.– NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 – 08650 Cabrianes
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com.
            
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. tractem la informació que ens faciliten les persones interessades amb la finalitat d’atendre la seva sol·licitud i enviar-li comunicacions comercials, així com els nostres butlletins.
            
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre l’interessat no sol·liciti la seva supressió.
            
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Consentiment de l’interessat: Atendre la seva sol·licitud i enviar-li comunicacions comercials, així com els nostres butlletins.
            
            
            A quins destinataris es comunicaran les seves dades?
            
            No es cediran dades a tercers, llevat obligació legal.
            
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa10 - 08650 – Cabrianes.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web www.agpd.es.
            
            
             Tractament de les dades personals de clients
            
             Informació bàsica sobre Protecció de dades
            
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L
            
            Finalitat:
            
            Gestió de clients i enviament de comunicacions comercials sobre els nostres productes i/o serveis.
            
            Legitimació:
            
            Execució d’un contracte.
            
            Consentiment de l’interessat.
            
            Destinataris:
            
            Estan previstes cessions de dades a: Assessoria fiscal i comptable, registres públics, Bancs i Caixes d'Estalvi, Administració Tributària.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així com altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com.
            
            
            Informació addicional sobre Protecció de dades
            
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 – 08650 CABRIANES
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com
            
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L tractem la informació que ens faciliten les persones interessades amb la finalitat de gestionar la gestió administrativa, comptable i fiscal dels serveis sol·licitats, així com enviar comunicacions comercials sobre els nostres productes i serveis.
            
            No es van a prendre decisions automatitzades en base de dades proporcionats.
            
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre es mantingui la relació comercial, i si escau, durant els anys necessaris per complir amb les obligacions legals.
            
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Consentiment de l’interessat: Enviar-li comunicacions comercials.
            
            
            A quins destinataris es comunicaran les seves dades?
            
            Les dades es comunicaran als següents destinataris:
            
            ·Assessoria fiscal i comptable, registres públics, Bancs i Caixes d'Estalvi, Administració Tributària, amb la finalitat de Complir amb les obligacions legals.
            
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa,10 08650 CABRIANES.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web: www.agpd.es.
            
            
            Tractament de les dades personals de contactes de correu electrònic.
            
            Informació bàsica sobre Protecció de dades
            
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L
            
            Finalitat:
            
            Prestar els serveis sol·licitats, així com l’enviament de comunicacions comercials i newsletters informatives.
            
            Legitimació:
            
            Execució d’un contracte, Interès legítim del responsable o Consentiment de l’interessat.
            
            Destinataris:
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així como altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com
            
            
            Informació addicional sobre Protecció de dades
            
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 08650 - CABRIANES
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L tractem la informació que ens faciliten les persones interessades amb la finalitat de prestar els serveis que ens ha sol·licitat, atendre les seves sol·licituds d’informació i enviar-li comunicacions comercials.
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre l’interessat no sol·liciti la seva supressió.
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Interès legítim del responsable: Enviament de comunicacions comercials sobre els nostres serveis i/o productes.
            
            ·        Consentiment de l’interessat: Enviament de comunicacions comercials sobre els nostres serveis i/o productes en base a sol·licituds prèvies mantingudes amb el responsable del tractament.
            
            A quins destinataris es comunicaran les seves dades?
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa,10 08650 CABRIANES.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web: www.agpd.es.
            
            
            PROCEDIMENT EN CAS DE REALITZACIÓ D'ACTIVITATS DE CARÀCTER IL·LÍCIT
            
            En el cas que qualsevol usuari o un tercer consideri que existeixen fets o circumstàncies que revelin el caràcter il·lícit de la utilització de qualsevol contingut i/o de la realització de qualsevol activitat a les pàgines web incloses o accessibles a través del lloc web, haurà d'enviar una notificació a PROPIETARI DE LA WEB identificant-se degudament i especificant les suposades infraccions i declarant expressament i sota la seva responsabilitat que la informació proporcionada en la notificació és exacta.
            
             Per a tota qüestió litigiosa que incumbeixi al lloc web del PROPIETARI DE LA WEB, serà aplicable la legislació espanyola, sent competents els Jutjats i Tribunals de Barcelona (España).
            
             
            
            PUBLICACIONS
            
            La informació administrativa facilitada a través del lloc web no substitueix la publicitat legal de les lleis, normatives, plans, disposicions generals i actes que hagin de ser publicats formalment als diaris oficials de les administracions públiques, que constitueixen l'únic instrument que dona fe de la seva autenticitat i contingut. La informació disponible en aquest lloc web ha d'entendre's com una guia sense propòsit de validesa legal. 
            
            
            ",
            "POLÍTICA DE PRIVACIDAD


            A continuación le indicamos los detalles referidos a los distintos tratamientos de datos personales que realizamos en LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L. 
            
            5.1. Tratamiento de los datos personales de potenciales clientes y contactos vía web
            
            5.1.1. Información básica sobre Protección de datos
            
            Responsable:          
            
            LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L.
            
            Finalidad:     
            
            Prestar los servicios solicitados, así como el envío de comunicaciones comerciales y newsletter informativo.
            
            Legitimación:         
            
            Ejecución de un contrato.
            
            Consentimiento del interesado.
            
            Destinatarios:         
            
            No se cederán datos a terceros, salvo obligación legal.
            
            Derechos:    
            
            Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, indicados en la información adicional, que puede ejercer dirigiéndose a la dirección del responsable del tratamiento en info@lafactoriainteractiva.com.
            
            
             
            
            5.1.2. Información adicional sobre protección de datos
            
            ¿Quién es el responsable del tratamiento de sus datos?
            
            Entidad: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L.– NIF: B63379937
            
            Dirección postal: C/ Gavarresa,10 – 08650 Cabrianes
            
            Teléfono: 938206362
            
            Correo electrónico: info@lafactoriainteractiva.com.
            
            
            ¿Con qué finalidad tratamos sus datos personales?
            
            En LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. tratamos la información que nos facilitan las personas interesadas con el fin de atender su solicitud y enviarle comunicaciones comerciales, así como nuestros boletines.
            
            
            ¿Por cuánto tiempo conservaremos sus datos?
            
            Los datos se conservarán mientras el interesado no solicite su supresión.
            
            
            ¿Cuál es la legitimación para el tratamiento de sus datos?
            
            Le indicamos la base legal para el tratamiento de sus datos:
            
            Ejecución de un contrato: Prestación de los servicios solicitados
            
            Consentimiento del interesado: Atender su solicitud y enviarle comunicaciones comerciales, así como nuestros boletines.
            
            
            ¿A qué destinatarios se comunicarán sus datos?
            
            No se cederán datos a terceros, salvo obligación legal.
            
            Transferencias de datos a terceros países.
            
            No están previstas transferencias de datos a terceros países.
            
            
            ¿Cuáles son sus derechos cuando nos facilita sus datos?
            
            Cualquier persona tiene derecho a obtener confirmación sobre si en LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L estamos tratando datos personales que les conciernan, o no.
            
            Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos.
            
            En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones. Asimismo, en los supuestos legalmente establecidos, tendrá derecho a la portabilidad de sus datos personales.
            
            En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos. En este caso, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L dejará de tratar los datos, salvo por motivos legítimos imperiosos, o el ejercicio o la defensa de posibles reclamaciones.
            
            Podrá ejercitar materialmente sus derechos de la siguiente forma: enviando un correo electrónico a info@lafactoriainteractiva.com o una carta a C/ Gavarresa, 10 – 08650 - Cabrianes.
            
            Cuando se realice el envío de comunicaciones comerciales utilizando como base jurídica el interés legítimo del responsable, el interesado podrá oponerse al tratamiento de sus datos con ese fin.
            
            Si ha otorgado su consentimiento para alguna finalidad concreta, tiene derecho a retirar el consentimiento otorgado en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.
            
            En caso de que sienta vulnerados sus derechos en lo concerniente a la protección de sus datos personales, especialmente cuando no haya obtenido satisfacción en el ejercicio de sus derechos, puede presentar una reclamación ante la Autoridad de Control en materia de Protección de Datos competente a través de su sitio web: www.agpd.es.
            
             
            
            5.2. Tratamiento de los datos personales de clientes
            
            5.2.1. Información básica sobre Protección de datos
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L
            
            Finalidad:
            
            Gestión de clientes y envío de comunicaciones comerciales sobre nuestros productos y/o servicios.
            
            Legitimación:
            
            Ejecución de un contrato.
            
            Consentimiento del interesado.
            
            Destinatarios:
            
            Están previstas cesiones de datos a: Asesoría fiscal y contable, registros públicos, Bancos y Cajas de Ahorro, Administración Tributaria.
            
            Derechos:
            
            Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, indicados en la información adicional, que puede ejercer dirigiéndose a la dirección del responsable del tratamiento en info@lafactoriainteractiva.com
            
            
             5.2.2. Información adicional sobre protección de datos
            
            ¿Quién es el responsable del tratamiento de sus datos?
            
            Entidad: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Dirección postal: C/ Gavarresa,10 – 08650 CABRIANES
            
            Teléfono: 938206362
            
            Correo electrónico: info@lafactoriainteractiva.com
            
            
            ¿Con qué finalidad tratamos sus datos personales?
            
            En LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L tratamos la información que nos facilitan las personas interesadas con el fin de realizar la gestión administrativa, contable y fiscal de los servicios solicitados, así como enviar comunicaciones comerciales sobre nuestros productos y servicios.
            
            No se van a tomar decisiones automatizadas en base de datos proporcionados.
            
            
            ¿Por cuánto tiempo conservaremos sus datos?
            
            Los datos se conservarán mientras se mantenga la relación comercial, y en su caso, durante los años necesarios para cumplir con las obligaciones legales.
            
            
            ¿Cuál es la legitimación para el tratamiento de sus datos?
            
            Le indicamos la base legal para el tratamiento de sus datos:
            
            Ejecución de un contrato: Prestación de los servicios solicitados
            
            Consentimiento del interesado: Envío de comunicaciones comerciales
            
            
            ¿A qué destinatarios se comunicarán sus datos?
            
            Los datos se comunicarán a los siguientes destinatarios:
            
            Asesoría fiscal y contable, registros públicos, Bancos y Cajas de Ahorro, Administración Tributaria, con la finalidad de Cumplir con las obligaciones legales.
            
            
            Transferencias de datos a terceros países
            
            No están previstas transferencias de datos a terceros países.
            
            
            ¿Cuáles son sus derechos cuando nos facilita sus datos?
            
            Cualquier persona tiene derecho a obtener confirmación sobre si en LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L. estamos tratando, o no, datos personales que les conciernan.
            
            Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos. Igualmente tiene derecho a la portabilidad de sus datos.
            
            En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones.
            
            En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos. En este caso, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L. dejará de tratar los datos, salvo por motivos legítimos imperiosos, o el ejercicio o la defensa de posibles reclamaciones.
            
            Podrá ejercitar materialmente sus derechos de la siguiente forma: enviando un correo electrónico a info@lafactoriainteractiva.com o una carta a C/ Gavarresa, 10 - 08650 – Cabrianes.
            
            Cuando se realice el envío de comunicaciones comerciales utilizando como base jurídica el interés legítimo del responsable, el interesado podrá oponerse al tratamiento de sus datos con ese fin.
            
            Si ha otorgado su consentimiento para alguna finalidad concreta, tiene derecho a retirar el consentimiento otorgado en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.
            
            En caso de que sienta vulnerados sus derechos en lo concerniente a la protección de sus datos personales, especialmente cuando no haya obtenido satisfacción en el ejercicio de sus derechos, puede presentar una reclamación ante la Autoridad de Control en materia de Protección de Datos competente a través de su sitio web: www.agpd.es.
            
            
            5.3. Tratamiento de los datos personales de los contactos de correo electrónico
            
             5.3.1. Información básica sobre Protección de datos
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L
            
            Finalidad:
            
            Prestarle los servicios que nos ha solicitado, atender sus solicitudes de información y enviarle comunicaciones comerciales.
            
            Legitimación:
            
            Ejecución de contrato, Interés legítimo del responsable o Consentimiento del Interesado.
            
            Destinatarios:
            
            No se cederán datos a terceros, salvo obligación legal.
            
            Derechos:
            
            Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, indicados en la información adicional, que puede ejercer dirigiéndose a la dirección del responsable del tratamiento en info@lafactoriainteractiva.com
            
             
            
            5.3.2. Información adicional sobre protección de datos
            
            ¿Quién es el responsable del tratamiento de sus datos?
            
            Entidad: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Dirección postal: C/ Gavarresa,10 08650 - CABRIANES 
            
            Teléfono: 938206362
            
            Correo electrónico: info@lafactoriainteractiva.com
            
            
            ¿Con qué finalidad tratamos sus datos personales?
            
            En LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L tratamos la información que nos facilitan las personas interesadas con el fin de prestarle los servicios que nos ha solicitado, atender sus solicitudes de información y enviarle comunicaciones comerciales.
            
            
            ¿Por cuánto tiempo conservaremos sus datos?
            
            Los datos se conservarán mientras el interesado no solicite su supresión.
            
            
            ¿Cuál es la legitimación para el tratamiento de sus datos?
            
            Le indicamos la base legal para el tratamiento de sus datos:
            
            Ejecución de contrato: Prestación los servicios solicitados
            
            Interés legítimo del responsable: Envío de comunicaciones comerciales sobre nuestros servicios y/o productos.
            
            Consentimiento del Interesado: Envío de comunicaciones comerciales sobre nuestros servicios y/o productos en base a solicitudes previas mantenidas con el responsable del tratamiento.
            
            
            ¿A qué destinatarios se comunicarán sus datos?
            
            No se cederán datos a terceros, salvo obligación legal.
            
            
            Transferencias de datos a terceros países.
            
            No están previstas transferencias de datos a terceros países.
            
            
            ¿Cuáles son sus derechos cuando nos facilita sus datos?
            
            Cualquier persona tiene derecho a obtener confirmación sobre si en LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L estamos tratando datos personales que les conciernan, o no.
            
            Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos.
            
            En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones. Asimismo, en los supuestos legalmente establecidos, tendrá derecho a la portabilidad de sus datos personales.
            
            En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos. En este caso, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS, S.L dejará de tratar los datos, salvo por motivos legítimos imperiosos, o el ejercicio o la defensa de posibles reclamaciones.
            
            Podrá ejercitar materialmente sus derechos de la siguiente forma: enviando un correo electrónico a info@lafactoriainteractiva.com o una carta a C/ Gavarresa, 10 - 08650 – Cabrianes.
            
            Cuando se realice el envío de comunicaciones comerciales utilizando como base jurídica el interés legítimo del responsable, el interesado podrá oponerse al tratamiento de sus datos con ese fin.
            
            Si ha otorgado su consentimiento para alguna finalidad concreta, tiene derecho a retirar el consentimiento otorgado en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.
            
            En caso de que sienta vulnerados sus derechos en lo concerniente a la protección de sus datos personales, especialmente cuando no haya obtenido satisfacción en el ejercicio de sus derechos, puede presentar una reclamación ante la Autoridad de Control en materia de Protección de Datos competente a través de su sitio web: www.agpd.es.
            
            
            1. PROCEDIMIENTO EN CASO DE REALIZACIÓN DE ACTIVIDADES DE CARÁCTER ILÍCITO
            
            
            En el caso de que cualquier usuario o un tercero considere que existen hechos o circunstancias que revelen el carácter ilícito de la utilización de cualquier contenido y/o de la realización de cualquier actividad en las páginas web incluidas o accesibles a través del sitio web, deberá enviar una notificación a LA FACTORIA identificándose debidamente, especificando las supuestas infracciones y declarando expresamente y bajo su responsabilidad que la información proporcionada en la notificación es exacta.
            
            Para toda cuestión litigiosa que incumba al sitio web de LA FACTORIA, será de aplicación la legislación española, siendo competentes los Juzgados y Tribunales de Barcelona (España).
            
            
            2. PUBLICACIONES
            
            
            La información administrativa facilitada a través del sitio web no sustituye la publicidad legal de las leyes, normativas, planes, disposiciones generales y actos que tengan que ser publicados formalmente a los diarios oficiales de las administraciones públicas, que constituyen el único instrumento que da fe de su autenticidad y contenido. La información disponible en este sitio web debe entenderse como una guía sin propósito de validez legal. 
            
            
            ",
            "POLÍTICA DE PRIVACITAT

            A continuació li indiquem els detalls referits als diferents tractaments de dades personals que realitzem a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.
            
            
            Tractament de les dades personals de potencials clients i contactes via web
            
            Informació bàsica sobre Protecció de dades
            
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.
            
            Finalitat:
            
            Prestar els serveis sol·licitats, així com l’enviament de comunicacions comercials i newsletters informatives.
            
            Legitimació:
            
            Execució d’un contracte.
            
            Consentiment de l’interessat.
            
            Destinataris:
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així com altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com.
            
            
            Informació addicional sobre Protecció de dades
            
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L.– NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 – 08650 Cabrianes
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com.
            
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. tractem la informació que ens faciliten les persones interessades amb la finalitat d’atendre la seva sol·licitud i enviar-li comunicacions comercials, així com els nostres butlletins.
            
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre l’interessat no sol·liciti la seva supressió.
            
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Consentiment de l’interessat: Atendre la seva sol·licitud i enviar-li comunicacions comercials, així com els nostres butlletins.
            
            
            A quins destinataris es comunicaran les seves dades?
            
            No es cediran dades a tercers, llevat obligació legal.
            
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa10 - 08650 – Cabrianes.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web www.agpd.es.
            
            
             Tractament de les dades personals de clients
            
             Informació bàsica sobre Protecció de dades
            
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L
            
            Finalitat:
            
            Gestió de clients i enviament de comunicacions comercials sobre els nostres productes i/o serveis.
            
            Legitimació:
            
            Execució d’un contracte.
            
            Consentiment de l’interessat.
            
            Destinataris:
            
            Estan previstes cessions de dades a: Assessoria fiscal i comptable, registres públics, Bancs i Caixes d'Estalvi, Administració Tributària.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així com altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com.
            
            
            Informació addicional sobre Protecció de dades
            
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 – 08650 CABRIANES
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com
            
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L tractem la informació que ens faciliten les persones interessades amb la finalitat de gestionar la gestió administrativa, comptable i fiscal dels serveis sol·licitats, així com enviar comunicacions comercials sobre els nostres productes i serveis.
            
            No es van a prendre decisions automatitzades en base de dades proporcionats.
            
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre es mantingui la relació comercial, i si escau, durant els anys necessaris per complir amb les obligacions legals.
            
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Consentiment de l’interessat: Enviar-li comunicacions comercials.
            
            
            A quins destinataris es comunicaran les seves dades?
            
            Les dades es comunicaran als següents destinataris:
            
            ·Assessoria fiscal i comptable, registres públics, Bancs i Caixes d'Estalvi, Administració Tributària, amb la finalitat de Complir amb les obligacions legals.
            
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L. deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa,10 08650 CABRIANES.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web: www.agpd.es.
            
            
            Tractament de les dades personals de contactes de correu electrònic.
            
            Informació bàsica sobre Protecció de dades
            
            
            Responsable:
            
             LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L
            
            Finalitat:
            
            Prestar els serveis sol·licitats, així com l’enviament de comunicacions comercials i newsletters informatives.
            
            Legitimació:
            
            Execució d’un contracte, Interès legítim del responsable o Consentiment de l’interessat.
            
            Destinataris:
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Drets:
            
            Té dret a accedir, rectificar i suprimir les dades, així como altres drets, indicats a la informació addicional, que pot exercir dirigint-se a l’adreça del responsable del tractament a info@lafactoriainteractiva.com
            
            
            Informació addicional sobre Protecció de dades
            
            
            Qui és el responsable del tractament de les seves dades?
            
            Entitat: LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L – NIF: B63379937
            
            Direcció postal: C/ Gavarresa,10 08650 - CABRIANES
            
            Telèfon: 938206362
            
            Correu electrònic: info@lafactoriainteractiva.com
            
            Amb quina finalitat tractem les seves dades personals?
            
            A LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L tractem la informació que ens faciliten les persones interessades amb la finalitat de prestar els serveis que ens ha sol·licitat, atendre les seves sol·licituds d’informació i enviar-li comunicacions comercials.
            
            Durant quant de temps conservarem les seves dades?
            
            Les dades es conservaran mentre l’interessat no sol·liciti la seva supressió.
            
            Quina és la legitimació per al tractament de les seves dades?
            
            Li indiquem la base legal per al tractament de les seves dades:
            
            ·        Execució d’un contracte: Prestació dels serveis sol·licitats.
            
            ·        Interès legítim del responsable: Enviament de comunicacions comercials sobre els nostres serveis i/o productes.
            
            ·        Consentiment de l’interessat: Enviament de comunicacions comercials sobre els nostres serveis i/o productes en base a sol·licituds prèvies mantingudes amb el responsable del tractament.
            
            A quins destinataris es comunicaran les seves dades?
            
            No es cediran dades a tercers, llevat obligació legal.
            
            Transferències de dades a tercers països.
            
            No estan previstes transferències de dades a tercers països.
            
            Quins són els seus drets quan ens facilita les seves dades?
            
            Qualsevol persona té dret a obtenir confirmació sobre si a LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L estem tractant dades personals que els concerneixin, o no.
            
            Les persones interessades tenen dret a accedir a les seves dades personals, així com a sol·licitar la rectificació de les dades inexactes o, en el seu cas, sol·licitar la seva supressió quan, entre altres motius, les dades ja no siguin necessàries per a les finalitats per a què van ser recollides.
            
            En determinades circumstàncies, els interessats podran sol·licitar la limitació del tractament de les seves dades, en aquest cas únicament les conservarem per a l’exercici o la defensa de reclamacions. Així mateix, en els supòsits legalment establerts, tindrà dret a la portabilitat de les seves dades personals.
            
            En determinades circumstàncies i per motius relacionats amb la seva situació particular, els interessats podran oposar-se al tractament de les seves dades. En aquest cas, LA FACTORIA D’IMATGES, SERVEIS GRÀFICS,S.L deixarà de tractar les dades, llevat per motius legítims imperiosos, o l’exercici o la defensa de possibles reclamacions.
            
            Podrà exercitar materialment els seus drets de la següent forma: enviant un correu electrònic a info@lafactoriainteractiva.com o una carta a C/ Gavarresa,10 08650 CABRIANES.
            
            Quan es realitzi l'enviament de comunicacions comercials utilitzant com a base jurídica l'interès legítim del responsable, l'interessat podrà oposar-se al tractament de les seves dades amb aquesta fi.
            
            Si ha donat el seu consentiment per a alguna finalitat concreta, té dret a retirar el consentiment donat en qualsevol moment, sense que això afecti a la licitud del tractament basat en el consentiment previ a la seva retirada.
            
            En cas de que senti vulnerats els seus drets pel que fa a la protecció de les seves dades personals, especialment quan no hagi obtingut satisfacció en l’exercici dels seus drets, pot presentar una reclamació davant l’Autoritat de Control en matèria de Protecció de Dades competent a través del seu lloc web: www.agpd.es.
            
            
            PROCEDIMENT EN CAS DE REALITZACIÓ D'ACTIVITATS DE CARÀCTER IL·LÍCIT
            
            En el cas que qualsevol usuari o un tercer consideri que existeixen fets o circumstàncies que revelin el caràcter il·lícit de la utilització de qualsevol contingut i/o de la realització de qualsevol activitat a les pàgines web incloses o accessibles a través del lloc web, haurà d'enviar una notificació a PROPIETARI DE LA WEB identificant-se degudament i especificant les suposades infraccions i declarant expressament i sota la seva responsabilitat que la informació proporcionada en la notificació és exacta.
            
             Per a tota qüestió litigiosa que incumbeixi al lloc web del PROPIETARI DE LA WEB, serà aplicable la legislació espanyola, sent competents els Jutjats i Tribunals de Barcelona (España).
            
             
            
            PUBLICACIONS
            
            La informació administrativa facilitada a través del lloc web no substitueix la publicitat legal de les lleis, normatives, plans, disposicions generals i actes que hagin de ser publicats formalment als diaris oficials de les administracions públiques, que constitueixen l'únic instrument que dona fe de la seva autenticitat i contingut. La informació disponible en aquest lloc web ha d'entendre's com una guia sense propòsit de validesa legal. 
            
            
            "
        ];

        $g_cookies = General::create();
        $g_cookies_desc = General::create();
        $g_legal = General::create();
        $g_privacy = General::create();

        for($i = 0; $i < count($locales); $i++){
			// Select the english translation if is locale is not catalan or spanish
			$translationIndex = ($locales[$i] == 'ca' ? 0 : ($locales[$i] == 'es' ? 1 : 2));

            $trans = $g_cookies->translateOrNew($locales[$i]);
            $trans->title= $cookies_title[$translationIndex];
            $trans->text = $cookies_text[$translationIndex];
            $trans->general_id = $g_cookies->id;
            $trans->save();

            $trans = $g_cookies_desc->translateOrNew($locales[$i]);
            $trans->title= $cookies_desc_title[$translationIndex];
            $trans->text = $cookies_desc_text[$translationIndex];
            $trans->general_id = $g_cookies_desc->id;
            $trans->save();

            $trans = $g_legal->translateOrNew($locales[$i]);
            $trans->title= $legal_title[$translationIndex];
            $trans->text = $legal_text[$translationIndex];
            $trans->general_id = $g_legal->id;
            $trans->save();

            $trans = $g_privacy->translateOrNew($locales[$i]);
            $trans->title= $privacy_title[$translationIndex];
            $trans->text = $privacy_text[$translationIndex];
            $trans->general_id = $g_privacy->id;
            $trans->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_translations');
        Schema::dropIfExists('generals');
    }
}

