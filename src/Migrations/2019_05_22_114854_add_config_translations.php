<?php

use Illuminate\Support\Facades\DB;
use Lafactoria\Backend\Models\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfigTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $locales = config('translatable.locales');

        Schema::create('config_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale')->index();
            $table->integer('config_id');
            $table->text('tags')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });


        // check if table content has data
        $count = Config::count();
        if($count == 0){
             Config::create(config('backend.config')); // Creates a default configuration set.
        }
        // get config content
        $configs = DB::table('config')->get();
        // apply translations to config
        $configs->each(function ($config) use ($locales) {
            $new_config = Config::findOrFail($config->id);

            foreach($locales as $locale) {
                $trans = $new_config->translateOrNew($locale);
                $trans->locale = $locale;
                $trans->tags = $config->tags;
                $trans->description = $config->description;
                $trans->config_id = $config->id;
                $trans->save();
            }
        });
        Schema::table('config', function (Blueprint $table) {
            $table->dropColumn('tags');
            $table->dropColumn('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_translations');

        Schema::table('config', function (Blueprint $table) {
            $table->text('tags')->nullable();
            $table->text('description')->nullable();
        });
    }
}
