<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminPasswordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_passwords', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('password');
            $table->integer('admin_id');
            $table->timestamps();
        });

        Schema::table('admins', function (Blueprint $table) {
            $table->timestamp('password_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_passwords');
        Schema::table('admins', function (Blueprint $table) {
            $table->dropColumn('password_updated_at');
        });
    }
}
