<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImagesAlt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_image_model_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('header_image_model_id');
            $table->string('seo_alt')->default('');
            $table->string('locale');
            $table->timestamps();
        });

        Schema::create('image_model_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('image_model_id');
            $table->string('seo_alt')->default('');
            $table->string('locale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_image_model_translations');
        Schema::dropIfExists('image_model_translations');
    }
}
