<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlidesUrlTranslatable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $slides = \Illuminate\Support\Facades\DB::table('slides')->get();

        Schema::table('slide_translations', function (Blueprint $table) {
            $table->text('url')->nullable();
        });

        Schema::table('slides', function (Blueprint $table) {
            $table->dropColumn('url');
        });

        $slides->each(function ($slide) {
            \Lafactoria\Backend\Models\SlideTranslation::where('slide_id', $slide->id)->update(['url' => $slide->url]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slide_translations', function (Blueprint $table) {
            $table->dropColumn('url');
        });

        Schema::table('slides', function (Blueprint $table) {
            $table->text('url')->nullable();
        });
    }
}
