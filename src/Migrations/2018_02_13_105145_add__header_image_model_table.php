<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHeaderImageModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menus', function (Blueprint $table) {
            $table->dropColumn('image_id');
        });
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('image_id');
        });
        Schema::table('banners', function (Blueprint $table) {
            $table->dropColumn('image_id');
        });
        Schema::table('slides', function (Blueprint $table) {
            $table->dropColumn('image_id');
        });

        Schema::create('header_images_model', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('image_id');
            $table->integer('model_id');
            $table->string('model');
            $table->unique(['image_id', 'model_id', 'model']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menus', function (Blueprint $table) {
            $table->integer('image_id');
        });
        Schema::table('news', function (Blueprint $table) {
            $table->integer('image_id');
        });
        Schema::table('banners', function (Blueprint $table) {
            $table->integer('image_id');
        });
        Schema::table('slides', function (Blueprint $table) {
            $table->integer('image_id');
        });

        Schema::dropIfExists('header_images_model');
    }
}
