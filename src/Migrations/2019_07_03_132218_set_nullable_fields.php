<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news_translations', function (Blueprint $table) {
            $table->string('description')->nullable()->change();
            $table->longText('content')->nullable()->change();
        });
        Schema::table('banners', function (Blueprint $table) {
            $table->string('description')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_translations', function (Blueprint $table) {
            $table->string('description')->nullable(false)->change();
            $table->longText('content')->nullable(false)->change();
        });
        Schema::table('banners', function (Blueprint $table) {
            $table->string('description')->nullable(false)->change();
        });
    }
}
