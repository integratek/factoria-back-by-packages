<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Lafactoria\Backend\Models\Slide;

class AddTranslationsSlidesModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('slide_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale')->index();
            $table->integer('slide_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });

         $this -> copyContentFromActualStructureToTranslations();

         Schema::table('slides', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	 Schema::table('slides', function (Blueprint $table) {
            $table->string('name');
            $table->text('description')->nullable();
        });

        Schema::dropIfExists('slide_translations');
    }

    function copyContentFromActualStructureToTranslations(){
    	$slides = DB::table('slides')->get();
    	$locales = config('translatable.locales');
    	$slides->map(function($slide) use ($locales) {
    		foreach($locales as $locale) {
    			$newSlide = Slide::find($slide->id);
    			$trans = $newSlide->translateOrNew($locale);
    			$data = [];
    			$trans->locale = $locale;
    			$trans->name = $slide -> name;
    			$trans->description = $slide -> description;
    			$trans->slide_id = $slide -> id;

    			$trans->save();
		    }
	    });
    }
}
