<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class');
            $table->integer('reference');
            $table->timestamps();
        });

        Schema::create('seo_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seo_id');
            $table->string('title');
            $table->text('description');
            $table->text('tags');
            $table->string('locale')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seos');
        Schema::dropIfExists('seo_translations');
    }
}
