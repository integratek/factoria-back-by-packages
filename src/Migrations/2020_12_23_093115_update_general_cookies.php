<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\App;
use Lafactoria\Backend\Models\General;

class UpdateGeneralCookies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        for ($i=1; $i <= 2; $i++) { 
            $general = General::find($i);
            if ($general) {
                $translations = $general->translations;
                foreach ($translations as $translation) {
                    App::setLocale($translation->locale);
                    $title = __('backend::admin-general.cookies_declaration_script');
                    if ($i == 2) {
                        $title = __('backend::admin-general.cookies_head_script');
                    }
                    $translation->update([
                        'title' => $title
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
