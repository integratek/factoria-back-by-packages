<?php

namespace Lafactoria\Backend\Requests\Admin\Config;

use Illuminate\Foundation\Http\FormRequest;

class SaveConfig extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'email' => 'nullable|string|email|max:255',
            'city' => 'nullable|string|max:255',
            'adress' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:255',
            'country' => 'nullable|string|max:255',
            'scripts' => 'nullable|string',
            'styles' => 'nullable|string',
        ];
    }
}
