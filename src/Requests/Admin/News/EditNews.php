<?php

namespace Lafactoria\Backend\Requests\Admin\News;

use Illuminate\Foundation\Http\FormRequest;

class EditNews extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'news' => 'required',
            'news.slug' => 'required|string',
            'news.display_after' => 'required',
            'translations.*.title' => 'required|string',
        ];
    }
}
