<?php

namespace Lafactoria\Backend\Requests\Admin\Menus;

use Lafactoria\Backend\Models\Menu;
use Illuminate\Foundation\Http\FormRequest;

class SaveMenu extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'menu' => 'required',
            'menu.active' => 'required|boolean',
            'translations.*.title' => 'required|string|max:255',
            'translations.*.content' => 'nullable|string',
            'translations.*.highlighted' => 'nullable|string',
            'translations.*.url' => 'required_unless:menu.type,' . Menu::SUBMENU_TYPE,
            'translations.*.copy_from_english' => 'required|boolean',
            'translations.*.files' => 'sometimes|array',
        ];
    }
}
