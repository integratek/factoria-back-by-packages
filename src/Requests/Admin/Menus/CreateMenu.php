<?php

namespace Lafactoria\Backend\Requests\Admin\Menus;

use Illuminate\Validation\Rule;
use Lafactoria\Backend\Models\Menu;
use Illuminate\Foundation\Http\FormRequest;

class CreateMenu extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'menu' => 'required',
            'menu.active' => 'required|boolean',
            'menu.type' => 'required|in:' . implode(',', Menu::types()->values()->toArray()),
            'menu.parent_id' => 'nullable',
            'menu.on_nav' => 'required|boolean',
            'menu.on_footer' => 'required|boolean',
            'translations.*.title' => 'required|string|max:255',
            'translations.*.content' => 'nullable|string',
            'translations.*.highlighted' => 'nullable|string',
            'translations.*.url' => 'required_unless:menu.type,' . Menu::SUBMENU_TYPE,
            'translations.*.copy_from_english' => 'sometimes|boolean',
            'translations.*.files' => 'sometimes|array',
        ];
    }
}
