<?php

namespace Lafactoria\Backend\Requests\Admin\General;

use Illuminate\Foundation\Http\FormRequest;

class SaveGeneral extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cookies_description' => 'nullable',
            'cookies' => 'nullable',
            'legal' => 'nullable',
        ];
    }
}
