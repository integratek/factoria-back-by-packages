<?php

namespace Lafactoria\Backend\Requests\Admin\Slides;

use Illuminate\Foundation\Http\FormRequest;

class CreateSlide extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'active' => 'required',
            'url' => 'nullable',
            'new_tab' => 'nullable',
            'headerImageModel' => 'required|numeric|exists:images,id',
            'translations.*.name' => 'required|string|max:255',
            'translations.*.video_webm' => '',
            'translations.*.video_mp4' => '',
            'translations.*.description' => 'required|string|min:6',
            'translations.*.url' => 'nullable|string',
        ];
    }
}
