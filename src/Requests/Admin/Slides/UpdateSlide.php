<?php

namespace Lafactoria\Backend\Requests\Admin\Slides;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSlide extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slide' => 'required|exists:slides,id',
            'headerImageModel' => 'required|numeric|exists:images,id',
            'translations.*.name' => 'required|string|max:255',
            'translations.*.description' => 'required|string|min:6',
            'translations.*.url' => 'nullable|string',
            'translations.*.video_name' => '',
            'translations.*.video_webm' => '',
            'translations.*.video_mp4' => ''
        ];
    }
}
