<?php

namespace Lafactoria\Backend\Requests\Admin\Slides;

use Illuminate\Foundation\Http\FormRequest;

class VideosSlide extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [];
        foreach (config('translatable.locales')  as $locale ){
             $validations ['video_mp4_'.$locale ] = 'nullable|file|max:5000|mimes:mp4';
             $validations ['video_webm_'.$locale] = 'nullable|file|max:5000|mimes:webm';
             $validations ['video_name'] = '';
        }
        $validations ["to_delete" ] = '';
        $validations ["uploading_files"] = '';

        return $validations;
    }
}
