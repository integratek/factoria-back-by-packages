<?php

namespace Lafactoria\Backend\Requests\LFPublic;

use Illuminate\Foundation\Http\FormRequest;

class SendContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|max:255',
            'message' => 'required|min:1|max:500',
            'g-recaptcha-response' => 'required|captcha',
			'accept_legal' => 'required|accepted',
			'accept_privacy' => 'required|accepted'
        ];
    }
}
