@extends('backend::layouts.admin')
@section('title', __('backend::admin-news.news'))
@section('subtitle', __('backend::admin-news.news_sub'))
@section('icon', 'mdi-newspaper')
@section('content')
	<div class="uk-width-1-1">
		<div uk-grid>
			<div class="uk-width-1-1">
				<div class="uk-card uk-card-default">
					<div class="uk-card-header">@lang('backend::admin-news.news')</div>
					<div class="uk-card-body">
					<a href="{{ route('admin.news.create') }}" class="uk-button uk-button-default">@lang('backend::admin-news.create_news')</a>
					@include("backend::layouts.filter")
					<br><br>
					<div class="uk-overflow-auto">
						<table class="uk-table uk-table-striped">
						    <thead>
						        <tr>
						            <th class="uk-visible@m">#</th>
						            <th>@lang('backend::admin-news.title')</th>
						            <th class="uk-visible@m">@lang('backend::admin-news.author')</th>
						            <th class="uk-visible@m">@lang('backend::admin-news.description')</th>
						            <th>@lang('backend::admin-news.active')</th>
						            <th class="uk-visible@l">@lang('backend::admin-news.created_at')</th>
						            <th>@lang('backend::admin-news.actions')</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr v-for="(entry, k) in news">
						            <td class="uk-visible@m">@{{ entry.id }}</td>
						            <td>@{{ entry.title }}</td>
						            <td class="uk-visible@m">@{{ entry.admin.name }}</td>
						            <td class="uk-visible@m">@{{ entry.description | striphtml }}</td>
						            <td>
						            	<span v-if="entry.active" class="uk-text-success mdi mdi-circle"> @lang('backend::admin-news.yes')</span>
										<span v-if="!entry.active" class="uk-text-danger mdi mdi-circle"> @lang('backend::admin-news.no')</span>
						            </td>
						            <td class="uk-visible@l">@{{ entry.created_at }}</td>
						            <td>
						            	<!-- The SEO modal linked to the seo button -->
						            	<seo
						            		save-url="{{ route('admin.api.seo.save') }}"
						            		get-url="{{ route('admin.api.seo') }}"
						            		seo-trans-url="{{ route('admin.api.seo.translations') }}"
						            		languages-url="{{ route('admin.api.seo.languages') }}"
						            		:locales="{{ collect(config('translatable.locales'))->toJson() }}"
						            		class-r="Lafactoria\Backend\Models\News"
						            		:lang-locale="null"
						            		:reference="entry.id"
											:id-r="'seo-' + entry.id"
											:bus="bus">
						            	</seo>
						            	<div class="uk-button-group">
										    <a :href="'{{ route('admin.news.edit', ['news' => null]) }}/' + entry.id" class="uk-button uk-button-default uk-button-small">
												<i class="mdi mdi-pencil"></i>
										    </a>
										    <a class="uk-button uk-button-default uk-button-small" @click="onClickOpenSeo(entry.id)">
												<i class="mdi mdi-tag-multiple"></i>
										    </a>
										    <a href="#" @click="deleteNews(entry.id)" class="uk-button uk-button-danger uk-button-small">
												<i class="mdi mdi-delete"></i>
										    </a>
										</div>
						            </td>
						        </tr>
						    </tbody>
						</table>
						<br>
						<div class="row">
							{{ $news->links('backend::public.pagination-uikit') }}
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				news: [],
                bus: new Vue(),
			},
			filters: {
				striphtml: function(value){
					var div = document.createElement("div");
					div.innerHTML = value;
					var text = div.textContent || div.innerText || "";
					return text;
				}
			},
			methods: {
				updateNews: function () {
					axios.get("{{ route('admin.api.news') }}")
						.then((response) => {
							this.news = response.data;
							this.news.map(function (news) {
								news.created_at = moment(news.created_at).fromNow();
								return news;
							});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
				},
				deleteNews: function (id) {
                    UIkit.modal.confirm("@lang('backend::admin-news.confirm_delete')", {labels: {ok: "@lang('backend::admin-news.delete_yes')", cancel: "@lang('backend::admin-news.delete_no')"}}).then(function() {
						axios.post("{{ route('admin.api.news.delete') }}", { news: id })
							.then ((response) => {
								this.updateNews();
								$.mdtoast("@lang('backend::admin-news.news_deleted')", { duration: 5000 });
							})
							.catch((error) => {
								$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
							});
					}.bind(this));
				},
				onClickOpenSeo(id){
					this.bus.$emit('id-selected', 'seo-' + id);
				}
			},
			created: function () {
				//this.updateNews();
				this.news = <?php echo json_encode($news) ;?>;
				this.news = this.news.data;
			}
		});
	</script>
@endsection
