@extends('backend::layouts.admin')
@section('title', __('backend::admin-news.edit_news'))
@section('subtitle', __('backend::admin-news.edit_news_sub'))
@section('icon', 'mdi-pencil')
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-news.edit_news')
			</div>
			<div class="uk-card-body">
				<div uk-grid>
					<div class="uk-width-1-1">
						<ul class="uk-flex-center" :uk-tab="'connect: #seo-translation; media: 960'">
			                <li v-for="(translation, key) in seotranslations">
			                	<a href="#">@{{ languages[translation.locale] }}</a>
			                </li>
			            </ul>
						<ul id="seo-translation" class="uk-switcher">
						    <li v-for="(translation, k) in seotranslations">
						    	<div uk-grid>
						    		<div class="uk-width-1-1@m uk-width-1-2@l">
								        <label class="uk-form-label">@lang('backend::admin-news.title')</label>
								        <div class="uk-form-controls">
								        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.title') }" required autofocus v-model.trim="translation.title" placeholder="@lang('backend::admin-news.title')" type="text" class="uk-input">
								        	<span v-if="errors.hasOwnProperty('translations.' + k + '.title')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.title'] }}</span>
								        </div>
								    </div>
								    <div class="uk-width-1-1@m uk-width-1-2@l">
								        <label class="uk-form-label">@lang('backend::admin-news.description')</label>
								        <div class="uk-form-controls">
								        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.description') }" required v-model.trim="translation.description" placeholder="@lang('backend::admin-news.description')" type="text" class="uk-input">
								        	<span v-if="errors.hasOwnProperty('translations.' + k + '.description')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.description'] }}</span>
								        </div>
								    </div>
								    <div  v-if="!showHtmlTextArea" class="uk-width-1-1">
								    	<label class="uk-form-label">@lang('backend::admin-news.content')</label>
								    	<div class="uk-form-controls">
								    		<quill-editor v-model="translation.content" ref="quill_content"></quill-editor>
								    		<span v-if="errors.hasOwnProperty('translations.' + k + '.content')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.content'] }}</span>
								    	</div>
								    </div>
									<div v-if="showHtmlTextArea" id="textAreaHTML" class="uk-width-1-1">
										<div class="uk-form-label">@lang('backend::admin-menus.content')</div>
										<div class="uk-form-controls">
											<textarea v-model="translation.content" class="uk-textarea" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.content') }" rows="5"></textarea>
											<span v-if="errors.hasOwnProperty('translations.' + k + '.content')" class="uk-text-small uk-text-danger">
                                                @{{ errors['translations.' + k + '.content'] }}
                                            </span>
										</div>
									</div>
									<div>
										<button @click="showHtmlTextArea = !showHtmlTextArea" class="uk-button uk-button-default" :class="{ 'uk-button-danger': showHtmlTextArea }">@lang('backend::admin-news.show-html')</button>
									</div>
								</div>
								<div class="uk-width-1-1 uk-margin-large-top">
									<label class="uk-form-label">@lang('backend::admin-documents.documents')</label>
									<a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #docs-' + translation.locale">
										<i class="mdi mdi-file"></i>
									</a>

									<div class="uk-margin">
										<docs-repo
												:values.sync="translation.files"
												:id-r="translation.locale"
												:show-selected="true"
												:unique-doc="false"
												:allow-repository="true"
												csrf="{{ csrf_token() }}">
										</docs-repo>
										<span v-if="errors && errors.hasOwnProperty('file')" class="uk-text-small uk-text-danger">@{{ errors.file[0] }}</span>
									</div>
								</div>
						    </li>
						</ul>
					</div>

					<div class="uk-width-1-1@m uk-width-1-2@l">
				        <label class="uk-form-label">@lang('backend::admin-news.slug')</label>
				        <div class="uk-form-controls">
				        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('news.slug') }" :disabled="autoSlug" required v-model.trim="entry.slug" placeholder="@lang('backend::admin-news.slug')" type="text" class="uk-input">
				        	<span v-if="errors.hasOwnProperty('news.slug')" class="uk-text-small uk-text-danger">@{{ errors['news.slug'] }}</span>
				        </div>
				    </div>
				    <div class="uk-width-1-1@m uk-width-1-2@l">
				    	<div class="uk-margin-top uk-visible@l"></div>
						<div style="padding-top: 10px;"></div>
						<label><input @click="autoSlugToggle" v-model="autoSlug" class="uk-checkbox" type="checkbox"> @lang('backend::admin-news.autoslug')</label>
				    </div>
				    <div class="uk-width-1-1@m uk-width-1-2@l">
				        <label class="uk-form-label">@lang('backend::admin-news.display_after')</label>
				        <div class="uk-form-controls">
                            <input id="display_after" :class="{ 'uk-form-danger': errors.hasOwnProperty('news.display_after') }" required type="text" class="uk-input">
                            <span v-if="errors.hasOwnProperty('news.display_after')" class="uk-text-small uk-text-danger">@{{ errors['news.display_after'] }}</span>
                        </div>
				    </div>
				    <div class="uk-width-1-2">
						<div class="uk-margin-top uk-visible@l"></div>
						<div style="padding-top: 10px;"></div>
				    	<label><input v-model="entry.active" class="uk-checkbox" type="checkbox"> @lang('backend::admin-news.active')</label>
				    </div>

					<div class="uk-width-1-1">
						<label class="uk-form-label">@lang('backend::admin-news.image')</label>
						<a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #images-seo-' + 1">
							<i class="mdi mdi-image"></i>
						</a>
						<image-repo-seo
								v-if="entry.headerImageModel !== false"
								:values.sync="entry.headerImageModel"
								id-r="1"
								:show-selected="true"
								:unique-image="true"
								:allow-repository="true"
								csrf="{{ csrf_token() }}"
								model-class="Lafactoria\Backend\Models\HeaderImageModel"
						></image-repo-seo>
					</div>

					<div class="uk-width-1-1">
						<label class="uk-form-label">@lang('backend::admin-news.more_images')</label>
						<a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #images-seo-' + 2">
							<i class="mdi mdi-image"></i>
						</a>
						<image-repo-seo
								v-if="entry.imageModel !== false"
								:values.sync="entry.imageModel"
								id-r="2"
								:show-selected="true"
								:unique-image="false"
                                :allow-repository="true"
								csrf="{{ csrf_token() }}"
								model-class="Lafactoria\Backend\Models\ImageModel"
						></image-repo-seo>
					</div>

					<div class="uk-width-1-1 uk-text-center">
						<button @click="editNews" class="uk-button uk-button-primary">@lang('backend::admin-news.save')</button>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				display_after_picker: {},
				errors: {},
				autoSlug: false,
				images: [],
				seotranslations: [],
				languages: [],
                showHtmlTextArea: false,
				entry: {
					id: {{ request()->news }},
					active: true,
                    headerImageModel: false,
					slug: '',
					display_after: null,
                    imageModel: false
				},
			},
			methods: {
				editNews: function () {
					axios.post("{{ route('admin.api.news.edit') }}", { news: this.entry, translations: this.seotranslations })
						.then((response) => {
							this.entry = response.data;
							this.errors = {};
							$.mdtoast("@lang('backend::admin-news.new_edited')", { duration: 5000 });
                            window.location.replace("{{ route('admin.news') }}");
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });

                            // remove object and number translation information in strings and set in vue errors var
                            this.errors = cleanErrors(error).errors;
						});
				},
				autoSlugToggle: function () {
					if (!this.autoSlug) {
						this.makeSlug();
					}
				},
				makeSlug: function () {
					this.entry.slug = slugify(this.entry.title, {
						replacement: '-',
						lower: true,
					});
				},
				generateSlug: function () {
					if (this.autoSlug) {
						this.makeSlug();
					}
				},
                getImagesRepo () {
                    axios.get("{{ route('admin.api.images') }}")
                        .then((response) => {
                            this.images = response.data;
                        })
                        .catch((error) => {
                            $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                        });
                }
			},
			created: function () {
				axios.get("{{ route('admin.api.images') }}")
					.then((response) => {
						this.images = response.data;
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
				axios.post("{{ route('admin.api.news.seotranslations') }}", { news: this.entry.id })
					.then((response) => {
						this.seotranslations = response.data;
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
				axios.post("{{ route('admin.api.seo.languages') }}")
	                .then((response) => {
	                    this.languages = response.data;
	                })
	                .catch((error) => {
	                    $.mdtoast('Error while getting the languages...', { duration: 5000, type: $.mdtoast.type.ERROR });
	                });
			},
			mounted: function () {
				var picker = new Pikaday({
					field: $('#display_after')[0],
					format: 'YYYY-MM-DD',
					onSelect: function (date) {
				        this.entry.display_after = moment(picker.toString()).format('YYYY-MM-DD');
				    }.bind(this)
				});

				axios.get("{{ route('admin.api.news.info', ['news' => null]) }}/" + this.entry.id)
					.then((response) => {
						this.entry = response.data;
						picker.setMoment(moment(this.entry.display_after, 'YYYY-MM-DD'));
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
			}
		});
	</script>
@endsection
