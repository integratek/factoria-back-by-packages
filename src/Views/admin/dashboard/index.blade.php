@extends('backend::layouts.admin')
@section('title', __('backend::admin-dashboard.dashboard'))
@section('subtitle', __('backend::admin-dashboard.dashboard_sub', ['user' => auth()->guard('admin')->user()->name]))
@section('icon', 'mdi-view-dashboard')
@section('content')
	@foreach (config('backend.dashboard_counters') as $counter)
		<div class="uk-width-1-1@m uk-width-1-3@l">
			<div class="uk-card uk-card-default uk-card-body uk-text-center">
				@if(isset($counter['custom_count']))
				<p class="feature-text">{{ $counter['class']::{$counter['custom_count']}() }}</p>
				@else
				<p class="feature-text">{{ $counter['class']::all()->count() }}</p>
				@endif
				<p class="mdi {{ $counter['icon'] }} feature-desc light-text"> @lang($counter['text'])</p>
			</div>
		</div>
	@endforeach
@endsection
