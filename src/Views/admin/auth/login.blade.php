@extends('backend::layouts.simple')
@section('title', __('backend::admin-auth.login'))
@section('subtitle', __('backend::admin-auth.login_sub'))
@section('content')
<div class="uk-container">
	<div uk-grid>
		<div class="uk-width-1-1@s uk-width-1-6@m uk-width-1-4@l uk-width-1-4@xl">&nbsp;</div>
		<div class="uk-width-1-1@s uk-width-2-3@m uk-width-1-2@l uk-width-1-2@xl">
			<div class="uk-section uk-section-large">
				<div class="uk-card uk-card-default">
					<div class="uk-card-header uk-text-center">
						{{ config('app.name') }} - @lang('backend::admin-auth.login')
					</div>
					<div class="uk-card-body">
						<form method="POST" class="uk-form-stacked">
							{{ csrf_field() }}
							<div class="uk-margin">
								<label class="uk-form-label">@lang('backend::admin-auth.email')</label>
								<div class="uk-form-controls">
									<input name="email" required value="{{ old('email') }}"
										{{ old('email') ? '' : 'autofocus' }}
										placeholder="@lang('backend::admin-auth.email')" type="email" class="uk-input">
								</div>
							</div>
							<div class="uk-margin">
								<label class="uk-form-label">@lang('backend::admin-auth.password')</label>
								<div class="uk-form-controls">
									<input name="password" required {{ old('email') ? 'autofocus' : '' }}
										placeholder="@lang('backend::admin-auth.password')" type="password"
										class="uk-input">
								</div>
							</div>
							<div class="uk-margin">
								<label><input name="remember" class="uk-checkbox" type="checkbox">
									@lang('backend::admin-auth.remember_me')</label>
							</div>
							<div class="uk-margin">
								<label style="cursor:pointer"
									@click="openSendResetPassword">@lang('backend::admin-auth.reset_password')</label>
							</div>
							<div class="uk-margin uk-text-center">
								<button type="Submit"
									class="uk-button uk-button-primary">@lang('backend::admin-auth.login')</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1@s uk-width-1-6@m uk-width-1-4@l uk-width-1-4@xl">&nbsp;</div>
	</div>
	<div id="password_update_modal" uk-modal>
		<div class="uk-modal-dialog uk-modal-body">
			<h2 class="uk-modal-title">@lang('backend::admin-auth.update_password')</h2>
			<div class="uk-margin">
				<label class="uk-form-label">@lang('backend::admin-auth.new_password')</label>
				<div class="uk-form-controls">
					<input name="password" v-model="password" placeholder="@lang('backend::admin-auth.new_password')"
						type="password" class="uk-input" readonly onfocus="this.removeAttribute('readonly')">
					<span v-if="errors.hasOwnProperty('password')"
						class="uk-text-small uk-text-danger">@{{ errors['password'] }}</span>
				</div>
			</div>
			<div class="uk-margin">
				<label class="uk-form-label">@lang('backend::admin-auth.repeat_password')</label>
				<div class="uk-form-controls">
					<input name="password" v-model="password_confirmation"
						placeholder="@lang('backend::admin-auth.repeat_password')" type="password" class="uk-input">
					<span v-if="errors.hasOwnProperty('password_confirmation')"
						class="uk-text-small uk-text-danger">@{{ errors['password_confirmation'] }}</span>
				</div>
			</div>
			<button @click="updatePassword"
				class="uk-button uk-button-primary">@lang('backend::admin-auth.update_password')</button>
		</div>
	</div>
	<div id="password_reset_send_modal" uk-modal>
		<div class="uk-modal-dialog uk-modal-body">
			<h2 class="uk-modal-title">@lang('backend::admin-auth.reset_password')</h2>
			<div class="uk-margin">
				<label class="uk-form-label">@lang('backend::admin-auth.email')</label>
				<div class="uk-form-controls">
					<input name="email" v-model="email" placeholder="@lang('backend::admin-auth.email')" type="email"
						class="uk-input" readonly onfocus="this.removeAttribute('readonly')">
					<span v-if="errors.hasOwnProperty('email')"
						class="uk-text-small uk-text-danger">@{{ errors['email'] }}</span>
				</div>
			</div>
			<button @click="sendResetPassword"
				class="uk-button uk-button-primary">@lang('backend::admin-auth.reset_password')</button>
		</div>
	</div>
	<div id="password_reset_modal" uk-modal>
		<div class="uk-modal-dialog uk-modal-body">
			<h2 class="uk-modal-title">@lang('backend::admin-auth.reset_password')</h2>
			<div class="uk-margin">
				<label class="uk-form-label">@lang('backend::admin-auth.email')</label>
				<div class="uk-form-controls">
					<input name="email" v-model="email" placeholder="@lang('backend::admin-auth.email')" type="email"
						class="uk-input" readonly>
					<span v-if="errors.hasOwnProperty('email')"
						class="uk-text-small uk-text-danger">@{{ errors['email'] }}</span>
				</div>
			</div>
			<div class="uk-margin">
				<label class="uk-form-label">@lang('backend::admin-auth.new_password')</label>
				<div class="uk-form-controls">
					<input name="password" v-model="password" placeholder="@lang('backend::admin-auth.new_password')"
						type="password" class="uk-input" readonly onfocus="this.removeAttribute('readonly')">
					<span v-if="errors.hasOwnProperty('password')"
						class="uk-text-small uk-text-danger">@{{ errors['password'] }}</span>
				</div>
			</div>
			<div class="uk-margin">
				<label class="uk-form-label">@lang('backend::admin-auth.repeat_password')</label>
				<div class="uk-form-controls">
					<input name="password" v-model="password_confirmation"
						placeholder="@lang('backend::admin-auth.repeat_password')" type="password" class="uk-input">
					<span v-if="errors.hasOwnProperty('password_confirmation')"
						class="uk-text-small uk-text-danger">@{{ errors['password_confirmation'] }}</span>
				</div>
			</div>
			<button @click="resetPassword"
				class="uk-button uk-button-primary">@lang('backend::admin-auth.reset_password')</button>
		</div>
	</div>
</div>
@endsection
@section('js')
<script>
	const app = new Vue({
        el: '#app',
        data: {
			password: null,
			password_confirmation: null,
			errors: {},
			email: null
        },
        methods:{
			updatePassword() {                
                axios.post("{{ route('admin.updatePassword') }}", {password: this.password, password_confirmation: this.password_confirmation})
                .then((response) => {                    
                    swal({
                        text: "@lang('backend::admin-auth.updated_correctly')",
                        icon: "success",
                        button: "@lang('public.ok')",
                        className: "center-align"
                    }).then((isConfirm) => {
                        window.location.replace("{{ route('admin.dashboard') }}");
                    });
                })
                .catch((error) => {
                    this.errors = cleanErrors(error).errors;
                });
            },
			openSendResetPassword() {
				UIkit.modal('#password_reset_send_modal', {}).show();
			},
			sendResetPassword() {
				axios.post("{{ route('admin.sendResetPassword') }}", {email: this.email})
                .then((response) => {                    
                    swal({
                        text: "@lang('backend::admin-auth.sended_correctly')",
                        icon: "success",
                        button: "@lang('public.ok')",
                        className: "center-align"
                    }).then((isConfirm) => {});
                })
                .catch((error) => {
                    this.errors = cleanErrors(error).errors;
                });
			},
			resetPassword() {
				axios.post("{{ route('admin.resetPassword') }}", {password: this.password, password_confirmation: this.password_confirmation, email: this.email})
                .then((response) => {                    
                    swal({
                        text: "@lang('backend::admin-auth.updated_correctly')",
                        icon: "success",
                        button: "@lang('public.ok')",
                        className: "center-align"
                    }).then((isConfirm) => {
                        window.location.replace("{{ route('admin.login') }}");
                    });
                })
                .catch((error) => {
                    this.errors = cleanErrors(error).errors;
                });
			}
        },
        created: function (){
        },
        mounted: function (){
			@if(session('password_update'))
				UIkit.modal('#password_update_modal', {
					escClose: false,
					bgClose: false
				}).show();
			@elseif(isset($admin))
				this.email = @json($admin).email;
				UIkit.modal('#password_reset_modal', {
					escClose: false,
					bgClose: false
				}).show();
            @endif
		}
    });
</script>
@endsection