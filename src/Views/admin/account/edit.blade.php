@extends('backend::layouts.admin')
@section('title', __('backend::admin-account.account_settings'))
@section('subtitle', __('backend::admin-account.account_settings_sub'))
@section('icon', 'mdi-account-edit')
@section('content')
	<div class="uk-width-1-1@m uk-width-1-4@l"></div>
	<div class="uk-width-1-1@m uk-width-1-2@l">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				@lang('backend::admin-account.account_settings')
			</div>
			<div class="uk-card-body">
				<form method="POST">
					{{ csrf_field() }}
					<div class="uk-margin">
				        <label class="uk-form-label">@lang('backend::admin-account.name')</label>
				        <div class="uk-form-controls">
				        	<input name="name" required autofocus value="{{ auth()->guard('admin')->user()->name }}" placeholder="@lang('backend::admin-account.name')" type="text" class="uk-input">
				        </div>
				    </div>
					<div class="uk-margin">
				        <label class="uk-form-label">@lang('backend::admin-auth.email')</label>
				        <div class="uk-form-controls">
				        	<input disabled value="{{ auth()->guard('admin')->user()->email }}" placeholder="@lang('backend::admin-account.email')" type="email" class="uk-input">
				        </div>
				    </div>
				    <div class="uk-margin">
				        <label class="uk-form-label">@lang('backend::admin-account.new_password') (@lang('backend::admin-account.leave_blank'))</label>
				        <div class="uk-form-controls">
				        	<input name="new_password" placeholder="@lang('backend::admin-account.new_password')" type="password" class="uk-input">
				        </div>
				    </div>
				    <div class="uk-margin">
				        <label class="uk-form-label">@lang('backend::admin-account.new_password_r')</label>
				        <div class="uk-form-controls">
				        	<input name="new_password_confirmation" placeholder="@lang('backend::admin-account.new_password_r')" type="password" class="uk-input">
				        </div>
				    </div>
				    <br>
				    <div class="uk-margin">
				        <label class="uk-form-label">@lang('backend::admin-account.current_password')</label>
				        <div class="uk-form-controls">
				        	<input name="current_password" required placeholder="@lang('backend::admin-account.current_password')" type="password" class="uk-input">
				        </div>
				    </div>
				    <div class="uk-margin uk-text-center">
						<button type="Submit" class="uk-button uk-button-primary">@lang('backend::admin-account.save')</button>
				    </div>
				</form>
			</div>
		</div>
	</div>
	<div class="uk-width-1-1@m uk-width-1-4@l"></div>
@endsection
