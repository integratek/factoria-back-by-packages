@component('mail::message')
# @lang('backend::public.contact_request')

@lang('backend::public.contact_request_info', ['name' => $contact->name, 'email' => $contact->email])

@component('mail::button', ['url' => route('admin.contact.show', $contact)])
@lang('backend::public.review_message')
@endcomponent

@lang('backend::public.thanks'),<br>
{{ config('app.name') }}
@endcomponent
