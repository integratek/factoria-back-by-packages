@component('mail::message')
# @lang('backend::admin-auth.reset_password')

@lang('backend::admin-auth.reset_password_message')

@component('mail::button', ['url' => route('admin.resetPassword.view', ['admin' => $admin])])
@lang('backend::admin-auth.reset_password')
@endcomponent

@lang('backend::public.thanks'),<br>
{{ config('app.name') }}
@endcomponent
