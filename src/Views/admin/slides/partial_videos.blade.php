<div class="uk-width-1-1@m ">
	<label class="uk-form-label">@lang('backend::admin-slides.video_name')</label>
	<div class="uk-form-controls">
		<input :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.video_name') }" required autofocus v-model.trim="translation.video_name" placeholder="@lang('backend::admin-slides.video_name')" type="text" class="uk-input">
		<span v-if="errors.hasOwnProperty('translations.' + k + '.video_name')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.video_name'] }}</span>
	</div>
</div>
<div class="uk-width-1-2@m uk-width-1-2@l">
	<label class="uk-form-label uk-display-block">
		@lang('backend::admin-slides.mp4')
	</label>
	<span v-show="translation.video_mp4"  class="uk-text-bold">
			@{{ translation && typeof translation.video_mp4 === 'string' ? ' ('+translation.video_mp4+')' : ''}}
			<i class="mdi mdi-delete" @click="onVideoFileDeleted(translation, 'video_mp4')"></i>
	</span>
	<div uk-form-custom="target: true" v-show="!translation.video_mp4">
		<input  :id="k +'video_mp4'" type="file" accept="video/mp4,video/x-m4v,video/*" @change="onFileChange($event, 'video_mp4', translation)">
		<input class="uk-input" :class="{ 'uk-form-danger': errorsVideos.hasOwnProperty('video_mp4_'+ translation.locale)}" type="text" placeholder="@lang('backend::admin-slides.select_video')" disabled>
	</div>
	<div class="uk-margin-small-top">
		<span v-if="errorsVideos.hasOwnProperty('video_mp4_'+ translation.locale)" class="uk-text-small uk-text-danger">* @{{ errorsVideos['video_mp4_'+ translation.locale][0] }}</span>
	</div>
</div>
<div class="uk-width-1-1@m uk-width-1-1@l">
	<label class="uk-form-label uk-display-block uk-margin-small-top">
		@lang('backend::admin-slides.webm')
	</label>
	<span v-show="translation.video_webm" class="uk-text-bold" >
		@{{translation.video_web}} @{{translation && typeof translation.video_webm === 'string' ? ' ('+ translation.video_webm + ')' : '' }}
		<i class="mdi mdi-delete" @click="onVideoFileDeleted(translation, 'video_webm')"></i>
	</span>
	<div uk-form-custom="target: true" v-show="!translation.video_webm">
		<input  :id="k +'video_webm'" type="file" accept="video/mp4,video/x-m4v,video/*" @change="onFileChange($event, 'video_webm', translation)">
		<input class="uk-input" :class="{ 'uk-form-danger': errorsVideos.hasOwnProperty('video_webm_'+ translation.locale)}" type="text" placeholder="@lang('backend::admin-slides.select_video')" disabled>
	</div>
	<div class="uk-margin-small-top">
		<span v-if="errorsVideos.hasOwnProperty('video_webm_'+ translation.locale)" class="uk-text-small uk-text-danger">* @{{ errorsVideos['video_webm_'+ translation.locale][0] }}</span>
	</div>
</div>
