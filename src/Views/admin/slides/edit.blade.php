@extends('backend::layouts.admin')
@section('title', __('backend::admin-slides.edit_slide'))
@section('subtitle', __('backend::admin-slides.edit_slide_sub'))
@section('icon', 'mdi-pencil')
@section('content')

	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<a href="{{route('admin.slides')}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-slides.edit_slide')
			</div>
			<div class="uk-card-body">
				<div uk-grid>
					<div class="uk-width-1-1">
						<ul class="uk-flex-center" :uk-tab="'connect: #seo-translation; media: 960'">
								<li v-for="translation, key) in seotranslations">
									<a href="#">@{{ languages[translation.locale] }}</a>
								</li>
						</ul>
						<ul id="seo-translation" class="uk-switcher">
						    <li v-for="(translation, k) in seotranslations">
						    	<div uk-grid>
						    		<div class="uk-width-1-1 uk-width-1-2@l">
								        <label class="uk-form-label">@lang('backend::admin-slides.name')</label>
								        <div class="uk-form-controls">
								        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.name') }" required autofocus v-model.trim="translation.name" placeholder="@lang('backend::admin-slides.name')" type="text" class="uk-input">
								        	<span v-if="errors.hasOwnProperty('translations.' + k + '.name')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.name'] }}</span>
								        </div>
								    </div>
									<div class="uk-width-1-1 uk-width-1-2@l">
										<label class="uk-form-label">@lang('backend::admin-slides.url')</label>
										<div class="uk-form-controls">
											<input :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.url') }" required autofocus v-model.trim="translation.url" placeholder="@lang('backend::admin-slides.url')" type="text" class="uk-input">
											<span v-if="errors.hasOwnProperty('translations.' + k + '.url')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.url'] }}</span>
										</div>
									</div>
								    <div class="uk-width-1-1">
								        <label class="uk-form-label">@lang('backend::admin-slides.description')</label>
								        <div class="uk-form-controls">
								        	@if (config('backend.modules_configuration.slides.description_with_quill'))
												<quill-editor v-model="translation.description"></quill-editor>
											@else
								        		<input :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.description') }" required v-model.trim="translation.description" placeholder="@lang('backend::admin-slides.description')" type="text" class="uk-input">
											@endif
								        	<span v-if="errors.hasOwnProperty('translations.' + k + '.description')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.description'] }}</span>
								        </div>
								    </div>
										@include('backend::admin.slides.partial_videos')
						    	</div>
						    </li>
						</ul>
					</div>
					<div class="uk-width-1-1">
						<label><input v-model="slide.new_tab" class="uk-checkbox" type="checkbox"> @lang('backend::admin-slides.new_tab')</label>
					</div>
					<div class="uk-width-1-1">
						<label><input v-model="slide.active" class="uk-checkbox" type="checkbox"> @lang('backend::admin-slides.active')</label>
					</div>

					<div class="uk-width-1-1">
						<label class="uk-form-label">@lang('backend::admin-slides.image')</label>
						<a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #images-seo-' + 1">
							<i class="mdi mdi-image"></i>
						</a>
						<image-repo-seo
								v-if="slide.headerImageModel !== false"
								:values.sync="slide.headerImageModel"
								id-r="1"
								:show-selected="true"
								:unique-image="true"
								:allow-repository="true"
								csrf="{{ csrf_token() }}"
								model-class="Lafactoria\Backend\Models\HeaderImageModel"
						></image-repo-seo>
						<span v-if="errors.hasOwnProperty('headerImageModel')" class="uk-text-small uk-text-danger">@{{ errors.headerImageModel }}</span>
					</div>
					<div class="uk-width-1-1 uk-text-center">
						<button @click="updateSlide" class="uk-button uk-button-primary">@lang('backend::admin-slides.save')</button>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				errors: {},
				errorsVideos: {},
				translations: [],
				default_translations: [],
				seotranslations: [],
				languages: [],
				uploading_files: [],
				delete_files: [],
				slide: {
					id: '{{ request()->slide }}',
					name: '',
					description: '',
					active: true,
                    headerImageModel: false,
					url: '',
					new_tab: false,
				},
				video_update: false,
				videos: null,
				images: [],
			},
			methods: {
				updateSlide: function () {
					axios.post("{{ route('admin.api.slides.save') }}", {
						slide: this.slide.id,
						name: this.slide.name,
						description: this.slide.description,
						url: this.slide.url,
						new_tab: this.slide.new_tab,
						active: this.slide.active,
						headerImageModel: this.slide.headerImageModel,
						translations: this.seotranslations,
					}).then((response) => {
							this.slide = response.data;
							this.errors = {};
							$.mdtoast("@lang('backend::admin-slides.slide_updated')", { duration: 5000 });
							this.uploadVideos (this.slide.id)
                            //window.location.replace("{{ route('admin.slides') }}");
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
							this.errors = cleanErrors(error).errors;
						});
				},
				setVideoUpdated(bool) {
					this.video_update = bool;
				},
				onVideoFileDeleted(translation, field) {
					let completeField = field + "_" + translation.locale
					if (!this.uploading_files.includes(completeField)) {
						this.delete_files.push(completeField)
					}else{
						this.uploading_files = this.uploading_files.filter(item => [completeField].includes(item))
					}
					translation[field] = null
				},
				onFileChange(e, video_type, translation) {
					this.errors_before_after = {};
					let files = e.target.files || e.dataTransfer.files;
					if (!files.length)
						return;
					this.uploading_files.push(video_type + '_' + translation.locale)
					this.videos[translation.locale][video_type] = files[0]
				},
				uploadVideos (slide_id) {
					// before after files
					if (this.videos) {
						const formData = new FormData();
						let translationsArray = [];
						for (let x = 0; x < this.slide.translations.length; x ++) {
							let translation = this.slide.translations[x]
							let video = this.videos[translation.locale]
							let videoMp4 = 'video_mp4_' + translation.locale
							let videoWebm = 'video_webm_' + translation.locale
							if (video.video_mp4 && this.uploading_files.includes(videoMp4)) {
								formData.append(videoMp4, video.video_mp4)
							}
							if (video.video_webm && this.uploading_files.includes(videoWebm)) {
								formData.append(videoWebm, video.video_webm);
							}
							if (video.video_name) {
								formData.append('video_name_' + translation.locale, video.video_name);
							}
							translationsArray.push(translation.locale)
						}
						formData.append('translations', JSON.stringify(translationsArray));
						formData.append('to_delete', JSON.stringify(this.delete_files));
						formData.append('uploading_files', JSON.stringify(this.uploading_files));

						const config = {headers: {'Content-Type': 'multipart/form-data'}}
						axios.post('{{ route('admin.api.slides.upload_videos', ['slide' => 'slide_id_here']) }}'.replace('slide_id_here', slide_id), formData, config)
								.then(res => {
									window.location.replace("{{ route('admin.slides') }}");
									this.errorsVideos = {}
								})
								.catch(error => {
									this.errorsVideos = error.response.data && error.response.data.errors ? error.response.data.errors : {};
									$.mdtoast('Error', {
										duration: 5000,
										type: $.mdtoast.type.ERROR
									});
									this.video_update = false;
								})
					}
				},
				initVideos(translations){
					this.videos = []
					for (let x = 0; x < translations.length; x ++){
						 let translation = translations[x]
						this.videos[translation["locale"]] = {}
						this.videos[translation.locale]["video_name"] = translation["video_name"]
					}
				}
			},
			created: function () {
				axios.get("{{ route('admin.api.slides.info', ['slide' => null]) }}/" + this.slide.id)
					.then((response) => {
						this.slide = response.data;
						this.initVideos(this.slide.translations)
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
				axios.get("{{ route('admin.api.images') }}")
					.then((response) => {
						this.images = response.data;
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
				 axios.post("{{ route('admin.api.slides.seotranslations') }}", { slide: this.slide.id })
                    .then((response) => {
                        this.seotranslations = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
				axios.post("{{ route('admin.api.seo.languages') }}")
	                .then((response) => {
	                    this.languages = response.data;
	                })
	                .catch((error) => {
	                    $.mdtoast('Error while getting the languages...', { duration: 5000, type: $.mdtoast.type.ERROR });
	                });
			}
		});
	</script>
@endsection
