@extends('backend::layouts.admin')
@section('title', __('backend::admin-slides.slides'))
@section('subtitle', __('backend::admin-slides.slides_sub'))
@section('icon', 'mdi-camera-burst')
@section('content')
	<div class="uk-width-1-1">
		<div uk-grid>
			<div class="uk-width-1-1">
				<div class="uk-card uk-card-default">
					<div class="uk-card-header">@lang('backend::admin-slides.slides')</div>
					<div class="uk-card-body">
					<a href="{{ route('admin.slides.create') }}" class="uk-button uk-button-default">@lang('backend::admin-slides.create_slide')</a>

					@include("backend::layouts.filter")

					<br><br>
					<div class="uk-overflow-auto">
						<table class="uk-table uk-table-striped">
						    <thead>
						        <tr>
						            <th class="uk-visible@m">#</th>
						            <th>@lang('backend::admin-slides.name')</th>
									<th class="uk-visible@m">@lang('backend::admin-slides.description')</th>
									<th>@lang('backend::admin-news.image')</th>
						            <th class="uk-visible@m">@lang('backend::admin-slides.created_at')</th>
						            <th>@lang('backend::admin-slides.active')</th>
						            <th>@lang('backend::admin-slides.url')</th>
						            <th class="uk-visible@l">@lang('backend::admin-slides.new_tab')</th>
						            <th>@lang('backend::admin-slides.actions')</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr v-for="slide in slides">
						            <td class="uk-visible@m">@{{ slide.id }}</td>
						            <td>@{{ slide.name }}</td>
									<td class="uk-visible@m">@{{ slide.description.replace(/(<([^>]+)>)/gi, "") }}</td>
									<td>
										<img v-if="slide.headerImageModel != null" :src="getImage(slide.headerImageModel.image_id)" align="middle" style="max-height: 100px">
						            </td>
						            <td class="uk-visible@m">@{{ slide.created_at }}</td>
						            <td>
						            	<span v-if="slide.active" class="uk-text-success mdi mdi-circle"> @lang('backend::admin-slides.yes')</span>
										<span v-if="!slide.active" class="uk-text-danger mdi mdi-circle"> @lang('backend::admin-slides.no')</span>
						            </td>
						            <td>
						            	<span v-if="!slide.url" class="mdi mdi-close"></span>
						            	<a v-if="slide.url" class="mdi mdi-link-variant" :href="slide.url" :uk-tooltip="'title: ' + slide.url"></a>
						            </td>
						            <td class="uk-visible@l">
						            	<span v-if="slide.new_tab" class="uk-text-success mdi mdi-circle"> @lang('backend::admin-slides.yes')</span>
										<span v-if="!slide.new_tab" class="uk-text-danger mdi mdi-circle"> @lang('backend::admin-slides.no')</span>
						            </td>
						            <td>
						            	<div class="uk-button-group">
										    <a :href="'{{ route('admin.slides.edit', ['slide' => null]) }}/' + slide.id" class="uk-button uk-button-default uk-button-small">
												<i class="mdi mdi-pencil"></i>
										    </a>
										    <a href="#" @click="deleteSlide(slide.id)" class="uk-button uk-button-danger uk-button-small">
												<i class="mdi mdi-delete"></i>
										    </a>
										</div>
						            </td>
						        </tr>
						    </tbody>
						</table>
						<br>
						<div class="row">
							{{ $slides->links('backend::public.pagination-uikit') }}
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				slides: [],
				images: [],
			},
			methods: {
				updateSlides: function () {
					axios.get("{{ route('admin.api.slides') }}")
						.then((response) => {
							this.slides = response.data;
							this.slides.map(function (slide) {
								slide.created_at = moment(slide.created_at).fromNow();
								return slide;
							});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
				},
				deleteSlide: function (id) {
                    UIkit.modal.confirm("@lang('backend::admin-slides.confirm_delete')", {labels: {ok: "@lang('backend::admin-slides.delete_yes')", cancel: "@lang('backend::admin-slides.delete_no')"}}).then(function() {
						axios.post("{{ route('admin.api.slides.delete') }}", { slide: id })
							.then ((response) => {
								this.updateSlides();
								$.mdtoast("@lang('backend::admin-slides.slide_deleted')", { duration: 5000 });
							})
							.catch((error) => {
								$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
							});
					}.bind(this));
				},
				getImages(){
					var ids = this.slides.map(a => (a.headerImageModel != null)  ? a.headerImageModel.image_id : null);
					axios.post("{{ route('admin.api.getImages') }}", {ids: ids})
						.then((response) => {
							this.images = response.data;
						})
						.catch((error) => {
							console.log('Error updating images');
						});
				},
				getImage(id){
					var image = this.images.find(image => image.id == id);
                	return image ? image.base64 : '';
				}
			},
			created: function () {
				//this.updateSlides();
				this.slides = <?php echo json_encode($slides) ;?>;
				this.slides = this.slides.data;
				// Get images to preview
				this.getImages();
			}
		});
	</script>
@endsection
