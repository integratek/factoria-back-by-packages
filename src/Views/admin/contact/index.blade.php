@extends('backend::layouts.admin')
@section('title', __('backend::admin-contact.contact_requests'))
@section('subtitle', __('backend::admin-contact.contact_requests_sub'))
@section('icon', 'mdi-email')
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">@lang('backend::admin-contact.contact_requests')</div>
			<div class="uk-card-body">

				@include("backend::layouts.filter")
				<br>
				<a href="javascript:void(0)" @click="exportExcel()" class="uk-button uk-button-default">@lang('backend::public.export')</a>
				<br><br>
				<div class="uk-overflow-auto">
					<table class="uk-table uk-table-striped">
					    <thead>
					        <tr>
					            <th class="uk-visible@m">#</th>
					            <th>@lang('backend::admin-contact.name')</th>
					            <th>@lang('backend::admin-contact.surname')</th>
					            <th class="uk-visible@m">@lang('backend::admin-contact.email')</th>
					            <th class="uk-visible@m">@lang('backend::admin-contact.phone')</th>
					            <th class="uk-visible@l">@lang('backend::admin-contact.created_at')</th>
					            <th>@lang('backend::admin-contact.actions')</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr v-for="(contact, k) in contacts">
					            <td class="uk-visible@m">@{{ contact.id }}</td>
					            <td>@{{ contact.name }}</td>
					            <td>@{{ contact.surname }}</td>
					            <td class="uk-visible@m">@{{ contact.email }}</td>
					            <td class="uk-visible@m">@{{ contact.phone }}</td>
					            <td class="uk-visible@l">@{{ contact.created_at }}</td>
					            <td>
					            	<div v-if="contact.id != '-'" class="uk-button-group">
									    <a :href="'{{ route('admin.contact.show', ['user' => null]) }}/' + contact.id" class="uk-button uk-button-default uk-button-small">
											<i class="mdi mdi-eye"></i>
										</a>
										<a href="#" @click="deleteContact(contact.id)" class="uk-button uk-button-danger uk-button-small">
											<i class="mdi mdi-delete"></i>
										</a>
									</div>
									<span v-if="contact.id == '-'">-</span>
					            </td>
					        </tr>
					    </tbody>
					</table>
					<br>
					<div class="row">
						{{ $contacts->links('backend::public.pagination-uikit') }}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				contacts: [],
			},
			methods: {
				deleteContact: function (id) {
                    UIkit.modal.confirm("@lang('backend::admin-slides.confirm_delete')", {labels: {ok: "@lang('backend::admin-slides.delete_yes')", cancel: "@lang('backend::admin-slides.delete_no')"}}).then(function() {
						axios.post("{{ route('admin.api.contact.delete') }}", { contact: id })
							.then ((response) => {
								this.updateContacts();
								$.mdtoast("@lang('backend::admin-contact.contact_deleted')", { duration: 5000 });
							})
							.catch((error) => {
								$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
							});
					}.bind(this));
				},
				updateContacts: function () {
					axios.get("{{ route('admin.api.contact') }}")
						.then((response) => {
							if (response.data.length > 0) {
								this.contacts = response.data;
								this.contacts.map(function (contact) {
									contact.created_at = moment(contact.created_at, 'YYYY-MM-DD hh:mm:ss').fromNow();
									return contact;
								});
							}
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
				},
				exportExcel() {
					let url = window.location.origin + window.location.pathname + '/export';
					window.location = url;
				}
			},
			created: function () {
				//this.updateContacts();
				this.contacts = <?php echo json_encode($contacts) ;?>;
				this.contacts = this.contacts.data;
			}
		});
	</script>
@endsection
