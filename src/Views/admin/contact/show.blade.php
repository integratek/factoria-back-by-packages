@extends('backend::layouts.admin')
@section('title', __('backend::admin-contact.contact_request_info'))
@section('subtitle', __('backend::admin-contact.contact_request_info_sub'))
@section('icon', 'mdi-email')
@section('css')
<style>
	.contact-info {
		font-size: 1.25em;
	}
</style>
@endsection
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-contact.contact_request_info')
			</div>
			<div class="uk-card-body">
				<div uk-grid>
					<div class="uk-width-1-1@m uk-width-1-2@l">
						<label class="uk-form-label">@lang('backend::admin-contact.name')</label>
				        <div class="uk-form-controls uk-text-center">
				            <input readonly class="uk-input" type="text" v-model="contact.name">
				        </div>
					</div>
					<div class="uk-width-1-1@m uk-width-1-2@l">
						<label class="uk-form-label">@lang('backend::admin-contact.surname')</label>
				        <div class="uk-form-controls uk-text-center">
				            <input readonly class="uk-input" type="text" v-model="contact.surname">
				        </div>
					</div>
					<div class="uk-width-1-1@m uk-width-1-2@l">
						<label class="uk-form-label">@lang('backend::admin-contact.email')</label>
				        <div class="uk-form-controls uk-text-center">
				            <input readonly class="uk-input" type="text" v-model="contact.email">
				        </div>
					</div>
					<div class="uk-width-1-1@m uk-width-1-2@l">
						<label class="uk-form-label">@lang('backend::admin-contact.phone')</label>
				        <div class="uk-form-controls uk-text-center">
				            <input readonly class="uk-input" type="text" v-model="contact.phone">
				        </div>
					</div>
					<div class="uk-width-1-1">
						<label class="uk-form-label">@lang('backend::admin-contact.message')</label>
				        <div class="uk-form-controls uk-text-center">
				            <textarea rows="10" class="uk-textarea" readonly v-model="contact.message"></textarea>
				        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				contact: {
					id: '{{ request()->contact }}',
					name: '-',
					surname: '-',
					email: '-',
					phone: '-',
					created_at: '-',
				},
			},
			created: function () {
				axios.get("{{ route('admin.api.contact.info', ['contact' => null]) }}/" + this.contact.id )
					.then((response) => {
						this.contact = response.data;
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
			}
		});
	</script>
@endsection
