@extends('backend::layouts.admin')
@section('title', __('backend::admin-config.configuration'))
@section('subtitle', __('backend::admin-config.configuration_sub'))
@section('icon', 'mdi-settings')
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default uk-card-body">
			@if (config('mail.username') and config('mail.password'))
				<div class="uk-alert-success" uk-alert>
					<h3 class="light-text">@lang('backend::admin-config.mail_information')</h3>
					<p>@lang('backend::admin-config.mail_configured', ['name' => config('mail.from.name')])</p>
				</div>
			@else
				<div class="uk-alert-danger" uk-alert>
					<h3 class="light-text">@lang('backend::admin-config.mail_information')</h3>
					<p>@lang('backend::admin-config.mail_not_configured')</p>
				</div>
			@endif
		</div>
		<br>
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">@lang('backend::admin-config.edit_configuration')</div>
			<div class="uk-card-body">
                <div uk-grid class="uk-grid-small">
					<div class="uk-width-1-1@m uk-width-1-2@l">
				        <label class="uk-form-label">@lang('backend::admin-config.name')</label>
				        <div class="uk-form-controls">
				        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('name') }" required autofocus v-model.trim="config.name" placeholder="@lang('backend::admin-config.name')" type="text" class="uk-input">
				        	<span v-if="errors.hasOwnProperty('name')" class="uk-text-small uk-text-danger">@{{ errors.name[0] }}</span>
				        </div>
				    </div>
				    <div class="uk-width-1-1@m uk-width-1-2@l">
				        <label class="uk-form-label">@lang('backend::admin-config.email')</label>
				        <div class="uk-form-controls">
				        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('email') }" required autofocus v-model.trim="config.email" placeholder="@lang('backend::admin-config.email')" type="text" class="uk-input">
				        	<span v-if="errors.hasOwnProperty('email')" class="uk-text-small uk-text-danger">@{{ errors.email[0] }}</span>
				        </div>
				    </div>
				    <div class="uk-width-1-1@m uk-width-1-2@l">
				        <label class="uk-form-label">@lang('backend::admin-config.phone')</label>
				        <div class="uk-form-controls">
				        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('phone') }" required autofocus v-model.trim="config.phone" placeholder="@lang('backend::admin-config.phone')" type="text" class="uk-input">
				        	<span v-if="errors.hasOwnProperty('phone')" class="uk-text-small uk-text-danger">@{{ errors.phone[0] }}</span>
				        </div>
				    </div>
				    <div class="uk-width-1-1@m uk-width-1-2@l">
				        <label class="uk-form-label">@lang('backend::admin-config.adress')</label>
				        <div class="uk-form-controls">
				        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('adress') }" required autofocus v-model.trim="config.adress" placeholder="@lang('backend::admin-config.adress')" type="text" class="uk-input">
				        	<span v-if="errors.hasOwnProperty('adress')" class="uk-text-small uk-text-danger">@{{ errors.adress[0] }}</span>
				        </div>
				    </div>
				    <div class="uk-width-1-1@m uk-width-1-2@l">
				        <label class="uk-form-label">@lang('backend::admin-config.city')</label>
				        <div class="uk-form-controls">
				        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('city') }" required autofocus v-model.trim="config.city" placeholder="@lang('backend::admin-config.city')" type="text" class="uk-input">
				        	<span v-if="errors.hasOwnProperty('city')" class="uk-text-small uk-text-danger">@{{ errors.city[0] }}</span>
				        </div>
				    </div>
				    <div class="uk-width-1-1@m uk-width-1-2@l">
				        <label class="uk-form-label">@lang('backend::admin-config.country')</label>
				        <div class="uk-form-controls">
				        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('country') }" required autofocus v-model.trim="config.country" placeholder="@lang('backend::admin-config.country')" type="text" class="uk-input">
				        	<span v-if="errors.hasOwnProperty('country')" class="uk-text-small uk-text-danger">@{{ errors.country[0] }}</span>
				        </div>
				    </div>

                    <div class="uk-width-1-1 uk-margin-medium-top">
                        <ul class="uk-flex-center" :uk-tab="'connect: #seo-translation; media: 960'">
                            <li v-for="(translation, key) in seotranslations">
                                <a href="#">@{{ languages[translation.locale] }}</a>
                            </li>
                        </ul>
                        <ul id="seo-translation" class="uk-switcher">
                            <li v-for="(translation, k) in translations">
                                <div uk-grid>
                                    <div class="uk-width-1-1@m uk-width-1-2@l">
                                        <label class="uk-form-label">@lang('backend::admin-config.tags')</label>
                                        <div class="uk-form-controls">
                                            <input v-model="translation.tags" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.tags') }" required autofocus placeholder="@lang('backend::admin-config.tags')" type="text" class="uk-input">
                                            <span v-if="errors.hasOwnProperty('translations.' + k + '.tags')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.tags'][0] }}</span>
                                        </div>
                                    </div>
                                    <div class="uk-width-1-1@m uk-width-1-2@l">
                                        <label class="uk-form-label">@lang('backend::admin-config.description')</label>
                                        <div class="uk-form-controls">
                                            <input v-model="translation.description" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.description') }" required autofocus placeholder="@lang('backend::admin-config.description')" type="text" class="uk-input">
                                            <span v-if="errors.hasOwnProperty('translations.' + k + '.description')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.description'][0] }}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

					<div class="uk-width-1-1 uk-margin-medium-top">
				    	<label class="uk-form-label">@lang('backend::admin-config.scripts')</label>
				    	<div class="uk-form-controls">
				    		<textarea v-model.trim="config.scripts" :class="{ 'uk-form-danger': errors.hasOwnProperty('scripts') }" class="uk-textarea" rows="5" placeholder="@lang('backend::admin-config.scripts')"></textarea>
				    		<span v-if="errors.hasOwnProperty('scripts')" class="uk-text-small uk-text-danger">@{{ errors.scripts[0] }}</span>
				    	</div>
				    </div>
				    <div class="uk-width-1-1">
				    	<label class="uk-form-label">@lang('backend::admin-config.styles')</label>
				    	<div class="uk-form-controls">
				    		<textarea v-model.trim="config.styles" :class="{ 'uk-form-danger': errors.hasOwnProperty('styles') }" class="uk-textarea" rows="5" placeholder="@lang('backend::admin-config.styles')"></textarea>
				    		<span v-if="errors.hasOwnProperty('styles')" class="uk-text-small uk-text-danger">@{{ errors.styles[0] }}</span>
				    	</div>
				    </div>
				    <div class="uk-width-1-1 uk-margin-top uk-text-center">
						<button @click="updateConfig" class="uk-button uk-button-primary">@lang('backend::admin-config.save')</button>
				    </div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
                seotranslations: [],
                translations: [],
                languages: [],
				errors: {},
				config: {
					name: '',
					phone: '',
					email: '',
					adress: '',
					city: '',
					tags: '',
					description: '',
					country: '',
					scripts: '',
					styles: '',
				},
			},
			methods: {
				updateConfig: function () {
					axios.post("{{ route('admin.api.config') }}", this.config)
						.then((response) => {
							this.config = response.data;
							this.errors = {};
							$.mdtoast("{{ __('backend::admin-config.config_updated') }}", { duration: 5000 });
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
							this.errors = error.response.data.errors;
						});
				}
			},
			created: function () {
                axios.post("{{ route('admin.api.seo.languages') }}")
                    .then((response) => {
                        this.languages = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast('Error while getting the languages...', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });

                axios.post("{{ route('admin.api.news.seotranslations') }}", { news: null })
                    .then((response) => {
                        this.seotranslations = response.data;

                    })
                    .catch((error) => {
                        $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });

				axios.get("{{ route('admin.api.config') }}")
					.then((response) => {
						this.config = response.data;

                        this.translations = response.data.translations;
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
			}
		});
	</script>
@endsection
