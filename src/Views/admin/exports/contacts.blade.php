<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        tr th {
            background-color: #7886d7 !important;
        }
    </style>
</head>
<table>
    <thead>
    <tr>
        <th style="background-color: #7886d7;">@lang('backend::public.name')</th>
        <th style="background-color: #7886d7;">@lang('backend::public.surname')</th>
        <th style="background-color: #7886d7;">@lang('backend::public.email')</th>
        <th style="background-color: #7886d7;">@lang('backend::public.phone')</th>
        <th style="background-color: #7886d7;">@lang('backend::public.message')</th>
        <th style="background-color: #7886d7;">@lang('backend::public.date')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($contacts as $contact)
        <tr>
            <td>{{ $contact->name }}</td>
            <td>{{ $contact->surname }}</td>
            <td>{{ $contact->email }}</td>
            <td>{{ $contact->phone }}</td>
            <td>{{ $contact->message }}</td>
            <td>{{ \Carbon\Carbon::parse($contact->created_at)->format('d-m-Y H:i') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</html>
