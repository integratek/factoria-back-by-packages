@extends('backend::layouts.admin')
@section('title', __('backend::admin-banners.create_banner'))
@section('subtitle', __('backend::admin-banners.create_banner_sub'))
@section('icon', 'mdi-plus')
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-banners.create_banner')
			</div>
			<div class="uk-card-body">
				<div uk-grid>
                    <div class="uk-width-1-1">
                        <label class="uk-form-label">@lang('backend::admin-banners.name')</label>
                        <div class="uk-form-controls">
                            <input :class="{ 'uk-form-danger': errors.hasOwnProperty('name') }" required autofocus v-model.trim="banner.name" placeholder="@lang('backend::admin-banners.name')" type="text" class="uk-input">
                            <span v-if="errors.hasOwnProperty('name')" class="uk-text-small uk-text-danger">@{{ errors.name[0] }}</span>
                        </div>
                    </div>
                    <div class="uk-width-1-1">
                        <label class="uk-form-label">@lang('backend::admin-banners.url')</label>
                        <div class="uk-form-controls">
                            <input :class="{ 'uk-form-danger': errors.hasOwnProperty('url') }" required autofocus v-model.trim="banner.url" placeholder="@lang('backend::admin-banners.url')" type="text" class="uk-input">
                            <span v-if="errors.hasOwnProperty('url')" class="uk-text-small uk-text-danger">@{{ errors.url[0] }}</span>
                        </div>
                    </div>
                    <div class="uk-width-1-1">
                        <label class="uk-form-label">@lang('backend::admin-banners.description')</label>
                        <div class="uk-form-controls">
                            <input :class="{ 'uk-form-danger': errors.hasOwnProperty('description') }" v-model.trim="banner.description" placeholder="@lang('backend::admin-banners.description')" type="description" class="uk-input">
                            <span v-if="errors.hasOwnProperty('description')" class="uk-text-small uk-text-danger">@{{ errors.description[0] }}</span>
                        </div>
                    </div>
                    <div class="uk-width-1-1">
                        <label><input v-model="banner.active" class="uk-checkbox" type="checkbox"> @lang('backend::admin-banners.active')</label>
                    </div>

                    @if(config('backend.images.banners') === true)
						<div class="uk-width-1-1">
							<label class="uk-form-label">@lang('backend::admin-banners.image')</label>
							<a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #images-seo-' + 1">
								<i class="mdi mdi-image"></i>
							</a>
							<image-repo-seo
									:values.sync="banner.headerImageModel"
									id-r="1"
									:show-selected="true"
									:unique-image="true"
									:allow-repository="true"
									csrf="{{ csrf_token() }}"
									model-class="Lafactoria\Backend\Models\HeaderImageModel"
									:disable-seo="true"
							></image-repo-seo>
						</div>
					@endif

                    <div class="uk-width-1-1 uk-text-center">
                        <button @click="createBanner" class="uk-button uk-button-primary">@lang('backend::admin-banners.create')</button>
                    </div>
                </div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				errors: {},
				banner: {
					name: '',
					description: '',
					active: true,
                    headerImageModel: null,
					url: '',
				},
				images: [],
			},
			methods: {
				createBanner: function () {
					axios.post("{{ route('admin.api.banners.create') }}", this.banner)
						.then((response) => {
							this.errors = {};
							axios.post("{{ route('admin.flash') }}", { msg: 'admin-banners.banner_created', type: 'success' })
								.then((response) => {
									window.location.replace("{{ route('admin.banners') }}");
								});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
							this.errors = error.response.data.errors;
						});
				}
			},
			created: function () {
				axios.get("{{ route('admin.api.images') }}")
					.then((response) => {
						this.images = response.data;

					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						this.errors = error.response.data.errors;
					});
			}
		});
	</script>
@endsection
