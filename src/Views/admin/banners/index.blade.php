@extends('backend::layouts.admin')
@section('title', __('backend::admin-banners.banners'))
@section('subtitle', __('backend::admin-banners.banners_sub'))
@section('icon', 'mdi-image-area')
@section('content')
	<div class="uk-width-1-1">
		<div uk-grid>
			<div class="uk-width-1-1">
				<div class="uk-card uk-card-default">
					<div class="uk-card-header">@lang('backend::admin-banners.banners')</div>
					<div class="uk-card-body">
					<a href="{{ route('admin.banners.create') }}" class="uk-button uk-button-default">@lang('backend::admin-banners.create_banner')</a>
					
					@include("backend::layouts.filter")

					<br><br>
					<div class="uk-overflow-auto">
						<table class="uk-table uk-table-striped">
						    <thead>
						        <tr>
						            <th class="uk-visible@m">#</th>
						            <th>@lang('backend::admin-banners.name')</th>
									<th class="uk-visible@m">@lang('backend::admin-banners.description')</th>
									<th>@lang('backend::admin-news.image')</th>
						            <th class="uk-visible@m">@lang('backend::admin-banners.created_at')</th>
						            <th>@lang('backend::admin-banners.active')</th>
						            <th>@lang('backend::admin-banners.url')</th>
						            <th>@lang('backend::admin-banners.actions')</th>
						        </tr>
						    </thead>
						    <tbody>
						        <tr v-for="banner in banners">
						            <td class="uk-visible@m">@{{ banner.id }}</td>
						            <td>@{{ banner.name }}</td>
									<td class="uk-visible@m">@{{ banner.description }}</td>
									<td>
										<img v-if="banner.headerImageModel != null" :src="getImage(banner.headerImageModel.image_id)" align="middle" style="max-height: 100px">
						            </td>
						            <td class="uk-visible@m">@{{ banner.created_at }}</td>
						            <td>
						            	<span v-if="banner.active" class="uk-text-success mdi mdi-circle"> @lang('backend::admin-banners.yes')</span>
										<span v-if="!banner.active" class="uk-text-danger mdi mdi-circle"> @lang('backend::admin-banners.no')</span>
						            </td>
						            <td>
						            	<span v-if="!banner.url" class="mdi mdi-close"></span>
										<a v-if="banner.url" class="mdi mdi-link-variant" :href="banner.url" :uk-tooltip="'title: ' + banner.url"></a>
						            </td>
						            <td>
						            	<div class="uk-button-group">
										    <a :href="'{{ route('admin.banners.edit', ['banner' => null]) }}/' + banner.id" class="uk-button uk-button-default uk-button-small">
												<i class="mdi mdi-pencil"></i>
										    </a>
										    <a href="#" @click="deleteBanner(banner.id)" class="uk-button uk-button-danger uk-button-small">
												<i class="mdi mdi-delete"></i>
										    </a>
										</div>
						            </td>
						        </tr>
						    </tbody>
						</table>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				banners: [],
				images: [],
			},
			methods: {
				/*updateBanners: function () {
					axios.get("{{ route('admin.api.banners') }}")
						.then((response) => {
							this.banners = response.data;
							this.banners.map(function (banner) {
								banner.created_at = moment(banner.created_at).fromNow();
								return banner;
							});
							this.getImages();
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
				},*/
				deleteBanner: function (id) {
                    UIkit.modal.confirm("@lang('backend::admin-banners.confirm_delete')", {labels: {ok: "@lang('backend::admin-banners.delete_yes')", cancel: "@lang('backend::admin-banners.delete_no')"}}).then(function() {
						axios.post("{{ route('admin.api.banners.delete') }}", { banner: id })
							.then ((response) => {
								this.updateBanners();
								$.mdtoast("@lang('backend::admin-banners.banner_deleted')", { duration: 5000 });
							})
							.catch((error) => {
								$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
							});
					}.bind(this));
				},
				getImages(){
					var ids = this.banners.map(a => (a.headerImageModel != null)  ? a.headerImageModel.image_id : null);
					axios.post("{{ route('admin.api.getImages') }}", {ids: ids})
						.then((response) => {
							this.images = response.data;
						})
						.catch((error) => {
							console.log('Error updating images');
						});
				},
				getImage(id){
					var image = this.images.find(image => image.id == id);
                	return image ? image.base64 : '';
				}
			},
			created: function () {
				//this.updateBanners();
				this.banners = <?php echo json_encode($banners) ;?>;
				this.banners = this.banners.data;
				// Get images to preview
				this.getImages();
			}
		});
	</script>
@endsection
