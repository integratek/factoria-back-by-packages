@extends('backend::layouts.admin')
@section('title', __('backend::admin-users.users'))
@section('subtitle', __('backend::admin-users.users_sub'))
@section('icon', 'mdi-account-multiple')
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">@lang('backend::admin-users.users')</div>
			<div class="uk-card-body">
				<a href="{{ route('admin.users.create') }}" class="uk-button uk-button-default">@lang('backend::admin-users.create_user')</a>
				<br><br>
				<div class="uk-overflow-auto">
					<table class="uk-table uk-table-striped">
					    <thead>
					        <tr>
					            <th class="uk-visible@m">#</th>
					            <th>@lang('backend::admin-users.name')</th>
					            <th>@lang('backend::admin-users.email')</th>
					            <th class="uk-visible@m">@lang('backend::admin-users.created_at')</th>
					            <th class="uk-visible@m">@lang('backend::admin-users.updated_at')</th>
					            <th>@lang('backend::admin-users.actions')</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr v-for="user in users">
					            <td class="uk-visible@m">@{{ user.id }}</td>
					            <td>@{{ user.name }}</td>
					            <td>@{{ user.email }}</td>
					            <td class="uk-visible@m">@{{ user.created_at }}</td>
					            <td class="uk-visible@m">@{{ user.updated_at }}</td>
					            <td>
					            	<div class="uk-button-group">
									    <a :href="'{{ route('admin.users.edit', ['user' => null]) }}/' + user.id" class="uk-button uk-button-default uk-button-small">
											<i class="mdi mdi-pencil"></i>
									    </a>
									    <a href="#" @click="deleteUser(user.id)" class="uk-button uk-button-danger uk-button-small">
											<i class="mdi mdi-delete"></i>
									    </a>
									</div>
					            </td>
					        </tr>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				users: [
					{
						id: '-',
						name: '-',
						email: '-',
						created_at: '-',
						updated_at: '-',
					}
				],
			},
			methods: {
				deleteUser: function (id) {
                    UIkit.modal.confirm("@lang('backend::admin-users.confirm_delete')", {labels: {ok: "@lang('backend::admin-users.delete_yes')", cancel: "@lang('backend::admin-users.delete_no')"}}).then(function() {
	                    axios.post("{{ route('admin.api.users.delete') }}", { user: id })
						.then((response) => {
							this.updateUsers();
							axios.post("{{ route('admin.trans') }}", { msg: 'admin-users.user_deleted' }).then((response) => {
								$.mdtoast(response.data.msg, { duration: 5000 });
							});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
	                }.bind(this));
				},
				updateUsers: function () {
					axios.get("{{ route('admin.api.users') }}")
						.then((response) => {
							this.users = response.data;
							this.users.map(function (user) {
								user.created_at = moment(user.created_at, 'YYYY-MM-DD hh:mm:ss').fromNow();
								user.updated_at = moment(user.updated_at, 'YYYY-MM-DD hh:mm:ss').fromNow();
								return user;
							});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
				}
			},
			created: function () {
				this.updateUsers();
			}
		});
	</script>
@endsection
