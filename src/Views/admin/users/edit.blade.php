@extends('backend::layouts.admin')
@section('title', __('backend::admin-users.edit_user'))
@section('subtitle', __('backend::admin-users.edit_user_sub'))
@section('icon', 'mdi-account-edit')
@section('content')
	<div class="uk-width-1-1@m uk-width-1-4@l"></div>
	<div class="uk-width-1-1@m uk-width-1-2@l">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
                <a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-users.edit_user')
            </div>
			<div class="uk-card-body">
				<div class="uk-margin">
			        <label class="uk-form-label">@lang('backend::admin-users.name')</label>
			        <div class="uk-form-controls">
			        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('name') }" required autofocus v-model.trim="user.name" placeholder="@lang('backend::admin-users.name')" type="text" class="uk-input">
			        	<span v-if="errors.hasOwnProperty('name')" class="uk-text-small uk-text-danger">@{{ errors.name[0] }}</span>
			        </div>
			    </div>
				<div class="uk-margin">
			        <label class="uk-form-label">@lang('backend::admin-auth.email')</label>
			        <div class="uk-form-controls">
			        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('email') }" v-model.trim="user.email" placeholder="@lang('backend::admin-users.email')" type="email" class="uk-input">
			        	<span v-if="errors.hasOwnProperty('email')" class="uk-text-small uk-text-danger">@{{ errors.email[0] }}</span>
			        </div>
			    </div>
			    <div class="uk-margin">
			        <label class="uk-form-label">@lang('backend::admin-users.new_password') (@lang('backend::admin-users.leave_blank'))</label>
			        <div class="uk-form-controls">
						<input v-model="user.new_password" :class="{ 'uk-form-danger': (errors.hasOwnProperty('password') || errors.hasOwnProperty('new_password')) }" placeholder="@lang('backend::admin-users.new_password')" type="password" class="uk-input">
						<span v-if="errors.hasOwnProperty('new_password')" class="uk-text-small uk-text-danger">@{{ errors.new_password[0] }}</span>
			        	<span v-if="errors.hasOwnProperty('password')" class="uk-text-small uk-text-danger">@{{ errors.password[0] }}</span>
			        </div>
			    </div>
			    <div class="uk-margin">
			        <label class="uk-form-label">@lang('backend::admin-users.new_password_r')</label>
			        <div class="uk-form-controls">
			        	<input v-model="user.new_password_r" placeholder="@lang('backend::admin-users.new_password_r')" type="password" class="uk-input">
			        </div>
			    </div>
				<div class="uk-margin" v-if="roles && roles.length > 0">
					<label class="uk-form-label">@lang('backend::admin-users.role')</label>
					<div class="uk-form-controls">
						<multiselect
								v-model="user.role"
								:options="roles"
								placeholder="@lang('backend::admin-users.role')"
								track-by="id"
								label="name"
								:multiple="false"
								:show-labels="false">
						</multiselect>
						<span v-if="errors.hasOwnProperty('role_id')" class="uk-text-small uk-text-danger">@{{ errors.role_id[0] }}</span>
					</div>
				</div>
				<div class="uk-margin" v-if="user.role && user.role.id == 2">
					<label class="uk-form-label">@lang('backend::admin-users.languages_title')</label>
					<div class="uk-form-controls">
						<multiselect
								v-model="user.languages"
								:options="languages"
								placeholder="@lang('backend::admin-users.languages')"
								track-by="locale"
								label="locale_name"
								:multiple="true"
								:show-labels="false">
						</multiselect>
						<span v-if="errors.hasOwnProperty('languages')" class="uk-text-small uk-text-danger">@{{ errors.languages[0] }}</span>
					</div>
				</div>
			    <div class="uk-margin uk-text-center">
					<button @click="updateUser" class="uk-button uk-button-primary">@lang('backend::admin-users.save')</button>
			    </div>
			</div>
		</div>
	</div>
	<div class="uk-width-1-1@m uk-width-1-4@l"></div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				errors: {},
				user: {
					id: '{{ request()->user }}',
					name: '',
					email: '',
					new_password: '',
					new_password_r: '',
					role: null,
					role_id: null,
					languages: null,
				},
				roles: [],
				languages: @json(\Lafactoria\Backend\Traits\Helper::getLanguages()),
			},
			watch: {
				'user.role'(value) {
					if (value && value.id) {
						this.user.role_id = value.id

						if (value.id == 1)
							this.user.languages = null
					} else {
						this.user.role_id = null;
						this.user.languages = null
					}
				}
			},
			methods: {
				updateUser: function () {
					axios.post("{{ route('admin.api.users.edit') }}", {
							user: this.user.id,
							name: this.user.name,
							email: this.user.email,
							password: this.user.new_password,
							password_confirmation: this.user.new_password_r,
							role_id: this.user.role_id,
							languages: this.user.languages,
						})
						.then((response) => {
							this.errors = {};
							$.mdtoast("@lang('backend::admin-users.user_edited')", { duration: 5000 });
                            window.location.replace("{{ route('admin.users') }}");
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
							this.errors = error.response.data.errors;
						});
				}
			},
			created: function () {
				@php
					$roles = collect(config('backend.admin_roles'))->map(function ($role) {
						$role['name'] = __($role['name']);
						return $role;
					})
				@endphp
				this.roles = @json($roles);

				axios.get("{{ route('admin.api.users.info', ['user' => null]) }}/" + this.user.id)
					.then((response) => {
						response.data.role = this.roles.find(role => role.id == response.data.role_id)
						response.data.languages = response.data.languages ? response.data.languages.map(locale => {
							return this.languages.find(lang => lang.locale === locale);
						}) : null
						this.user = response.data;
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						this.errors = error.response.data.errors;
					});
			}
		});
	</script>
@endsection
