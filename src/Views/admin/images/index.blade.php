@extends('backend::layouts.admin')
@section('title', __('backend::admin-images.images'))
@section('subtitle', __('backend::admin-images.images_sub'))
@section('icon', 'mdi-camera')
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">@lang('backend::admin-images.images')</div>
			<div class="uk-card-body">
				<a href="#" uk-toggle="target: #images-1" class="uk-button uk-button-default">@lang('backend::admin-images.upload_image')</a>

				@include("backend::layouts.filter")

                <image-repo
                        :values.sync="uploadedFiles"
                        id-r="1"
                        :show-selected="false"
                        :unique-image="false"
                        :allow-repository="false"
                        csrf="{{ csrf_token() }}"
                        v-on:finished-upload="onFinished"
                ></image-repo>

				<br><br>
				<div class="uk-overflow-auto">
					<table class="uk-table uk-table-striped">
					    <thead>
					        <tr>
								<th class="uk-visible@m">#</th>
					            <th>@lang('backend::admin-images.name')</th>
								<th></th>
					            <th class="uk-visible@m">@lang('backend::admin-images.is_used')</th>
					            <th class="uk-visible@m">@lang('backend::admin-images.created_at')</th>
					            <th>@lang('backend::admin-images.actions')</th>
					        </tr>
					    </thead>
					    <tbody>
							<tr v-for="(image, k) in images">
					            <td class="uk-visible@m">@{{ image.id }}</td>
								<td>@{{ image.name }}</td>
								<td><img :src="image.base64" style="max-width: 50px"></td>
					            <td class="uk-visible@m">
									<span v-if="image.is_used" class="uk-text-success mdi mdi-circle"> @lang('backend::admin-images.yes')</span>
									<span v-if="!image.is_used" class="uk-text-danger mdi mdi-circle"> @lang('backend::admin-images.no')</span>
					            </td>
					            <td class="uk-visible@m">@{{ image.created_at }}</td>
					            <td>
					            	<div class="uk-button-group">
									    <a :href="'{{ route('admin.images.edit', ['user' => null]) }}/' + image.id" class="uk-button uk-button-default uk-button-small">
											<i class="mdi mdi-pencil"></i>
									    </a>
									    <a :href="'{{ route('admin.images.download', ['image' => null]) }}/' + image.id" class="uk-button uk-button-default uk-button-small">
											<i class="mdi mdi-cloud-download"></i>
									    </a>
										<a href="#" @click="deleteImage(image.id)" class="uk-button uk-button-danger uk-button-small">
											<i class="mdi mdi-delete"></i>
										</a>
									</div>
					            </td>
					        </tr>
					    </tbody>
					</table>
					<br>
					<div class="row">
						{{ $images->links('backend::public.pagination-uikit') }}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				images: [],
                uploadedFiles: [],
			},
			methods: {
			    onFinished: function () {
                    this.updateImages();
                    UIkit.modal($('#images-1')).hide();
                },
				deleteImage: function (id) {
                    UIkit.modal.confirm("@lang('backend::admin-images.confirm_delete')", {labels: {ok: "@lang('backend::admin-images.delete_yes')", cancel: "@lang('backend::admin-images.delete_no')"}}).then(function() {
	                    axios.post("{{ route('admin.api.images.delete') }}", { image: id })
						.then((response) => {
							this.updateImages();
							$.mdtoast("@lang('backend::admin-images.image_deleted')", { duration: 5000 });
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
	                }.bind(this));
				},
				updateImages: function () {
					axios.get("{{ route('admin.api.images') }}")
						.then((response) => {
							this.images = response.data.data;							
							this.images.map(function (image) {
								image.created_at = moment(image.created_at, 'YYYY-MM-DD hh:mm:ss').fromNow();
								return image;
							});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
				}
			},
			created: function () {
				//this.updateImages();
				this.images = <?php echo json_encode($images) ;?>;
				this.images = this.images.data;
			}
		});
	</script>
@endsection
