@extends('backend::layouts.admin')
@section('title', __('backend::admin-images.edit_image'))
@section('subtitle', __('backend::admin-images.edit_image_sub'))
@section('icon', 'mdi-pencil')
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-images.edit_image')
			</div>
			<div class="uk-card-body">
				<div uk-grid>
					<div class="uk-width-1-1@s uk-width-1-2@m uk-width-2-3@l">
						<label class="uk-form-label">@lang('backend::admin-images.name')</label>
						<div class="uk-form-controls">
							<input :class="{ 'uk-form-danger': errors.hasOwnProperty('name') }" required autofocus v-model.trim="image.name" placeholder="@lang('backend::admin-users.name')" type="text" class="uk-input">
							<span v-if="errors.hasOwnProperty('name')" class="uk-text-small uk-text-danger">@{{ errors.name[0] }}></span>
						</div>
					</div>
                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-3@l uk-text-center">
                        <img :src="image.base64" alt="">
                    </div>
					<div class="uk-width-1-1 uk-text-center">
						<button @click="saveImage" class="uk-button uk-button-primary">@lang('backend::admin-images.save')</button>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
@section('js')
<script>
	const app = new Vue({
		el: '#app',
		data: {
			errors: {},
			image: {
				id: '{{ request()->image }}',
				name: ''
			}
		},
		methods: {
			saveImage: function () {
				axios.post("{{ route('admin.api.images.save') }}", {
						image: this.image.id,
						name: this.image.name
					})
					.then((response) => {
						$.mdtoast("@lang('backend::admin-images.image_updated')", { duration: 5000 });
                        window.location.replace("{{ route('admin.images') }}");
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
			},
		},
		created: function () {
			axios.get("{{ route('admin.api.images.info', ['image' => null]) }}/" + this.image.id)
				.then((response) => {
					this.image = response.data;
				})
				.catch((error) => {
					$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
				});
		}
	});
</script>
@endsection
