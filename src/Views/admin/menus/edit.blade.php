@extends('backend::layouts.admin')
@section('title', __('backend::admin-menus.edit_menu'))
@section('subtitle', __('backend::admin-menus.edit_menu_sub'))
@section('icon', 'mdi-pencil')
@section('content')
    <div class="uk-width-1-1">
        <div class="uk-card uk-card-default">
            <div class="uk-card-header">
                <a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-menus.edit_menu')
            </div>
            <div class="uk-card-body">
                <div uk-grid>
                    <div class="uk-width-1-1">
                        <ul class="uk-flex-center" :uk-tab="'connect: #seo-translation; media: 960'">
                            <li v-for="(translation, key) in seotranslations">
                                <a href="#" @click="currentLocale = translation.locale">@{{ languages[translation.locale] }}</a>
                            </li>
                        </ul>
                        <ul id="seo-translation" class="uk-switcher">
                            <li v-for="(translation, k) in seotranslations">
                                <div uk-grid>
                                    <div class="uk-width-1-1" v-if="translation.locale != 'en'">
                                        <div class="uk-margin-top"></div>
                                        <label><input v-model="translation.copy_from_english" class="uk-checkbox" type="checkbox"> @lang('backend::admin-menus.copy_from_english')</label>
                                    </div>
                                    <div uk-grid v-if="!translation.copy_from_english">
                                        <div class="uk-width-1-1">
                                            <label class="uk-form-label">@lang('backend::admin-news.title')</label>
                                            <div class="uk-form-controls">
                                                <input :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.title') }" required autofocus v-model.trim="translation.title" placeholder="@lang('backend::admin-news.title')" type="text" class="uk-input">
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.title')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.title'] }}</span>
                                            </div>
                                        </div>
                                        <div v-if="!showHtmlContent" class="uk-width-1-1">
                                            <label class="uk-form-label">@lang('backend::admin-news.content')</label>
                                            <div class="uk-form-controls">
                                                <quill-editor v-model="translation.content" ref="quill_content"></quill-editor>
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.content')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.content'] }}</span>
                                            </div>
                                        </div>
                                        <div v-if="showHtmlContent" id="textAreaHTML" class="uk-width-1-1">
                                            <div class="uk-form-label">@lang('backend::admin-menus.content')</div>
                                            <div class="uk-form-controls">
                                                <textarea v-model="translation.content" class="uk-textarea" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.content') }" rows="5"></textarea>
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.content')" class="uk-text-small uk-text-danger">
                                                    @{{ errors['translations.' + k + '.content'] }}
                                                </span>
                                            </div>
                                        </div>
                                        <div>
                                            <button @click="showHtmlContent = !showHtmlContent" class="uk-button uk-button-default" :class="{ 'uk-button-danger': showHtmlContent }">@lang('backend::admin-menus.show-html')</button>
                                        </div>
                                        <div v-if="!showHtmlHighlighted" class="uk-width-1-1">
                                            <label class="uk-form-label">@lang('backend::admin-menus.highlighted')</label>
                                            <div class="uk-form-controls">
                                                <quill-editor v-model="translation.highlighted" ref="quill_highlighted"></quill-editor>
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.highlighted')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.highlighted'] }}</span>
                                            </div>
                                        </div>

                                        <div v-if="showHtmlHighlighted" id="textAreaHTML" class="uk-width-1-1">
                                            <div class="uk-form-label">@lang('backend::admin-menus.highlighted')</div>
                                            <div class="uk-form-controls">
                                                <textarea v-model="translation.highlighted" class="uk-textarea" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.highlighted') }" rows="5"></textarea>
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.highlighted')" class="uk-text-small uk-text-danger">
                                                    @{{ errors['translations.' + k + '.highlighted'] }}
                                                </span>
                                            </div>
                                        </div>
                                        <div>
                                            <button @click="showHtmlHighlighted = !showHtmlHighlighted" class="uk-button uk-button-default" :class="{ 'uk-button-danger': showHtmlHighlighted }">@lang('backend::admin-menus.show-html')</button>
                                        </div>
                                    </div>
                                    <div class="uk-width-1-1 uk-margin-large-top">
                                        <label class="uk-form-label">@lang('backend::admin-documents.documents')</label>
                                        <a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #docs-' + translation.locale">
                                            <i class="mdi mdi-file"></i>
                                        </a>

                                        <div class="uk-margin">
                                            <docs-repo
                                                    :values.sync="translation.files"
                                                    :id-r="translation.locale"
                                                    :show-selected="true"
                                                    :unique-doc="false"
                                                    :allow-repository="true"
                                                    csrf="{{ csrf_token() }}">
                                            </docs-repo>
                                            <span v-if="errors && errors.hasOwnProperty('file')" class="uk-text-small uk-text-danger">@{{ errors.file[0] }}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="uk-width-1-1@m uk-width-1-2@l" v-if="canUrl && seotranslations.length > 0">
                        <label class="uk-form-label">@lang('backend::admin-menus.route_list')</label>
                        <div class="uk-form-controls">
                            <select ref="routeSelect" :disabled="menu.type == {{ Lafactoria\Backend\Models\Menu::LAYOUT_TYPE }}" class="uk-select" @change="onSelectRoute">
                                <option></option>
                                <option
                                    v-for="(url, name) in routes"
                                    :value="getRouteUrlValue(url, currentLocale, true)"
                                    :selected="seotranslations.find(trans => trans.locale == currentLocale).url == getRouteUrlValue(url, currentLocale, false)"
                                >
                                    @{{ name }}
                            </option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-1-1@m uk-width-1-2@l" v-if="canUrl && seotranslations.length > 0">
                        <label class="uk-form-label">@lang('backend::admin-menus.url')</label>
                        <div class="uk-form-controls">
                            <input :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.'+seotranslations.findIndex(trans => trans.locale === currentLocale)+'.url') }" required autofocus v-model.trim="seotranslations.find(trans => trans.locale == currentLocale).url" placeholder="@lang('backend::admin-menus.url')" type="url" class="uk-input" :readonly="blockURLInput">
                            <span v-if="errors.hasOwnProperty('translations.'+seotranslations.findIndex(trans => trans.locale === currentLocale)+'.url')" class="uk-text-small uk-text-danger">@{{ errors['translations.'+seotranslations.findIndex(trans => trans.locale === currentLocale)+'.url'] }}</span>
                        </div>
                    </div>

                    <div class="uk-width-1-1">
                        <p>@lang('backend::admin-menus.active')</p>
                        <switches v-model="menu.active" theme="bulma" color="blue"></switches>
                    </div>

                    <div class="uk-width-1-1">
                        <label><input v-model="menu.on_nav" class="uk-checkbox" type="checkbox"> @lang('backend::admin-menus.on_nav')</label>
                    </div>
                    <div class="uk-width-1-1">
                        <label><input v-model="menu.on_footer" class="uk-checkbox" type="checkbox"> @lang('backend::admin-menus.on_footer')</label>
                    </div>

                    <div class="uk-width-1-1">
                        <label class="uk-form-label">@lang('backend::admin-news.image')</label>
                        <a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #images-seo-' + 1">
                            <i class="mdi mdi-image"></i>
                        </a>
                        <image-repo-seo
                                v-if="menu.headerImageModel !== false"
                                :values.sync="menu.headerImageModel"
                                id-r="1"
                                :show-selected="true"
                                :unique-image="true"
                                :allow-repository="true"
                                csrf="{{ csrf_token() }}"
                                model-class="Lafactoria\Backend\Models\HeaderImageModel"
                        ></image-repo-seo>
                    </div>

                    <div class="uk-width-1-1">
                        <label class="uk-form-label">@lang('backend::admin-news.more_images')</label>
                        <a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #images-seo-' + 2">
                            <i class="mdi mdi-image"></i>
                        </a>
                        <image-repo-seo
                                v-if="menu.imageModel !== false"
                                :values.sync="menu.imageModel"
                                id-r="2"
                                :show-selected="true"
                                :unique-image="false"
                                :allow-repository="true"
                                csrf="{{ csrf_token() }}"
                                model-class="Lafactoria\Backend\Models\ImageModel"
                        ></image-repo-seo>
                    </div>

                    <div class="uk-width-1-1 uk-text-center">
                        <button @click="edit" class="uk-button uk-button-default">@lang('backend::admin-menus.update_menu')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                errors: {},
                images: [],
                seotranslations: [],
                languages: [],
                showHtmlContent: false,
                showHtmlHighlighted: false,
                menu: {
                    id: {{ request()->menu }},
                    active: true,
                    headerImageModel: false,
                    type: '',
                    parent_id: '',
                    imageModel: false,
                    on_nav: true,
                    on_footer: true,
                },
                currentLocale: null,
                blockURLInput: false,
                routes: @json(Lafactoria\Backend\Models\Menu::routes())
            },
            computed: {
                canEdit () {
                    if (this.menu.type == {{ Lafactoria\Backend\Models\Menu::LAYOUT_TYPE }}) {
                        return true;
                    } else if (this.menu.type == {{ Lafactoria\Backend\Models\Menu::SUBMENU_TYPE }} && {{ Lafactoria\Backend\Models\Menu::SUBMENU_CONTENT ? 'true' : 'false' }}) {
                        return true;
                    }

                    return false;
                },
                canUrl () {
                    if (this.menu.type == {{ Lafactoria\Backend\Models\Menu::LAYOUT_TYPE }} || this.menu.type == {{ Lafactoria\Backend\Models\Menu::LINK_TYPE }}) {
                        return true;
                    } else if (this.menu.type == {{ Lafactoria\Backend\Models\Menu::SUBMENU_TYPE }} && {{ config('backend.submenus_with_urls', false) ? 'true' : 'false' }}) {
                        return true;
                    }

                    return false;
                }
            },
            methods: {
                getRouteUrlValue(url, locale, json) {
                    if (typeof url === 'object') {
                        if (json) {
                            return JSON.stringify(url)
                        } else {
                            return url[locale]
                        }
                    }
                    return url
                },
				onSelectRoute(ev) {
                    if (ev.target.value == '') {
                        this.blockURLInput = false
                    } else {
                        this.autoSlug = false
                        let json = null
                        try { json = JSON.parse(ev.target.value) } catch(e) {}
                        this.seotranslations.forEach(trans => {
                            if (json != null && typeof json === 'object') {
                                trans.url = json[trans.locale]
                            } else {
                                trans.url = ev.target.value
                            }
                        })
                        this.blockURLInput = true
                    }
                },
                getImagesRepo () {
                    axios.get("{{ route('admin.api.images') }}")
                        .then((response) => {
                            this.images = response.data;
                        })
                        .catch((error) => {
                            $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                        });
                },
                edit () {
                    this.seotranslations = this.checkTranslationsEnglishClones(this.seotranslations, ['title', 'content', 'highlighted']);

                    axios.post("{{ route('admin.api.menus.edit') }}", { menu: this.menu, translations: this.seotranslations })
                        .then((response) => {
                            this.errors = {};
                            axios.post("{{ route('admin.flash') }}", { msg: 'admin-menus.menu_edited', type: 'success' })
                                .then((response) => {
                                    window.location.replace("{{ route('admin.menus') }}");
                                });
                        })
                        .catch((error) => {
                            $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });

                            // remove object and number translation information in strings and set in vue errors var
                            this.errors = cleanErrors(error).errors;
                        });
                },
                showSuccess: function () {
                    this.errors = {};
                    $.mdtoast("@lang('backend::admin-images.image_uploaded')", { duration: 5000 });
                    this.getImagesRepo();
                }
            },
            created: function () {
                axios.get("{{ route('admin.api.info', request()->menu) }}")
                    .then((response) => {
                        this.menu = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
                this.getImagesRepo();
                axios.post("{{ route('admin.api.menus.seotranslations') }}", { menu: this.menu.id })
                    .then((response) => {
                        this.currentLocale = response.data[0].locale;
                        this.seotranslations = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
                axios.post("{{ route('admin.api.seo.languages') }}")
                    .then((response) => {
                        this.languages = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast('Error while getting the languages...', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
            }
        });
    </script>
@endsection

