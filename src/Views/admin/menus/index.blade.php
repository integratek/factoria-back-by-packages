@extends('backend::layouts.admin')
@section('title', __('backend::admin-menus.menus'))
@section('subtitle', __('backend::admin-menus.menus_sub'))
@section('icon', 'mdi-lan')
@section('content')
    <div class="uk-width-1-1">
        <div class="uk-card uk-card-default">
            <div class="uk-card-header">@lang('backend::admin-menus.menus')</div>
            <div class="uk-card-body">
                <a href="{{ route('admin.menus.create') }}" class="uk-button uk-button-default">@lang('backend::admin-menus.create_menu')</a>
                <br><br>

                <div class="uk-overflow-auto">
                    <table class="uk-table" style="margin-bottom: 0px;">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th class="uk-visible@m">Active</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody is="draggable" element="tbody" :list="menus" :move="checkMove" @end="dragged">
                            <tr v-for="(menu, k) in menus">
                                <td class="uk-hidden">
                                    <!-- The SEO modal linked to the seo button -->
                                    <seo
                                            save-url="{{ route('admin.api.seo.save') }}"
                                            get-url="{{ route('admin.api.seo') }}"
                                            seo-trans-url="{{ route('admin.api.seo.translations') }}"
                                            languages-url="{{ route('admin.api.seo.languages') }}"
                                            :locales="{{ collect(config('translatable.locales'))->toJson() }}"
                                            class-r="Lafactoria\Backend\Models\Menu"
                                            :lang-locale="null"
                                            :reference="menu.id"
                                            :id-r="'seo-' + menu.id"
                                            :bus="bus">
                                    </seo>
                                </td>
                                <td><span class="mdi mdi-cursor-move uk-margin-small-right" :class="{'uk-margin-left': menu.parent_id != null}"></span> @{{ menu.title }}
                                    &nbsp;
                                    <a v-if="menu.type == {{ Lafactoria\Backend\Models\Menu::SUBMENU_TYPE }}" :href="'{{ route('admin.menus.create') }}' + '/' + menu.id" class="uk-button uk-button-default uk-button-small uk-button-secondary">
                                        <i class="mdi mdi-plus"></i>
                                    </a>
                                </td>
                                <td class="uk-visible@m">
                                    <span v-if="menu.active" class="uk-text-success mdi mdi-circle" :class="{'uk-margin-left': menu.parent_id != null}"></span>
                                    <span v-if="!menu.active" class="uk-text-danger mdi mdi-circle" :class="{'uk-margin-left': menu.parent_id != null}"></span>
                                    <switches :emit-on-mount="false" @input="toggleActive(app)" v-model="menu.active" theme="bulma" color="blue"></switches>
                                </td>

                                <td>
                                    <div class="uk-button-group" :class="{'uk-margin-left': menu.parent_id != null}">
                                        <a :href="'{{ route('admin.menus.edit', ['menu' => null]) }}' + '/' + menu.id" class="uk-button uk-button-default uk-button-small">
                                            <i class="mdi mdi-pencil"></i>
                                        </a>
                                        <a class="uk-button uk-button-default uk-button-small" @click="onClickOpenSeo(menu.id)">
                                            <i class="mdi mdi-tag-multiple"></i>
                                        </a>
                                        <a href="#" @click="deleteMenu(menu.id)" class="uk-button uk-button-danger uk-button-small">
                                            <i class="mdi mdi-delete"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('js/clipboard.min.js') }}"></script>
    <script>
        var clipboard = new Clipboard('.btn');

        clipboard.on('success', function(e) {
            console.info('Text:', e.text);
            $.mdtoast("URL copiada.", { duration: 1000 });

            e.clearSelection();
        });

        const app = new Vue({
            el: '#app',
            data: {
                menus: [],
                bus: new Vue(),
            },
            computed: {
                allMenus() {
                    let allMenus = [];

                    this.menus.forEach((menu) => {
                        allMenus.push(menu);
                        if (menu.childs.length > 0) {
                            menu.childs.forEach((child) => {
                                allMenus.push(child);
                            })
                        }
                    });

                    return allMenus;
                }
            },
            watch: {
                menus: {
                    handler: function (newMenus) {
                        this.toggleActive();
                    },
                    deep: true,
                }
            },
            methods: {
                toggleActive() {
                    axios.post("{{ route('admin.api.menus.toggleActives') }}", { menus: this.menus })
                        .then((response) => {
                            $.mdtoast("@lang('backend::admin-menus.updated')", { duration: 1000 });
                        })
                        .catch((error) => {
                            $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                        });
                },
                checkMove: function(evt){
                    let menu_moved = evt.draggedContext.element;
                    let menu_passed = evt.relatedContext.element;

                    let isMenuMovedParent = menu_moved.parent_id == null;
                    let isMenuPassedParent = menu_passed.parent_id == null;

                    if (!isMenuMovedParent) {
                        if (isMenuPassedParent) {
                            return false;
                        }
                    } else {
                        if (!isMenuPassedParent) {
                            return false;
                        }
                    }
                    return true;
                },
                dragged(evt) {
                    this.reOrder();
                    axios.post("{{ route('admin.api.menus.order') }}", { menus: this.allMenus })
                        .then((response) => {
                            $.mdtoast("@lang('backend::admin-menus.updated')", { duration: 1000 });
                            this.updateMenus();
                        })
                        .catch((error) => {
                            $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                        });
                },
                reOrder() {
                    this.menus.forEach((element, index) => {
                        element.order = index + 1;
                        element.childs.forEach((celement, cindex) => {
                            celement.order = cindex + 1;
                        });
                    });
                },
                deleteMenu(id) {
                    UIkit.modal.confirm("@lang('backend::admin-menus.confirm_delete')", {labels: {ok: "@lang('backend::admin-menus.delete_yes')", cancel: "@lang('backend::admin-menus.delete_no')"}}).then(function() {
                        axios.post("{{ route('admin.api.menus.delete') }}", { menu: id })
                            .then((response) => {
                                this.updateMenus();
                            })
                            .catch((error) => {
                                $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                            });
                    }.bind(this));
                },
                updateMenus: function () {
                    axios.get("{{ route('admin.api.menus') }}")
                        .then((response) => {
                            this.menus = response.data;
                            this.menus.map((menu) => {
                                menu.created_at = moment(menu.created_at, 'YYYY-MM-DD hh:mm:ss').fromNow();
                                return menu;
                            });
                        })
                        .catch((error) => {
                            $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                        });
                },
                onClickOpenSeo(id){
					this.bus.$emit('id-selected', 'seo-' + id);
				}
            },
            created: function () {
                this.updateMenus();
            }
        });
    </script>
@endsection

