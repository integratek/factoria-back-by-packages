@extends('backend::layouts.admin')
@section('title', __('backend::admin-menus.create_menu'))
@section('subtitle', __('backend::admin-menus.create_menu_sub'))
@section('icon', 'mdi-plus')
@section('content')
    <div class="uk-width-1-1">
        <div class="uk-card uk-card-default">
            <div class="uk-card-header">
                <a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-menus.create_menu')
            </div>
            <div class="uk-card-body">
                <div uk-grid>
                    <div class="uk-width-1-1">
                        <ul class="uk-flex-center" :uk-tab="'connect: #seo-translation; media: 960'">
                            <li v-for="(translation, key) in seotranslations">
                                <a href="#" @click="currentLocale = translation.locale">@{{ languages[translation.locale] }}</a>
                            </li>
                        </ul>
                        <ul id="seo-translation" class="uk-switcher">
                            <li v-for="(translation, k) in seotranslations">
                                <div uk-grid>
                                    <div class="uk-width-1-1" v-if="translation.locale != 'en'">
                                        <div class="uk-margin-top"></div>
                                        <label><input v-model="translation.copy_from_english" class="uk-checkbox" type="checkbox"> @lang('backend::admin-menus.copy_from_english')</label>
                                    </div>
                                    <div uk-grid v-if="!translation.copy_from_english">
                                        <div class="uk-width-1-1">
                                            <label class="uk-form-label">@lang('backend::admin-menus.title')</label>
                                            <div class="uk-form-controls">
                                                <input @input="generateSlug(translation.title)" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.title') }" required autofocus v-model.trim="translation.title" placeholder="@lang('backend::admin-menus.title')" type="text" class="uk-input">
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.title')" class="uk-text-small uk-text-danger">
                                                    @{{ errors['translations.' + k + '.title'] }}
                                                </span>
                                            </div>
                                        </div>
                                        <div v-if="!showHtmlContent"class="uk-width-1-1">
                                            <label class="uk-form-label">@lang('backend::admin-menus.content')</label>
                                            <div class="uk-form-controls">
                                                <quill-editor v-model="translation.content" ref="quill_content"></quill-editor>
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.content')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.content'] }}</span>
                                            </div>
                                        </div>

                                        <div v-if="showHtmlContent" id="textAreaHTML" class="uk-width-1-1">
                                            <div class="uk-form-label">@lang('backend::admin-menus.content')</div>
                                            <div class="uk-form-controls">
                                                <textarea v-model="translation.content" class="uk-textarea" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.content') }" rows="5"></textarea>
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.content')" class="uk-text-small uk-text-danger">
                                                    @{{ errors['translations.' + k + '.content'] }}
                                                </span>
                                            </div>
                                        </div>
                                        <div>
                                            <button @click="showHtmlContent = !showHtmlContent" class="uk-button uk-button-default" :class="{ 'uk-button-danger': showHtmlContent }">@lang('backend::admin-menus.show-html')</button>
                                        </div>

                                        <div v-if="!showHtmlHighlighted" class="uk-width-1-1">
                                            <label class="uk-form-label">@lang('backend::admin-menus.highlighted')</label>
                                            <div class="uk-form-controls">
                                                <quill-editor v-model="translation.highlighted" ref="quill_highlighted"></quill-editor>
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.highlighted')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.highlighted'] }}</span>
                                            </div>
                                        </div>

                                        <div v-if="showHtmlHighlighted" id="textAreaHTML" class="uk-width-1-1">
                                            <div class="uk-form-label">@lang('backend::admin-menus.highlighted')</div>
                                            <div class="uk-form-controls">
                                                <textarea v-model="translation.highlighted" class="uk-textarea" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.highlighted') }" rows="5"></textarea>
                                                <span v-if="errors.hasOwnProperty('translations.' + k + '.highlighted')" class="uk-text-small uk-text-danger">
                                                    @{{ errors['translations.' + k + '.highlighted'] }}
                                                </span>
                                            </div>
                                        </div>
                                        <div>
                                            <button @click="showHtmlHighlighted = !showHtmlHighlighted" class="uk-button uk-button-default" :class="{ 'uk-button-danger': showHtmlHighlighted }">@lang('backend::admin-menus.show-html')</button>
                                        </div>
                                    </div>
                                    <div class="uk-width-1-1 uk-margin-large-top">
                                        <label class="uk-form-label">@lang('backend::admin-documents.documents')</label>
                                        <a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #docs-' + translation.locale">
                                            <i class="mdi mdi-file"></i>
                                        </a>

                                        <div class="uk-margin">
                                            <docs-repo
                                                    :values.sync="translation.files"
                                                    :id-r="translation.locale"
                                                    :show-selected="true"
                                                    :unique-doc="false"
                                                    :allow-repository="true"
                                                    csrf="{{ csrf_token() }}">
                                            </docs-repo>
                                            <span v-if="errors && errors.hasOwnProperty('file')" class="uk-text-small uk-text-danger">@{{ errors.file[0] }}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="uk-width-1-1@m uk-width-1-2@l">
                        <label class="uk-form-label">@lang('backend::admin-menus.type')</label>
                        <div class="uk-form-controls">
                            <select v-model="menu.type" class="uk-select">
                                @php
                                    $elements = request()->parent ? Lafactoria\Backend\Models\Menu::types()->except('Submenu') : Lafactoria\Backend\Models\Menu::types();
                                @endphp
                                @foreach ($elements as $name => $type)
                                    <option value="{{ $type }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-1-1@m uk-width-1-2@l">
                        <label class="uk-form-label">@lang('backend::admin-menus.parent_id')</label>
                        <div class="uk-form-controls">
                            <input readonly :class="{ 'uk-form-danger': errors.hasOwnProperty('menu.parent_id') }" required autofocus v-model.trim="parent.title" placeholder="@lang('backend::admin-menus.parent_id')" type="parent_id" class="uk-input">
                            <span v-if="errors.hasOwnProperty('menu.parent_id')" class="uk-text-small uk-text-danger">@{{ errors['menu.parent_id'] }}</span>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-width-1-2@l" v-if="canUrl && seotranslations.length > 0">
                        <label class="uk-form-label">@lang('backend::admin-menus.route_list')</label>
                        <div class="uk-form-controls">
                            <select :disabled="menu.type == {{ Lafactoria\Backend\Models\Menu::LAYOUT_TYPE }}" v-model="menu.url" class="uk-select" @change="onSelectRoute">
                                <option></option>
                                @foreach (Lafactoria\Backend\Models\Menu::routes() as $name => $url)
                                    <option value="{{ $url }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-width-1-2@l">
                        <p>@lang('backend::admin-menus.active')</p>
                        <switches v-model="menu.active" theme="bulma" color="blue"></switches>
                    </div>
                    <div class="uk-width-1-1@m uk-width-1-2@l" v-if="canUrl && seotranslations.length > 0">
                        <label class="uk-form-label">@lang('backend::admin-menus.url')</label>
                        <div class="uk-form-controls">
                            <label><input v-model="autoSlug" @change="autoSlugToggle()" class="uk-checkbox" type="checkbox"> @lang('backend::admin-news.autoslug')</label>
                            <input :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.'+seotranslations.findIndex(trans => trans.locale === currentLocale)+'.url') }" required autofocus v-model.trim="seotranslations.find(trans => trans.locale == currentLocale).url" placeholder="@lang('backend::admin-menus.url')" type="url" class="uk-input" :readonly="blockURLInput">
                            <span v-if="errors.hasOwnProperty('translations.'+seotranslations.findIndex(trans => trans.locale === currentLocale)+'.url')" class="uk-text-small uk-text-danger">@{{ errors['translations.'+seotranslations.findIndex(trans => trans.locale === currentLocale)+'.url'] }}</span>
                        </div>
                    </div>

                    <div class="uk-width-1-1">
                        <label><input v-model="menu.on_nav" class="uk-checkbox" type="checkbox"> @lang('backend::admin-menus.on_nav')</label>
                    </div>
                    <div class="uk-width-1-1">
                        <label><input v-model="menu.on_footer" class="uk-checkbox" type="checkbox"> @lang('backend::admin-menus.on_footer')</label>
                    </div>

                    <div class="uk-width-1-1">
                        <div class="uk-margin-top uk-visible@l"></div>
                        <label class="uk-form-label">@lang('backend::admin-menus.image')</label>
                        <a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #images-seo-' + 1">
                            <i class="mdi mdi-image"></i>
                        </a>
                        <image-repo-seo
                                :values.sync="menu.headerImageModel"
                                id-r="1"
                                :show-selected="true"
                                :unique-image="true"
                                :allow-repository="true"
                                csrf="{{ csrf_token() }}"
                                model-class="Lafactoria\Backend\Models\HeaderImageModel"
                                :disable-seo="true"
                        ></image-repo-seo>
                    </div>

                    <div class="uk-width-1-1">
                        <label class="uk-form-label">@lang('backend::admin-menus.more_images')</label>
                        <a class="uk-margin-left uk-button uk-button-default uk-button-small" :uk-toggle="'target: #images-seo-' + 2">
                            <i class="mdi mdi-image"></i>
                        </a>
                        <image-repo-seo
                                :values.sync="menu.imageModel"
                                id-r="2"
                                :show-selected="true"
                                :unique-image="false"
                                :allow-repository="true"
                                csrf="{{ csrf_token() }}"
                                model-class="Lafactoria\Backend\Models\ImageModel"
                                :disable-seo="true"
                        ></image-repo-seo>
                    </div>

                    <div class="uk-width-1-1 uk-text-center">
                        <button @click="create" class="uk-button uk-button-default">@lang('backend::admin-menus.create_menu')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                errors: {},
                menus: [],
                autoSlug: true,
                images: [],
                seotranslations: [],
                languages: [],
                showHtmlContent: false,
                showHtmlHighlighted: false,
                menu: {
                    active: true,
                    headerImageModel: null,
                    imageModel: [],
                    type: {{ Lafactoria\Backend\Models\Menu::LAYOUT_TYPE }},
                    parent_id: {{ request()->parent ? request()->parent : 'null' }},
                    on_nav: true,
                    on_footer: true
                },
                currentLocale: null,
                blockURLInput: false
            },
            computed: {
                parent () {
                    let menu = this.menus.filter((element) => element.id == this.menu.parent_id)
                    return menu.length > 0 ? menu[0] : { title: '-' };
                },
                canEdit () {
                    if (this.menu.type == {{ Lafactoria\Backend\Models\Menu::LAYOUT_TYPE }}) {
                        return true;
                    } else if (this.menu.type == {{ Lafactoria\Backend\Models\Menu::SUBMENU_TYPE }} && {{ Lafactoria\Backend\Models\Menu::SUBMENU_CONTENT ? 'true' : 'false' }}) {
                        return true;
                    }

                    return false;
                },
                canUrl () {
                    if (this.menu.type == {{ Lafactoria\Backend\Models\Menu::LAYOUT_TYPE }} || this.menu.type == {{ Lafactoria\Backend\Models\Menu::LINK_TYPE }}) {
                        return true;
                    } else if (this.menu.type == {{ Lafactoria\Backend\Models\Menu::SUBMENU_TYPE }} && {{ config('backend.submenus_with_urls', false) ? 'true' : 'false' }}) {
                        return true;
                    }

                    return false;
                }
            },
            methods: {
                onSelectRoute(ev) {
                    if (ev.target.value == '') {
                        this.blockURLInput = false
                    } else {
                        this.autoSlug = false
                        let json = null
                        try { json = JSON.parse(ev.target.value) } catch(e) {}
                        this.seotranslations.forEach(trans => {
                            if (json != null && typeof json === 'object') {
                                trans.url = json[trans.locale]
                            } else {
                                trans.url = ev.target.value
                            }
                        })
                        this.blockURLInput = true
                    }
                },
                autoSlugToggle: function () {
                    if (this.autoSlug)
                        this.generateSlug(this.seotranslations.find(trans => trans.locale === this.currentLocale).title);
                },
                generateSlug: function (text) {
                    if (this.autoSlug) {
                        if (text) {
                            let index = this.seotranslations.findIndex(trans => trans.locale === this.currentLocale);
                            if (index !== -1) {
                                this.seotranslations[index].url = "/" + slugify(text, {
                                    replacement: '-',
                                    lower: true,
                                });
                            }
                        }
                    }
                },
                create () {
                    this.seotranslations = this.checkTranslationsEnglishClones(this.seotranslations, ['title', 'content', 'highlighted']);

                    axios.post("{{ route('admin.api.menus.create') }}", { menu: this.menu, translations: this.seotranslations })
                        .then((response) => {
                            this.errors = {};
                            axios.post("{{ route('admin.flash') }}", { msg: 'admin-menus.menu_created', type: 'success' })
                                .then((response) => {
                                    window.location.replace("{{ route('admin.menus') }}");
                                });
                        })
                        .catch((error) => {
                            $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });

                            // remove object and number translation information in strings and set in vue errors var
                            this.errors = cleanErrors(error).errors;
                        });
                }
            },
            created: function () {
                axios.get("{{ route('admin.api.menus') }}")
                    .then((response) => {
                        this.menus = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
                axios.get("{{ route('admin.api.images') }}")
                    .then((response) => {
                        this.images = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
                axios.post("{{ route('admin.api.menus.seotranslations') }}", { menu: null })
                    .then((response) => {
                        this.currentLocale = response.data[0].locale;
                        this.seotranslations = this.addTranslationsCopyFromEnglish(response.data).map(trans => {
                            this.$set(trans, 'files', []);
                            return trans;
                        });
                    })
                    .catch((error) => {
                        $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
                axios.post("{{ route('admin.api.seo.languages') }}")
                    .then((response) => {
                        this.languages = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast('Error while getting the languages...', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
            }
        });
    </script>
@endsection

