@extends('backend::layouts.admin')
@section('title', __('backend::admin-documents.edit_document'))
@section('subtitle', __('backend::admin-documents.edit_document_sub'))
@section('icon', 'mdi-pencil')
@section('content')
	<div class="uk-width-1-1@m uk-width-1-4@l"></div>
	<div class="uk-width-1-1@m uk-width-1-2@l">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-documents.edit_document')
			</div>
			<div class="uk-card-body">
				<div class="uk-margin">
			        <label class="uk-form-label">@lang('backend::admin-documents.name')</label>
			        <div class="uk-form-controls">
			        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('name') }" required autofocus v-model.trim="document.name" placeholder="@lang('backend::admin-users.name')" type="text" class="uk-input">
			        	<span v-if="errors.hasOwnProperty('name')" class="uk-text-small uk-text-danger">@{{ errors.name[0] }}</span>
			        </div>
			    </div>
				<div class="uk-form-controls uk-margin-small-top">
					<switches
							v-model="document.seo_indexable"
							theme="bulma"
							text-enabled="@lang('backend::admin-seo.indexable')"
							text-disabled="@lang('backend::admin-seo.indexable_no')"
							type-bold="true"
							color="blue">
					</switches>
				</div>
			    <div class="uk-margin uk-text-center">
					<button @click="saveDocument" class="uk-button uk-button-primary">@lang('backend::admin-documents.save')</button>
			    </div>
			</div>
		</div>
	</div>
	<div class="uk-width-1-1@m uk-width-1-4@l"></div>
@endsection
@section('js')
<script>
	const app = new Vue({
		el: '#app',
		data: {
			errors: {},
			document: {
				id: '{{ request()->document }}',
				name: '',
				file: null,
			}
		},
		methods: {
			saveDocument: function () {
				axios.post("{{ route('admin.api.documents.save') }}", {
						document: this.document.id,
						name: this.document.name,
						seo_indexable: this.document.seo_indexable
					})
					.then((response) => {
						$.mdtoast("@lang('backend::admin-documents.document_updated')", { duration: 5000 });
					})
					.catch((error) => {
						$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
			},
		},
		created: function () {
			axios.get("{{ route('admin.api.documents.info', ['document' => null]) }}/" + this.document.id)
				.then((response) => {
					this.document = response.data;
				})
				.catch((error) => {
					$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
				});
		}
	});
</script>
@endsection
