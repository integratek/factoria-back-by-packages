@extends('backend::layouts.admin')
@section('title', __('backend::admin-documents.documents'))
@section('subtitle', __('backend::admin-documents.documents_sub'))
@section('icon', 'mdi-file-document')
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">@lang('backend::admin-documents.documents')</div>
			<div class="uk-card-body">
				<a href="#" uk-toggle="target: #docs-1" class="uk-button uk-button-default">@lang('backend::admin-documents.upload_document')</a>

				<div class="uk-margin">
					<docs-repo
							:values.sync="uploadedFiles"
							id-r="1"
							:show-selected="false"
							:unique-doc="true"
							:allow-repository="false"
							csrf="{{ csrf_token() }}"
							:on-uploaded-file="onFileUploaded"
							:on-finished-upload="onFinishedUpload">
					</docs-repo>
					<span v-if="errors && errors.hasOwnProperty('file')" class="uk-text-small uk-text-danger">@{{ errors.file[0] }}</span>
				</div>

				@include("backend::layouts.filter")

				<br><br>
				<div class="uk-overflow-auto">
					<table class="uk-table uk-table-striped">
					    <thead>
					        <tr>
					            <th>#</th>
					            <th>@lang('backend::admin-documents.name')</th>
					            <th class="uk-visible@m">@lang('backend::admin-documents.created_at')</th>
								<th>URL</th>
					            <th>@lang('backend::admin-documents.actions')</th>
					        </tr>
					    </thead>
					    <tbody>
					        <tr v-for="(doc, k) in documents" v-if="documents && documents.length > 0">
					            <td>@{{ doc.id }}</td>
					            <td>@{{ doc.name }}</td>
					            <td class="uk-visible@m">@{{ doc.created_at }}</td>
								<td>
									<a class="btn" :data-clipboard-text="'{{ route('documents.public.download', ['document' => null]) }}/' + doc.id" :uk-tooltip="'title: ' + '{{ route('documents.public.download', ['document' => null]) }}/' + doc.id">
										<i class="mdi mdi-link-variant"></i>
									</a>
								</td>
					            <td>
					            	<div class="uk-button-group">
										<!-- The SEO modal linked to the seo button -->
										<seo-indexable
												save-url="{{ route('admin.api.seo.save-indexable') }}"
												seo-trans-url="{{ route('admin.api.seo.translations') }}"
												model-class="Lafactoria\Backend\Models\Document"
												:model="doc"
												:id-r="doc.id">
										</seo-indexable>

									    <a :href="'{{ route('admin.documents.edit', ['document' => null]) }}/' + doc.id" class="uk-button uk-button-default uk-button-small">
											<i class="mdi mdi-pencil"></i>
									    </a>
										<a class="uk-button uk-button-default uk-button-small" @click="onClickOpenSeo(doc.id)">
											<i class="mdi mdi-tag-multiple"></i>
										</a>
									    <a :href="'{{ route('admin.documents.download', ['document' => null]) }}/' + doc.id" class="uk-button uk-button-default uk-button-small">
											<i class="mdi mdi-cloud-download"></i>
									    </a>
										<a href="#" @click="deleteDocument(doc.id)" class="uk-button uk-button-danger uk-button-small">
											<i class="mdi mdi-delete"></i>
										</a>
									</div>
					            </td>
					        </tr>
					    </tbody>
					</table>
					<br>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		{{ $documents->links() }}
	</div>
@endsection
@section('js')
    <script src="{{ asset('js/clipboard.min.js') }}"></script>
	<script>
        var clipboard = new Clipboard('.btn');

        clipboard.on('success', function(e) {
            console.info('Text:', e.text);
            $.mdtoast("URL copiada.", { duration: 1000 });

            e.clearSelection();
        });

		const app = new Vue({
			el: '#app',
			data: {
				documents: [],
				uploadedFiles: [],
				errors: null,
				bus: new Vue(),
				seoTranslations: null,
			},
			methods: {
				onFileUploaded(event, file) {
					if (event === 'added')
						this.documents.push(file)
					else // in deleted case file is the id
						this.documents.splice(this.documents.findIndex(doc => doc.id == file), 1)
				},
				onFinishedUpload() {
					UIkit.modal($('#docs-1')).hide();
				},
				deleteDocument: function (id) {
                    UIkit.modal.confirm("@lang('backend::admin-documents.confirm_delete')", {labels: {ok: "@lang('backend::admin-documents.delete_yes')", cancel: "@lang('backend::admin-documents.delete_no')"}}).then(function() {
	                    axios.post("{{ route('admin.api.documents.delete') }}", { document: id })
						.then((response) => {
							this.updateDocuments();
							$.mdtoast("@lang('backend::admin-documents.document_deleted')", { duration: 5000 });
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
	                }.bind(this));
				},
				updateDocuments: function () {
					axios.get("{{ route('admin.api.documents') }}")
						.then((response) => {
							this.documents = response.data;
							this.documents.map(function (doc) {
								doc.created_at = moment(doc.created_at, 'YYYY-MM-DD hh:mm:ss').fromNow();
								return doc;
							});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
				},
				onClickOpenSeo(id){
					UIkit.modal('#seo-doc-'+id).show();
				},
			},
			created: function () {
				this.documents = <?php echo json_encode($documents) ;?>;
				this.documents = this.documents.data;
			}
		});
	</script>
@endsection
