@extends('backend::layouts.admin')
@section('title', __('backend::admin-documents.upload_document'))
@section('subtitle', __('backend::admin-documents.upload_document_sub'))
@section('icon', 'mdi-cloud-upload')
@section('content')
	<div class="uk-width-1-1@m uk-width-1-4@l"></div>
	<div class="uk-width-1-1@m uk-width-1-2@l">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
				<a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-documents.upload_document')
			</div>
			<div class="uk-card-body">
				<div class="uk-margin">
			        <label class="uk-form-label">@lang('backend::admin-documents.name')</label>
			        <div class="uk-form-controls">
			        	<input :class="{ 'uk-form-danger': errors.hasOwnProperty('name') }" required autofocus v-model.trim="document.name" placeholder="@lang('backend::admin-users.name')" type="text" class="uk-input">
			        	<span v-if="errors.hasOwnProperty('name')" class="uk-text-small uk-text-danger">@{{ errors.name[0] }}</span>
			        </div>
			    </div>
			    <div class="uk-margin">
			        <label class="uk-form-label">@lang('backend::admin-documents.file')</label>
			        <div class="uk-form-controls">
			        	<dropzone
			        		ref="fileUploader"
			        		:options="{
			        			url: '{{ route('admin.api.documents.upload') }}',
			        			dictDefaultMessage: '@lang('backend::admin-documents.drop_files')',
								maxFiles: 1,
								autoProcessQueue: false
				        	}"
				        	id="fileUploader"
			        		v-on:vdropzone-success="showSuccess"
			        	>
					    </dropzone>
			        	<span v-if="errors.hasOwnProperty('file')" class="uk-text-small uk-text-danger">@{{ errors.file[0] }}</span>
			        </div>
			    </div>
			    <div class="uk-margin uk-text-center">
					<button @click="uploadDocument" class="uk-button uk-button-primary">@lang('backend::admin-documents.upload')</button>
			    </div>
			</div>
		</div>
	</div>
	<div class="uk-width-1-1@m uk-width-1-4@l"></div>
@endsection
@section('js')
<script>
	const app = new Vue({
		el: '#app',
		data: {
			errors: {},
			document: {
				name: '',
				file: null,
			}
		},
		methods: {
			uploadDocument: function () {
				this.$refs.fileUploader.dropzone.options.params = {
					'name': this.document.name,
                    '_token': '{!! csrf_token() !!}'
                };
				this.$refs.fileUploader.processQueue();
			},
			showSuccess: function () {
				this.errors = {};
				axios.post("{{ route('admin.flash') }}", { msg: 'admin-documents.document_uploaded', type: 'success' })
					.then((response) => {
						window.location.replace("{{ route('admin.documents') }}");
					});
			}
		}
	});
</script>
@endsection
