@extends('backend::layouts.admin')
@section('title', __('backend::admin-general.general'))
@section('subtitle', __('backend::admin-general.general_sub'))
@section('icon', 'mdi-settings')
@section('content')
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
            <div class="uk-card-header">
				<a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-general.create_general')
			</div>
			<div class="uk-card-body">

				<div uk-grid>
					<div class="uk-width-1-1">
						<ul class="uk-flex-center" :uk-tab="'connect: #seo-translation; media: 960'">
							<li v-for="(translation, key) in seotranslations">
								<a href="#">@{{ languages[translation.locale] }}</a>
							</li>
						</ul>
						<ul id="seo-translation" class="uk-switcher">
							<li v-for="(translation, k) in seotranslations">
								<div uk-grid>
                                    <div class="uk-width-1-1">
                                        <label class="uk-form-label">@lang('backend::admin-general.title')</label>
                                        <div class="uk-form-controls">
                                            <input v-model="translation.title" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.title') }" class="uk-input" rows="5" placeholder="@lang('backend::admin-general.title')">
                                            <span v-if="errors.hasOwnProperty('translations.' + k + '.title')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.title'] }}</span>
                                        </div>
                                    </div>
                                    <div class="uk-width-1-1">
                                        <label class="uk-form-label">@lang('backend::admin-general.text')</label>
                                        <div class="uk-form-controls">
                                            <quill-editor v-model="translation.text" ref="quill_cookies"></quill-editor>
                                            <span v-if="errors.hasOwnProperty('translations.' + k + '.text')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.text'] }}</span>
                                        </div>
                                    </div>
                                    <!--div class="uk-width-1-1">
                                        <label class="uk-form-label">@lang('backend::admin-general.legal')</label>
                                        <div class="uk-form-controls">
                                            <span v-if="errors.hasOwnProperty('translations.' + k + '.legal')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.legal'] }}</span>
                                        </div>
                                    </div-->
								</div>
							</li>
						</ul>
					</div>
                    <div class="uk-width-1-1 uk-margin-top uk-text-center">
                        <button @click="createGeneral" class="uk-button uk-button-primary">@lang('backend::admin-general.save')</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				errors: {},
                seotranslations: [],
                languages: [],
				general: {
				    id: null
				},
			},
			methods: {
				createGeneral: function () {
					axios.post("{{ route('admin.api.general.create') }}", {translations: this.seotranslations})
						.then((response) => {
							this.general = response.data;
							this.errors = {};
							axios.post("{{ route('admin.trans') }}", { msg: 'admin-general.general_updated' })
								.then((response) => {
									$.mdtoast(response.data.msg, { duration: 5000 });
								});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });

                            // remove object and number translation information in strings and set in vue errors var
                            this.errors = cleanErrors(error).errors;
						});
				}
			},
			created: function () {
                axios.post("{{ route('admin.api.news.seotranslations') }}", { news: null })
                    .then((response) => {
                        this.seotranslations = response.data;
                        console.log(this.seotranslations);
                    })
                    .catch((error) => {
                        $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
                axios.post("{{ route('admin.api.seo.languages') }}")
                    .then((response) => {
                        this.languages = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast('Error while getting the languages...', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
			}
		});
	</script>
@endsection
