@extends('backend::layouts.admin')
@section('title', __('backend::admin-general.general'))
@section('subtitle', __('backend::admin-general.general_sub'))
@section('icon', 'mdi-settings')
@section('content')
	@php
		$isCookies = ($general->id == 1 || $general->id == 2);
		if ($isCookies && strpos($general->text, '</script>') !== false) {
			$cookiesText = str_replace('</script>', '<\/script>', $general->text);
		} else {
			$cookiesText = '';
		}
	@endphp
	<div class="uk-width-1-1">
		<div class="uk-card uk-card-default">
			<div class="uk-card-header">
            <a href="{{url()->previous()}}" class="lf-breadcrumb"><i class="mdi mdi-arrow-left"></i></a>&nbsp;&nbsp;@lang('backend::admin-general.edit_general')
            </div>
			<div class="uk-card-body">
				@if ($isCookies)
					<div uk-grid>
						<div class="uk-width-1-1">
							@if ($general->id == 2)
								<label class="uk-form-label">@lang('backend::admin-general.cookies_head_script')</label>
							@else
								<label class="uk-form-label">@lang('backend::admin-general.cookies_declaration_script')</label>
							@endif
							<div class="uk-form-controls">
								<textarea v-model="cookiesModel.text" class="uk-input" rows="2" style="height: auto" placeholder="@lang('backend::admin-general.cookies_script')">
								</textarea>
							</div>
						</div>
					</div>
				@else
					<div uk-grid>
						<div class="uk-width-1-1">
							<ul class="uk-flex-center" :uk-tab="'connect: #seo-translation; media: 960'">
								<li v-for="(translation, key) in seotranslations">
									<a href="#">@{{ languages[translation.locale] }}</a>
								</li>
							</ul>
							<ul id="seo-translation" class="uk-switcher">
								<li v-for="(translation, k) in translations">
									<div uk-grid>
										<div class="uk-width-1-1">
											<label class="uk-form-label">@lang('backend::admin-general.title')</label>
											<div class="uk-form-controls">
												<input v-model="translation.title" :class="{ 'uk-form-danger': errors.hasOwnProperty('translations.' + k + '.title') }" class="uk-input" rows="5" placeholder="@lang('backend::admin-general.title')">
												<span v-if="errors.hasOwnProperty('translations.' + k + '.title')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.title'] }}</span>
											</div>
										</div>
										<div class="uk-width-1-1">
											<label class="uk-form-label">@lang('backend::admin-general.text')</label>
											<div class="uk-form-controls">
												<quill-editor v-model="translation.text" ref="quill_text"></quill-editor>
												<span v-if="errors.hasOwnProperty('translations.' + k + '.text')" class="uk-text-small uk-text-danger">@{{ errors['translations.' + k + '.text'] }}</span>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				@endif
				<div class="uk-width-1-1 uk-margin-top uk-text-center">
					<button @click="updateGeneral" class="uk-button uk-button-primary">@lang('backend::admin-general.save')</button>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				errors: {},
                seotranslations: [],
                languages: [],
				translations: [],
				general: {
				    id: null
				},
				isCookies: {{$isCookies}},
				cookiesModel: {
					title: '{{$general->title}}',
					text: '{!!$cookiesText!!}'
				}
			},
			methods: {
				updateGeneral: function () {
					let translations = this.translation
					if (this.isCookies) {
						translations = []
						this.seotranslations.forEach(translation => {
							translations.push({
								locale: translation.locale,
								title: this.cookiesModel.title,
								text: this.cookiesModel.text
							})
						})
					}
					axios.post("{{ route('admin.api.general.edit', ['id' => $general->id]) }}", {translations: translations})
						.then((response) => {
							this.general = response.data;
							this.errors = {};
							axios.post("{{ route('admin.trans') }}", { msg: 'admin-general.general_updated' })
								.then((response) => {
									$.mdtoast(response.data.msg, { duration: 5000 });
								});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });

                            // remove object and number translation information in strings and set in vue errors var
                            this.errors = cleanErrors(error).errors;
						});
				}
			},
			created: function () {
                axios.post("{{ route('admin.api.news.seotranslations') }}", { news: null })
                    .then((response) => {
                        this.seotranslations = response.data;
                        
                    })
                    .catch((error) => {
                        $.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });
                axios.post("{{ route('admin.api.seo.languages') }}")
                    .then((response) => {
                        this.languages = response.data;
                    })
                    .catch((error) => {
                        $.mdtoast('Error while getting the languages...', { duration: 5000, type: $.mdtoast.type.ERROR });
                    });

				var js = <?php echo json_encode($general) ;?>;
                _translations = <?php echo $general->translations->toJson();?>;
				var locales = <?php echo collect(config('translatable.locales'))->toJson() ?>;
				locales.forEach(locale => {
					_translations.forEach(translation => {
						if(translation.locale == locale){
							this.translations.push(translation);
						}
					});
				});
			}
		});
	</script>
@endsection
