@extends('backend::layouts.admin')
@section('title', __('backend::admin-general.general'))
@section('subtitle', __('backend::admin-general.general_sub'))
@section('icon', 'mdi-newspaper')
@section('content')
<div class="uk-width-1-1">
		<div uk-grid>
			<div class="uk-width-1-1">
				<div class="uk-card uk-card-default">
					<div class="uk-card-header">@lang('backend::admin-general.general')</div>
					<div class="uk-card-body">
					<!--a href="{{ route('admin.general.create') }}" class="uk-button uk-button-default">@lang('backend::admin-news.create_news')</a>
					<br><br-->
					<div class="uk-overflow-auto">
						<table class="uk-table uk-table-striped">
						    <thead>
						        <tr>
						            <th class="uk-visible@m">#</th>
						            <th>@lang('backend::admin-general.title')</th>
						            <!--th class="uk-visible@m">@lang('backend::admin-news.author')</th-->
						            <th class="uk-visible@m">@lang('backend::admin-general.text')</th>
						            <!--th>@lang('backend::admin-news.active')</th-->
						            <th class="uk-visible@l">@lang('backend::admin-news.created_at')</th>
						            <th>@lang('backend::admin-news.actions')</th>
						        </tr>
						    </thead>
						    <tbody>
								<paginate
										name="generals"
										:list="generals"
										:per="{{ config('backend.pagination.backend-news') }}"
								>
						        <tr v-for="entry in paginated('generals')">
						            <td class="uk-visible@m">@{{ entry.id }}</td>
						            <td>@{{ entry.title }}</td>
						            <!--td class="uk-visible@m">@{{ entry.admin.name }}</td-->
						            <td class="uk-visible@m" style="max-width: 10ch; overflow: hidden; text-overflow: ellipsis; white_space: nowrap;">@{{ entry.text }}</td>
						            <!--td>
						            	<span v-if="entry.active" class="uk-text-success mdi mdi-circle"> @lang('backend::admin-news.yes')</span>
										<span v-if="!entry.active" class="uk-text-danger mdi mdi-circle"> @lang('backend::admin-news.no')</span>
						            </td-->
						            <td class="uk-visible@l">@{{ entry.created_at }}</td>
						            <td>
						            	<!-- The SEO modal linked to the seo button -->
						            	<!--seo
						            		save-url="{{ route('admin.api.seo.save') }}"
						            		get-url="{{ route('admin.api.seo') }}"
						            		seo-trans-url="{{ route('admin.api.seo.translations') }}"
						            		languages-url="{{ route('admin.api.seo.languages') }}"
						            		:locales="{{ collect(config('translatable.locales'))->toJson() }}"
						            		class-r="Lafactoria\Backend\Models\General"
						            		:lang-locale="null"
						            		:reference="entry.id"
						            		:id-r="'seo-' + entry.id">
						            	</seo-->
						            	<div class="uk-button-group">
										    <a :href="'{{ route('admin.general.edit', ['id' => null]) }}/' + entry.id" class="uk-button uk-button-default uk-button-small">
												<i class="mdi mdi-pencil"></i>
										    </a>
										    <!--a class="uk-button uk-button-default uk-button-small" :uk-toggle="'target: #seo-' + entry.id">
												<i class="mdi mdi-tag-multiple"></i>
										    </a-->
										    <!--a href="#" @click="deleteGeneral(entry.id)" class="uk-button uk-button-danger uk-button-small">
												<i class="mdi mdi-delete"></i>
										    </a-->
										</div>
						            </td>
						        </tr>
                                </paginate>
						    </tbody>
						</table>
                        <br>
                        <div v-if="generals.length > 0">
                            <paginate-links
                                    for="generals"
                                    :show-step-links="true"
                                    :limit="5"
                                    :classes="{
                                        'ul': 'uk-pagination',
                                        '.active': 'uk-active',
                                        '.disabled' : 'uk-disabled',
                              }"
                            ></paginate-links>
                        </div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script>
		const app = new Vue({
			el: '#app',
			data: {
				generals: [],
                paginate: ['generals']
			},
			methods: {
				updateGeneral: function () {
					axios.get("{{ route('admin.api.general.index') }}")
						.then((response) => {
							this.generals = response.data;
							this.generals.map(function (generals) {
								generals.created_at = moment(generals.created_at).fromNow();
								if(generals.text){
									if(generals.text.length > 300){
										generals.text = generals.text.substring(0, 300) + '...';
									}
								}
								return generals;
							});
						})
						.catch((error) => {
							$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
						});
				},
				deleteGeneral: function (id) {
                    UIkit.modal.confirm("@lang('backend::admin-news.confirm_delete')", {labels: {ok: "@lang('backend::admin-news.delete_yes')", cancel: "@lang('backend::admin-news.delete_no')"}}).then(function() {
						axios.post("{{ route('admin.general.delete') }}", { id: id })
							.then ((response) => {
								this.updateGeneral();
								$.mdtoast("@lang('backend::admin-general.general_deleted')", { duration: 5000 });
							})
							.catch((error) => {
								$.mdtoast(error.response ? error.response.data.message : 'Error', { duration: 5000, type: $.mdtoast.type.ERROR });
							});
					}.bind(this));
				}
			},
			created: function () {
				this.updateGeneral();
			}
		});
	</script>
@endsection
