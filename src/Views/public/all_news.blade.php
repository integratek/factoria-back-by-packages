@extends('backend::layouts.app')
@section('title', __('backend::admin-news.news'))
@section('content')
<div class="container">
    <br>
    {{ csrf_field() }}
    @foreach ($news->chunk(2) as $block)
        <div class="row">
            @foreach ($block as $entry)
                <div class="col col-sm-12" style="margin-top: 10px;">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="{{ route(Lafactoria\Backend\Models\News::ROUTE_PREFIX."_get_by_slug", ['slug' => $entry->slug]) }}" style="text-decoration: none;">
                                    {{ $entry->title }}
                                </a>
                            </h4>
                            <p class="card-text">
                                <small class="text-muted">
                                    @lang('backend::admin-news.published_by', ['name' => $entry->admin->name, 'time' => $entry->created_at->diffForHumans()])
                                </small>
                            </p>
                            <div class="card-text">
                                {!! $entry->description !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <br><br>
        <div class="row">
            <div class="col">
                {{ $news->links('pagination::bootstrap-4') }}
            </div>
        </div>
        <br>
    @endforeach
</div>
@endsection
