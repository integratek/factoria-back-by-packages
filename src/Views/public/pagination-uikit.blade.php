@if ($paginator->hasPages())
    <ul class="uk-pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="uk-disabled"><a href="#!"><span class="uk-align-center" style="font-size: 10px; margin-top: 4px;"> << </span></a></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}"><span class="uk-align-center" style="font-size: 10px; margin-top: 4px;"> << </span></a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="uk-disabled"><a href="#!">{{ $element }}</a></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="uk-active"><a href="#!">{{ $page }}</a></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}"><span class="uk-align-center" style="font-size: 10px; margin-top: 4px;"> >> </span></a></li>
        @else
            <li class="uk-disabled"><a href="#!"><span class="uk-align-center" style="font-size: 10px; margin-top: 4px;"> >> </span></a></li>
        @endif
    </ul>
@endif
