@extends('backend::layouts.app')
@section('title', __('backend::admin-contact.contact'))
@section('content')
<div class="container">
	<br>
	<form method="POST">
		{{ csrf_field() }}
		<div class="row">
			<div class="col">
				<div class="form-group">
					<label>@lang('backend::public.name')</label>
					<input type="text" value="{{ old('name') }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" placeholder="@lang('backend::public.name')">
					<div class="invalid-feedback">
						{{ $errors->has('name') ? $errors->get('name')[0] : '' }}
					</div>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
					<label>@lang('backend::public.surname')</label>
					<input type="text" value="{{ old('surname') }}" class="form-control {{ $errors->has('surname') ? 'is-invalid' : '' }}" name="surname" placeholder="@lang('backend::public.surname')">
					<div class="invalid-feedback">
						{{ $errors->has('surname') ? $errors->get('surname')[0] : '' }}
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="form-group">
					<label>@lang('backend::public.email')</label>
					<input type="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" placeholder="@lang('backend::public.email')">
					<div class="invalid-feedback">
						{{ $errors->has('email') ? $errors->get('email')[0] : '' }}
					</div>
				</div>
			</div>
			<div class="col">
				<div class="form-group">
					<label>@lang('backend::public.phone')</label>
					<input type="number" value="{{ old('phone') }}" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" name="phone" placeholder="@lang('backend::public.phone')">
					<div class="invalid-feedback">
						{{ $errors->has('phone') ? $errors->get('phone')[0] : '' }}
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="form-group">
					<label>@lang('backend::public.message')</label>
					<textarea class="form-control {{ $errors->has('message') ? 'is-invalid' : '' }}" name="message" placeholder="@lang('backend::public.message')" rows="5">{{ old('message') }}</textarea>
					<div class="invalid-feedback">
						{{ $errors->has('message') ? $errors->get('message')[0] : '' }}
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
                {!! NoCaptcha::renderJs(\Illuminate\Support\Facades\App::getLocale()) !!}
                {!! NoCaptcha::display() !!}
                <div class="invalid-feedback">
                    {{ $errors->has('g-recaptcha-response') ? $errors->get('g-recaptcha-response')[0] : '' }}
                </div>
			</div>
			 <div class="">
                            <span id="" class="input-error custom-hide"></span>
                             <p>
                                <label class="login-input">
                                    <input id="accepted_contract" type="checkbox" />
                                    <span class="login-input">@lang('backend::public.accept') </span> <a href="{{route('privacy')}}" target="_blank" >@lang('backend::public.privacy')</a>
                                </label>
                            </p>
                            <span id="" class="input-error custom-hide"></span>
             </div>
		</div>
		<div class="row">
			<div class="col">
				<center>
					<button class="btn btn-primary" type="submit"  id="submit-validate" disabled>@lang('backend::public.send')</button>
				</center>
			</div>
		</div>
	</form>
</div>
@endsection
@section('js')
<script>
    // Initialize selects
    var checker_contract = document.getElementById('accepted_contract');
    var sendbtn = document.getElementById('submit-validate');
    checker_contract.onchange = checkAccepts
    function checkAccepts(){
          (checker_contract.checked) ? $('#submit-validate').prop('disabled', false) : $('#submit-validate').prop('disabled', true)
    }
 @endsection