@extends('backend::layouts.app')
@section('title', $page->title)
@section('content')
<div class="container">
    <br>
    {{ csrf_field() }}
    <div class="row">
        <div class="col">
            <div class="card">
                @if ($page->image)
                    <img style="height: {{ Lafactoria\Backend\Models\Menu::LAYOUT_IMAGE_HEIGHT }}px; object-fit: cover;" class="card-img-top" src="{{ $page->image->preview([1280, 720]) }}" alt="Card image cap">
                @endif
                <div class="card-body">
                    <h4 class="card-title">{{ $page->title }}</h4>
                    <div class="card-text">
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
