@extends('backend::layouts.app')
@section('title', optional($translation)->title ? $translation->title: $news->title)
@section('content')
<div class="container">
    <br>
    {{ csrf_field() }}
    <div class="row">
        <div class="col">
            <div class="card">
                @if ($news->image)
                    <img style="height: {{ Lafactoria\Backend\Models\News::LAYOUT_IMAGE_HEIGHT }}px; object-fit: cover;" class="card-img-top" src="{{ $news->image->preview([1280, 720]) }}" alt="Card image cap">
                @endif
                <div class="card-body">
                    <h4 class="card-title">{{ $news->title }}</h4>
                    <p class="card-text"><small class="text-muted">@lang('backend::admin-news.published_by', ['name' => $news->admin->name, 'time' => $news->created_at->diffForHumans()])</small></p>
                    <div class="card-text">
                        {!! $news->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
