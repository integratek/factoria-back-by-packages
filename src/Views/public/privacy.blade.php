@extends('backend::layouts.app')
@section('title', __('backend::admin-general.privacy'))
@section('content')
<div class="container">
	<br>
	{!! config('backend.general.privacy') !!}
</div>
@endsection
