@extends('backend::layouts.app')
@section('title', __('backend::admin-general.legal'))
@section('content')
<div class="container">
	<br>
	{!! config('backend.general.legal') !!}
</div>
@endsection
