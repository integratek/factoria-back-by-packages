@extends('backend::layouts.app')
@section('title', __('backend::admin-general.cookies'))
@section('content')
<div class="container">
	<br>
	@if (!is_null(config('backend.cookie_bot_page')))
		{!! config('backend.cookie_bot_page') !!}
	@else
		{!! config('backend.general.cookies') !!}
	@endif
</div>
@endsection
