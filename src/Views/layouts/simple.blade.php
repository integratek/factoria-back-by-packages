<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>@yield('title') - {{ config('app.name') }}</title>

	<!-- Meta fields -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('css/back.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdtoast.css') }}" rel="stylesheet">
</head>
<body class="back--gradient">


	<div id="app">
        @yield('content')
    </div>

	<!-- Scripts -->
    <script src="{{ asset('js/back.js') }}"></script>
    <script src="{{ asset('js/mdtoast.js') }}"></script>
    <script>
    	$(function () {
			@if (session()->has('success'))
				$.mdtoast("{{ session()->get('success') }}", { duration: 5000});
	    	@elseif (session()->has('error'))
	    		$.mdtoast("{{ session()->get('error') }}", { duration: 5000, type: $.mdtoast.type.ERROR });
	    	@elseif ($errors->count() > 0)
	    		$.mdtoast("{{ $errors->first() }}", { duration: 5000, type: $.mdtoast.type.ERROR });
	    	@endif
		});
	</script>
	<!-- Helper functions -->
    <script>
        function cleanErrors (error, seotranslations = null, partsToRemove = []) {
            // remove object and number translation information in strings
            for (var key in error.response.data.errors) {
                var obj = error.response.data.errors[key];
                var error_info = obj[0];

				var performSplit = false
                while (error_info.indexOf('translations.') !== -1) {
                    error_info = error_info.replace('translations', '');
					performSplit = true
				}

                while (error_info.indexOf('news.') !== -1)
                    error_info = error_info.replace('news.', '');

                partsToRemove.forEach(partToRemove => {
                    error_info = error_info.split(partToRemove + '.').join('')
                });

				if (performSplit) {
					error_info = error_info.split('.')
					delete error_info[1]
					error_info = error_info.join('')
					error_info += '.'
				}

                if (seotranslations) {
                    for (i = 0; i < seotranslations.length; i++) {
                        if (error_info.indexOf(i.toString() + '.') !== -1)
                            error_info = error_info.replace(i.toString() + '.', '');
                    }
                }
                error.response.data.errors[key] = error_info;
            }
            return error.response.data;
        }
    </script>
	@yield('js')
</body>
</html>
