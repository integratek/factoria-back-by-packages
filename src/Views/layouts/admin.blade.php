<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>@yield('title') - {{ config('app.name') }}</title>

	<!-- Meta fields -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link href="{{ asset('css/back.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdtoast.css') }}" rel="stylesheet">
    <!--link href="{{ asset('css/material-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/materialdesignicons.min.css') }}" rel="stylesheet" /-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.0.45/css/materialdesignicons.min.css">
    <link href="{{ asset('css/pikaday.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/mdtoast.css') }}" rel="stylesheet">
    @yield('css')
    {!! config('backend.config.styles') !!}
</head>
<body>
    <div class="loader">
        <div>
            <img style="height: 75px" src="{{ asset('svg-loaders/tail-spin.svg') }}" />
        </div>
    </div>
	<form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
	<div class="body--background"></div>
	<div class="fadder"></div>
	<div class="sidebar">
		<div class="sidebar--user">
            @if (file_exists(public_path(config('backend.logo'))))
                <img src="{{ asset(config('backend.logo')) }}" alt="{{ config('app.name') }}"><br />
            @else
                <img src="http://via.placeholder.com/150x100" alt="{{ config('app.name') }}"><br />
            @endif
			<span class="sidebar--user--name">
				{{ auth()->guard('admin')->user()->name }}
			</span>
			<span class="sidebar--user--email">
				{{ auth()->guard('admin')->user()->email }}
			</span>
		</div>
		<div class="sidebar--navigation">
		    <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
		        @foreach (config('backend.menu') as $menu)
					@if(array_key_exists('childs', $menu))
						@if(\Lafactoria\Backend\Traits\Helper::showMenuByRole($menu))
							<li class="uk-parent">
								<a href="#">
									<i class="mdi {{ $menu['icon'] }}"></i>
									{{ $menu['name'] }}
								</a>
								<ul class="uk-nav-sub">
									@foreach ($menu['childs'] as $child)
										@if(\Lafactoria\Backend\Traits\Helper::showMenuByRole($child))
											<li class="{{ Lafactoria\Backend\Traits\Helper::activeSegUrl($child['segment'], 2) }}">
												<a href="{{ Route::has($child['url']) ? route($child['url']) : url($child['url']) }}">
													<i class="mdi {{ $child['icon'] }}"></i> {{ __($child['name']) == $child['name'] ? $child['name'] : __($child['name']) }}
												</a>
											</li>
										@endif
									@endforeach
								</ul>
							</li>
						@endif
					@else
						@if(\Lafactoria\Backend\Traits\Helper::showMenuByRole($menu))
							<li class="{{ Lafactoria\Backend\Traits\Helper::activeSegUrl($menu['segment'], 2) }}">
								<a href="{{ Route::has($menu['url']) ? route($menu['url']) : url($menu['url']) }}">
									<i class="mdi {{ $menu['icon'] }}"></i> {{ __($menu['name']) == $menu['name'] ? $menu['name'] : __($menu['name']) }}
								</a>
							</li>
						@endif
					@endif
		        @endforeach
		    </ul>
		</div>
        <div class="sidebar--logo uk-text-center">
            <h2 class="uk-text-uppercase logo">La Factoria</h2>
        </div>
	</div>
	<div class="sidebar-padder">
		<div class="header--background"></div>
		<nav class="uk-light" uk-navbar>
	    	<div class="uk-navbar-left">
		        <ul class="uk-navbar-nav">
		            <li><a id="menu-toggle" href="#" class="mdi mdi-menu" style="font-size: 25px;"></a></li>
		        </ul>
		    </div>
		    <div class="uk-navbar-right">
		        <ul class="uk-navbar-nav">
                    <li>
                        <a href="javascript:void(0)" class="mdi mdi-robot" onclick="submitSitemap()"></a>
                    </li>
		            <li>
		            	<a href="#">{{ auth()->guard('admin')->user()->name }}</a>
		            	<div uk-dropdown="mode: click">
						    <ul class="uk-nav uk-dropdown-nav">
						        <li class="uk-nav-header">@lang('backend::admin-auth.actions')</li>
						        <li>
						        	<a href="{{ route('admin.account') }}">
							        	@lang('backend::admin-auth.account_settings')
							        </a>
					        	</li>
						        <li>
						        	<a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
							        	@lang('backend::admin-auth.logout')
							        </a>
					        	</li>
						    </ul>
						</div>
		            </li>
		        </ul>
		    </div>
		</nav>
		<div class="header">
			<span class="header--title"><i class="mdi @yield('icon')"></i> @yield('title')</span>
			<span class="header--subtitle">@yield('subtitle')</span>
		</div>
		<div class="content" id="app">
			<div v-cloak uk-grid>
				@yield('content')
			</div>
		</div>
		<br>

	</div>

	<!-- Scripts -->
	<script>
		<!-- Global js variables -->
		window.auth_user = @json(auth('admin')->user());
		window.locales = @json($locales);
		window.translatable_locales = @json(\Lafactoria\Backend\Traits\Helper::getLanguages());
		window.translations_seo = @json($translations_seo);
		window.translations_images = @json($translations_images);
		window.languages = @json($languages);
		window.upload_images_url="{{ route('admin.api.images.upload') }}";
		window.images_url="{{ route('admin.api.images') }}";
		window.single_image_url="{{ route('admin.api.image') }}";
		window.seo_save_image_url="{{ route('admin.api.seo.save-image') }}"
		window.upload_docs_url="{{ route('admin.api.documents.upload') }}";
		window.translations_docs = @json($translations_docs);
		window.docs_url="{{ route('admin.api.documents') }}";
		window.single_doc_url="{{ route('admin.api.document') }}";
		window.get_docs_url="{{ route('admin.api.getDocuments') }}";
	</script>
    <script src="{{ asset('js/back.js') }}"></script>
    <script src="{{ asset('js/mdtoast.js') }}"></script>
    <script src="{{ asset('js/pikaday.min.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script>
    	$(function () {
			@if (session()->has('success'))
                $.mdtoast("{{ session()->get('success') }}", { duration: 5000});
                {{session()->forget('success')}}
	    	@elseif (session()->has('error'))
                $.mdtoast("{{ session()->get('error') }}", { duration: 5000, type: $.mdtoast.type.ERROR });
                {{session()->forget('error')}}
	    	@elseif ($errors->count() > 0)
	    		$.mdtoast("{{ $errors->first() }}", { duration: 5000, type: $.mdtoast.type.ERROR });
	    	@endif
		});

    	// check permissions by admin role
		var EDITOR = 2;
		if (auth_user.role_id == EDITOR) {
			$('a[href*="create"]').each(function () {
				$(this).remove();
			});
			$('a > i.mdi-delete').each(function () {
				$(this).parent().remove();
			});
		}

    	// active parent menus
		let menu_parents = $('li.active').parents('li');
		for (let i = 0; i < menu_parents.length; i++) {
			menu_parents[i].classList.add("uk-open");
		}
	</script>
	<!-- Helper functions -->
    <script>
		function submitSitemap() {
			axios.post("{{ route('admin.sitemap.generate') }}")
					.then((response) => {
						$.mdtoast("Success", { duration: 5000 });
					})
					.catch((error) => {
						$.mdtoast('Error', { duration: 5000, type: $.mdtoast.type.ERROR });
					});
		}

        function cleanErrors (error, seotranslations = null, partsToRemove = []) {
            // remove object and number translation information in strings
            for (var key in error.response.data.errors) {
                var obj = error.response.data.errors[key];
                var error_info = obj[0];

				var performSplit = false
                while (error_info.indexOf('translations.') !== -1) {
                    error_info = error_info.replace('translations', '');
					performSplit = true
				}

                while (error_info.indexOf('news.') !== -1)
                    error_info = error_info.replace('news.', '');

                partsToRemove.forEach(partToRemove => {
                    error_info = error_info.split(partToRemove + '.').join('')
                });

				if (performSplit) {
					error_info = error_info.split('.')
					delete error_info[1]
					error_info = error_info.join('')
					error_info += '.'
				}

                if (seotranslations) {
                    for (i = 0; i < seotranslations.length; i++) {
                        if (error_info.indexOf(i.toString() + '.') !== -1)
                            error_info = error_info.replace(i.toString() + '.', '');
                    }
                }
                error.response.data.errors[key] = error_info;
            }
            return error.response.data;
        }
    </script>
    @yield('js')
    {!! config('backend.config.scripts') !!}
</body>
</html>
