<div style="margin-top: 20px">
    <form method="GET" style="display: inline;">
        <input autofocus placeholder="Filtrar" name="filter" type="text" class="uk-input" style="width: 17%" value={{$filter}}>
        <button type="submit" class="uk-button uk-button-default uk-button-small">
            <i class="mdi mdi-filter"></i>
        </button>
    </form>
    @if($filter != '')
        <form method="GET" style="display: inline;">
            <input autofocus placeholder="Filtrar" name="filter" type="hidden" class="uk-input" value=''>
            <button type="submit" class="uk-button uk-button-default uk-button-small">
                <i class="mdi mdi-filter-remove"></i>
            </button>
        </form>
    @endif
</div>