<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ config('backend.config.description') }}">
    <meta name="Description" content="{{ config('backend.config.description') }}">
    <meta name="DC.Description" content="{{ config('backend.config.description') }}">
    <meta name="keywords" content="{{ config('backend.config.tags') }}">
    <meta name="Keywords" content="{{ config('backend.config.tags') }}">
    <meta name="author" content="{{ config('backend.config.name') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name') }}</title>

    {!! config('backend.cookie_bot_header') !!}

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link href="{{ asset('css/mdtoast.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.min.css" integrity="sha256-kj+0nJ3EFGj4aYqeUiykzr34sWWfFCIWQUVpZOHyP/o=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css">
    {!! config('backend.config.styles') !!}
</head>
<body>
    <div id="app">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="{{ url('/') }}">{{ config('backend.config.name') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    @foreach (config('backend.menus') as $menu)
                        @if ($menu->active)
                            @if ($menu->type === 1)
                                <div class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ $menu['title'] }}
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        @foreach ($menu['childs'] as $child)
                                            <a href="{{ $child['url'] }}" class="dropdown-item">{{ $child['title'] }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                <a class="nav-item nav-link" href="{{ $menu['url'] }}">{{ $menu['title'] }}</a>
                            @endif
                        @endif
                    @endforeach
                </div>
                <div class="navbar-nav ml-auto">
                    @auth
                        <div class="dropdown">
                            <a class="nav-item nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                @lang('backend::public.logout') ({{ auth()->user()->name }})
                            </a>
                        </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @elseif (Route::has('login') && Route::has('register'))
                        <a class="nav-item nav-link" href="{{ route('login') }}">@lang('backend::public.login')</a>
                        <a class="nav-item nav-link" href="{{ route('register') }}">@lang('backend::public.register')</a>
                    @endauth
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.min.js" integrity="sha256-d4yul8PcQyvwWRLT1IiGhNJKpkrdUtXS/LPHtZHKx+M=" crossorigin="anonymous"></script>
    <script>
        @if ($errors->count() > 0)
            swal(
                "@lang('backend::public.whops')",
                "{{ $errors->first() }}",
                'error'
            );
        @elseif (session()->has('error'))
            swal(
                "@lang('backend::public.whops')",
                "{{ session()->get('error') }}",
                'error'
            );
        @elseif (session()->has('success'))
            swal(
                "@lang('backend::public.yeah')",
                "{{ session()->get('success') }}",
                'success'
            );
        @endif
    </script>
    {!! config('backend.config.scripts') !!}
</body>
</html>
