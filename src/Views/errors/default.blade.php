<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	<title>Error</title>
</head>
<body>
	<div class="error_container">
		<div class="row img_container">
			<div class="col s2 offset-s2 center-align cloud bottom">
				<img src={{ asset('img/error_cloud.svg') }}>
			</div>
			<div class="col s4 center-align">
				<img src={{ asset('img/error_face.svg') }}>
			</div>
			<div class="col s2 center-align cloud">
				<img src={{ asset('img/error_cloud.svg') }}>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6 offset-m3 center-align">
				<h1 class="title">@lang('backend::public.error_title') {{ $exception->getStatusCode() }}...</h1>
				<h2 class="subtitle">@lang('backend::public.error_subtitle')</h2>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6 offset-m3 center-align">
				<a href="{{url('/')}}" class="return">{{ mb_strtoupper(trans('backend::public.error_return')) }}</a>
			</div>
		</div>
	</div>
</body>
<style>
	.error_container .img_container {
		height: 32vw;
		padding-top: 80px;
	}

	.error_container .img_container .col {
		height: 100%;
	}

	.error_container .img_container img {
		width: 100%;
		max-width: max-content;
	}

	.error_container .img_container .cloud.bottom {
		height: 23vw;
		position: relative;
	}

	.error_container .img_container .cloud.bottom img {
		position: absolute;
		bottom: 0;
	}

	.error_container .img_container .cloud {
		-webkit-animation: translateUpAndDown 1s infinite alternate;
		animation: translateUpAndDown 1s infinite alternate;
	}

	@keyframes translateUpAndDown {
		0% {
			transform: translateY(0);
		}

		100% {
			transform: translateY(20px);
		}
	}

	.error_container .title {
		color: rgb(112, 112, 112);
		font-family: 'Futura';
	}

	.error_container .subtitle {
		color: rgb(112, 112, 112);
		font-family: 'Futura';
		font-weight: lighter;
	}

	.error_container .return {
		color: #DD0000;
		font-family: 'Futura';
		font-size: 32pt;
	}

	@media only screen and (max-width : 992px) {
		.error_container .title {
			font-size: 35pt
		}
	}
</style>
</html>