# lafactoria/backend

Backend de la factoria.

## Setup

```
composer require lafactoria/backend
```

Et demanara consumer key i private key. Les pots generar a la pàgina de bitbucket.

### Publicar Config, Views, Translations i Assets

```
php artisan vendor:publish --tag=backend_config
php artisan vendor:publish --tag=backend_assets
php artisan vendor:publish --tag=backend_public_assets --force
php artisan vendor:publish --tag=backend_translations #Nomes si es nessesita, no gaire recomanable
php artisan vendor:publish --tag=backend_views #Gens recomanable
```

### Configurar arxiu translatable

Publicar arxiu de configuració

```
php artisan vendor:publish --tag=translatable
```

Mofidicar array de locales

```
'locales' => [
        'ca',
        'es',
    ],
```

### Configurar arxiu backend

Publicar arxiu de configuració `backend_config`

Mofidicar array `public_routes` i `public_views` amb els desitjats (opcional).

Mofidicar array `menu` amb els desitjats.

Modificar array `admin_roles` amb els rols desitjats **(No borrar els 2 per defecte!)**.
 
Modificar array `admin_role_editor_segments` amb segments dels moduls accessibles pel rol `editor` (* significa tots).

```
'admin_roles' => [
    ['id' => 1, 'name' => 'backend::admin-users.admin'],
    ['id' => 2, 'name' => 'backend::admin-users.editor']
],
'admin_role_editor_segments' => ['dashboard', 'menus', 'users', 'news', 'contact', 'images', 'documents', 'slides', 'banners', 'general', 'config']
```

### Modificar laravel mix

```webpack.mix.js```

```
mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .js('resources/assets/js/back.js', 'public/js')
   .sass('resources/assets/sass/back.scss', 'public/css');
```

### Afegir dependencies

```package.json```

```
"dependencies": {
    "clipboard": "^1.7.1",
    "jquery": "^3.2.1",
    "moment": "^2.19.2",
    "slugify": "^1.2.6",
    "sweetalert": "^2.1.0",
    "uikit": "^3.1.6",
    "velocity-animate": "^1.5.0",
    "vue": "^2.5.9",
    "vue-multiselect": "^2.0.8",
    "vue-quill-editor": "^2.3.3",
    "vue-switches": "^2.0.1",
    "vue-template-compiler": "^2.5.9",
    "vue2-dropzone": "^3.0.4",
    "vuedraggable": "^2.15.0",
    "vue-paginate": "^3.5.1"
}
```

### Instalar i compilar

```
npm install
npm update
npm run dev #Si estas en desenvolupament
npm run prod #Si estas en produccio
```

### Preparar correu

```.env```

```
MAIL_DRIVER=smtp
MAIL_HOST=MAIL_HOST_HERE
MAIL_PORT=2525
MAIL_USERNAME=USERNAME_HERE
MAIL_PASSWORD=PASSWORD_HERE
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=lafactoria@lafactoria.eu
MAIL_FROM_NAME="La Factoria Interactiva"
```

### Afegir administrador

```
php artisan tinker
>>> Lafactoria\Backend\Models\Admin::create(['name' => 'Admin', 'email' => 'admin@admin.com', 'password' => 'password']);
```

### Visitar admin panel

GET ```/admin```

## Editar el menu

Si vols editar el menu, pots fer servir la següent classe:

```Lafactoria\Backend\Classes\Menu```

Si la fem servir com a realtime facade obtenim una manera elegant per registrar les nostres rutes al menu:

```App\Providers\AppServiceProvider```

```php
<?php

namespace App\Providers;

use Facades\Lafactoria\Backend\Classes\Menu;
use Illuminate\Support\ServiceProvider;
// ...

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // ...

        Menu::add('Sample', 'https://google.com', 'mdi-account-multiple')
            ->add('Sample2', 'https://erik.cat', 'mdi-account-multiple');
    }

    // ...
}
```

### Afegir filter

- Controller

```php
<?php

// Heredar de LaFacotiraBaseController
use Lafactoria\Backend\Controllers\Admin\LaFactoriaBaseController;
extends LaFactoriaBaseController    

// Afegir el constructor donant referéncia cap al model que ha d'utilitzar el filter
public function __construct(){
    $this->model = new Model();
}

// Utilitzar el filter
public function index(Request $request)
{
    $collection = null;
    $collection = $this->filter($request->filter);
    return view('view', ['filter' => $request->filter] ,compact('collection'));
}

```
- Model

```php
<?php


// Si el model es traduible, heredar de BaseListWithTranslation, sino heredar de BaseList
use Lafactoria\Backend\Models\BaseListWithTranslation;
extends BaseListWithTranslation
use Lafactoria\Backend\Models\BaseList;
extends BaseList

// NO UTILITZAR Astrotomic\Translatable\Translatable AL MODEL TRADUIBLE //

// Afegir atributs de filtrat
// Poden ser tant de translatedAttributtes com normals
public $filterAttributes = [
    'title', 'description', 'content'
];

// OPCIONAL //
// Es pot sobrescriure el métode de BaseList 'filterPersonalized' per a moficiar la query del filtre
public function filterPersonalized($query)
{
    return $query->with('admin')->orderBy('id','desc'); // Exemple
}
```

- Vista
<?php
```php
// Agefir el partial
@include("backend::layouts.filter")     // Ruta per defecte cap al backend
@include("vendor.backend.layouts.filter")   // Ruta fent php artisan vendor:publish de LaFacotria
```

### Pantalla d'error

Es pot publicar el Handler.php del backend

```
php artisan vendor:publish --tag=backend_exceptions --force
```

O simplement sobreescriure el métode 'render' al nostre propi Handler

```php
/**
 * Render an exception into an HTTP response.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \Exception  $exception
 * @return \Illuminate\Http\Response
 */
public function render($request, Exception $exception)
{
    if($exception instanceof ValidationException || env('APP_DEBUG')){
        return parent::render($request, $exception);
    } else{
        $e = $request->expectsJson()
            ? $this->prepareJsonResponse($request, $exception)
            : $this->prepareResponse($request, $exception);
        return response()->view('backend::errors.default', array('exception' => $e));
    }
}
```

Es poden personalitzar els estils de la pàgina sobreescrivint les classes declarades a la vista default.blade.php

### Avançat

Podem determinar si el text s'ha de traduir, si la ruta es està definida com a nom o fins i tot quin sagment determina l'actiu del URL.

```add(string $name, string $url, string $icon, string $segment = '')```

```
Menu::add('sample.title', 'admin.sample', 'mdi-account-multiple', 'sample');
```

Icones: https://materialdesignicons.com/

## Views

- Admin: ```backend::layouts.admin```
- Public: ```backend::layouts.app```

## Middleware

Usar el middlewre admin.features a totes les rutes que defineixis, siguin publiques o no.

```admin.features```


## Sitemap

Al backend.php hi ha una variable on es posen els models que volem que actuïn al sitemap. 
Llavors cada model ha de tenir dues funcions estatíques. (sitemap i urlFormat)

'sitemap' => [
        'models' => [
            Lafactoria\Backend\Models\Menu::class,
            Lafactoria\Backend\Models\News::class,
        ],
        'urls' => [],
    ]
