import glob, os

stage = 3

for filename in glob.iglob('./**/*.php', recursive=True):
    print('Executing Stage: ' + str(stage))
    print()
    print('REPLACING: ' + filename)
    f = open(filename,'r')
    filedata = f.read()
    f.close()
    if stage == 0:
        # Namespaces
        newdata = filedata.replace('App\\Http\\', 'Lafactoria\\Backend\\')
        newdata = newdata.replace('App\\', 'Lafactoria\\Backend\\Models\\')
    if stage == 1:
        # Translations
        newdata = filedata.replace("@lang('", "@lang('backend::")
        newdata = newdata.replace("__('", "__('backend::")
    if stage == 2:
        # Views & Route Views
        newdata = filedata.replace("@extends('layouts.", "@extends('backend::layouts.")
        newdata = newdata.replace(", 'public.", ", 'backend::public.")
        newdata = newdata.replace(", 'admin.", ", 'backend::admin.")
    if stage == 3:
        # Configuration
        newdata = filedata.replace("config(['admin.", "config(['backend.")
        newdata = newdata.replace("config('admin.", "config('backend.")
    f = open(filename,'w')
    f.write(newdata)
    f.close()
    os.system('clear')
print('STAGE ' + str(stage) + ' COMPLETED!')
