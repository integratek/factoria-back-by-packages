import glob, os

word = "include"

print('Finding: ' + word)
for filename in glob.iglob('./**/*.php', recursive=True):
    i = 1
    f = open(filename,'r')
    for line in f:
        if word in line:
            print('FILE (line ' + str(i) + '): ' + filename)
        i += 1
